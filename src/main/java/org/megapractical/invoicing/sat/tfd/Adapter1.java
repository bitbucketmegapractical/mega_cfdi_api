//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.06.19 a las 05:39:24 PM CDT 
//

package org.megapractical.invoicing.sat.tfd;

import java.util.Calendar;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1 extends XmlAdapter<String, Calendar> {

	public Calendar unmarshal(String value) {
		return (javax.xml.bind.DatatypeConverter.parseDateTime(value));
	}

	public String marshal(Calendar value) {
		if (value == null) {
			return null;
		}
		return (javax.xml.bind.DatatypeConverter.printDateTime(value));
	}

}
