package org.megapractical.invoicing.sat.tfd;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class TfdParser {
	
	private TfdParser() {		
	}
	
	public static TimbreFiscalDigital getTfd(String xml) {
		try {
			// Parse the XML string
			val factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			
			val builder = factory.newDocumentBuilder();
			val inputSource = new InputSource(new StringReader(xml));
			val doc = builder.parse(inputSource);

			val xpath = parseNamespace();
			val tfdNode = (Node) xpath.evaluate("//tfd:TimbreFiscalDigital", doc, XPathConstants.NODE);
			return buildTfd(tfdNode);
		} catch (IOException | ParserConfigurationException | SAXException | XPathExpressionException e) {
			Logger.getLogger(TfdParser.class.getName()).log(Level.SEVERE, (String) null, e);
		}
		return null;
	}

	/**
	 * Get the TimbreFiscalDigital node
	 * 
	 * @return
	 */
	private static XPath parseNamespace() {
		val xPathfactory = XPathFactory.newInstance();
		val xpath = xPathfactory.newXPath();
		xpath.setNamespaceContext(new NamespaceContext() {
		    @Override
		    public String getNamespaceURI(String prefix) {
		        if (prefix.equals("tfd")) {
		            return "http://www.sat.gob.mx/TimbreFiscalDigital";
		        } else {
		            return null;
		        }
		    }

		    @Override
		    public String getPrefix(String namespaceURI) {
		        throw new UnsupportedOperationException();
		    }

		    @Override
		    public Iterator<String> getPrefixes(String namespaceURI) {
		        throw new UnsupportedOperationException();
		    }
		});
		return xpath;
	}
	
	private static TimbreFiscalDigital buildTfd(Node tfdNode) {
		try {
			if (tfdNode != null && tfdNode.getAttributes() != null) {
				val attributes = tfdNode.getAttributes();
				val tfd = new TimbreFiscalDigital();
				tfd.setVersion(attributes.getNamedItem("Version").getNodeValue());
				tfd.setUUID(attributes.getNamedItem("UUID").getNodeValue());
				tfd.setFechaTimbrado(attributes.getNamedItem("FechaTimbrado").getNodeValue());
				tfd.setRfcProvCertif(attributes.getNamedItem("RfcProvCertif").getNodeValue());
				tfd.setSelloCFD(attributes.getNamedItem("SelloCFD").getNodeValue());
				tfd.setNoCertificadoSAT(attributes.getNamedItem("NoCertificadoSAT").getNodeValue());
				tfd.setSelloSAT(attributes.getNamedItem("SelloSAT").getNodeValue());
				return tfd;
			}
		} catch (Exception e) {
			Logger.getLogger(TfdParser.class.getName()).log(Level.SEVERE, (String) null, e);
		}
		return null;
	}
	
}