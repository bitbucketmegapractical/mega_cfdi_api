//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.6 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: PM.02.21 a las 03:35:18 PM CST 
//


package org.megapractical.invoicing.sat.complement.dividendos;

import javax.xml.bind.annotation.XmlRegistry;

import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.megapractical.ebill.cfd.complemento.dividendos package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.megapractical.ebill.cfd.complemento.dividendos
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Dividendos }
     * 
     */
    public Dividendos createDividendos() {
        return new Dividendos();
    }

    /**
     * Create an instance of {@link Dividendos.DividOUtil }
     * 
     */
    public Dividendos.DividOUtil createDividendosDividOUtil() {
        return new Dividendos.DividOUtil();
    }

    /**
     * Create an instance of {@link Dividendos.Remanente }
     * 
     */
    public Dividendos.Remanente createDividendosRemanente() {
        return new Dividendos.Remanente();
    }

}
