//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.6 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: PM.02.21 a las 03:38:48 PM CST 
//


package org.megapractical.invoicing.sat.complement.intereses;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Version" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="1.0" />
 *       &lt;attribute name="SistFinanciero" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;whiteSpace value="collapse"/>
 *             &lt;enumeration value="SI"/>
 *             &lt;enumeration value="NO"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="RetiroAORESRetInt" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;whiteSpace value="collapse"/>
 *             &lt;enumeration value="SI"/>
 *             &lt;enumeration value="NO"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="OperFinancDerivad" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;whiteSpace value="collapse"/>
 *             &lt;enumeration value="SI"/>
 *             &lt;enumeration value="NO"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="MontIntNominal" use="required" type="{http://www.sat.gob.mx/esquemas/retencionpago/1/intereses}t_Importe" />
 *       &lt;attribute name="MontIntReal" use="required" type="{http://www.sat.gob.mx/esquemas/retencionpago/1/intereses}t_Importe" />
 *       &lt;attribute name="Perdida" use="required" type="{http://www.sat.gob.mx/esquemas/retencionpago/1/intereses}t_Importe" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Intereses")
public class Intereses {

    @XmlAttribute(name = "Version", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String version;
    @XmlAttribute(name = "SistFinanciero", required = true)
    protected String sistFinanciero;
    @XmlAttribute(name = "RetiroAORESRetInt", required = true)
    protected String retiroAORESRetInt;
    @XmlAttribute(name = "OperFinancDerivad", required = true)
    protected String operFinancDerivad;
    @XmlAttribute(name = "MontIntNominal", required = true)
    protected BigDecimal montIntNominal;
    @XmlAttribute(name = "MontIntReal", required = true)
    protected BigDecimal montIntReal;
    @XmlAttribute(name = "Perdida", required = true)
    protected BigDecimal perdida;

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad sistFinanciero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistFinanciero() {
        return sistFinanciero;
    }

    /**
     * Define el valor de la propiedad sistFinanciero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistFinanciero(String value) {
        this.sistFinanciero = value;
    }

    /**
     * Obtiene el valor de la propiedad retiroAORESRetInt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetiroAORESRetInt() {
        return retiroAORESRetInt;
    }

    /**
     * Define el valor de la propiedad retiroAORESRetInt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetiroAORESRetInt(String value) {
        this.retiroAORESRetInt = value;
    }

    /**
     * Obtiene el valor de la propiedad operFinancDerivad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperFinancDerivad() {
        return operFinancDerivad;
    }

    /**
     * Define el valor de la propiedad operFinancDerivad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperFinancDerivad(String value) {
        this.operFinancDerivad = value;
    }

    /**
     * Obtiene el valor de la propiedad montIntNominal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontIntNominal() {
        return montIntNominal;
    }

    /**
     * Define el valor de la propiedad montIntNominal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontIntNominal(BigDecimal value) {
        this.montIntNominal = value;
    }

    /**
     * Obtiene el valor de la propiedad montIntReal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontIntReal() {
        return montIntReal;
    }

    /**
     * Define el valor de la propiedad montIntReal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontIntReal(BigDecimal value) {
        this.montIntReal = value;
    }

    /**
     * Obtiene el valor de la propiedad perdida.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPerdida() {
        return perdida;
    }

    /**
     * Define el valor de la propiedad perdida.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPerdida(BigDecimal value) {
        this.perdida = value;
    }

}
