//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.6 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: PM.02.21 a las 03:37:48 PM CST 
//


package org.megapractical.invoicing.sat.complement.enajenaciondeacciones;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Version" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="1.0" />
 *       &lt;attribute name="ContratoIntermediacion" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;minLength value="1"/>
 *             &lt;maxLength value="300"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="Ganancia" use="required" type="{http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones}t_Importe" />
 *       &lt;attribute name="Perdida" use="required" type="{http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones}t_Importe" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnajenaciondeAcciones")
public class EnajenaciondeAcciones {

    @XmlAttribute(name = "Version", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String version;
    @XmlAttribute(name = "ContratoIntermediacion", required = true)
    protected String contratoIntermediacion;
    @XmlAttribute(name = "Ganancia", required = true)
    protected BigDecimal ganancia;
    @XmlAttribute(name = "Perdida", required = true)
    protected BigDecimal perdida;

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad contratoIntermediacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContratoIntermediacion() {
        return contratoIntermediacion;
    }

    /**
     * Define el valor de la propiedad contratoIntermediacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContratoIntermediacion(String value) {
        this.contratoIntermediacion = value;
    }

    /**
     * Obtiene el valor de la propiedad ganancia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGanancia() {
        return ganancia;
    }

    /**
     * Define el valor de la propiedad ganancia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGanancia(BigDecimal value) {
        this.ganancia = value;
    }

    /**
     * Obtiene el valor de la propiedad perdida.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPerdida() {
        return perdida;
    }

    /**
     * Define el valor de la propiedad perdida.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPerdida(BigDecimal value) {
        this.perdida = value;
    }

}
