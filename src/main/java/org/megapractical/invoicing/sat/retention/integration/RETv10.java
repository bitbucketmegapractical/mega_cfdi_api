package org.megapractical.invoicing.sat.retention.integration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.codec.binary.Base64;
import org.apache.xerces.dom.ElementNSImpl;
import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;
import org.megapractical.invoicing.sat.retention.integration.Retenciones;
import org.megapractical.invoicing.sat.retention.tfd.TimbreFiscalDigital;
import org.megapractical.invoicing.api.stub.KeyLoader;
import org.megapractical.invoicing.sat.retention.common.CFD;
import org.megapractical.invoicing.sat.retention.common.NamespacePrefixMapperImpl;
import org.megapractical.invoicing.sat.retention.common.URIResolverImpl;
import org.megapractical.invoicing.sat.retention.common.ValidationErrorHandler;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class RETv10 implements CFD {
	
	// Definiendo el XSLT de Retenciones y pagos
	private static final String XSLT = "/xslt/retv10/retenciones.xslt";
	
	// Definiendo los XSD
	//##### Paso 1
	private static final String[] XSD = new String[]{
        "/xsd/retv10/retencionpagov1.xsd",
        "/xsd/retv10/TimbreFiscalDigital.xsd",
        "/xsd/complements/dividendos/dividendos.xsd",
        "/xsd/complements/enajenacion_de_acciones/enajenaciondeacciones.xsd",
        "/xsd/complements/intereses/intereses.xsd",
        "/xsd/complements/operaciones_con_derivados/operacionesconderivados.xsd",
        "/xsd/complements/pagos_a_extranjeros/pagosaextranjeros.xsd",
        "/xsd/complements/sector_financiero/sectorfinanciero.xsd"
    };
	
	private static final String BASE_CONTEXT = "org.megapractical.invoicing.sat.retention.integration";
	
	private final static Joiner JOINER = Joiner.on(':');
	
	private final JAXBContext context;
	
	static final ImmutableMap<String, String> PREFIXES = new ImmutableMap.Builder<String, String>()
	    .put("http://www.w3.org/2001/XMLSchema-instance", "xsi")
	    .put("http://www.sat.gob.mx/esquemas/retencionpago/1", "retenciones")
	    .put("http://www.sat.gob.mx/esquemas/retencionpago/1 http://www.sat.gob.mx/esquemas/retencionpago/1/retencionpagov1.xsd", "xsi:schemaLocation")
	    //.put("http://www.sat.gob.mx/TimbreFiscalDigital", "tfd")
	    .build();
	
	public static final Map<String, String> localPrefixes = Maps.newHashMap(PREFIXES);
	
	private TransformerFactory tf;
	
	final Retenciones document;
	
	public RETv10(InputStream in, String... contexts) throws Exception {
        this.document = load(in, this.getClass().getClassLoader());
        this.context = getContext(this.document, contexts);
    }

    public RETv10(Retenciones retencion, String... contexts) throws Exception {
        this.context = getContext(retencion, contexts);
        this.document = copy(retencion);
    }
	
    public void addNamespace(String uri, String prefix) {
        localPrefixes.put(uri, prefix);
    }
    
    public Retenciones sellarRetencion(PrivateKey key, X509Certificate cert) throws Exception {
        sellar(key, cert);
        return getRetenciones();
    }
    
	public void setTransformerFactory(TransformerFactory tf) {
		this.tf = tf;
        tf.setURIResolver(new URIResolverImpl());
	}

	public void sellar(PrivateKey key, X509Certificate cert) throws Exception {
		cert.checkValidity();
        String signature = getSignature(key);
        document.setSello(signature);
        byte[] bytes = cert.getEncoded();

        Base64 b64 = new Base64(-1);
        String certStr = b64.encodeToString(bytes);
        document.setCert(certStr);
        BigInteger bi = cert.getSerialNumber();
        document.setNumCert(new String(bi.toByteArray()));
	}

	public void validar() throws Exception {
        ValidationErrorHandler error2 = new ValidationErrorHandler();
        validar(error2);
        List<SAXParseException> errores = error2.getErrors();
        String listadoErrores = null;
        if (!errores.isEmpty()) {
        	System.out.println("[SAX ERROR] METHOD VALIDAR\n");
            for (SAXParseException saxParseException : errores) {
                System.out.println("SAX ERROR MESSAGE ==> " + saxParseException.getMessage() + "\nSAX ERROR LOCALIZED MESSAGE ==> " + saxParseException.getLocalizedMessage() + "\n");
                listadoErrores += saxParseException.getMessage();
            }
            throw new SAXException(listadoErrores);
        }
    }

    public void validar(ErrorHandler handler) throws SAXException, IOException, JAXBException {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source[] schemas = new Source[XSD.length];
        for (int i = 0; i < XSD.length; i++) {
            schemas[i] = new StreamSource(getClass().getResourceAsStream(XSD[i]));
        }
        Schema schema = sf.newSchema(schemas);
        Validator validator = schema.newValidator();
        if (handler != null) {
            validator.setErrorHandler(handler);
        }
        validator.validate(new JAXBSource(context, document));
    }
    
    public List<SAXParseException> saxParseValidate() throws Exception {
    	try {
    		ValidationErrorHandler validationErrorHandler = new ValidationErrorHandler();
            validar(validationErrorHandler);
            List<SAXParseException> saxParseExceptionList = validationErrorHandler.getErrors();
            return saxParseExceptionList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }

	public void verificar() throws Exception {
		String certStr = document.getCert();
        Base64 b64 = new Base64();
        byte[] cbs = b64.decode(certStr);
        X509Certificate cert = KeyLoader.loadX509Certificate(new ByteArrayInputStream(cbs));
        String sigStr = document.getSello();
        byte[] signature = b64.decode(sigStr);
        String cadena = this.getCadenaOriginal();
        // System.out.println("la cadena que me da la clase del componente" + cadena);
        byte[] bytes = cadena.getBytes("UTF-8");
        // byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(cert); // certificado
        sig.update(bytes); // cadena
        boolean bool = sig.verify(signature); // sello
        if (!bool) {
            throw new Exception("Firma inválida, el texto actual del documento no coincide con el texto original firmado");
        }
	}

	public void guardar(OutputStream out) throws Exception {
		JAXBContext context = getContext(this.getRetenciones(), new String[0]);
        Marshaller m = context.createMarshaller();
        
        //String schemalocation = getSchemaLocation(this.getRetenciones());
        String schemalocation = getSchemaLocationDefault();
        
        m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemalocation);        
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapperImpl(localPrefixes));
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);  //con esto eliminamos el encabezado del XML
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); //con esto formateamos el XML para que no se vea en una sola linea.
        m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        m.marshal(document, out);
	}

	public String getCadenaOriginal() throws Exception {
		byte[] bytes = getOriginalBytes();
        return new String(bytes, "UTF8");
	}
	
	byte[] getOriginalBytes() throws Exception {
        JAXBSource in = new JAXBSource(context, document);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Result out = new StreamResult(baos);
        TransformerFactory factory = tf;
        if (factory == null) {
            factory = TransformerFactory.newInstance();
            factory.setURIResolver(new URIResolverImpl());
        }
        Transformer transformer = factory.newTransformer(new StreamSource(getClass().getResourceAsStream(XSLT)));
        transformer.transform(in, out);
        return baos.toByteArray();
    }
	
	String getSignature(PrivateKey key) throws Exception {
        byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initSign(key);
        sig.update(bytes);
        byte[] signed = sig.sign();
        Base64 b64 = new Base64(-1);
        return b64.encodeToString(signed);
    }
	
	public Retenciones getRetenciones() throws Exception {
        return this.document;
    }
	
	public static String getSchemaLocationDefault(){
		return "http://www.sat.gob.mx/esquemas/retencionpago/1 http://www.sat.gob.mx/esquemas/retencionpago/1/retencionpagov1.xsd";
	}
	
	public static String getSchemaLocation(Retenciones document) throws Exception {
		
		String cadena = "http://www.sat.gob.mx/esquemas/retencionpago/1 http://www.sat.gob.mx/esquemas/retencionpago/1/retencionpagov1.xsd";
		
		if (document != null && document.getComplemento() != null && document.getComplemento().getAny().size() > 0) {
        	
        	List<Object> list = document.getComplemento().getAny();
        	for (Object object : list) {
                String nombre = object.getClass().getName();

                //##### Paso 2
                // Complemento: Dividendo
                if (object instanceof Dividendos) {
                    nombre = "org.megapractical.invoicing.sat.complement.dividendos.Dividendos";
                }
                
                // Complemento: Enajenacion de Acciones
                if (object instanceof EnajenaciondeAcciones) {
                    nombre = "org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenacionDeAcciones";
                } 
                
                // Complemento: Intereses
                if (object instanceof Intereses) {
                    nombre = "org.megapractical.invoicing.sat.complement.intereses.Intereses";
                }
                
                // Complemento: Operaciones con Derivados
                if (object instanceof OperacionesconDerivados) {
                    nombre = "org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados";
                }
                
                // Complemento: Pagos a Extranjeros
                if (object instanceof PagosaExtranjeros) {
                    nombre = "org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros";
                }
                
                // Complemento: Sector Financiero
                if (object instanceof SectorFinanciero) {
                    nombre = "org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero";
                }
                
                // Timbre Fiscal Digital
                if(object instanceof TimbreFiscalDigital){
                	nombre = "org.megapractical.invoicing.sat.retention.tfd.TimbreFiscalDigital";
                }
                
                if (object instanceof ElementNSImpl) {
                    nombre = ((ElementNSImpl) object).getLocalName();
                }

                //##### Paso 3
                // Complemento: Dividendo
                if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.dividendos.Dividendos")) {
                    cadena += " http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos/dividendos.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos", "dividendos");
                } 
                
                // Complemento: Enajenacion de Acciones
                if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones")) {
                    cadena += " http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones/enajenaciondeacciones.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones", "enajenaciondeacciones");
                } 
                
                // Complemento: Intereses
                if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.intereses.Intereses")) {
                    cadena += " http://www.sat.gob.mx/esquemas/retencionpago/1/intereses http://www.sat.gob.mx/esquemas/retencionpago/1/intereses/intereses.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/esquemas/retencionpago/1/intereses", "intereses");
                }
                
                // Complemento: Operaciones con Derivados
                if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados")) {
                    cadena += " http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados/operacionesconderivados.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados", "operacionesconderivados");
                }
                
                // Complemento: Pagos a Extranjeros
                if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros")) {
                    cadena += " http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros/pagosaextranjeros.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros", "pagosaextranjeros");
                }
                
                // Complemento: Sector Financiero
                if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero")) {
                    cadena += " http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero/sectorfinanciero.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero", "sectorfinanciero");
                }
                
                /*// Complemento: Timbre Fiscal Digital
                if (nombre.equalsIgnoreCase("org.megapractical.ebill.retencion.lib.integration.TimbreFiscalDigital")) {
                    cadena += " http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/TimbreFiscalDigital", "tfd");
                }*/
            }
        }
		
	    /*if (((Retenciones) document).getAddenda() != null && ((Retenciones) document).getAddenda().getAny().size() > 0) {
	        for (Object o : ((Retenciones) document).getAddenda().getAny()) {
	            if (o instanceof Addenda) {
	                cadena += " http://cfdi.megapractical.com/retencionpago/1/addendas http://cfdi.megapractical.com/retencionpago/1/addendas/addendas.xsd";
	                localPrefixes.put("http://cfdi.megapractical.com/retencionpago/1/addendas", "addendas");
	            }
	        }
	    }*/
	    
	    return cadena;
	}
	
	private static JAXBContext getContext(Retenciones retencion, String[] contexts) throws Exception {
        List<String> ctx = Lists.asList(BASE_CONTEXT, contexts);
        List<String> tmp = new ArrayList<String>(ctx);

        if (retencion.getComplemento() != null && retencion.getComplemento().getAny().size() > 0) {
            for (Object object : retencion.getComplemento().getAny()) {
            	
            	// Paso 4
            	// Complemento: Dividendo
                if (object instanceof Dividendos) {
                	tmp.add("org.megapractical.invoicing.sat.complement.dividendos");
                } 
                
                // Complemento: Enajenacion de Acciones
                if (object instanceof EnajenaciondeAcciones) {
                	tmp.add("org.megapractical.invoicing.sat.complement.enajenaciondeacciones");
                } 
                
                // Complemento: Intereses
                if (object instanceof Intereses) {
                	tmp.add("org.megapractical.invoicing.sat.complement.intereses");
                }
                
                // Complemento: Operaciones con Derivados
                if (object instanceof OperacionesconDerivados) {
                	tmp.add("org.megapractical.invoicing.sat.complement.operacionesconderivados");
                }
                
                // Complemento: Pagos a Extranjeros
                if (object instanceof PagosaExtranjeros) {
                	tmp.add("org.megapractical.invoicing.sat.complement.pagosaextranjeros");
                }
                
                // Complemento: Sector Financiero
                if (object instanceof SectorFinanciero) {
                	tmp.add("org.megapractical.invoicing.sat.complement.sectorfinanciero");
                }
            }
        }

        return JAXBContext.newInstance(JOINER.join(tmp));
    }

    private static JAXBContext getContext(String[] contexts) throws Exception {
        
    	List<String> ctx = Lists.asList(BASE_CONTEXT, contexts);
        List<String> tmp = new ArrayList<String>(ctx);

        // Paso 5
        // Complemento: Dividendo
        tmp.add("org.megapractical.invoicing.sat.complement.dividendos");
        // Complemento: Enajenacion de Acciones
        tmp.add("org.megapractical.invoicing.sat.complement.enajenaciondeacciones");
        // Complemento: Intereses
        tmp.add("org.megapractical.invoicing.sat.complement.intereses");        
        // Complemento: Operaciones con Derivados
        tmp.add("org.megapractical.invoicing.sat.complement.operacionesconderivados");
        // Complemento: Pagos a Extranjeros
        tmp.add("org.megapractical.invoicing.sat.complement.pagosaextranjeros");
        // Complemento: Sector Financiero
        tmp.add("org.megapractical.invoicing.sat.complement.sectorfinanciero");
        
        return JAXBContext.newInstance(JOINER.join(tmp));
    }

    private static JAXBContext getContext(String[] contexts, ClassLoader loader) throws Exception {
        
    	List<String> ctx = Lists.asList(BASE_CONTEXT, contexts);
        List<String> tmp = new ArrayList<String>(ctx);

        // Paso 6
        // Complemento: Dividendo
        tmp.add("org.megapractical.invoicing.sat.complement.dividendos");
        // Complemento: Enajenacion de Acciones
        tmp.add("org.megapractical.invoicing.sat.complement.enajenaciondeacciones");
        // Complemento: Intereses
        tmp.add("org.megapractical.invoicing.sat.complement.intereses");
        // Complemento: Operaciones con Derivados
        tmp.add("org.megapractical.invoicing.sat.complement.operacionesconderivados");
        // Complemento: Pagos a Extranjeros
        tmp.add("org.megapractical.invoicing.sat.complement.pagosaextranjeros");
        // Complemento: Sector Financiero
        tmp.add("org.megapractical.invoicing.sat.complement.sectorfinanciero");
        
        return JAXBContext.newInstance(JOINER.join(tmp), loader);
    }
	
    private static Retenciones load(InputStream source, String... contexts) throws Exception {
        JAXBContext context = getContext(contexts);
        try {
            Unmarshaller u = context.createUnmarshaller();
            return (Retenciones) u.unmarshal(source);
        } finally {
            source.close();
        }
    }

    /**
     * Obtener objeto Retenciones a partir de Imput Stream.
     *
     * @param source
     * @param loader
     * @param contexts
     * @return
     * @throws Exception
     */
    private static Retenciones load(InputStream source, ClassLoader loader, String... contexts) throws Exception {
        JAXBContext context = getContext(contexts, loader);
        try {
            Unmarshaller u = context.createUnmarshaller();
            return (Retenciones) u.unmarshal(source);
        } finally {
            source.close();
        }
    }
    
    /**
     * Obtener objeto retenciones a partir de Retenciones.
     *
     * @param comprobante
     * @return
     * @throws Exception
     */
    private Retenciones copy(Retenciones retencion) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();
        Marshaller m = context.createMarshaller();
        m.marshal(retencion, doc);
        Unmarshaller u = context.createUnmarshaller();
        return (Retenciones) u.unmarshal(doc);
    }
    
    String getSignatureHSM(PrivateKey key) throws Exception {
        byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initSign(key);
        sig.update(bytes);
        byte[] signed = sig.sign();
        Base64 b64 = new Base64(-1);
        return b64.encodeToString(signed);
    }
	
}
