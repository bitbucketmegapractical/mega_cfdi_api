package org.megapractical.invoicing.sat.retention.tfd;

import static org.megapractical.invoicing.sat.retention.integration.RETv10.localPrefixes;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.codec.binary.Base64;
import org.apache.xerces.dom.ElementNSImpl;
import org.megapractical.invoicing.api.util.UDate;
import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;
import org.megapractical.invoicing.sat.retention.integration.RETv10;
import org.megapractical.invoicing.sat.retention.integration.Retenciones;
import org.megapractical.invoicing.sat.retention.tfd.ObjectFactory;
import org.megapractical.invoicing.sat.retention.tfd.TFDv10;
import org.megapractical.invoicing.sat.retention.tfd.TimbreFiscalDigital;
import org.megapractical.invoicing.sat.retention.common.NamespacePrefixMapperImpl;
import org.megapractical.invoicing.sat.retention.integration.Retenciones.Complemento;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;

import com.google.common.base.Joiner;

public class TFDv10 {

	private static final String XSLT = "/xslt/retv10/cadenaoriginal_TFD_1_0.xslt";

    private static final String XSD = "/xsd/retv10/TimbreFiscalDigital.xsd";
    
    static ClassLoader loader = null;
    
    private final static String CONTEXT = "org.megapractical.invoicing.sat.retention.tfd";
    
    private final static Joiner JOINER = Joiner.on(':');
    
    private final JAXBContext context;
    
    private final JAXBContext contextC;
    
    private static Map<String, String> namespaces = RETv10.localPrefixes;
    
    private final Retenciones document;
    
    private final String cadenaCFDI;

    private final TimbreFiscalDigital tfd;

    private final X509Certificate cert;

    private TransformerFactory tf;
    
    private static final JAXBContext createContext(ClassLoader loader) {
        try {
            return JAXBContext.newInstance("org.megapractical.invoicing.sat.retention.tfd", loader);
        } catch (Exception e) {
            throw new Error(e);
        }
    }
    
    private static JAXBContext createContext(String contexts, ClassLoader loader) throws Exception {
        return JAXBContext.newInstance(contexts, loader);
    }
    
    public TFDv10(RETv10 retencion, X509Certificate cert) throws Exception {
        this(retencion, cert, UUID.randomUUID(), new Date());
    }

    public TFDv10(RETv10 retencion, X509Certificate cert, UUID uuid, Date date) throws Exception {
    	if(retencion.getRetenciones().getComplemento() != null) {
    		if (retencion.getRetenciones().getComplemento().getAny() != null) {
                ListIterator<Object> iter = retencion.getRetenciones().getComplemento().getAny().listIterator();
                while (iter.hasNext()) {
                    Object teess = iter.next();
                    System.out.println(teess.toString());
                }
            }
    	}
    	
        loader = this.getClass().getClassLoader();
        String contextos = getComplementos(retencion.getRetenciones());
        this.contextC = createContext(contextos, loader);
        this.context = createContext(loader);
        this.cert = cert;
        this.document = retencion.getRetenciones();
        this.cadenaCFDI = retencion.getCadenaOriginal();
        this.tfd = getTimbreFiscalDigital(document, uuid, date);
    }

    public TFDv10(RETv10 retencion, X509Certificate cert, UUID uuid, String date, String serialNumber) throws Exception {
    	if(retencion.getRetenciones().getComplemento() != null) {
    		if (retencion.getRetenciones().getComplemento().getAny() != null) {
                ListIterator<Object> iter = retencion.getRetenciones().getComplemento().getAny().listIterator();
                while (iter.hasNext()) {
                    Object teess = iter.next();
                    System.out.println(teess.toString());
                }
            }
    	}
    	
        loader = this.getClass().getClassLoader();
        String contextos = getComplementos(retencion.getRetenciones());
        this.contextC = createContext(contextos, loader);
        this.context = createContext(loader);
        this.cert = cert;
        this.document = retencion.getRetenciones();
        this.cadenaCFDI = retencion.getCadenaOriginal();
        this.tfd = getTimbreFiscalDigital(document, uuid, date, serialNumber);
    }
    
    
    public void setTransformerFactory(TransformerFactory tf) {
        this.tf = tf;
    }

    public int timbrar(PrivateKey key) throws Exception {
        if (tfd.getSelloSAT() != null) {
            return 304;
        }
        String signature = getSignature(key);

        tfd.setSelloSAT(signature);

        System.out.println("************[SERVICIO DE TIMRADO]*****");
        System.out.println("Firma ubicada en el sello SAT : " + signature);

        stamp();
        return 300;
    }

    public void validar() throws Exception {
        validar(null);
    }

    public void validar(ErrorHandler handler) throws Exception {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(getClass().getResource(XSD));
        Validator validator = schema.newValidator();
        if (handler != null) {
            validator.setErrorHandler(handler);
        }
        validator.validate(new JAXBSource(context, tfd));
    }

    public int verificar() throws Exception {
        if (tfd == null) {
            return 601; // No contiene timbrado
        }
        Base64 b64 = new Base64();
        String sigStr = tfd.getSelloSAT();
        byte[] signature = b64.decode(sigStr);
        byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(cert);
        sig.update(bytes);
        boolean verified = sig.verify(signature);
        return verified ? 600 : 602; // Sello del timbrado no valido
    }

    public String getCadenaOriginal() throws Exception {
        byte[] bytes = getOriginalBytes();
        return new String(bytes);
    }

    public String getUUID() {
        return this.tfd.getUUID().toUpperCase();
    }

    public String getselloSAT() {
        return this.tfd.getSelloSAT();
    }

    public String getselloCFDI() {
        return this.tfd.getSelloCFD();
    }

    public String getNoCertificadoSAT() {
        return this.tfd.getNoCertificadoSAT();
    }

    public String getFechaTimbrado() {
        return this.tfd.getFechaTimbrado();
    }

    public String getcadenaCFDI() {
        return this.cadenaCFDI;
    }

    public String getRFCemisor() {
        return this.document.getEmisor().getRFCEmisor();
    }

    public void guardar(OutputStream out) throws Exception {
        Marshaller m = contextC.createMarshaller();
        String schemalocation = getSchemaLocation(document);
        m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemalocation);
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapperImpl(namespaces));
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); // con esto
        // eliminamos el
        // encabezado
        // del XML
        m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        m.marshal(document.getComplemento(), out);
    }
    
    TimbreFiscalDigital getTimbre() {
        return tfd;
    }

    byte[] getOriginalBytes() throws Exception {
        JAXBSource in = new JAXBSource(context, tfd);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Result out = new StreamResult(baos);
        TransformerFactory factory = tf;
        if (factory == null) {
            factory = TransformerFactory.newInstance();
        }
        Transformer transformer = factory.newTransformer(new StreamSource(
                getClass().getResourceAsStream(XSLT)));
        transformer.transform(in, out);
        return baos.toByteArray();
    }

    String getSignature(PrivateKey key) throws Exception {
        byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initSign(key);
        sig.update(bytes);
        byte[] signed = sig.sign();
        Base64 b64 = new Base64(-1);
        return b64.encodeToString(signed);
    }

    private void stamp() {
        try {
            Element element = marshalTFD();
            document.setComplemento((Complemento) element);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private Element marshalTFD() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();
        Marshaller m = context.createMarshaller();
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapperImpl(namespaces));
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.sat.gob.mx/TimbreFiscalDigitalll http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd");
        m.marshal(tfd, doc);
        return doc.getDocumentElement();
    }

    private TimbreFiscalDigital createStamp(UUID uuid, Date date) {
        ObjectFactory objectFactory = new ObjectFactory();
        TimbreFiscalDigital tfd = objectFactory.createTimbreFiscalDigital();
        tfd.setVersion("1.0");
        tfd.setUUID(uuid.toString().toUpperCase());
        
        /*GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        XMLGregorianCalendar xmlGC = null;
        try {
        	xmlGC = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(TFDv10.class.getName()).log(Level.SEVERE, null, ex);
        }
        tfd.setFechaTimbrado(xmlGC);*/
        tfd.setFechaTimbrado(UDate.formattedDate(date));
        
        BigInteger bi = cert.getSerialNumber();
        tfd.setNoCertificadoSAT(new String(bi.toByteArray()));
        tfd.setSelloCFD(document.getSello());

        return tfd;
    }
    
    private TimbreFiscalDigital createStamp(UUID uuid, String date, String serialNumber) {
        ObjectFactory objectFactory = new ObjectFactory();
        TimbreFiscalDigital tfd = objectFactory.createTimbreFiscalDigital();
        tfd.setVersion("1.0");
        tfd.setUUID(uuid.toString().toUpperCase());
        tfd.setFechaTimbrado(date);
        
        tfd.setNoCertificadoSAT(serialNumber);
        tfd.setSelloCFD(document.getSello());

        return tfd;
    }

    private TimbreFiscalDigital getTimbreFiscalDigital(Retenciones document, UUID uuid, Date date) throws Exception {
    	if(document.getComplemento() != null) {
    		if (document.getComplemento().getAny() != null) {
                List<Object> list = document.getComplemento().getAny();
                for (Object object : list) {
                    if (object instanceof TimbreFiscalDigital) {
                        return (TimbreFiscalDigital) object;
                    }
                }
            }
    	}
        return createStamp(uuid, date);
    }
    
    private TimbreFiscalDigital getTimbreFiscalDigital(Retenciones document, UUID uuid, String date, String serialNumber) throws Exception {
    	if(document.getComplemento() != null) {
    		if (document.getComplemento().getAny() != null) {
                List<Object> list = document.getComplemento().getAny();
                for (Object object : list) {
                    if (object instanceof TimbreFiscalDigital) {
                        return (TimbreFiscalDigital) object;
                    }
                }
            }
    	}
        return createStamp(uuid, date, serialNumber);
    }
    
    public static TimbreFiscalDigital createTimbreFiscalDigital(org.w3c.dom.Element elementNSImpl){//ElementNSImpl
    	try {
			
    		ObjectFactory objectFactory = new ObjectFactory();
            TimbreFiscalDigital tfd = objectFactory.createTimbreFiscalDigital();
    		tfd.setFechaTimbrado(elementNSImpl.getAttribute("FechaTimbrado"));
    		tfd.setNoCertificadoSAT(elementNSImpl.getAttribute("noCertificadoSAT"));
    		tfd.setSelloCFD(elementNSImpl.getAttribute("selloCFD"));
    		tfd.setSelloSAT(elementNSImpl.getAttribute("selloSAT"));
    		tfd.setUUID(elementNSImpl.getAttribute("UUID"));
    		tfd.setVersion(elementNSImpl.getAttribute("version"));
    		
    		return tfd;
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String getSchemaLocation(Retenciones document) throws Exception {
        String cadena = "http://www.sat.gob.mx/esquemas/retencionpago/1 http://www.sat.gob.mx/esquemas/retencionpago/1/retencionpagov1.xsd";
        
        if(document.getComplemento().getAny() != null){
        	
        	List<Object> list = document.getComplemento().getAny();
        	for (Object object : list) {
                String nombre = "";

                // Paso 1
                if (object instanceof Dividendos) { // Complemento: Dividendo
                    nombre = "Dividendos";
                } else if (object instanceof EnajenaciondeAcciones) { // Complemento: Enajenacion de Acciones
                    nombre = "EnajenaciondeAcciones";
                } else if (object instanceof Intereses) { // Complemento: Intereses
                    nombre = "Intereses";
                } else if (object instanceof OperacionesconDerivados) { // Complemento: Operaciones con Derivados
                	nombre = "OperacionesconDerivados";
                } else if (object instanceof PagosaExtranjeros) { // Complemento: Pagos a Extranjeros
                    nombre = "PagosaExtranjeros";
                } else if (object instanceof SectorFinanciero) { // Complemento: Sector Financiero
                    nombre = "SectorFinanciero";
                } 
                
                /*else if (object instanceof org.apache.xerces.dom.ElementNSImpl) {
                    nombre = ((org.apache.xerces.dom.ElementNSImpl) object).getLocalName();
                } else if (object instanceof ElementNSImpl) {
                    nombre = ((ElementNSImpl) object).getLocalName();
                }*/
                else if (object instanceof org.w3c.dom.Element) {
                    nombre = ((org.w3c.dom.Element) object).getLocalName();
                } else if (object instanceof org.w3c.dom.Element) {
                    nombre = ((org.w3c.dom.Element) object).getLocalName();
                }

                // Paso 2
                if (nombre.equalsIgnoreCase("Dividendos")) {  // Complemento: Dividendo
                    cadena += " http://www.sat.gob.mx/dividendos http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos/dividendos.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/dividendos", "dividendos");
                } else if (nombre.equalsIgnoreCase("EnajenaciondeAcciones")) { // Complemento: Enajenacion de Acciones
                    cadena += " http://www.sat.gob.mx/enajenaciondeacciones http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones/enajenaciondeacciones.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/enajenaciondeacciones", "enajenaciondeacciones");
                } else if (nombre.equalsIgnoreCase("Intereses")) { // Complemento: Intereses
                    cadena += " http://www.sat.gob.mx/intereses http://www.sat.gob.mx/esquemas/retencionpago/1/intereses/intereses.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/intereses", "intereses");
                } else if (nombre.equalsIgnoreCase("OperacionesconDerivados")) { // Complemento: Operaciones con Derivados
                    cadena += " http://www.sat.gob.mx/operacionesconderivados http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados/operacionesconderivados.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/operacionesconderivados", "operacionesconderivados");
                } else if (nombre.equalsIgnoreCase("PagosaExtranjeros")) { // Complemento: Pagos a Extranjeros
                    cadena += " http://www.sat.gob.mx/pagosaextranjeros http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros/pagosaextranjeros.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/pagosaextranjeros", "pagosaextranjeros");
                } else if (nombre.equalsIgnoreCase("SectorFinanciero")) { // Complemento: Sector Financiero
                    cadena += " http://www.sat.gob.mx/sectorfinanciero http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero/sectorfinanciero.xsd";
                    localPrefixes.put("http://www.sat.gob.mx/sectorfinanciero", "sectorfinanciero");
                }
            }
        }
        
        return cadena;
    }
    
    private String getComplementos(Retenciones retenciones) throws Exception {
        
    	String cadena = "org.megapractical.invoicing.sat.retention.tfd:";
        
        if (retenciones.getComplemento() != null && retenciones.getComplemento().getAny().size() > 0) {
        	
        	List<Object> list = retenciones.getComplemento().getAny();
            
        	for (Object object : list) {

                // Paso 3
                if (!(object instanceof TimbreFiscalDigital)) {
                    String nombre = "";
                    if (object instanceof Dividendos) { // Complemento: Dividendo
                        nombre = "Dividendos";
                    } else if (object instanceof EnajenaciondeAcciones) { // Complemento: Enajenacion de Acciones
                        nombre = "EnajenaciondeAcciones";
                    } else if (object instanceof Intereses) { // Complemento: Intereses
                        nombre = "Intereses";
                    } else if (object instanceof OperacionesconDerivados) { // Complemento: Operaciones con Derivados
                        nombre = "OperacionesconDerivados";
                    } else if (object instanceof PagosaExtranjeros) { // Complemento: Pagos a Extranjeros
                        nombre = "PagosaExtranjeros";
                    } else if (object instanceof SectorFinanciero) { // Complemento: Sector Financiero
                        nombre = "SectorFinanciero";
                    } else if (object instanceof org.w3c.dom.Element) {//ElementNSImpl
                        nombre = ((org.w3c.dom.Element) object).getLocalName();//ElementNSImpl
                    }
                    
                    // Paso 4
                    if (nombre.equalsIgnoreCase("Dividendos")) {  // Complemento: Divisas
                        cadena += "org.megapractical.invoicing.sat.complement.dividendos:";
                    } else if (nombre.equalsIgnoreCase("EnajenaciondeAcciones")) { // Complemento: Enajenacion de Acciones
                        cadena += "org.megapractical.invoicing.sat.complement.enajenaciondeacciones:";
                    } else if (nombre.equalsIgnoreCase("Intereses")) { // Complemento: Intereses
                        cadena += "org.megapractical.invoicing.sat.complement.intereses:";
                    } else if (nombre.equalsIgnoreCase("OperacionesconDerivados")) { // Complemento: Operaciones con Derivados
                        cadena += "org.megapractical.invoicing.sat.complement.operacionesconderivados:";
                    } else if (nombre.equalsIgnoreCase("PagosaExtranjeros")) { // Complemento: Pagos a Extranjeros
                        cadena += "org.megapractical.invoicing.sat.complement.pagosaextranjeros:";
                    } else if (nombre.equalsIgnoreCase("SectorFinanciero")) { // Complemento: Sector Financiero
                        cadena += "org.megapractical.invoicing.sat.complement.sectorfinanciero:";
                    }
                }
            }
        }

        return cadena;
    }
    
}
