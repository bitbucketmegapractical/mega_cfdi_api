/*
 *  Copyright 2017 Megapractical
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.megapractical.invoicing.sat.integration.v3;

import static org.megapractical.invoicing.sat.integration.v3.CFDv33.localPrefixes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.codec.binary.Base64;
import org.megapractical.invoicing.api.common.CFDI;
import org.megapractical.invoicing.api.common.NamespacePrefixMapperImpl;
import org.megapractical.invoicing.api.common.URIResolverImpl;
import org.megapractical.invoicing.api.common.ValidationErrorHandler;
import org.megapractical.invoicing.api.common.VoucherBase;
import org.megapractical.invoicing.api.stub.KeyLoader;
import org.megapractical.invoicing.sat.addenda.AddendaSimple;
import org.megapractical.invoicing.sat.integration.v3.Comprobante.Conceptos.Concepto;
import org.megapractical.invoicing.sat.integration.v3.Comprobante.Conceptos.Concepto.ComplementoConcepto;
import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public final class CFDv33 implements CFDI {

    private static final String XSLT = "/xslt/v33/cadenaoriginal_3_3.xslt";

    // Paso 1
    private static final String[] XSD = new String[]{
        "/xsd/v33/cfdv33.xsd",
        "/xsd/v33/TimbreFiscalDigitalv11.xsd",
        "/xsd/complements/payments/Pagos10.xsd",
        "/xsd/complements/payroll/nomina12.xsd",
        "/xsd/complements/instituciones_educativas_privadas/iedu.xsd"
    };

    private static final String BASE_CONTEXT = "org.megapractical.invoicing.sat.integration.v3";

    private final static Joiner JOINER = Joiner.on(':');

    private final JAXBContext context;

    static final ImmutableMap<String, String> PREFIXES = new ImmutableMap.Builder<String, String>()
            .put("http://www.w3.org/2001/XMLSchema-instance", "xsi")
            .put("http://www.sat.gob.mx/cfd/3", "cfdi")
            //.put("http://www.sat.gob.mx/TimbreFiscalDigital", "tfd")
            .build();

    public static final Map<String, String> localPrefixes = Maps.newHashMap(PREFIXES);

    private TransformerFactory tf;

    final Comprobante document;

    public CFDv33(InputStream in, String... contexts) throws Exception {
        this.document = load(in, this.getClass().getClassLoader());
        this.context = getContext(this.document, contexts);
    }

    public CFDv33(Comprobante comprobante, String... contexts) throws Exception {
        this.context = getContext(comprobante, contexts);
        this.document = copy(comprobante);
    }

    public void addNamespace(String uri, String prefix) {
        localPrefixes.put(uri, prefix);
    }

    public void setTransformerFactory(TransformerFactory tf) {
        this.tf = tf;
        tf.setURIResolver(new URIResolverImpl());
    }

    public void sellar(PrivateKey key, X509Certificate cert) throws Exception {
        cert.checkValidity();
        String signature = getSignature(key);
        document.setSello(signature);
        byte[] bytes = cert.getEncoded();

        Base64 b64 = new Base64(-1);
        String certStr = b64.encodeToString(bytes);
        document.setCertificado(certStr);
        BigInteger bi = cert.getSerialNumber();
        document.setNoCertificado(new String(bi.toByteArray()));
    }

    public Comprobante sellarComprobante(PrivateKey key, X509Certificate cert) throws Exception {
        sellar(key, cert);
        return doGetComprobante();
    }

    public void validar() throws Exception {
        ValidationErrorHandler error2 = new ValidationErrorHandler();
        validar(error2);
        List<SAXParseException> errores = error2.getErrors();
        String listadoErrores = null;
        if (!errores.isEmpty()) {
        	System.out.println("[SAX ERROR] METHOD VALIDAR\n");
            for (SAXParseException saxParseException : errores) {
                System.out.println("SAX ERROR MESSAGE > " + saxParseException.getMessage() + "\nSAX ERROR LOCALIZED MESSAGE > " + saxParseException.getLocalizedMessage() + "\n");
                listadoErrores += saxParseException.getMessage();
            }
            throw new SAXException(listadoErrores);
        }
    }

    public void validar(ErrorHandler handler) throws SAXException, IOException, JAXBException {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source[] schemas = new Source[XSD.length];
        for (int i = 0; i < XSD.length; i++) {
            schemas[i] = new StreamSource(getClass().getResourceAsStream(XSD[i]));
        }
        Schema schema = sf.newSchema(schemas);
        Validator validator = schema.newValidator();
        if (handler != null) {
            validator.setErrorHandler(handler);
        }
        validator.validate(new JAXBSource(context, document));
    }
    
    public List<SAXParseException> saxParseValidate() throws Exception {
    	try {
    		ValidationErrorHandler validationErrorHandler = new ValidationErrorHandler();
            validar(validationErrorHandler);
            List<SAXParseException> saxParseExceptionList = validationErrorHandler.getErrors();
            return saxParseExceptionList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    } 

    public void verificar() throws Exception {
        String certStr = document.getCertificado();
        Base64 b64 = new Base64();
        byte[] cbs = b64.decode(certStr);
        X509Certificate cert = KeyLoader.loadX509Certificate(new ByteArrayInputStream(cbs));
        String sigStr = document.getSello();
        byte[] signature = b64.decode(sigStr);
        String cadena = this.getCadenaOriginal();
        //  System.out.println("la cadena que me da la clase del componente" + cadena);
        byte[] bytes = cadena.getBytes("UTF-8");
        //byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(cert); // certificado
        sig.update(bytes); // cadena
        boolean bool = sig.verify(signature); // sello
        if (!bool) {
            throw new Exception("Firma Invalida, el texto actual del documento no coincide con el texto original firmado");
        }
    }

    public void guardar(OutputStream out) throws Exception {
        JAXBContext context = getContext(this.doGetComprobante(), new String[0]);
        Marshaller m = context.createMarshaller();
        String schemalocation = getSchemaLocation(this.doGetComprobante());
        m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemalocation);
        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapperImpl(localPrefixes));
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);  //con esto eliminamos el encabezado del XML
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); //con esto formateamos el XML para que no se vea en una sola linea.
        m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        m.marshal(document, out);
    }

    public static String getSchemaLocation(Comprobante document) throws Exception {
        String cadena = "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd";
        if (document != null) {
        	if(!document.getComplementos().isEmpty()) {
        		List<Object> list = document.getComplementos().iterator().next().getAnies();
                for (Object object : list) {
                    String nombre = object.getClass().getName();

                    // ===========================================================================================================
                    // Paso 2
                    // ===========================================================================================================
                    //##### Complemento: Timbre Fiscal Digital v1.1
                    if (object instanceof TimbreFiscalDigital) {
                        nombre = "org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital";
                    }
                    //##### Complemento: Recepcion de pagos v1.0
                    if(object instanceof org.megapractical.invoicing.sat.complement.payments.Pagos){
                    	nombre = "org.megapractical.invoicing.sat.complement.payments.Pagos";
                    }
                    //##### Complemento: Nomina v1.2
                    if(object instanceof org.megapractical.invoicing.sat.complement.payroll.Nomina){
                    	nombre = "org.megapractical.invoicing.sat.complement.payroll.Nomina";
                    }
                    
                    // ===========================================================================================================
                    // Paso 3
                    // ===========================================================================================================
                    //##### Complemento: Timbre Fiscal Digital v1.1
                    /*if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.integration.v3.TimbreFiscalDigital")) {
                        cadena += " http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital11.xsd";
                        localPrefixes.put("http://www.sat.gob.mx/TimbreFiscalDigital", "tfd");
                    }*/
                    //##### Complemento: Recepcion de pagos v1.0
                    if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.payments.Pagos")) {
                        cadena += " http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd";
                        localPrefixes.put("http://www.sat.gob.mx/Pagos", "pago10");
                    }
                    //##### Complemento: Nomina v1.2
                    if (nombre.equalsIgnoreCase("org.megapractical.invoicing.sat.complement.payroll.Nomina")) {
                        cadena += " http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd";
                        localPrefixes.put("http://www.sat.gob.mx/nomina12", "nomina12");
                    }
                }
        	}
            
        	if(document.getConceptos() != null && !document.getConceptos().getConceptos().isEmpty()) {
            	List<Concepto> source = document.getConceptos().getConceptos();
            	for (Concepto item : source) {
            		ComplementoConcepto cc = item.getComplementoConcepto();
            		if(cc != null) {
            			for (Object object : cc.getAnies()) {
        	        		String nombre = object.getClass().getName();
                    		
                    		//##### Complemento Concepto: Instituciones Educativas Privadas v1.0
                            if (object instanceof org.megapractical.invoicing.sat.complement.concept.iedu.InstEducativas) {
                                nombre = "InstEducativas";
                            }
                    		
                            //##### Complemento Concepto: Instituciones Educativas Privadas v1.0
                            if (nombre.equalsIgnoreCase("InstEducativas")) {
                                cadena += " http://www.sat.gob.mx/iedu http://www.sat.gob.mx/sitio_internet/cfd/iedu/iedu.xsd";
                                localPrefixes.put("http://www.sat.gob.mx/iedu", "iedu");
                            }
        				}
            		}
    			}        	
            }
        }

        if (((Comprobante) document).getAddenda() != null && ((Comprobante) document).getAddenda().getAnies().size() > 0) {
            for (Object object : ((Comprobante) document).getAddenda().getAnies()) {
                if (object instanceof AddendaSimple) {
                    cadena += " http://cfdi.megapractical.com/addendas/simple http://cfdi.megapractical.com/addendas/simple.xsd";
                    localPrefixes.put("http://cfdi.megapractical.com/addendas/simple", "addendasimple");
                }
            }
        }
        return cadena;
    }

    public String getCadenaOriginal() throws Exception {
        byte[] bytes = getOriginalBytes();
        return new String(bytes, "UTF8");
    }

    public String getRFC() {
        return document.getEmisor().getRfc();
    }

    public static Comprobante newComprobante(InputStream in) throws Exception {
        return load(in);
    }

    byte[] getOriginalBytes() throws Exception {
        JAXBSource in = new JAXBSource(context, document);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Result out = new StreamResult(baos);
        TransformerFactory factory = tf;
        if (factory == null) {
            factory = TransformerFactory.newInstance();
            factory.setURIResolver(new URIResolverImpl());
        }
        Transformer transformer = factory.newTransformer(new StreamSource(getClass().getResourceAsStream(XSLT)));
        transformer.transform(in, out);
        return baos.toByteArray();
    }

    String getSignatureHSM(PrivateKey key) throws Exception {
        byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initSign(key);
        sig.update(bytes);
        byte[] signed = sig.sign();
        Base64 b64 = new Base64(-1);
        return b64.encodeToString(signed);
    }

    String getSignature(PrivateKey key) throws Exception {
        byte[] bytes = getOriginalBytes();
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initSign(key);
        sig.update(bytes);
        byte[] signed = sig.sign();
        Base64 b64 = new Base64(-1);
        return b64.encodeToString(signed);
    }

    public VoucherBase getComprobante() throws Exception {
        return new CFDv33ComprobanteBase(doGetComprobante());
    }

    public Comprobante doGetComprobante() throws Exception {
        return this.document;
    }

    // Defensive deep-copy
    public static final class CFDv33ComprobanteBase implements VoucherBase {

        private final Comprobante document;

        public CFDv33ComprobanteBase(Comprobante document) {
            this.document = document;
        }

        public boolean hasComplemento() {
            if (document.getComplementos() != null) {
                return document.getComplementos().iterator().next().getAnies().size() != 0;
            } else {
                return false;
            }
        }

        public boolean hasComplementoConcepto() {
            List<Concepto> listConceptos = document.getConceptos().getConceptos();
            for (Concepto concepto : listConceptos) {
                if (concepto.getComplementoConcepto() != null && concepto.getComplementoConcepto().getAnies() != null) {
                    return true;
                }
            }
            return false;
        }

        public List<Object> getComplementoConceptoGetAny() {
            List<Object> listCC = new ArrayList<Object>();
            List<Concepto> listConceptos = document.getConceptos().getConceptos();
            for (Concepto concepto : listConceptos) {
                if (concepto.getComplementoConcepto() != null) {
                    listCC.addAll(concepto.getComplementoConcepto().getAnies());
                }
            }
            return listCC;
        }

        public List<Object> getComplementoGetAny() {
            if (document.getComplementos() != null) {
                return document.getComplementos().iterator().next().getAnies();
            }
            return null;
        }

        public String getSello() {
            return document.getSello();
        }

        public String getRFC() {
            return document.getEmisor().getRfc();
        }

        @Override
        public String getFecha() {
            return document.getFecha();
        }

        public void setComplemento(Element element) {
            if (document.getComplementos() == null) {
                ObjectFactory of = new ObjectFactory();
                Comprobante.Complemento comp = of.createComprobanteComplemento();

                comp.getAnies().add(element);
                document.getComplementos().add(comp);

            } else {
                document.getComplementos().iterator().next().getAnies().add(element);
            }
        }

        public Object getComprobante() {
            return document;
        }

    }

    private static JAXBContext getContext(Comprobante comp, String[] contexts) throws Exception {
        List<String> ctx = Lists.asList(BASE_CONTEXT, contexts);
        List<String> tmp = new ArrayList<String>(ctx);

        if (comp.getComplementos() != null && comp.getComplementos().size() > 0) {
            for (Object object : comp.getComplementos().iterator().next().getAnies()) {
            	// ===========================================================================================================
                // Paso 4
                // ===========================================================================================================
            	//##### Complemento: Recepcion de pagos v1.0
            	if(object instanceof org.megapractical.invoicing.sat.complement.payments.Pagos){
            		tmp.add("org.megapractical.invoicing.sat.complement.payments");
            	}
            	//##### Complemento: Nomina v1.2
            	if(object instanceof org.megapractical.invoicing.sat.complement.payroll.Nomina){
            		tmp.add("org.megapractical.invoicing.sat.complement.payroll");
            	}
            }
        }
        
        if(comp.getConceptos() != null && !comp.getConceptos().getConceptos().isEmpty()) {
        	List<Concepto> source = comp.getConceptos().getConceptos();
        	for (Concepto item : source) {
        		ComplementoConcepto cc = item.getComplementoConcepto();
        		if(cc != null) {
        			for (Object object : cc.getAnies()) {
    	        		//##### Complemento Concepto: Instituciones Educativas Privadas v1.0
    					if(object instanceof org.megapractical.invoicing.sat.complement.concept.iedu.InstEducativas) {
    						tmp.add("org.megapractical.invoicing.sat.complement.concept.iedu");
    					}
    				}
        		}
			}        	
        }
        
        // Addenda simple
        if (comp.getAddenda() != null && comp.getAddenda().getAnies().size() > 0) {
            for (Object obj : comp.getAddenda().getAnies()) {
                if (obj instanceof AddendaSimple) {
                    tmp.add("org.megapractical.invoicing.sat.addenda");
                }
            }
        }
        return JAXBContext.newInstance(JOINER.join(tmp));
    }

    private static JAXBContext getContext(String[] contexts) throws Exception {
        List<String> ctx = Lists.asList(BASE_CONTEXT, contexts);
        List<String> tmp = new ArrayList<String>(ctx);
        // ===========================================================================================================
        // Paso 5
        // ===========================================================================================================
        //##### Complemento: Recepcion de pagos v1.0
        tmp.add("org.megapractical.invoicing.sat.complement.payments");
        //##### Complemento: Nomina v1.2
        tmp.add("org.megapractical.invoicing.sat.complement.payroll");
        //##### Complemento Concepto: Instituciones Educativas Privadas v1.0
        tmp.add("org.megapractical.invoicing.sat.complement.concept.iedu");
        
        // Addenda simple
        tmp.add("org.megapractical.invoicing.sat.addenda");
        return JAXBContext.newInstance(JOINER.join(tmp));
    }

    private static JAXBContext getContext(String[] contexts, ClassLoader loader) throws Exception {
        List<String> ctx = Lists.asList(BASE_CONTEXT, contexts);
        List<String> tmp = new ArrayList<String>(ctx);
        // ===========================================================================================================
        // Paso 6
        // ===========================================================================================================
        //Complemento: Recepcion de pagos
        tmp.add("org.megapractical.invoicing.sat.complement.payments");
        //##### Complemento: Nomina v1.2
        tmp.add("org.megapractical.invoicing.sat.complement.payroll");
        //##### Complemento Concepto: Instituciones Educativas Privadas v1.0
        tmp.add("org.megapractical.invoicing.sat.complement.concept.iedu");
        
        // Addenda simple
        //tmp.add("org.megapractical.invoicing.sat.addenda");
        return JAXBContext.newInstance(JOINER.join(tmp), loader);
    }

    private static Comprobante load(InputStream source, String... contexts)
            throws Exception {
        JAXBContext context = getContext(contexts);
        try {
            Unmarshaller u = context.createUnmarshaller();
            return (Comprobante) u.unmarshal(source);
        } finally {
            source.close();
        }
    }

    /**
     * Obtener objeto comprobante a partir de un Comprobante.
     *
     * @param comprobante
     * @return
     * @throws Exception
     */
    private Comprobante copy(Comprobante comprobante) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();
        Marshaller m = context.createMarshaller();
        m.marshal(comprobante, doc);
        Unmarshaller u = context.createUnmarshaller();
        return (Comprobante) u.unmarshal(doc);
    }

    /**
     * Obtener objeto Comprobante a partir de Imput Stream.
     *
     * @param source
     * @param loader
     * @param contexts
     * @return
     * @throws Exception
     */
    private static Comprobante load(InputStream source, ClassLoader loader, String... contexts)
            throws Exception {
        JAXBContext context = getContext(contexts, loader);
        try {
            Unmarshaller u = context.createUnmarshaller();
            return (Comprobante) u.unmarshal(source);
        } finally {
            source.close();
        }
    }

    static void dump(String title, byte[] bytes, PrintStream out) {
        out.printf("%s: ", title);
        for (byte b : bytes) {
            out.printf("%02x ", b & 0xff);
        }
        out.println();
    }
}
