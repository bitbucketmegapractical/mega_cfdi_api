package org.megapractical.invoicing.api.report;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UNumberToLetter;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;
import org.megapractical.invoicing.sat.integration.v40.CFDv4;
import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;

import lombok.val;
import lombok.var;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class ReportManagerTools {

	private ReportManagerTools() {		
	}
	
	public static final String QR_BASE_URL = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx";
	
	public static String totalInLetter(CFDv4 cfdi) {
        try {
        	val importeTotal = cfdi.doGetComprobante().getTotal().setScale(2, RoundingMode.HALF_UP);
            val moneda = cfdi.doGetComprobante().getMoneda().value() != null ? cfdi.doGetComprobante().getMoneda().value() : "MXN";
            return UNumberToLetter.numberToLetter(importeTotal.toPlainString(), 2, moneda);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
	
	public static String totalInLetter(BigDecimal importeTotal, String moneda) {
        try {
        	importeTotal = importeTotal.setScale(2, RoundingMode.HALF_UP);
			if (UValidator.isNullOrEmpty(moneda)) {
				moneda = "MXN";
			}
            return UNumberToLetter.numberToLetter(importeTotal.toPlainString(), 2, moneda);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
	
	public static final ByteArrayInputStream getQR() {
		return getQrInputStream(QR_BASE_URL);
    }
	
	public static final ByteArrayInputStream getQR(CFDv4 cfdi, TimbreFiscalDigital tfd, StampResponse stamp) {
		try {
	        val qrContent = getQRString(cfdi, tfd, stamp);
	        return getQrInputStream(qrContent);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
	
	public static final ByteArrayInputStream getQR(CFDv4 cfdi, StampResponse stamp) {
		try {
			val qrContent = getQRString(cfdi, stamp);
			return getQrInputStream(qrContent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static final ByteArrayInputStream getQR(StampResponse stamp) {
		try {
			val qrContent = getQRString(stamp);
			return getQrInputStream(qrContent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static final ByteArrayInputStream getQR(Retenciones ret, StampResponse stamp) {
		try {
			val qrContent = getQRString(ret, stamp);
			return getQrInputStream(qrContent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static final String getQRString(CFDv4 cfdi, TimbreFiscalDigital tfd, StampResponse stamp) {
		if (tfd != null) {
			return getQR(cfdi, stamp, tfd.getUUID());
		}
		return null;
    }
	
	public static final String getQRString(CFDv4 cfdi, StampResponse stamp) {
		return getQR(cfdi, stamp, stamp.getUuid());
	}
	
	public static final String getQRString(StampResponse stamp) {
		try {
			if (stamp != null && stamp.getEmitterSeal() != null) {
				val unusualSymbols = new DecimalFormatSymbols();
				unusualSymbols.setDecimalSeparator('.');
				
				val decimalFormat = new DecimalFormat("0000000000.000000", unusualSymbols);
				decimalFormat.setCurrency(java.util.Currency.getInstance(Locale.US));
				
				var emitterSeal = stamp.getEmitterSeal();
				emitterSeal = emitterSeal.substring(emitterSeal.length() - 8, emitterSeal.length());
				
				val emitterRfc = stamp.getVoucher().getEmisor().getRfc();
				val receiverRfc = stamp.getVoucher().getReceptor().getRfc();
				val total = UValue.bigDecimalString(stamp.getVoucher().getTotal());
				
				return getQR(stamp.getUuid(), emitterRfc, receiverRfc, total, emitterSeal);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static final ByteArrayInputStream getQrInputStream(String qrContent) {
		try {
			if (qrContent != null) {
				System.out.println("-----------------------------------------------------------------------------------------------------------");
				System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] QR GENERATED > " + qrContent);
				
				var qrCode = QRCode.from(qrContent);
				qrCode = (QRCode) qrCode.withSize(120, 120);
				val qrCodeOut = qrCode.to(ImageType.PNG).stream().toByteArray();
				return new ByteArrayInputStream(qrCodeOut);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static final String getQR(CFDv4 cfdi, StampResponse stamp, String uuid) {
		try {
			if (cfdi != null && stamp != null && uuid != null) {
				val unusualSymbols = new DecimalFormatSymbols();
				unusualSymbols.setDecimalSeparator('.');
				
				val decimalFormat = new DecimalFormat("0000000000.000000", unusualSymbols);
				decimalFormat.setCurrency(java.util.Currency.getInstance(Locale.US));
				
				var emitterSeal = stamp.getEmitterSeal();
				emitterSeal = emitterSeal.substring(emitterSeal.length() - 8, emitterSeal.length());
				
				val emitterRfc = cfdi.doGetComprobante().getEmisor().getRfc();
				val receiverRfc = cfdi.doGetComprobante().getReceptor().getRfc();
				val total = UValue.bigDecimalString(cfdi.doGetComprobante().getTotal());
				
				return getQR(uuid, emitterRfc, receiverRfc, total, emitterSeal);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static final String getQRString(Retenciones ret, StampResponse stamp) {
		try {
			if (ret != null && stamp != null) {
				val unusualSymbols = new DecimalFormatSymbols();
				unusualSymbols.setDecimalSeparator('.');
				
				val decimalFormat = new DecimalFormat("0000000000.000000", unusualSymbols);
				decimalFormat.setCurrency(java.util.Currency.getInstance(Locale.US));
				
				String receiverRfc = null;
				if (ret.getReceptor().getNacional() != null) {
					receiverRfc = ret.getReceptor().getNacional().getRfcR();
				} else if (ret.getReceptor().getExtranjero() != null) {
					receiverRfc = ret.getReceptor().getExtranjero().getNumRegIdTribR();
				}
				
				var emitterSeal = stamp.getEmitterSeal();
				emitterSeal = emitterSeal.substring(emitterSeal.length() - 8, emitterSeal.length());
				
				val emitterRfc = ret.getEmisor().getRfcE();
				val total = UValue.bigDecimalString(ret.getTotales().getMontoTotRet());
				
				return getQR(stamp.getUuid(), emitterRfc, receiverRfc, total, emitterSeal);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static final String getQR(String uuid, String emitterRfc, String receiverRfc, String total, String emitterSeal) {
		// Forma de conformar el QR
        // Prefijo	Datos																		Caracteres
        // 			https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx
        //	id		UUID del comprobante, precedido por el texto “&id=”								40
        //	re		RFC del Emisor, a 12/13 posiciones, precedido por el texto ”&re=”				16/21
        //	rr		RFC del Receptor, a 12/13 posiciones, precedido por el texto					16/84
        //			“&rr=”, para el comprobante de retenciones se usa el dato que 
        //			esté registrado en el RFC del receptor o el NumRegIdTrib (son excluyentes).
        //	tt		Total del comprobante máximo a 25 posiciones (18 para los enteros,				07/29 
        //			1 para carácter “.”, 6 para los decimales), se deben omitir los ceros 
        //			no significativos, precedido por el texto “&tt=”
        //	fe		Ocho últimos caracteres del sello digital del emisor del comprobante,			12/24 
        //			precedido por el texto “&fe=”
        
        //	Total de caracteres																		198
        //	Ejemplo: https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=5803EB8D-81CD-4557-8719-26632D2FA434&re=XOCD720319T86&rr=CARR861127SB0&tt=0000014300.000000&fe=rH8/bw==
        
		return String.format(
				"https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?&id=%s&re=%s&rr=%s&tt=%s&fe=%s", uuid,
				emitterRfc, receiverRfc, total, emitterSeal);
	}
	
}