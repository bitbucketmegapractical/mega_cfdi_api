package org.megapractical.invoicing.api.xml;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class XmlTransformerCfdiResponse {
	private Comprobante voucher;
	private TimbreFiscalDigital tfd;
	private StampResponse stampResponse;
}