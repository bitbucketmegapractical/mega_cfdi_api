package org.megapractical.invoicing.api.xml;

import java.io.File;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.megapractical.invoicing.api.stamp.converter.StampResponseConverter;
import org.megapractical.invoicing.sat.complement.payroll.Nomina;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class XmlTransformer {

	private XmlTransformer() {
	}

	/**
	 * Transform a XML file to string
	 * 
	 * @param xmlPath Path to xml
	 * @return String
	 */
	public static String xmlToString(String xmlPath) {
    	try {
    		// Loads the XML file from a resource or file path
            val xmlSource = new StreamSource(xmlPath);
            // Creates a StringWriter to store the output
            val stringWriter = new StringWriter();
            // Configures the XML to text transformation
            val transformerFactory = TransformerFactory.newInstance();
            val transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); // Omit XML declaration
            transformer.setOutputProperty(OutputKeys.INDENT, "yes"); // Format the text
            transformer.transform(xmlSource, new StreamResult(stringWriter));
            // Gets the resulting string
            return stringWriter.toString();
		} catch (Exception e) {
			Logger.getLogger(XmlTransformer.class.getName()).log(Level.SEVERE, (String) null, e);
		}
        return null;
    }

	public static XmlTransformerCfdiResponse transformToCfdi(String xmlPath) {
		try {
			// Create a JAXBContext object for Comprobante and Nomina classes
			val jaxbContext = JAXBContext.newInstance(Comprobante.class, TimbreFiscalDigital.class);
			// Convert the XML to a Comprobante object
			val xmlTransformerResponse = trasformToVoucher(jaxbContext, xmlPath);
			if (xmlTransformerResponse != null) {
				return XmlTransformerPayrollResponse
						.builder()
						.voucher(xmlTransformerResponse.getVoucher())
						.tfd(xmlTransformerResponse.getTfd())
						.stampResponse(xmlTransformerResponse.getStampResponse())
						.build();
			}
		} catch (Exception e) {
			Logger.getLogger(XmlTransformer.class.getName()).log(Level.SEVERE, (String) null, e);
		}
		return null;
	}
	
	public static XmlTransformerPayrollResponse transformToPayroll(String xmlPath) {
		try {
			// Create a JAXBContext object for Comprobante and Nomina classes
			val jaxbContext = JAXBContext.newInstance(Comprobante.class, Nomina.class, TimbreFiscalDigital.class);
			// Convert the XML to a Comprobante object
			val xmlTransformerResponse = trasformToVoucher(jaxbContext, xmlPath);
			if (xmlTransformerResponse != null) {
				// Get the Nomina object from the Comprobante object
				Nomina payroll = null;
				for (Object object : xmlTransformerResponse.getVoucher().getComplementos().iterator().next().getAnies()) {
					if (object instanceof Nomina) {
						payroll = (Nomina) object;
					}
				}
				
				return XmlTransformerPayrollResponse
						.builder()
						.voucher(xmlTransformerResponse.getVoucher())
						.tfd(xmlTransformerResponse.getTfd())
						.stampResponse(xmlTransformerResponse.getStampResponse())
						.payroll(payroll)
						.build();
			}
		} catch (JAXBException e) {
			Logger.getLogger(XmlTransformer.class.getName()).log(Level.SEVERE, (String) null, e);
		}
		return null;
	}

	/**
	 * Converts an XML to a Comprobante object
	 * 
	 * @param jaxbContext JAXBContext
	 * @param xmlPath Path of the xml file
	 * @return XmlTransformerCfdiResponse or null
	 */
	private static XmlTransformerCfdiResponse trasformToVoucher(JAXBContext jaxbContext, String xmlPath) {
		try {
			if (jaxbContext != null) {
				// Create an Unmarshaller object to convert the XML to objects
				val unmarshaller = jaxbContext.createUnmarshaller();
				// Load the XML file into a File object
				val xmlFile = new File(xmlPath);
				// Convert the XML to a Comprobante object
				val voucher = (Comprobante) unmarshaller.unmarshal(xmlFile);
				// Get the TimbreFiscalDigital object from the Comprobante object
				val tfd = getTfd(voucher);
				// Populating StampResponse
				val xmlStr = xmlToString(xmlPath);
				val stampResponse = StampResponseConverter.convert(voucher, tfd, xmlStr);
				if (voucher != null && tfd != null && stampResponse != null) {
					return new XmlTransformerCfdiResponse(voucher, tfd, stampResponse);
				}
			}
		} catch (JAXBException e) {
			Logger.getLogger(XmlTransformer.class.getName()).log(Level.SEVERE, (String) null, e);
		}
		return null;
	}

	/**
	 * Get the TimbreFiscalDigital object from the Comprobante object
	 * 
	 * @param voucher Comprobante
	 * @return TimbreFiscalDigital or null
	 */
	private static TimbreFiscalDigital getTfd(Comprobante voucher) {
		if (voucher != null) {
			for (Object object : voucher.getComplementos().iterator().next().getAnies()) {
				if (object instanceof TimbreFiscalDigital) {
					return (TimbreFiscalDigital) object;
				}
			}
		}
		return null;
	}

}