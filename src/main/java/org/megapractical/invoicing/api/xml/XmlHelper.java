package org.megapractical.invoicing.api.xml;

import org.megapractical.invoicing.api.wsdl.ComprobanteRequest;
import org.megapractical.seal.core.XmlSeal;
import org.megapractical.seal.payload.XmlSealRequest;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class XmlHelper {
	
	private XmlHelper() {
	}
	
	public static String sealedXml(ComprobanteRequest voucherRequest) {
		System.out.println("[INFO] XML HELPER > SEALING XML...");
		
		val sealRequest = XmlSealRequest
							.builder()
							.xml(voucherRequest.getComprobanteCFDIin())
							.privateKey(voucherRequest.getKeyByteArr())
							.privateKeyPwd(voucherRequest.getKeyPassword())
							.cfdi(true)
							.build();

		val sealedXml = XmlSeal.seal(sealRequest);
		
		System.out.println("[INFO] XML HELPER > SEALED XML");
		System.out.println(sealedXml);
		
		return sealedXml;
	}
	
	public static String sealedXml(String xml, byte[] privateKey, String keyPwd) {
		System.out.println("[INFO] XML HELPER > SEALING XML...");
		
		val sealRequest = XmlSealRequest
				.builder()
				.xml(xml)
				.privateKey(privateKey)
				.privateKeyPwd(keyPwd)
				.cfdi(true)
				.build();
		
		val sealedXml = XmlSeal.seal(sealRequest);
		
		System.out.println("[INFO] XML HELPER > SEALED XML");
		System.out.println(sealedXml);
		
		return sealedXml;
	}
	
}