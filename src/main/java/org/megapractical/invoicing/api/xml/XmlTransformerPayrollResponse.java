package org.megapractical.invoicing.api.xml;

import org.megapractical.invoicing.sat.complement.payroll.Nomina;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class XmlTransformerPayrollResponse extends XmlTransformerCfdiResponse {
	private Nomina payroll;
}