package org.megapractical.invoicing.api.common;

import java.util.ArrayList;
import java.util.List;

public class ApiPayrollFileSubHeader {

	private List<PayrollSubHeader> payrollSubHeaders;
	
	public static class PayrollSubHeader{
		//##### Nombre del encabezado
		private String fieldName;
		
		//##### Posicion del encabezado
		private Integer fieldPosition;

		/*Getters and Setters*/
		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public Integer getFieldPosition() {
			return fieldPosition;
		}

		public void setFieldPosition(Integer fieldPosition) {
			this.fieldPosition = fieldPosition;
		}
	}

	public List<PayrollSubHeader> getPayrollSubHeaders() {
		if(payrollSubHeaders == null)
			payrollSubHeaders = new ArrayList<>();
		return payrollSubHeaders;
	}

	public void setPayrollSubHeaders(List<PayrollSubHeader> payrollSubHeaders) {
		this.payrollSubHeaders = payrollSubHeaders;
	}	
}