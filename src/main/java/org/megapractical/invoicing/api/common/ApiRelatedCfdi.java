package org.megapractical.invoicing.api.common;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiRelatedCfdi {
	private String relationshipTypeCode;
	private String relationshipTypeValue;
	@Getter(AccessLevel.NONE)
	private List<RelatedCfdiUuid> uuids;
	
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RelatedCfdiUuid {
		private String uuid;
	}
	
	public List<RelatedCfdiUuid> getUuids() {
		if (uuids == null) {
			uuids = new ArrayList<>();
		}
		return uuids;
	}

}