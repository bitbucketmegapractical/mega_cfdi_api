package org.megapractical.invoicing.api.common;

import javax.xml.bind.ValidationException;

/**
 * Created by cool2k on 31/10/14.
 */
public class StampingException extends ValidationException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StampingException(String message, String errorCouldNotStamp, Throwable cause) {
        super(message, errorCouldNotStamp, cause);
    }
}
