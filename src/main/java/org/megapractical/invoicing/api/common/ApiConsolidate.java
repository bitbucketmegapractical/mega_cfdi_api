package org.megapractical.invoicing.api.common;

public class ApiConsolidate {

	//##### Directorio absoluto del archivo
	private String absolutePath;
	
	//##### Directorio donde se encuentras los pdf
	private String sourcePath;
	
	//##### Nombre del archivo
	private String fileName;
	
	//##### Nombre del consolidado
	private String consolidateName;
	
	//##### Directorio del consolidado
	private String consolidatePath;
	
	//##### Directorio del consolidado para db
	private String consolidateDbPath;
	
	//##### Total de pdf seleccionados para generar el consolidado
	private Integer consolidateTotalFile;
	
	//##### Tipo de consolidado
	private String consolidateType;

	/*Getters and Setters*/
	public String getConsolidateName() {
		return consolidateName;
	}

	public void setConsolidateName(String consolidateName) {
		this.consolidateName = consolidateName;
	}

	public String getConsolidatePath() {
		return consolidatePath;
	}

	public void setConsolidatePath(String consolidatePath) {
		this.consolidatePath = consolidatePath;
	}

	public Integer getConsolidateTotalFile() {
		return consolidateTotalFile;
	}

	public void setConsolidateTotalFile(Integer consolidateTotalFile) {
		this.consolidateTotalFile = consolidateTotalFile;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getConsolidateType() {
		return consolidateType;
	}

	public void setConsolidateType(String consolidateType) {
		this.consolidateType = consolidateType;
	}

	public String getConsolidateDbPath() {
		return consolidateDbPath;
	}

	public void setConsolidateDbPath(String consolidateDbPath) {
		this.consolidateDbPath = consolidateDbPath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}	
}