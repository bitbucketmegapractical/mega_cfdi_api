package org.megapractical.invoicing.api.common;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiVoucherTaxes {
	private BigDecimal totalTaxTransferred;
	private BigDecimal totalTaxWithheld;
}