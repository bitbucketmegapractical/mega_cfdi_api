package org.megapractical.invoicing.api.common;

import java.math.BigDecimal;

import org.megapractical.invoicing.voucher.catalogs.CTipoFactor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiConceptTaxes {
	private String base;
	private BigDecimal baseBigDecimal;
	private String tax;
	private String taxPrint;
	private String factor;
	private CTipoFactor factorType;
	private String rateOrFee;
	private BigDecimal rateOrFeeBigDecimal;
	private String amount;
	private BigDecimal amountBigDecimal;
	private String conceptNumber;
}