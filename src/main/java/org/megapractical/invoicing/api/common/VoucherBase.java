/*
*  Copyright 2013 MegaPractical
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.megapractical.invoicing.api.common;

import java.util.List;

import org.w3c.dom.Element;

public interface VoucherBase {

    boolean hasComplemento();

    public boolean hasComplementoConcepto();

    List<Object> getComplementoGetAny();

    public List<Object> getComplementoConceptoGetAny();

    String getSello();

    void setComplemento(Element e);

    Object getComprobante();

    public String getRFC();

    public String getFecha();
}
