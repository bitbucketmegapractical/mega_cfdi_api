package org.megapractical.invoicing.api.common;

import java.util.ArrayList;
import java.util.List;

public class ApiPayrollFileHeader {

	private List<PayrollHeader> payrollHeaders;
	
	public static class PayrollHeader{
		//##### Trama
		private String entry;
		
		//##### Posicion inicial del contenido en la linea
		private Integer startPosition;
		
		//##### Posicion final del contenido en la linea
		private Integer endPosition;

		/*Getters and Setters*/
		public String getEntry() {
			return entry;
		}

		public void setEntry(String entry) {
			this.entry = entry;
		}

		public Integer getStartPosition() {
			return startPosition;
		}

		public void setStartPosition(Integer startPosition) {
			this.startPosition = startPosition;
		}

		public Integer getEndPosition() {
			return endPosition;
		}

		public void setEndPosition(Integer endPosition) {
			this.endPosition = endPosition;
		}
	}

	public List<PayrollHeader> getPayrollHeaders() {
		if(payrollHeaders == null)
			payrollHeaders = new ArrayList<>();
		return payrollHeaders;
	}

	public void setPayrollHeaders(List<PayrollHeader> payrollHeaders) {
		this.payrollHeaders = payrollHeaders;
	}	
}