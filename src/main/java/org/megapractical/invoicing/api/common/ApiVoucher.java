package org.megapractical.invoicing.api.common;

import java.math.BigDecimal;

import org.megapractical.invoicing.voucher.catalogs.CFormaPago;
import org.megapractical.invoicing.voucher.catalogs.CMetodoPago;
import org.megapractical.invoicing.voucher.catalogs.CMoneda;
import org.megapractical.invoicing.voucher.catalogs.CTipoDeComprobante;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiVoucher {
	// @Required
	private String version;
	// @Required
	private String dateTimeExpedition;
	// @Required
	private String dateTimeStamp;
	// @Required
	private String emitterSeal;
	// @Required
	private String satSeal;
	// @Required
	private String originalString;
	// @Required
	private String taxSheet;
	// @Required
	private String certificateEmitterNumber;
	// @Required
	private String certificateSatNumber;
	// @Required
	private BigDecimal subTotal;
	private String subTotalString;
	// @Required
	private CMoneda currency;
	private String currencyString;
	// @Required
	private BigDecimal total;
	private String totalString;
	// @Required
	private CTipoDeComprobante voucherType;
	private String voucherTypeString;
	// @Required
	private String postalCode;
	// @Nullable
	private String serie;
	private boolean indicateSerie;
	// @Nullable
	private String folio;
	private boolean indicateSheet;
	// @Nullable
	private CFormaPago paymentWay;
	private String paymentWayString;
	// @Nullable
	private String paymentConditions;
	// @Nullable
	private BigDecimal discount;
	private String discountString;
	// @Nullable
	private BigDecimal changeType;
	private String changeTypeString;
	// @Nullable
	private CMetodoPago paymentMethod;
	private String paymentMethodString;
	// @Nullable
	private String totalInLetter;
	// @Nullable
	private String confirmation;
	// @Nullable
	private ApiVoucherTaxes taxes;
	// @Nullable
	private ApiRelatedCfdi relatedCfdi;
	// @Required
	private String exportation;
	private String exportationString;
	// @Nullable
	private String addenda;
}