package org.megapractical.invoicing.api.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.megapractical.invoicing.api.stub.KeyLoader;
import org.megapractical.invoicing.sat.complement.dividendos.Dividendos;
import org.megapractical.invoicing.sat.complement.enajenaciondeacciones.EnajenaciondeAcciones;
import org.megapractical.invoicing.sat.complement.intereses.Intereses;
import org.megapractical.invoicing.sat.complement.operacionesconderivados.OperacionesconDerivados;
import org.megapractical.invoicing.sat.complement.pagosaextranjeros.PagosaExtranjeros;
import org.megapractical.invoicing.sat.complement.sectorfinanciero.SectorFinanciero;
import org.megapractical.invoicing.sat.integration.retention.v2.RETv20;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;

import lombok.val;

public final class RetentionXmlService {
	
	private RetentionXmlService() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
    /**
     * Generate XML from Retenciones object (Overload).
     *
     * @param retencion
     * @param emisorCertificatePath
     * @param emisorPrivatyKeyPath
     * @param emisorPrivatyKeyPassword
     * @return
     * @throws Exception
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public static byte[] generateXML(Retenciones retencion, String emisorCertificatePath, String emisorPrivatyKeyPath, String emisorPrivatyKeyPassword) {
    	try {
			
    		// Getting file from path.
            File certFile = new File(emisorCertificatePath);
            File keyFile = new File(emisorPrivatyKeyPath);

            // Getting byte arrays from file.
            byte[] keyBytes = new byte[(int) keyFile.length()];
            FileInputStream fis = new FileInputStream(emisorPrivatyKeyPath);
            fis.read(keyBytes);
            fis.close();

            byte[] certBytes = new byte[(int) certFile.length()];
            fis = new FileInputStream(emisorCertificatePath);
            fis.read(certBytes);
            fis.close();

            // Getting XML.
            return generateXML(retencion, certBytes, keyBytes, emisorPrivatyKeyPassword);
    		
		} catch (IOException e) {
			System.out.println(String.format("#generateXML error - %s", e.getMessage()));
		}
        return null;
    }
    
    /**
     * Generate XML from Retenciones object (Overload).
     * @param comprobante
     * @param emisorCertificate
     * @param emisorPrivatyKey
     * @param emisorPrivatyKeyPassword
     * @return
     * @throws Exception
     * @throws GeneralSecurityException
     * @throws IOException 
     */
    public static byte[] generateXML(Retenciones retencion, byte[] emisorCertificate, byte[] emisorPrivatyKey, String emisorPrivatyKeyPassword) {
        // Getting certificate in Base64
        String encodeEmisorCertificate = encodeBase64(emisorCertificate);

        // Generating XML...
        return generateXML(encodeEmisorCertificate, emisorPrivatyKey, emisorPrivatyKeyPassword, retencion);
    }
    
    public static byte[] generateXML(Retenciones retencion, byte[] emisorCertificate) {
    	try {
			
	        // Retenciones context list.
	        val context = new ArrayList<String>();
	
	        // Name spaces list.
	        Map<String, String> mapNameSpaces = new HashMap<>();
	
	        // Patch: Adding Complement NameSpaces.
	        if (retencion.getComplemento() != null && retencion.getComplemento().getAny() != null && retencion.getComplemento().getAny().size() > 0) {
	            for (Object object : retencion.getComplemento().getAny()) {
	                if (object instanceof Dividendos) {
	                    mapNameSpaces.put("dividendos", "http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos");
	                    context.add("org.megapractical.invoicing.sat.complement.dividendos");
	                } else if (object instanceof EnajenaciondeAcciones) {
	                	mapNameSpaces.put("enajenaciondeacciones", "http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones");
	                    context.add("org.megapractical.invoicing.sat.complement.enajenaciondeacciones");
	                } else if (object instanceof Intereses) {
	                    mapNameSpaces.put("intereses", "http://www.sat.gob.mx/esquemas/retencionpago/1/intereses");
	                    context.add("org.megapractical.invoicing.sat.complement.intereses");
	                } else if (object instanceof OperacionesconDerivados) {
	                    mapNameSpaces.put("operacionesconderivados", "http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados");
	                    context.add("org.megapractical.invoicing.sat.complement.operacionesconderivados");
	                } else if (object instanceof PagosaExtranjeros) {
	                    mapNameSpaces.put("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
	                    context.add("org.megapractical.invoicing.sat.complement.pagosaextranjeros");
	                } else if (object instanceof SectorFinanciero) {
	                    mapNameSpaces.put("sectorfinanciero", "http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero");
	                    context.add("org.megapractical.invoicing.sat.complement.sectorfinanciero");
	                }
	            }
	        }
	
	        // Getting posible context.
	        String[] arrContextos = context.isEmpty() ? new String[0] : context.toArray(new String[context.size()]);
	
	        // Getting certificate in Base64
	        String encodeEmisorCertificate = encodeBase64(emisorCertificate);
	        // Getting certificate in X509 format.
	        encodeEmisorCertificate = "-----BEGIN CERTIFICATE-----\n" + encodeEmisorCertificate + "\n-----END CERTIFICATE-----";
	        InputStream is = new ByteArrayInputStream(encodeEmisorCertificate.getBytes("UTF-8"));
	        CertificateFactory factory = CertificateFactory.getInstance("X.509");
	        X509Certificate certificateX509 = (X509Certificate) factory.generateCertificate(is);
	        
	        // Conforming RETv20 object.
	        RETv20 retObj = new RETv20(retencion, arrContextos);
	
	        // Add namespaces to objet RETv20.
	        if (!mapNameSpaces.isEmpty()) {
	            Set<String> test1 = mapNameSpaces.keySet();
	            Iterator<String> iterator = test1.iterator();
	            while (iterator.hasNext()) {
	                String item = iterator.next();
	                retObj.addNamespace(mapNameSpaces.get(item), item);
	            }
	        }
	        
	        certificateX509.checkValidity();
	        byte[] bytes = certificateX509.getEncoded();

	        Base64 b64 = new Base64(-1);
	        String certStr = b64.encodeToString(bytes);
	        retObj.getRetenciones().setCertificado(certStr);
	        BigInteger bi = certificateX509.getSerialNumber();
	        retObj.getRetenciones().setNoCertificado(new String(bi.toByteArray()));
	
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        retObj.guardar(baos);
	
	        return baos.toByteArray();
        
    	} catch (Exception e) {
    		System.out.println(String.format("#generateXML error - %s", e.getMessage()));
		}
    	return null;
    }

    /**
     * Generate XML from Retenciones object.
     *
     * @param encodeEmisorCertificate
     * @param emisorPrivatyKey
     * @param emisorPrivatyKeyPassword
     * @param comprobante
     * @return
     * @throws Exception
     * @throws IOException
     */
    private static byte[] generateXML(String encodeEmisorCertificate, byte[] emisorPrivatyKey, String emisorPrivatyKeyPassword, Retenciones retencion) {
        try {
			
        	// Retenciones context list.
            val context = new ArrayList<String>();

            // Name spaces list.
            Map<String, String> mapNameSpaces = new HashMap<>();

            // Getting certificate in X509 format.
            encodeEmisorCertificate = "-----BEGIN CERTIFICATE-----\n" + encodeEmisorCertificate + "\n-----END CERTIFICATE-----";
            InputStream is = new ByteArrayInputStream(encodeEmisorCertificate.getBytes(StandardCharsets.UTF_8));
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            X509Certificate certificateX509 = (X509Certificate) factory.generateCertificate(is);

            // Patch: Adding Complement NameSpaces.
            if (retencion.getComplemento() != null && retencion.getComplemento().getAny() != null && !retencion.getComplemento().getAny().isEmpty()) {
                for (Object object : retencion.getComplemento().getAny()) {
                    if (object instanceof Dividendos) {
                        mapNameSpaces.put("dividendos", "http://www.sat.gob.mx/esquemas/retencionpago/1/dividendos");
                        context.add("org.megapractical.invoicing.sat.complement.dividendos");
                    } else if (object instanceof EnajenaciondeAcciones) {
                    	mapNameSpaces.put("enajenaciondeacciones", "http://www.sat.gob.mx/esquemas/retencionpago/1/enajenaciondeacciones");
                        context.add("org.megapractical.invoicing.sat.complement.enajenaciondeacciones");
                    } else if (object instanceof Intereses) {
                        mapNameSpaces.put("intereses", "http://www.sat.gob.mx/esquemas/retencionpago/1/intereses");
                        context.add("org.megapractical.invoicing.sat.complement.intereses");
                    } else if (object instanceof OperacionesconDerivados) {
                        mapNameSpaces.put("operacionesconderivados", "http://www.sat.gob.mx/esquemas/retencionpago/1/operacionesconderivados");
                        context.add("org.megapractical.invoicing.sat.complement.operacionesconderivados");
                    } else if (object instanceof PagosaExtranjeros) {
                        mapNameSpaces.put("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
                        context.add("org.megapractical.invoicing.sat.complement.pagosaextranjeros");
                    } else if (object instanceof SectorFinanciero) {
                        mapNameSpaces.put("sectorfinanciero", "http://www.sat.gob.mx/esquemas/retencionpago/1/sectorfinanciero");
                        context.add("org.megapractical.invoicing.sat.complement.sectorfinanciero");
                    }
                }
            }

            // Getting posible context.
            String[] arrContextos = context.isEmpty() ? new String[0] : context.toArray(new String[context.size()]);

            // Conforming RETv20 object.
            RETv20 retObj = new RETv20(retencion, arrContextos); 

            // Add namespaces to object RETv20.
            if (!mapNameSpaces.isEmpty()) {
                Set<String> test1 = mapNameSpaces.keySet();
                Iterator<String> iterator = test1.iterator();
                while (iterator.hasNext()) {
                    String item = iterator.next();
                    retObj.addNamespace(mapNameSpaces.get(item), item);
                }
            }

            // Convert byte array private key to object PrivateKey.
            PrivateKey key = KeyLoader.loadPKCS8PrivateKey(new ByteArrayInputStream(emisorPrivatyKey), emisorPrivatyKeyPassword);

            // Seal retencion.
            retObj.sellarRetencion(key, certificateX509);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            retObj.guardar(baos);

            // Store in file xml content (is optional, only for develop test)
            // retv1.guardar(new FileOutputStream("U:\\documento_para_ver_lo_que_tiene.xml"));
            // Getting XML.
            // String stringXML = baos.toString();
            // Return XML in byte array.
            return baos.toByteArray();
        	
		} catch (Exception e) {
			System.out.println(String.format("#generateXML error - %s", e.getMessage()));
		}
        return null;
    }
    
    /**
     * Encoding file(Certificate) in Base64 - Overload.
     *
     * @param value
     * @return
     */
    private static String encodeBase64(byte[] value) {
        String digitalSign = null;
        byte[] bytes = Base64.encodeBase64(value);

        digitalSign = new String(bytes);
        digitalSign = digitalSign.replace("\r\n", "");
        digitalSign = digitalSign.replace("\n", "");
        digitalSign = digitalSign.replace("\r", "");

        return digitalSign;
    }

}