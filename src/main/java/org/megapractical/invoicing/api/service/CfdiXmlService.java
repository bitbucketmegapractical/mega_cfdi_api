package org.megapractical.invoicing.api.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;

import org.megapractical.invoicing.api.stub.KeyLoader;
import org.megapractical.invoicing.api.util.PrintUtils;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.sat.integration.v40.CFDv4;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;

import lombok.val;
import lombok.var;

public final class CfdiXmlService {
	
	private CfdiXmlService() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	/**
     * Generate XML from Comprobante object.
     *
     * @param voucher (v4.0)     
     * @return
     * @throws Exception
     * @throws IOException
     */
	public static byte[] generateXml(Comprobante voucher) {
		PrintUtils.info("[INFO] MEGA-CFDI API > XML SERVICE INSTANCE > GENERATING XML...");
    	try {
			
    		// Comprobante context list.
    		val context = new ArrayList<String>();

            // Namespaces list.
            val mapNameSpaces = new HashMap<String, String>();
            
			// Patch: Adding complementos NameSpaces.
			if (voucher.getComplementos() != null && !voucher.getComplementos().isEmpty()) {
				for (Object object : voucher.getComplementos().iterator().next().getAnies()) {
					// Complemento: Recepcion de pagos v1.0
					if (object instanceof org.megapractical.invoicing.sat.complement.payments.Pagos) {
						mapNameSpaces.put("http://www.sat.gob.mx/Pagos", "pago10");
						context.add("org.megapractical.invoicing.sat.complement.payments");
					}
					
					// Complemento: Nomina v1.2
					if (object instanceof org.megapractical.invoicing.sat.complement.payroll.Nomina) {
						mapNameSpaces.put("nomina12", "http://www.sat.gob.mx/nomina12");
						context.add("org.megapractical.invoicing.sat.complement.payroll");
					}
				}
			}

            // Getting posible context.
			String[] arrContextos = context.isEmpty() ? new String[0] : context.toArray(new String[context.size()]);

            PrintUtils.info("[INFO] MEGA-CFDI API > XML SERVICE INSTANCE > CONFORMING CFDI V4.0...");
            
            // Conforming CFDv4 object.
            val cfdiv4 = new CFDv4(voucher, arrContextos);

            // Add namespaces to objet CFDv4.
            if (!mapNameSpaces.isEmpty()) {
                val keySet = mapNameSpaces.keySet();
                val iterator = keySet.iterator();
                while (iterator.hasNext()) {
                    val item = iterator.next();
                    cfdiv4.addNamespace(mapNameSpaces.get(item), item);
                }
            }

            val baos = new ByteArrayOutputStream();
            cfdiv4.guardar(baos);
            
            // Return XML in byte array.
            return baos.toByteArray();
    		
		} catch (Exception e) {
			System.out.println(String.format("#generateXML error - %s", e.getMessage()));
		}
        return null;
    }

    /**
     * Generate XML from Comprobante object.
     *
     * @param voucher (v4.0)
     * @param certificate
     * @param privatyKey
     * @param passwd
     * @return
     * @throws Exception
     * @throws IOException
     */
    public static byte[] generateXml(Comprobante voucher, byte[] certificate, byte[] privateKey, String passwd) {
    	PrintUtils.info("[INFO] MEGA-CFDI API > XML SERVICE INSTANCE > GENERATING XML...");
    	try {
			
    		// Comprobante context list.
    		val context = new ArrayList<String>();

            // Name spaces list.
            val mapNameSpaces = new HashMap<String, String>();
            
            // Getting certificate in Base64
            var certificateEncode = UBase64.base64Encode(certificate);

            // Getting certificate in X509 format.
            certificateEncode = "-----BEGIN CERTIFICATE-----\n" + certificateEncode + "\n-----END CERTIFICATE-----";
            
            val is = new ByteArrayInputStream(certificateEncode.getBytes(StandardCharsets.UTF_8));
            
            val factory = CertificateFactory.getInstance("X.509");
            val certificateX509 = (X509Certificate) factory.generateCertificate(is);

            // Patch: Adding complementos NameSpaces.
            if (voucher.getComplementos() != null && !voucher.getComplementos().isEmpty()) {
                for (Object object : voucher.getComplementos().iterator().next().getAnies()) {
                    // Complemento: Recepcion de pagos v1.0
                	if (object instanceof org.megapractical.invoicing.sat.complement.payments.Pagos) {
                    	mapNameSpaces.put("http://www.sat.gob.mx/Pagos", "pago10");
                        context.add("org.megapractical.invoicing.sat.complement.payments");
                    }
                	
                	// Complemento: Nomina v1.2
                	if (object instanceof org.megapractical.invoicing.sat.complement.payroll.Nomina) {
                		 mapNameSpaces.put("nomina12", "http://www.sat.gob.mx/nomina12");
                         context.add("org.megapractical.invoicing.sat.complement.payroll");
                    }
                }
            }

            // Getting posible context.
            String[] arrContextos = context.isEmpty() ? new String[0] : context.toArray(new String[context.size()]);

            PrintUtils.info("[INFO] MEGA-CFDI API > XML SERVICE INSTANCE > CONFORMING CFDI V4.0...");
            
            // Conforming CFDv4 object.
            val cfdiv4 = new CFDv4(voucher, arrContextos);

            // Add namespaces to objet CFDv33.
            if (!mapNameSpaces.isEmpty()) {
                val keySet = mapNameSpaces.keySet();
                val iterator = keySet.iterator();
                while (iterator.hasNext()) {
                    val item = iterator.next();
                    cfdiv4.addNamespace(mapNameSpaces.get(item), item);
                }
            }

            // Convert byte array private key to object PrivateKey.
            val key = KeyLoader.loadPKCS8PrivateKey(new ByteArrayInputStream(privateKey), passwd);

            PrintUtils.info("[INFO] MEGA-CFDI API > XML SERVICE INSTANCE > SEALING CFDI-V4.0...");
            
            // Voucher seal
            cfdiv4.sellarComprobante(key, certificateX509);
            val baos = new ByteArrayOutputStream();
            cfdiv4.guardar(baos);
            
            // Return XML in byte array.
            return baos.toByteArray();
    		
		} catch (Exception e) {
			System.out.println(String.format("#generateXML error - %s", e.getMessage()));
		}
        return null;
    }

}