/*
*  Copyright 2013 MegaPractical
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.megapractical.invoicing.api.stub;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public final class DateTimeCustomBinder {

    public static Date parseDateTime(String s) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            return formatter.parse(s);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String printDateTime(Date dt) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return (dt == null) ? null : formatter.format(dt);
    }

    public static Date parseDate(String s) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(s);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String printDate(Date dt) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return (dt == null) ? null : formatter.format(dt);
    }

    public static XMLGregorianCalendar parseGregorianCalendar(String s) {
        try {
            XMLGregorianCalendar xmlCalender;
            GregorianCalendar calender = new GregorianCalendar();
            calender.setTime(parseDate(s));
            xmlCalender = DatatypeFactory.newInstance().newXMLGregorianCalendar(calender);
            return xmlCalender;
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(DateTimeCustomBinder.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static String printGregorianCalendar(XMLGregorianCalendar dt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar gc = dt.toGregorianCalendar();
        return sdf.format(gc.getTime());

    }
}
