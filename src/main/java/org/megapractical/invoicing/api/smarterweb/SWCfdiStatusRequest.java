package org.megapractical.invoicing.api.smarterweb;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class SWCfdiStatusRequest {
	/**
	 * Ambiente (dev|qa|prod)
	 */
	private String environment;
	
	/**
	 * Rfc del emisor
	 */
	private String emitterRfc;
	
	/**
	 * Rfc del receptor
	 */
	private String receiverRfc;
	
	/**
	 * Total del comprobante
	 */
	private String voucherTotal;
	
	/**
	 * Uuid
	 */
	private String uuid;
	
}