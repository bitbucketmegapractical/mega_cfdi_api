package org.megapractical.invoicing.api.smarterweb;

import org.megapractical.invoicing.api.service.RetentionXmlService;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.PrintUtils;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wsdl.retention.schema.RetencionesRequest;

import lombok.val;
import lombok.var;

/**
 * Retention stamp with SW
 * 
 * @author Maikel Guerra Ferrer
 */
public final class SWRetentionStamp {
	
	private SWRetentionStamp() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	private static final Boolean APP_PRINT_VALUES = UProperties.getApplicationPrintValues();

    public static StampResponse retentionStamp(StampProperties stampProperties) {
        // Stamp responce object.
        var stampResponse = new StampResponse();

        // Retencion
        val retention = stampProperties.getRetention();
        
        // Certificado
        val certificate = stampProperties.getCertificate();
        
        // Llave privada
        val privateKey = stampProperties.getPrivateKey();
        
        // Clave de la llave privada
        val passwd = stampProperties.getPasswd();
        
        // Ambiente (PROD|DEV|QA)
        val environment = stampProperties.getEnvironment();

		try {
			// Getting Xml in byte array format to stmap
            var xml = RetentionXmlService.generateXML(retention, certificate, privateKey, passwd);
			if (xml != null) {
				val plainXml = UValue.byteToString(xml);
            	PrintUtils.info("MEGA-CFDI API - SW > [INFO] RETENTION STAMP INSTANCE > PREPARING TO STAMP XML...");
            	
            	// Getting stamp object request.
            	val retencionRequest = new RetencionesRequest();
            	retencionRequest.setKeyByteArr(privateKey);
            	retencionRequest.setCerByteArr(certificate);
            	retencionRequest.setCertByte(UCertificateX509.certificateBase64(certificate));
            	retencionRequest.setKeyByte(UCertificateX509.fileBase64(privateKey));
            	retencionRequest.setKeyPassword(passwd);
            	retencionRequest.setRetencionesCFDIin(plainXml);
            	
            	// Stamp request.
            	stampResponse = SWRetentionClient.retentionStamp(retencionRequest, environment);
            	
            	// Xml stamped in byte array format
            	val xmlStamped = stampResponse.getXml();
            	if (xmlStamped != null) {
            		if (Boolean.TRUE.equals(APP_PRINT_VALUES)) {
            			PrintUtils.print();
            			PrintUtils.info("[INFO] MEGA-CFDI API - SW > CFDI STAMP OPERATION INSTANCE > WSDL RESPONSE");
            			PrintUtils.print();
            			PrintUtils.print(UValue.byteToString(xmlStamped));
            		}
            		stampResponse.setRetention(retention);
            		stampResponse.setDateExpedition(retention.getFechaExp());
            	}
            }
        } catch (Exception ex) {
            System.out.println(String.format("#retentionStamp error - %s", ex.getMessage()));
            StampResponse.error("ERR0039");
        }
        return stampResponse;
    }
    
}