package org.megapractical.invoicing.api.smarterweb;

import java.time.LocalDate;
import java.time.LocalTime;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SWCancellationResponse {
	private String emitterRfc;
	private String receiverRfc;
	private String voucherTotal;
	private String reason;
	private String substitutionUuid;
	private String uuid;
	private String isCancelable;
	private String status;
	private String statusCode;
	private String responseUuidStatusCode;
	private String responseCancelableStatus;
	private String responseStatus;
	private String acuseXml;
	@Default
	private boolean success = Boolean.FALSE;
	private String errorMessage;
	private LocalDate cancellationDate;
	private LocalTime cancellationTime;
}