package org.megapractical.invoicing.api.smarterweb;

import Utils.Responses.Stamp.SuccessV4Response;

public class SWStampResponse extends SuccessV4Response {

	public SWStampResponse(SuccessV4Response response) {
		super(response.HttpStatusCode, response.Status, response.cfdi, response.cadenaOriginalSAT, response.noCertificadoSAT, 
				response.noCertificadoCFDI, response.uuid, response.selloSAT, response.selloCFDI, response.fechaTimbrado, 
				response.qrCode, response.message, response.messageDetail);
	}
	
	public SWStampResponse(String message) {
		super(-1, null, null, null, null, null, null, null, null, null, null, message, message);
	}
	
	@Override
	public String toString() {
		return "SWStampResponse [cadenaOriginalSAT=" + cadenaOriginalSAT + ", noCertificadoSAT=" + noCertificadoSAT
				+ ", noCertificadoCFDI=" + noCertificadoCFDI + ", uuid=" + uuid + ", selloSAT=" + selloSAT
				+ ", selloCFDI=" + selloCFDI + ", fechaTimbrado=" + fechaTimbrado + ", qrCode=" + qrCode + ", cfdi="
				+ cfdi + ", HttpStatusCode=" + HttpStatusCode + ", Status=" + Status + ", message=" + message
				+ ", messageDetail=" + messageDetail + "]";
	}

}