package org.megapractical.invoicing.api.smarterweb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.megapractical.invoicing.api.util.UXml;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class SWCfdiStatus {

	private SWCfdiStatus() {
	}
	
	public static SWCancellationResponse checkStatus(SWCfdiStatusRequest cfdiStatusRequest) {
		try {
			val xmlResponse = cfdiStatus(cfdiStatusRequest);
			return SWCancellationResponse
					.builder()
					.isCancelable(UXml.getAttributeNodeValue(xmlResponse, "a:EsCancelable")) 
					.status(UXml.getAttributeNodeValue(xmlResponse, "a:Estado"))
					.statusCode(UXml.getAttributeNodeValue(xmlResponse, "a:CodigoEstatus"))
					.build();
		} catch (Exception e) {
			System.out.println(String.format("#checkStatus error - %s", e.getMessage()));
		}		
		return null;
	}
	
	private static String cfdiStatus(SWCfdiStatusRequest properties) {
		try {
			// To prevent the error: javax.net.ssl.SSLException: java.lang.RuntimeException: Could not generate DH keypair
        	Security.addProvider(new BouncyCastleProvider());
        	
        	//##### Environment
        	String environment = properties.getEnvironment();
        	
        	//##### URL
        	String serviceUrl = null;
        	if(environment.equalsIgnoreCase("prod")) {
	    		//##### PROD
        		serviceUrl = SWUtil.getProperty("sw.prod.cancellation.status.check");
	        } else {
	        	//##### QA
	        	serviceUrl = SWUtil.getProperty("sw.test.cancellation.status.check");
	        }
    		
			String qr = "?re=" + properties.getEmitterRfc() + "&rr=" + properties.getReceiverRfc() + "&tt="
					+ properties.getVoucherTotal() + "&id=" + properties.getUuid();
    		
    		String envelope = "<?xml version='1.0' encoding='UTF-8'?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +							  
							  "<soapenv:Body><Consulta xmlns=\"http://tempuri.org/\">"+
							  "<expresionImpresa><![CDATA["+qr+"]]></expresionImpresa>"+
							  "</Consulta></soapenv:Body></soapenv:Envelope>";
    		
    		return cfdiStatus(envelope, serviceUrl);
		} catch (Exception e) {
			System.out.println(String.format("#cfdiStatus error - %s", e.getMessage()));
		}
		return null;
	}
	
	private static String cfdiStatus(String envelope, String serviceUrl) throws Exception {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(X509Certificate[] certs, String authType) {}
					public void checkServerTrusted(X509Certificate[] certs, String authType) {}
				}
	    	};
			
			SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	 
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	        
	        
	        URL url = new URL(serviceUrl);
	        URLConnection conn = url.openConnection();
	        conn.setConnectTimeout(50000);
	        conn.setReadTimeout(50000);
	        conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
	        conn.setRequestProperty("SOAPAction", "\"http://tempuri.org/IConsultaCFDIService/Consulta\"");
	         
	        //##### Write SOAP
	        conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            os.write(envelope.getBytes(StandardCharsets.UTF_8));
            os.close();

            //##### Answer
            InputStream in = conn.getInputStream();
            InputStreamReader is = new InputStreamReader(in);
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(is);
            String read = br.readLine();

            while(read != null) {
                sb.append(read);
                read = br.readLine();
            }
        
            os.flush();
            os = null;
            conn= null;
                
            return sb.toString();
			
		} catch (MalformedURLException ex3) {
            Logger.getLogger(SWCfdiStatus.class.getName()).log(Level.SEVERE, null, ex3);
        } catch (IOException ex2) {
        	Logger.getLogger(SWCfdiStatus.class.getName()).log(Level.SEVERE, null, ex2);
        } catch (Exception ex) {
           Logger.getLogger(SWCfdiStatus.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
    }
}
