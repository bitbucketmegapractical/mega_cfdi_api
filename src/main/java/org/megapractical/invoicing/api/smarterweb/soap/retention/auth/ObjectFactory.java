
package org.megapractical.invoicing.api.smarterweb.soap.retention.auth;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.megapractical.invoicing.api.wsdl.ws.authentication package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.megapractical.invoicing.api.wsdl.ws.authentication
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AutenticarBasico }
     * 
     */
    public AutenticarBasico createAutenticarBasico() {
        return new AutenticarBasico();
    }

    /**
     * Create an instance of {@link AutenticarBasicoResponse }
     * 
     */
    public AutenticarBasicoResponse createAutenticarBasicoResponse() {
        return new AutenticarBasicoResponse();
    }

    /**
     * Create an instance of {@link AutenticarBasicoBatchProcess }
     * 
     */
    public AutenticarBasicoBatchProcess createAutenticarBasicoBatchProcess() {
        return new AutenticarBasicoBatchProcess();
    }

    /**
     * Create an instance of {@link AutenticarBasicoBatchProcessResponse }
     * 
     */
    public AutenticarBasicoBatchProcessResponse createAutenticarBasicoBatchProcessResponse() {
        return new AutenticarBasicoBatchProcessResponse();
    }

    /**
     * Create an instance of {@link TokenCambioContraseña }
     * 
     */
    public TokenCambioContraseña createTokenCambioContraseña() {
        return new TokenCambioContraseña();
    }

    /**
     * Create an instance of {@link TokenCambioContraseñaResponse }
     * 
     */
    public TokenCambioContraseñaResponse createTokenCambioContraseñaResponse() {
        return new TokenCambioContraseñaResponse();
    }

    /**
     * Create an instance of {@link CambioContraseña }
     * 
     */
    public CambioContraseña createCambioContraseña() {
        return new CambioContraseña();
    }

    /**
     * Create an instance of {@link CambioContraseñaResponse }
     * 
     */
    public CambioContraseñaResponse createCambioContraseñaResponse() {
        return new CambioContraseñaResponse();
    }

}
