package org.megapractical.invoicing.api.smarterweb;

import java.io.IOException;

import org.megapractical.invoicing.api.service.CfdiXmlService;
import org.megapractical.invoicing.api.stamp.converter.StampResponseConverter;
import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.api.wsdl.ComprobanteRequest;
import org.megapractical.invoicing.api.xml.XmlHelper;

import Exceptions.AuthException;
import Exceptions.GeneralException;
import Utils.Responses.Stamp.SuccessV4Response;
import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 * 
 * CFDI/NOMINA (REST)
 * 
 */
public final class SWStampClient {
	
	private SWStampClient() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	private static final Boolean APP_PRINT_VALUES = UProperties.getApplicationPrintValues();
	private static final String SW_EXTERNAL_ERROR = "SW-ERR001";
	
	public static StampResponse stampRequest(StampProperties stampProperties) {
		StampResponse stampResponse = null;
		
		val xml = CfdiXmlService.generateXml(stampProperties.getVoucher());
		if (Boolean.TRUE.equals(APP_PRINT_VALUES)) {
			System.out.println("[INFO] SW STAMP CLIENT > XML TO STAMP");
			System.out.println(UValue.byteToString(xml));
		}
		
		val voucherRequest = voucherRequest(stampProperties, xml);
		val sealedXml = XmlHelper.sealedXml(voucherRequest);
		val environment = stampProperties.getEnvironment();
		
		val swResponse = swStamp(sealedXml, environment);
		System.out.println(String.format("[INFO] SW STAMP CLIENT > SW RESPONSE > HttpStatusCode: %s", swResponse.HttpStatusCode));
		if (swResponse.HttpStatusCode == 200) {
			System.out.println("[INFO] SW STAMP CLIENT > STAMPED XML");
			System.out.println(swResponse.cfdi);
			
			stampResponse = StampResponseConverter.convert(swResponse);
			stampResponse.setXmlBeforeStamp(xml);
		} else {
			stampResponse = StampResponse.error(swResponse.message, swResponse.messageDetail, sealedXml);
			System.out.println("[ERROR] SW STAMP CLIENT > STAMPED ERROR");
			System.out.println("[ERROR] SW STAMP CLIENT > ERROR MESSAGE : " + swResponse.message);
			System.out.println("[ERROR] SW STAMP CLIENT > ERROR DETAIL : " + swResponse.messageDetail);
		}
		
		return stampResponse;
	}
	
	public static ComprobanteRequest voucherRequest(StampProperties stampProperties, byte[] xml) {
		val voucherRequest = new ComprobanteRequest();
		voucherRequest.setKeyByteArr(stampProperties.getPrivateKey());
		voucherRequest.setCerByteArr(stampProperties.getCertificate());
		voucherRequest.setCertByte(UCertificateX509.certificateBase64(stampProperties.getCertificate()));
		voucherRequest.setKeyByte(UCertificateX509.fileBase64(stampProperties.getPrivateKey()));
		voucherRequest.setKeyPassword(stampProperties.getPasswd());
		voucherRequest.setComprobanteCFDIin(UValue.unescape(UValue.byteToString(xml)));
		
		return voucherRequest;
	}
	
	private static SWStampResponse swStamp(String xml, String environment) {
	    System.out.println("SW API > STAMPING XML...");
    	try {
    		val swApi = SWUtil.swStampApi(environment);
    		if (swApi != null) {
    			// Se asigna el resultado de la respuesta a dicho objeto
	    	    // Se ejecuta el metodo "Stamp", que timbrara el comprobante posteriormente sellado, asi como la versión del servicio de timbrado
	    		val response = (SuccessV4Response) swApi.Stamp(xml, "v4");
	    		if (response != null) {
	    			System.out.println("===== SW API > Stamp Response =====");
	    			System.out.println(String.format("HttpStatusCode: %s", response.HttpStatusCode));
	    			System.out.println(String.format("Status: %s", response.Status));
	    			System.out.println(String.format("Message: %s", response.message));
		    	    System.out.println(String.format("QrCode: %s", response.qrCode));
		    	    System.out.println(String.format("CadenaOriginalSAT: %s", response.cadenaOriginalSAT));
		    	    System.out.println(String.format("SelloCFDI: %s", response.selloCFDI));
		    	    System.out.println(String.format("SelloSAT: %s", response.selloSAT));
		    	    System.out.println(String.format("NoCertificadoCFDI: %s", response.noCertificadoCFDI));
		    	    System.out.println(String.format("NoCertificadoSAT: %s", response.noCertificadoSAT));
		    	    System.out.println(String.format("FechaTimbrado: %s", response.fechaTimbrado));
		    	    System.out.println(String.format("UUID: %s", response.uuid));
		    	    System.out.println(String.format("CFDI: %s", response.cfdi));
		    	    
	    			return new SWStampResponse(response);
	    		}
    		}
		} catch (AuthException e) {
			System.out.println(String.format("#stampRequest AuthException - %s", e.getMessage()));
		} catch (GeneralException e) {
			System.out.println(String.format("#stampRequest GeneralException - %s", e.getMessage()));
		} catch (IOException e) {
			System.out.println(String.format("#stampRequest IOException - %s", e.getMessage()));
		}
		return new SWStampResponse(SW_EXTERNAL_ERROR);
	}
	
}