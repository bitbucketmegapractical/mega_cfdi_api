package org.megapractical.invoicing.api.smarterweb;

import java.time.LocalDate;
import java.time.LocalTime;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wrapper.ApiCancellationRequest;

import Services.Cancelation.SWCancelationService;
import lombok.val;
import lombok.var;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class SWCancellationClient {
	
	private SWCancellationClient() {
	}

	public static SWCancellationResponse cancellationRequest(ApiCancellationRequest apiCancellationRequest) {
		var response = new SWCancellationResponse();
		String errorMessage = null;
		
    	UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > CANCELATION REQUEST");
    	try {
    		val cfdiStatusRequest = buildStatusRequest(apiCancellationRequest);
    		val useCheckStatusFake = apiCancellationRequest.isUseCheckStatusFake();
    		UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > USING FAKE STATUS CHECK: " + useCheckStatusFake);
    		
    		// Verificando el status del comprobante para ver si es cancelable
    		SWCancellationResponse swCancellationResponse = null;
    		if (apiCancellationRequest.getEnvironment().equals("prod") || !useCheckStatusFake) {
    			swCancellationResponse = SWCfdiStatus.checkStatus(cfdiStatusRequest);
    		} else {
    			swCancellationResponse = SWCfdiStatusFake.checkStatusBeforeFake();
    		}
    		UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > CHECK STATUS RESPONSE: " + swCancellationResponse);
    		
    		if (AssertUtils.nonNull(swCancellationResponse)) {
    			val isCancelable = swCancellationResponse.getIsCancelable(); 
    			val status = swCancellationResponse.getStatus();
				if (status.equals("Vigente")) {
					if (isCancelable.equals("Cancelable sin aceptación") || isCancelable.equals("Cancelable con aceptación")) {
						response = cancellation(apiCancellationRequest, cfdiStatusRequest);
						if (!UValidator.isNullOrEmpty(response) && !UValidator.isNullOrEmpty(response.getErrorMessage())) {
							errorMessage = response.getErrorMessage();
						}
					} else if (swCancellationResponse.getIsCancelable().equals("No Cancelable")) {
						errorMessage = "No Cancelable";
						UPrint.logWithLine("MEGA-CFDI API - [ERROR] SW CANCELLATION CLIENT > NOT CANCELLABLE");
					} else if (swCancellationResponse.getIsCancelable().equals("")) {
						errorMessage = "El comprobante no existe o no se pudo verficiar su status";
						UPrint.logWithLine("MEGA-CFDI API - [ERROR] SW CANCELLATION CLIENT > VOUCHER DOESN'T EXIST OR ITS STATUS COULD NOT BE VERIFIED");
					}
				}
    		} else {
    			errorMessage = "El comprobante no existe o no se pudo verficiar su status";
    			UPrint.logWithLine("MEGA-CFDI API - [ERROR] SW CANCELLATION CLIENT > VOUCHER DOESN'T EXIST OR ITS STATUS COULD NOT BE VERIFIED");
    		}
		} catch (Exception e) {
			UPrint.logWithLine("MEGA-CFDI API - [ERROR] SW CANCELLATION CLIENT > AN UNEXPECTED ERROR OCCURRED WHILE CANCELING THE VOUCHER");
			e.printStackTrace();
			errorMessage = "Error inesperando cancelando el comprobante";
		}
    	
    	response.setEmitterRfc(apiCancellationRequest.getEmitterRfc());
		response.setReceiverRfc(apiCancellationRequest.getReceiverRfc());
		response.setUuid(apiCancellationRequest.getUuid());
		response.setVoucherTotal(apiCancellationRequest.getVoucherTotal());
		response.setReason(apiCancellationRequest.getReason());
		response.setSubstitutionUuid(apiCancellationRequest.getSubstitutionUuid());
    	
		if (AssertUtils.hasValue(errorMessage)) {
    		response.setSuccess(Boolean.FALSE);
			response.setErrorMessage(errorMessage);
    	}
		return response;
	}
	
	private static final SWCfdiStatusRequest buildStatusRequest(ApiCancellationRequest cancellationRequest) {
		return SWCfdiStatusRequest
				.builder()
				.emitterRfc(cancellationRequest.getEmitterRfc())
				.receiverRfc(cancellationRequest.getReceiverRfc())
				.voucherTotal(cancellationRequest.getVoucherTotal())
				.uuid(cancellationRequest.getUuid())
				.environment(cancellationRequest.getEnvironment())
				.build();
	}
	
	private static SWCancellationResponse cancellation(ApiCancellationRequest apiCancellationRequest, SWCfdiStatusRequest swCfdiStatusRequest) {
		val response = new SWCancellationResponse();
		try {
			val environment = swCfdiStatusRequest.getEnvironment();
			val api = SWUtil.swCancellationApi(environment);
	    	if (AssertUtils.nonNull(api)) {
	    		UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > CANCELLATION ENVIRONMENT: " + environment);
	    		Utils.Responses.Cancelation.CancelationResponse cancellationResponse = cancellation(api, apiCancellationRequest, swCfdiStatusRequest);
				if (cancellationResponse != null) {
					UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > RESPONSE HTTP STATUS CODE: " + cancellationResponse.HttpStatusCode);
					if (cancellationResponse.HttpStatusCode == 200) {
						// Verificando el status del comprobante después de la cancelación
						SWCancellationResponse swCancellationResponse = null;
			    		if (swCfdiStatusRequest.getEnvironment().equals("prod")) {
			    			swCancellationResponse = SWCfdiStatus.checkStatus(swCfdiStatusRequest);
			    		} else {
			    			swCancellationResponse = SWCfdiStatusFake.checkStatusAfterFake();
			    		}
			    		
						response.setIsCancelable(swCancellationResponse.getIsCancelable());
						response.setStatus(swCancellationResponse.getStatus());
						response.setStatusCode(swCancellationResponse.getStatusCode());
						response.setResponseUuidStatusCode(Integer.toString(cancellationResponse.uuidStatusCode));
						response.setAcuseXml(cancellationResponse.acuse);
						response.setSuccess(cancellationResponse.Status.equals("success"));
						response.setCancellationDate(LocalDate.now());
						response.setCancellationTime(LocalTime.now());
					} else {
						val errorMessage = cancellationResponse.message + ". " + cancellationResponse.messageDetail;
						response.setErrorMessage(errorMessage);
						UPrint.logWithLine("MEGA-CFDI API - [ERROR] SW CANCELLATION CLIENT > SW ERROR RESPONSE: " + errorMessage);
					}
				} else {
					response.setErrorMessage("Sin respuesta de acuse");
					UPrint.logWithLine("MEGA-CFDI API - [ERROR] SW CANCELLATION CLIENT > NO ACUSE RESPONSE");
				}
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	private static Utils.Responses.Cancelation.CancelationResponse cancellation(SWCancelationService api,
																				ApiCancellationRequest apiCancellationRequest, 
																				SWCfdiStatusRequest swCfdiStatusRequest) {
		try {
			UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > PERFORMING REST REQUEST");
			
			val uuid = swCfdiStatusRequest.getUuid();
			val emitterRfc = swCfdiStatusRequest.getEmitterRfc();
			val cerB64 = apiCancellationRequest.getB64Cer();
			val keyB64 = apiCancellationRequest.getB64Key();
			val passwd = apiCancellationRequest.getPassword();
			val reason = apiCancellationRequest.getReason();
			val substitutionUuid = apiCancellationRequest.getSubstitutionUuid() != null
					? apiCancellationRequest.getSubstitutionUuid()
					: "";
			
			// Se inicializa un objeto response, que obtendra la respuesta del api
			Utils.Responses.Cancelation.CancelationResponse cancellationResponse = null;
    		
			// Se asigna el resultado de la respuesta a dicho objeto, haciendo el cast a un objeto SWRetCancellationResponse
            // Se ejecuta el metodo "Cancelar", que generará el acuse de cancelación, el uuid y el status de la cancelación
            cancellationResponse = (Utils.Responses.Cancelation.CancelationResponse) api.Cancelation(uuid, passwd, emitterRfc, cerB64, keyB64, reason, substitutionUuid);
            
            // El objeto response tendra así los atributos:
            // Status: estado de la petición procesada, puede ser : "success", "fail", "error"
            // HttpStatusCode: Codigo de respuesta HTTP del servidor: eg. 200, 400, 500
            // Acuse: xml del acuse de cancelación
            // uuid: uuid del xml cancelado
            // uuidStatusCode: código de respuesta de folios de cancelación
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.Status: " + cancellationResponse.Status);
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.HttpStatusCode: " + cancellationResponse.HttpStatusCode);
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.acuse: " + cancellationResponse.acuse);
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.uuid: " + cancellationResponse.uuid);
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.uuidStatusCode: " + cancellationResponse.uuidStatusCode);
            // En caso de error, se pueden visualizar los campos message y/o messageDetail
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.message: " + cancellationResponse.message);
            UPrint.logWithLine("MEGA-CFDI API - [INFO] SW CANCELLATION CLIENT > cancellationResponse.messageDetail: " + cancellationResponse.messageDetail);
            
            return cancellationResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}