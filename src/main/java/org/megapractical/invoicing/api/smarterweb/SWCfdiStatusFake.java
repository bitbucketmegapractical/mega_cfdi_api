package org.megapractical.invoicing.api.smarterweb;

/**
 * Use this class only in development
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public final class SWCfdiStatusFake {

	private SWCfdiStatusFake() {
	}
	
	public static SWCancellationResponse checkStatusBeforeFake() {
		return SWCancellationResponse
				.builder()
				.isCancelable("Cancelable sin aceptación") 
				.status("Vigente")
				.statusCode("201")
				.build();
	}
	
	public static SWCancellationResponse checkStatusAfterFake() {
		return SWCancellationResponse
				.builder()
				.isCancelable("Cancelable sin aceptación") 
				.status("Cancelado")
				.statusCode("Cancelado sin aceptación")
				.build();
	}
	
}