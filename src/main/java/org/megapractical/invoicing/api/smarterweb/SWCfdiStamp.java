package org.megapractical.invoicing.api.smarterweb;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;

/**
 * @author Maikel Guerra Ferrer
 * 
 */
public final class SWCfdiStamp {
	
	private SWCfdiStamp() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	/**
	 * Stamp with SW
	 *
	 * @param stampProperties
	 */
	public static StampResponse cfdiStamp(StampProperties stampProperties) {
		System.out.println("SW API > CFDI STAMP INSTANCE");
		return SWStampClient.stampRequest(stampProperties);
	}
    
}