package org.megapractical.invoicing.api.smarterweb;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SWRetCancellationResponse extends SWCancellationResponse {
	@Default
	private boolean malformedUrlException = Boolean.FALSE;
	@Default
	private boolean datatypeConfigurationException = Boolean.FALSE;
	@Default
	private boolean wsdlException = Boolean.FALSE;
	private String wsdlUrl;
	private String wsdlEndpoint;
	private String wsdlOperation;
}