package org.megapractical.invoicing.api.smarterweb;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import Services.Cancelation.SWCancelationService;
import Services.Stamp.SWStampService;
import lombok.val;

/**
 * SW utility class
 * 
 * @author Maikel Guerra Ferrer
 *
 */
public final class SWUtil {

	private SWUtil() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	private static final String SW_TEST_URL = "sw.test.rest.end.point";
	private static final String SW_PROD_URL = "sw.prod.rest.end.point";
	private static final String API_URL = "url";
	private static final String API_TOKEN = "token";
	
	public static SWStampService swStampApi(String environment) {
		val swApiData = swApiData(environment);
		if (swApiData != null) {
			return new SWStampService(swApiData.get(API_TOKEN), swApiData.get(API_URL));
		}
		return null;
	}
	
	public static SWCancelationService swCancellationApi(String environment) {
		val swApiData = swApiData(environment);
		if (swApiData != null) {
			return new SWCancelationService(swApiData.get(API_TOKEN), swApiData.get(API_URL));
		}
		return null;
	}

	private static Map<String, String> swApiData(String environment) {
		try {
			String url = null;
			String jwt = null;
			
			if (environment.equalsIgnoreCase("prod")) {
				url = getProperty(SW_PROD_URL);
				jwt = getProperty("sw.prod.jwt");
			} else {
				url = getProperty(SW_TEST_URL);
				jwt = getProperty("sw.test.jwt");
			}
			
			val data = new HashMap<String, String>();
			data.put(API_URL, url);
			data.put(API_TOKEN, jwt);
			
			return data;
		} catch (Exception e) {
			System.out.println(String.format("#swApiData error - %s", e.getMessage()));
		}
		return Collections.emptyMap();
	}

	public static String getProperty(String key) throws IOException {
		val properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config/api.sw.config.properties"));
		return properties.getProperty(key).trim();
	}
}