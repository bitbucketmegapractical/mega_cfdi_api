
package org.megapractical.invoicing.api.smarterweb.soap.retention.auth;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TokenCambioContraseñaResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tokenCambioContrase\u00f1aResult"
})
@XmlRootElement(name = "TokenCambioContrase\u00f1aResponse")
public class TokenCambioContraseñaResponse {

    @XmlElement(name = "TokenCambioContrase\u00f1aResult")
    protected String tokenCambioContraseñaResult;

    /**
     * Gets the value of the tokenCambioContraseñaResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenCambioContraseñaResult() {
        return tokenCambioContraseñaResult;
    }

    /**
     * Sets the value of the tokenCambioContraseñaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenCambioContraseñaResult(String value) {
        this.tokenCambioContraseñaResult = value;
    }

}
