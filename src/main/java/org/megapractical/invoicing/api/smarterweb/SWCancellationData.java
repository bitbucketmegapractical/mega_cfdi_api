package org.megapractical.invoicing.api.smarterweb;

import org.megapractical.invoicing.api.wrapper.ApiCancellationRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SWCancellationData {
	private ApiCancellationRequest cancellationRequest;
	private SWCancellationResponse cancellationResponse;
}