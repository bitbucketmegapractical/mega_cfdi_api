package org.megapractical.invoicing.api.smarterweb;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import org.apache.axis.AxisFault;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.PrintUtils;
import org.megapractical.invoicing.api.wsdl.retention.schema.RetencionesRequest;
import org.megapractical.invoicing.sat.tfd.TfdParser;
import org.megapractical.seal.core.OriginalString;
import org.megapractical.seal.core.XmlSeal;
import org.megapractical.seal.payload.XmlSealRequest;
import org.megapractical.seal.uriresolver.RetentionXsltURIResolver;
import org.megapractical.seal.xslt.XsltResolver;
import org.tempuri.WcfTimbradoRetencionesEndpointStub;
import org.tempuri.WcfTimbradoRetencionesLocator;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 * 
 * Retenciones (SOAP)
 * 
 */
public final class SWRetentionClient {
	
	private SWRetentionClient() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	private static final String SW_URL_TEST = "http://pruebascfdi.smartweb.com.mx/Timbrado/wcfTimbradoRetenciones.svc";
    private static final String SW_URL_PROD = "http://cfdi.smartweb.com.mx/Timbrado/wcfTimbradoRetenciones.svc";
    private static final String SW_JWT_ACCOUNT_PROD = "T2lYQ0t4L0RHVkR4dHZ5Nkk1VHNEakZ3Y0J4Nk9GODZuRyt4cE1wVm5tbXB3YVZxTHdOdHAwVXY2NTdJb1hkREtXTzE3dk9pMmdMdkFDR2xFWFVPUXpTUm9mTG1ySXdZbFNja3FRa0RlYURqbzdzdlI2UUx1WGJiKzViUWY2dnZGbFloUDJ6RjhFTGF4M1BySnJ4cHF0YjUvbmRyWWpjTkVLN3ppd3RxL0dJPQ.T2lYQ0t4L0RHVkR4dHZ5Nkk1VHNEakZ3Y0J4Nk9GODZuRyt4cE1wVm5tbFlVcU92YUJTZWlHU3pER1kySnlXRTF4alNUS0ZWcUlVS0NhelhqaXdnWTRncklVSWVvZlFZMWNyUjVxYUFxMWFxcStUL1IzdGpHRTJqdS9Zakw2UGREa2hKVnkzdThrdFh3d1RCMWE1aS9HV3poMXlsZE03MUVjUjVpc2E5SE9jUWlEcWtiS3NHNVEvNlM2amRyRmFwZHpGK1Vod1djZzdKQ21DVk9CblllZXFLQng5WjdlTjdJVzNsQ2FpeDNGYlhJeFAzYjZxRzVqRzVrdkw2aXU1ODRlL0RoY1l5RzBoOHhxL0lkRU1rR0RWbUl6dTlpbUplaGZEVFdObWxkMVB6SDFNRGFWZUhVTzVieStRcFJHSDRlYTQzUU9HNGg4dDd4Rkcxd09zTUhqeDZhTUxHQkJYb0FhVC9Wcml3dDZ5UDlPalQvaHVaMWhMZDBreTFLd0NrdzRDQTM1KzRkYk5ycmRZL3U3ZjJ0aGwrb0NHYVpCeUdPcHpmZ0o0elEwSlJqMTdINGJSQ0dIeHhXS2YzdHNSOE9hb3Q3a3JGUkJ0NXZUc2I4QXd3cXhiRWk4VEZnbElXVXh5QitTQmcvaC9FM0NHMlRQN3RBanVPNjhHRmtFd2RoSFYzU0xWZU1OTDB3MXNNajd3eGF3PT0.66XsE7IssNXfdXjDdS2LgDUhhqspbnQvsC2LrR5k0Og";
    private static final String SW_JWT_ACCOUNT_TEST = "T2lYQ0t4L0RHVkR4dHZ5Nkk1VHNEakZ3Y0J4Nk9GODZuRyt4cE1wVm5tbXB3YVZxTHdOdHAwVXY2NTdJb1hkREtXTzE3dk9pMmdMdkFDR2xFWFVPUXpTUm9mTG1ySXdZbFNja3FRa0RlYURqbzdzdlI2UUx1WGJiKzViUWY2dnZGbFloUDJ6RjhFTGF4M1BySnJ4cHF0YjUvbmRyWWpjTkVLN3ppd3RxL0dJPQ.T2lYQ0t4L0RHVkR4dHZ5Nkk1VHNEakZ3Y0J4Nk9GODZuRyt4cE1wVm5tbFlVcU92YUJTZWlHU3pER1kySnlXRTF4alNUS0ZWcUlVS0NhelhqaXdnWTRncklVSWVvZlFZMWNyUjVxYUFxMWFxcStUL1IzdGpHRTJqdS9Zakw2UGRZbFlVYmJVSkxXa1NZNzN5VUlSUzlJMDhHcC90SHkzcWNWMDhxTjhLSGpFSVJFMHA0dWpwSFNSall2THZaTzhrbzdkdkxjMC9kM3JzYU9XcVhJNWFPMldobGhDaGZaZ2RFVHY0Unk5Q214ZGFvOGxFdXYxT1hXbWVJcWx6cCt1Ny9WVXBORjJETTRKNGlzeU5vK1NlZjIrOUhHMmJjdVhDVDZzYlp5N1RQa1kzNHdEQTBTNXYwb1VUWWNuUmU4SXJxRElkWnZjbXBxTlVCVFhkWmJhWVlrZG5ldEtNeDRBN3lzQ256cEdNUm4yMncvTVgzTDJnUWlucGEyL2RqQUdNd0tUYnF5ZTdxanp5VkliSDhFbFJETklnbU1qRVNUb3g2Z0RxYjN5ZHJCV1lyMzVJcVJjY1VEeU1UUnlIVGQ2OWw1NSs4aWNpUG9FL3pHSDd4dWhZNjcrL3JMNXNjSCtPNnUxd21kMXlmbVFRbTNNT0lGdjJRdzEwSVN3U3ZYdEpMT3RSYUx3UUFCUDdzS3BSeUtaMjBBPT0.2-I221MYnsUPfh6mGTRbv8XkOsdUjVc0mfKtNxmgpzk";
	
    public static StampResponse retentionStamp(RetencionesRequest retRequest, String environment) {
    	PrintUtils.info("[INFO] SW API RET > CFDI STAMP INSTANCE > stampResponse");
    	
    	val stampResponse = new StampResponse();
    	val xml = retRequest.getRetencionesCFDIin(); 		
    	
    	PrintUtils.info("[INFO] SW API RET > XML");
    	PrintUtils.print(xml);
    	
    	val sealRequest = XmlSealRequest
							.builder()
							.xml(xml)
							.privateKey(retRequest.getKeyByteArr())
							.privateKeyPwd(retRequest.getKeyPassword())
							.retention(true)
							.build();
    	
		val xmlSeal = XmlSeal.seal(sealRequest);
		
    	PrintUtils.info("[INFO] SW API RET > XML After Seal");
    	PrintUtils.print(xmlSeal);
    	
		try {
			PrintUtils.info(String.format("[INFO] SW API RET > GET URL BY ENVIRONMENT > ENVIRONMENT %s", environment));
			
			URL url = null;
	    	String jwt = null;
			
			if (environment.equalsIgnoreCase("prod")) {
	            url = new URL(SW_URL_PROD);
	            jwt = SW_JWT_ACCOUNT_PROD;
        	} else {
        		url = new URL(SW_URL_TEST);
        		jwt = SW_JWT_ACCOUNT_TEST;
        	} 
			
			PrintUtils.info(String.format("[INFO] SW API RET > STAMP INTANCE URL > %s", url));
	       	PrintUtils.info(String.format("[INFO] SW API RET > STAMP INTANCE TOKEN > %s", jwt));
			
			val timbrado = new WcfTimbradoRetencionesLocator();
			PrintUtils.info("[INFO] SW API RET > STAMP INSTANCE > WcfTimbradoRetencionesEndpointStub");
			PrintUtils.print(timbrado.getwcfTimbradoRetencionesEndpointAddress());
			
			val ws = new WcfTimbradoRetencionesEndpointStub(url, timbrado);
			val stampedXml = ws.timbrarRetencionXMLV2(xmlSeal, jwt);
			
			PrintUtils.info("[INFO] SW API RET > timbrarRetencionXML RESULT");
			PrintUtils.print(stampedXml);
	       	
			stampResponse.setStamped(Boolean.TRUE);
	       	stampResponse.setXml(stampedXml.getBytes());
	       	stampResponse.setStampedXml(stampedXml);
	       	populateTfd(stampResponse);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			stampResponse.setMalformedUrlException(true);
         } catch (AxisFault e) {
            PrintUtils.print();
            PrintUtils.info(String.format("[ERROR] SW API RET > ERROR > MESSAGE > %s", e.getMessage()));
            PrintUtils.info(String.format("[ERROR] SW API RET > ERROR > FAULTSTRING > %s", e.getFaultString()));
            
			stampResponse.setStamped(Boolean.FALSE);
    		stampResponse.setError(e.getMessage());
    		stampResponse.setErrorCode("error");
    		stampResponse.setErrorMessage(e.getFaultString());
		} catch (RemoteException e) {
            PrintUtils.info(String.format("[ERROR] SW API RET > ERROR > FAULTSTRING > %s", e.getMessage()));
            return null;
		}
		return stampResponse;
    }
    
	private static void populateTfd(StampResponse stampResponse) {
		if (stampResponse != null) {
			val tfd = TfdParser.getTfd(stampResponse.getStampedXml());
			stampResponse.setTfd(tfd);
			stampResponse.setUuid(tfd.getUUID());
			stampResponse.setDateStamp(tfd.getFechaTimbrado());
			stampResponse.setEmitterSeal(tfd.getSelloCFD());
			stampResponse.setSatSeal(tfd.getSelloSAT());
			stampResponse.setCertificateEmitterNumber(tfd.getRfcProvCertif());
			stampResponse.setCertificateSatNumber(tfd.getNoCertificadoSAT());
			stampResponse.setOriginalString(OriginalString.getOriginalString(stampResponse.getStampedXml(),
					XsltResolver.RETENTION_XSLT, new RetentionXsltURIResolver()));
		}
	}
	
}