
package org.megapractical.invoicing.api.smarterweb.soap.retention.stamp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TimbrarRetencionXMLV2Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "timbrarRetencionXMLV2Result"
})
@XmlRootElement(name = "TimbrarRetencionXMLV2Response")
public class TimbrarRetencionXMLV2Response {

    @XmlElementRef(name = "TimbrarRetencionXMLV2Result", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> timbrarRetencionXMLV2Result;

    /**
     * Gets the value of the timbrarRetencionXMLV2Result property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTimbrarRetencionXMLV2Result() {
        return timbrarRetencionXMLV2Result;
    }

    /**
     * Sets the value of the timbrarRetencionXMLV2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTimbrarRetencionXMLV2Result(JAXBElement<String> value) {
        this.timbrarRetencionXMLV2Result = value;
    }

}
