package org.megapractical.invoicing.api.mapper;

import static java.util.stream.Collectors.toList;
import static org.megapractical.invoicing.api.util.MoneyUtils.format;

import java.util.Collections;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi;
import org.megapractical.invoicing.api.common.ApiRelatedCfdi.RelatedCfdiUuid;
import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.common.ApiVoucherTaxes;
import org.megapractical.invoicing.api.wrapper.ApiCfdi;
import org.megapractical.invoicing.api.wrapper.ApiConcept;
import org.megapractical.invoicing.api.wrapper.ApiEmitter;
import org.megapractical.invoicing.api.wrapper.ApiReceiver;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.CfdiRelacionados.CfdiRelacionado;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.Impuestos;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado;
import org.springframework.stereotype.Component;

import lombok.val;

@Component
public class CfdiMapper {
	
	public ApiCfdi map(Comprobante voucher) {
		if (voucher != null) {
			return mapToApiCfdi(voucher);
		}
		return null;
	}
	
	private ApiCfdi mapToApiCfdi(Comprobante voucher) {
		return ApiCfdi
				.builder()
				.voucher(mapToApiVoucher(voucher))
				.emitter(mapToApiEmitter(voucher))
				.receiver(mapToApiReceiver(voucher))
				.concepts(mapToConcepts(voucher))
				.voucherTypeCatalog(voucher.getTipoDeComprobante())
				.currencyCatalog(voucher.getMoneda())
				.paymentMethodCatalog(voucher.getMetodoPago())
				.expeditionPlace(voucher.getLugarExpedicion())
				.build();
	}
	
	private ApiVoucher mapToApiVoucher(Comprobante voucher) {
		return ApiVoucher
				.builder()
				.version(voucher.getVersion())
				.serie(voucher.getSerie())
				.folio(voucher.getFolio())
				.subTotal(voucher.getSubTotal())
				.subTotalString(format(voucher.getSubTotal()))
				.discount(voucher.getDescuento())
				.discountString(format(voucher.getDescuento()))
				.total(voucher.getTotal())
				.totalString(format(voucher.getTotal()))
				.postalCode(voucher.getLugarExpedicion())
				.emitterSeal(voucher.getSello())
				.certificateEmitterNumber(voucher.getNoCertificado())
				.dateTimeExpedition(voucher.getFecha())
				.exportation(voucher.getExportacion())
				.relatedCfdi(mapToApiRelatedCfdi(voucher))
				.taxes(mapToApiVoucherTaxes(voucher))
				.build();
	}
	
	private ApiRelatedCfdi mapToApiRelatedCfdi(Comprobante voucher) {
		if (voucher.getCfdiRelacionados() != null) {
			val uuids = voucher.getCfdiRelacionados().getCfdiRelacionados()
													 .stream()
													 .map(CfdiRelacionado::getUUID)
													 .map(RelatedCfdiUuid::new)
													 .collect(toList());
			return ApiRelatedCfdi
					.builder()
					.relationshipTypeCode(voucher.getCfdiRelacionados().getTipoRelacion())
					.uuids(uuids)
					.build();
		}
		return null;
	}
	
	private ApiVoucherTaxes mapToApiVoucherTaxes(Comprobante voucher) {
		val taxes = voucher.getImpuestos();
		if (taxes != null) {
			return ApiVoucherTaxes
					.builder()
					.totalTaxTransferred(taxes.getTotalImpuestosTrasladados())
					.totalTaxWithheld(taxes.getTotalImpuestosRetenidos())
					.build();
		}
		return null;
	}
	
	private ApiEmitter mapToApiEmitter(Comprobante voucher) {
		val emitter = voucher.getEmisor();
		return ApiEmitter
				.builder()
				.nombreRazonSocial(emitter.getNombre())
				.rfc(emitter.getRfc())
				.taxRegimeCode(emitter.getRegimenFiscal())
				.facAtrAdquirente(emitter.getFacAtrAdquirente())
				.build();
	}
	
	private ApiReceiver mapToApiReceiver(Comprobante voucher) {
		val receiver = voucher.getReceptor();
		return ApiReceiver
				.builder()
				.rfc(receiver.getRfc())
				.nombreRazonSocial(receiver.getNombre())
				.domicilioFiscal(receiver.getDomicilioFiscalReceptor())
				.usoCfdiCatalog(receiver.getUsoCFDI())
				.taxRegime(receiver.getRegimenFiscalReceptor())
				.countryCatalog(receiver.getResidenciaFiscal())
				.numRegIdTrib(receiver.getNumRegIdTrib())
				.build();
	}
	
	private List<ApiConcept> mapToConcepts(Comprobante voucher) {
		return voucher.getConceptos().getConceptos()
									 .stream()
									 .map(this::mapToApiConcept)
									 .collect(toList());
	}
	
	private ApiConcept mapToApiConcept(Concepto concept) {
		return ApiConcept
				.builder()
				.description(concept.getDescripcion())
				.keyProductServiceCode(concept.getClaveProdServ())
				.measurementUnitCode(concept.getClaveUnidad())
				.quantity(concept.getCantidad())
				.unitValue(concept.getValorUnitario())
				.unitValueString(format(concept.getValorUnitario()))
				.amount(concept.getImporte())
				.amountString(format(concept.getImporte()))
				.discount(concept.getDescuento())
				.discountString(format(concept.getDescuento()))
				.objImp(concept.getObjetoImp())
				.taxesTransferred(mapToApiConceptTaxesTransferred(concept.getImpuestos()))
				.taxesWithheld(mapToApiConceptTaxesWithheld(concept.getImpuestos()))
				.build();
	}
	
	private List<ApiConceptTaxes> mapToApiConceptTaxesTransferred(Impuestos taxes) {
		if (taxes != null && taxes.getTraslados() != null) {
			return taxes.getTraslados().getTraslados()
									   .stream()
									   .map(this::mapToApiConceptTaxesTransferred)
									   .collect(toList());
		}
		return Collections.emptyList();
	}
	
	private ApiConceptTaxes mapToApiConceptTaxesTransferred(Traslado transferred) {
		return ApiConceptTaxes
				.builder()
				.baseBigDecimal(transferred.getBase())
				.base(format(transferred.getBase()))
				.tax(transferred.getImpuesto())
				.factorType(transferred.getTipoFactor())
				.factor(transferred.getTipoFactor().name())
				.rateOrFeeBigDecimal(transferred.getTasaOCuota())
				.rateOrFee(format(transferred.getTasaOCuota()))
				.amountBigDecimal(transferred.getImporte())
				.amount(format(transferred.getImporte()))
				.build();
	}
	
	private List<ApiConceptTaxes> mapToApiConceptTaxesWithheld(Impuestos taxes) {
		if (taxes != null && taxes.getRetenciones() != null) {
			return taxes.getRetenciones().getRetencions()
										 .stream()
										 .map(this::mapToApiConceptTaxesWithheld)
										 .collect(toList());
		}
		return Collections.emptyList();
	}
	
	private ApiConceptTaxes mapToApiConceptTaxesWithheld(Retencion withheld) {
		return ApiConceptTaxes
				.builder()
				.baseBigDecimal(withheld.getBase())
				.base(format(withheld.getBase()))
				.tax(withheld.getImpuesto())
				.factorType(withheld.getTipoFactor())
				.factor(withheld.getTipoFactor().name())
				.rateOrFeeBigDecimal(withheld.getTasaOCuota())
				.rateOrFee(format(withheld.getTasaOCuota()))
				.amountBigDecimal(withheld.getImporte())
				.amount(format(withheld.getImporte()))
				.build();
	}
	
}