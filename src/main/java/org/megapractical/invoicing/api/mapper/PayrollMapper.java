package org.megapractical.invoicing.api.mapper;

import static java.util.stream.Collectors.toList;
import static org.megapractical.common.datetime.DateUtils.customFormat;
import static org.megapractical.invoicing.api.util.MoneyUtils.format;

import java.util.Collections;
import java.util.List;

import org.megapractical.common.datetime.LocalDateUtils;
import org.megapractical.invoicing.api.util.UTools;
import org.megapractical.invoicing.api.wrapper.ApiPayroll;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollEmitter;
import org.megapractical.invoicing.api.wrapper.ApiPayrollInability;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.CompensationCreditBalances;
import org.megapractical.invoicing.api.wrapper.ApiPayrollOtherPayment.EmploymentSubsidy;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPensionRetirement;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception.PayrollActionTitle;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception.PayrollExtraHour;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollSeparationCompensation;
import org.megapractical.invoicing.api.wrapper.ApiPayrollReceiver;
import org.megapractical.invoicing.api.wrapper.ApiPayrollReceiver.PayrollOutsourcing;
import org.megapractical.invoicing.sat.complement.payroll.Nomina;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Deducciones;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Deducciones.Deduccion;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Incapacidades.Incapacidad;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.OtrosPagos.OtroPago;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Percepciones;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Percepciones.Percepcion;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Percepciones.Percepcion.HorasExtra;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Receptor;
import org.megapractical.invoicing.sat.complement.payroll.Nomina.Receptor.SubContratacion;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.val;

@Component
@RequiredArgsConstructor
public class PayrollMapper {
	
	private final CfdiMapper cfdiMapper;
	
	public ApiPayroll map(Comprobante voucher, Nomina payroll) {
		if (payroll != null) {
			return mapToApiPayroll(voucher, payroll);
		}
		return null;
	}
	
	private ApiPayroll mapToApiPayroll(Comprobante voucher, Nomina payroll) {
		val apiCfdi = cfdiMapper.map(voucher);
		return ApiPayroll
				.builder()
				.apiCfdi(apiCfdi)
				.payrollTypeCatalog(payroll.getTipoNomina())
				.paymentDate(LocalDateUtils.parse(payroll.getFechaPago()))
				.paymentDateString(customFormat(payroll.getFechaPago()))
				.paymentIntialDate(LocalDateUtils.parse(payroll.getFechaInicialPago()))
				.paymentIntialDateString(customFormat(payroll.getFechaInicialPago()))
				.paymentEndDate(LocalDateUtils.parse(payroll.getFechaFinalPago()))
				.paymentEndDateString(customFormat(payroll.getFechaFinalPago()))
				.numberDaysPaid(format(payroll.getNumDiasPagados()).replace(".00", ""))
				.totalPerceptions(format(payroll.getTotalPercepciones()))
				.totalDeductions(format(payroll.getTotalDeducciones()))
				.totalOtherPayments(format(payroll.getTotalOtrosPagos()))
				.payrollEmitter(mapToApiPayrollEmitter(payroll))
				.payrollReceiver(mapToApiPayrollReceiver(payroll))
				.payrollPerception(mapToApiPayrollPerception(payroll))
				.payrollDeduction(mapToApiPayrollDeduction(payroll))
				.payrollOtherPayments(mapToApiPayrollOtherPayments(payroll))
				.payrollInabilities(mapToApiPayrollInabilities(payroll))
				.build();
	}
	
	private ApiPayrollEmitter mapToApiPayrollEmitter(Nomina payroll) {
		val emitter = payroll.getEmisor();
		val payrollEmitter = ApiPayrollEmitter
								.builder()
								.curp(emitter.getCurp())
								.employerRegistration(emitter.getRegistroPatronal())
								.rfcPatronOrigin(emitter.getRfcPatronOrigen())
								.build();
		
		val sncfEntity = emitter.getEntidadSNCF();
		if (sncfEntity != null) {
			payrollEmitter.setResourceOriginCatalog(sncfEntity.getOrigenRecurso());
			payrollEmitter.setOwnResourceAmount(format(sncfEntity.getMontoRecursoPropio()));
		}
		
		return payrollEmitter;
	}
	
	private ApiPayrollReceiver mapToApiPayrollReceiver(Nomina payroll) {
		val receiver = payroll.getReceptor();
		return ApiPayrollReceiver
				.builder()
				.curp(receiver.getCurp())
				.socialSecurityNumber(receiver.getNumSeguridadSocial())
				.startDateEmploymentRelationship(LocalDateUtils.parse(receiver.getFechaInicioRelLaboral()))
				.startDateEmploymentRelationshipString(customFormat(receiver.getFechaInicioRelLaboral()))
				.age(receiver.getAntigüedad())
				.ageDescription(UTools.jobAge(receiver.getAntigüedad()))
				.contractTypeCode(receiver.getTipoContrato())
				.unionized(receiver.getSindicalizado())
				.dayTypeCode(receiver.getTipoJornada())
				.regimeTypeCode(receiver.getTipoRegimen())
				.employeeNumber(receiver.getNumEmpleado())
				.department(receiver.getDepartamento())
				.jobTitle(receiver.getPuesto())
				.jobRiskCode(receiver.getRiesgoPuesto())
				.paymentFrequencyCode(receiver.getPeriodicidadPago())
				.bankCode(receiver.getBanco())
				.bankAccount(receiver.getCuentaBancaria())
				.shareContributionBaseSalary(format(receiver.getSalarioBaseCotApor()))
				.integratedDailySalary(format(receiver.getSalarioDiarioIntegrado()))
				.federalEntityCatalog(receiver.getClaveEntFed())
				.payrollOutsourcings(mapToPayrollOutsourcings(receiver))
				.build();
	}
	
	private List<PayrollOutsourcing> mapToPayrollOutsourcings(Receptor receiver) {
		return receiver.getSubContratacions().stream().map(this::mapToPayrollOutsourcing).collect(toList());
	}
	
	private PayrollOutsourcing mapToPayrollOutsourcing(SubContratacion outsourcing) {
		return new PayrollOutsourcing(outsourcing.getRfcLabora(), format(outsourcing.getPorcentajeTiempo()));
	}
	
	private ApiPayrollPerception mapToApiPayrollPerception(Nomina payroll) {
		val perception = payroll.getPercepciones();
		if (payroll.getPercepciones() != null) {
			return ApiPayrollPerception
					.builder()
					.totalSalaries(format(perception.getTotalSueldos()))
					.totalSeparationCompensation(format(perception.getTotalSeparacionIndemnizacion()))
					.totalPensionRetirement(format(perception.getTotalJubilacionPensionRetiro()))
					.totalTaxed(format(perception.getTotalGravado()))
					.totalExempt(format(perception.getTotalExento()))
					.payrollPerceptions(mapToPayrollPerceptions(perception))
					.payrollPensionRetirement(mapToPayrollPensionRetirement(perception))
					.payrollSeparationCompensation(mapToPayrollSeparationCompensation(perception))
					.build();
		}
		return null;
	}
	
	private List<PayrollPerception> mapToPayrollPerceptions(Percepciones perception) {
		return perception.getPercepcions().stream().map(this::mapToPayrollPerception).collect(toList());
	}
	
	private PayrollPerception mapToPayrollPerception(Percepcion perception) {
		return PayrollPerception
				.builder()
				.perceptionTypeCode(perception.getTipoPercepcion())
				.key(perception.getClave())
				.concept(perception.getConcepto())
				.taxableAmount(format(perception.getImporteGravado()))
				.exemptAmount(format(perception.getImporteExento()))
				.payrollActionTitle(mapToPayrollActionTitle(perception))
				.payrollExtraHours(mapToPayrollExtraHours(perception.getHorasExtras()))
				.build();
	}
	
	private PayrollActionTitle mapToPayrollActionTitle(Percepcion perception) {
		val actionTitle = perception.getAccionesOTitulos();
		if (actionTitle != null) {
			return new PayrollActionTitle(format(actionTitle.getValorMercado()),
					format(actionTitle.getPrecioAlOtorgarse()));
		}
		return null;
	}
	
	private List<PayrollExtraHour> mapToPayrollExtraHours(List<HorasExtra> extraHours) {
		return extraHours.stream().map(this::mapToPayrollExtraHour).collect(toList());
	}
	
	private PayrollExtraHour mapToPayrollExtraHour(HorasExtra extraHour) {
		return PayrollExtraHour
				.builder()
				.days(String.valueOf(extraHour.getDias()))
				.hourTypeCode(extraHour.getTipoHoras())
				.extraHour(String.valueOf(extraHour.getHorasExtra()))
				.amountPaid(format(extraHour.getImportePagado()))
				.build();
	}
	
	private PayrollPensionRetirement mapToPayrollPensionRetirement(Percepciones perception) {
		val pensionRetirement = perception.getJubilacionPensionRetiro();
		if (pensionRetirement != null) {
			return PayrollPensionRetirement
					.builder()
					.totalExhibition(format(pensionRetirement.getTotalUnaExhibicion()))
					.totalPartiality(format(pensionRetirement.getTotalParcialidad()))
					.dailyAmount(format(pensionRetirement.getMontoDiario()))
					.incomeAccumulate(format(pensionRetirement.getIngresoAcumulable()))
					.incomeNoAccumulate(format(pensionRetirement.getIngresoNoAcumulable()))
					.build();
		}
		return null;
	}
	
	private PayrollSeparationCompensation mapToPayrollSeparationCompensation(Percepciones perception) {
		val sc = perception.getSeparacionIndemnizacion();
		if (sc != null) {
			return PayrollSeparationCompensation
					.builder()
					.totalPaid(format(sc.getTotalPagado()))
					.exercisesNumberService(String.valueOf(sc.getNumAñosServicio()))
					.lastSalary(format(sc.getUltimoSueldoMensOrd()))
					.incomeAccumulate(format(sc.getIngresoAcumulable()))
					.incomeNoAccumulate(format(sc.getIngresoNoAcumulable()))
					.build();
		}
		return null;
	}
	
	private ApiPayrollDeduction mapToApiPayrollDeduction(Nomina payroll) {
		val deduction = payroll.getDeducciones();
		if (deduction != null) {
			return ApiPayrollDeduction
					.builder()
					.totalOtherDeductions(format(deduction.getTotalOtrasDeducciones()))
					.totalTaxesWithheld(format(deduction.getTotalImpuestosRetenidos()))
					.payrollDeductions(mapToPayrollDeductions(deduction))
					.build();
		}
		return null;
	}
	
	private List<PayrollDeduction> mapToPayrollDeductions(Deducciones deduction) {
		return deduction.getDeduccions().stream().map(this::mapToPayrollDeduction).collect(toList());
	}
	
	private PayrollDeduction mapToPayrollDeduction(Deduccion deduction) {
		return PayrollDeduction
				.builder()
				.deductionTypeCode(deduction.getTipoDeduccion())
				.key(deduction.getClave())
				.concept(deduction.getConcepto())
				.amount(format(deduction.getImporte()))
				.build();
	}
	
	private List<ApiPayrollOtherPayment> mapToApiPayrollOtherPayments(Nomina payroll) {
		val otherPayment = payroll.getOtrosPagos();
		if (otherPayment != null) {
			return otherPayment.getOtroPagos().stream().map(this::mapToApiPayrollOtherPayment).collect(toList());
		}
		return Collections.emptyList();
	}
	
	private ApiPayrollOtherPayment mapToApiPayrollOtherPayment(OtroPago otherPayment) {
		return ApiPayrollOtherPayment
				.builder()
				.otherPaymentTypeCode(otherPayment.getTipoOtroPago())
				.key(otherPayment.getClave())
				.concept(otherPayment.getConcepto())
				.amount(format(otherPayment.getImporte()))
				.employmentSubsidy(mapToEmploymentSubsidy(otherPayment))
				.compensationCreditBalance(mapToCompensationCreditBalances(otherPayment))
				.build();
	}
	
	private EmploymentSubsidy mapToEmploymentSubsidy(OtroPago otherPayment) {
		val employmentSubsidy = otherPayment.getSubsidioAlEmpleo();
		if (employmentSubsidy != null) {
			return EmploymentSubsidy
					.builder()
					.subsidyCaused(format(employmentSubsidy.getSubsidioCausado()))
					.build();
		}
		return null;
	}
	
	private CompensationCreditBalances mapToCompensationCreditBalances(OtroPago otherPayment) {
		val ccb = otherPayment.getCompensacionSaldosAFavor();
		if (ccb != null) {
			return CompensationCreditBalances
					.builder()
					.positiveBalance(format(ccb.getSaldoAFavor()))
					.exercise(String.valueOf(ccb.getAño()))
					.remainingPositiveBalance(format(ccb.getRemanenteSalFav()))
					.build();
		}
		return null;
	}
	
	private List<ApiPayrollInability> mapToApiPayrollInabilities(Nomina payroll) {
		val inabilities = payroll.getIncapacidades();
		if (inabilities != null) {
			return inabilities.getIncapacidads().stream().map(this::mapToApiPayrollInability).collect(toList());
		}
		return Collections.emptyList();
	}
	
	private ApiPayrollInability mapToApiPayrollInability(Incapacidad inability) {
		return ApiPayrollInability
				.builder()
				.inabilityDays(String.valueOf(inability.getDiasIncapacidad()))
				.inabilityTypeCode(inability.getTipoIncapacidad())
				.monetaryAmount(format(inability.getImporteMonetario()))
				.build();
	}
	
}