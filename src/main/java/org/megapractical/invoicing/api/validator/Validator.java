package org.megapractical.invoicing.api.validator;

import java.util.regex.Pattern;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.util.UValue;

import lombok.val;

public final class Validator {
	
	private Validator() {		
	}

	public static AppWebValidator validate(StampResponse stampResponse) {
		return AppWebValidator
				.builder()
				.error(Boolean.TRUE)
				.errorCode(stampResponse.getErrorCode())
				.errorDescription(stampResponse.getError())
				.errorMessage(stampResponse.getErrorMessage())
				.activateTab("tab-voucher")
				.build();
	}
	
	public static AppWebValidator stampValidate(StampResponse stampResponse) {
		return AppWebValidator
				.builder()
				.error(Boolean.TRUE)
				.errorCode(stampResponse.getErrorCode())
				.errorDescription(stampResponse.getError())
				.errorMessage(stampResponse.getErrorMessage())
				.build();
			
	}
	
	public static String validateError(String errorCode) {
		try {
			if (!UValidator.isNullOrEmpty(errorCode)) {
				errorCode = UValue.stringValue(errorCode);
				return ErrorSource.fromValue(errorCode).value;
			}
		} catch (Exception e) {
	    	UPrint.logWithLine("[WARNING] MEGA-CFDI API > VALIDATOR > VALIDATE ERROR > INVALID ERROR CODE: " + errorCode);
		}
		return null;
	}
	
	public static String getErrorCode(String value) {
		try {
			if (!UValidator.isNullOrEmpty(value)) {
				value = UValue.stringValue(value);
				val splited = value.split(Pattern.quote(" : "));
				return splited[0].trim();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getErrorMessage(String value) {
		try {
			if (!UValidator.isNullOrEmpty(value)) {
				value = UValue.stringValue(value);
				val splited = value.split(Pattern.quote(" : "));
				return splited[1].trim();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public enum ErrorSource {
		// CFDI V3.3
		CFDI33105("CFDI33105"),
		CFDI33106("CFDI33106"),
		CFDI33107("CFDI33107"),
		CFDI33108("CFDI33108"),
		CFDI33109("CFDI33109"),
		CFDI33110("CFDI33110"),
		CFDI33111("CFDI33111"),
		CFDI33112("CFDI33112"),
		CFDI33113("CFDI33113"),
		CFDI33114("CFDI33114"),
		CFDI33115("CFDI33115"),
		CFDI33116("CFDI33116"),
		CFDI33117("CFDI33117"),
		CFDI33118("CFDI33118"),
		CFDI33119("CFDI33119"),
		CFDI33120("CFDI33120"),
		CFDI33121("CFDI33121"),
		CFDI33122("CFDI33122"),
		CFDI33123("CFDI33123"),
		CFDI33124("CFDI33124"),
		CFDI33125("CFDI33125"),
		CFDI33126("CFDI33126"),
		CFDI33127("CFDI33127"),
		CFDI33128("CFDI33128"),
		CFDI33129("CFDI33129"),
		CFDI33130("CFDI33130"),
		CFDI33131("CFDI33131"),
		CFDI33132("CFDI33132"),
		CFDI33133("CFDI33133"),
		CFDI33134("CFDI33134"),
		CFDI33135("CFDI33135"),
		CFDI33136("CFDI33136"),
		CFDI33137("CFDI33137"),
		CFDI33138("CFDI33138"),
		CFDI33139("CFDI33139"),
		CFDI33140("CFDI33140"),
		CFDI33141("CFDI33141"),
		CFDI33142("CFDI33142"),
		CFDI33143("CFDI33143"),
		CFDI33144("CFDI33144"),
		CFDI33145("CFDI33145"),
		CFDI33146("CFDI33146"),
		CFDI33147("CFDI33147"),
		CFDI33148("CFDI33148"),
		CFDI33149("CFDI33149"),
		CFDI33150("CFDI33150"),
		CFDI33151("CFDI33151"),
		CFDI33152("CFDI33152"),
		CFDI33153("CFDI33153"),
		CFDI33154("CFDI33154"),
		CFDI33155("CFDI33155"),
		CFDI33156("CFDI33156"),
		CFDI33157("CFDI33157"),
		CFDI33158("CFDI33158"),
		CFDI33159("CFDI33159"),
		CFDI33160("CFDI33160"),
		CFDI33161("CFDI33161"),
		CFDI33162("CFDI33162"),
		CFDI33163("CFDI33163"),
		CFDI33164("CFDI33164"),
		CFDI33165("CFDI33165"),
		CFDI33166("CFDI33166"),
		CFDI33167("CFDI33167"),
		CFDI33168("CFDI33168"),
		CFDI33169("CFDI33169"),
		CFDI33170("CFDI33170"),
		CFDI33171("CFDI33171"),
		CFDI33172("CFDI33172"),
		CFDI33173("CFDI33173"),
		CFDI33174("CFDI33174"),
		CFDI33175("CFDI33175"),
		CFDI33176("CFDI33176"),
		CFDI33177("CFDI33177"),
		CFDI33178("CFDI33178"),
		CFDI33179("CFDI33179"),
		CFDI33180("CFDI33180"),
		CFDI33181("CFDI33181"),
		CFDI33182("CFDI33182"),
		CFDI33183("CFDI33183"),
		CFDI33184("CFDI33184"),
		CFDI33185("CFDI33185"),
		CFDI33186("CFDI33186"),
		CFDI33187("CFDI33187"),
		CFDI33188("CFDI33188"),
		CFDI33189("CFDI33189"),
		CFDI33190("CFDI33190"),
		CFDI33191("CFDI33191"),
		CFDI33192("CFDI33192"),
		CFDI33193("CFDI33193"),
		CFDI33194("CFDI33194"),
		CFDI33195("CFDI33195"),
		CFDI33197("CFDI33197"),
		
		// COMPLEMENTO PAGO
		CRP101("CRP101"),
		CRP102("CRP102"),
		CRP103("CRP103"),
		CRP104("CRP104"),
		CRP105("CRP105"),
		CRP106("CRP106"),
		CRP107("CRP107"),
		CRP108("CRP108"),
		CRP109("CRP109"),
		CRP110("CRP110"),
		CRP111("CRP111"),
		CRP112("CRP112"),
		CRP113("CRP113"),
		CRP114("CRP114"),
		CRP115("CRP115"),
		CRP116("CRP116"),
		CRP117("CRP117"),
		CRP118("CRP118"),
		CRP119("CRP119"),
		CRP120("CRP120"),
		CRP121("CRP121"),
		CRP122("CRP122"),
		CRP201("CRP201"),
		CRP202("CRP201"),
		CRP203("CRP203"),
		CRP204("CRP204"),
		CRP205("CRP205"),
		CRP206("CRP206"),
		CRP207("CRP207"),
		CRP208("CRP208"),
		CRP209("CRP209"),
		CRP210("CRP210"),
		CRP211("CRP211"),
		CRP212("CRP212"),
		CRP213("CRP213"),
		CRP214("CRP214"),
		CRP215("CRP215"),
		CRP216("CRP216"),
		CRP217("CRP217"),
		CRP218("CRP218"),
		CRP219("CRP219"),
		CRP220("CRP220"),
		CRP221("CRP221"),
		CRP222("CRP222"),
		CRP223("CRP223"),
		CRP224("CRP224"),
		CRP225("CRP225"),
		CRP226("CRP226"),
		CRP227("CRP227"),
		CRP228("CRP228"),
		CRP229("CRP229"),
		CRP230("CRP230"),
		CRP231("CRP231"),
		CRP232("CRP232"),
		CRP233("CRP233"),
		CRP234("CRP234"),
		CRP235("CRP235"),
		CRP236("CRP236"),
		CRP237("CRP237"),
		CRP238("CRP238"),
		CRP239("CRP239"),
		CRP299("CRP299"),
		CRP20238("CRP20238"),
		
		// COMPLEMENTO NOMINA
		NOM101("NOM101"),
		NOM102("NOM102"),
		NOM103("NOM103"),
		NOM104("NOM104"),
		NOM105("NOM105"),
		NOM106("NOM106"),
		NOM107("NOM107"),
		NOM108("NOM108"),
		NOM109("NOM109"),
		NOM110("NOM110"),
		NOM111("NOM111"),
		NOM112("NOM112"),
		NOM113("NOM113"),
		NOM114("NOM114"),
		NOM115("NOM115"),
		NOM116("NOM116"),
		NOM117("NOM117"),
		NOM118("NOM118"),
		NOM119("NOM119"),
		NOM120("NOM120"),
		NOM121("NOM121"),
		NOM122("NOM122"),
		NOM123("NOM123"),
		NOM124("NOM124"),
		NOM125("NOM125"),
		NOM126("NOM126"),
		NOM127("NOM127"),
		NOM128("NOM128"),
		NOM129("NOM129"),
		NOM130("NOM130"),
		NOM131("NOM131"),
		NOM132("NOM132"),
		NOM133("NOM133"),
		NOM134("NOM134"),
		NOM135("NOM135"),
		NOM136("NOM136"),
		NOM137("NOM137"),
		NOM138("NOM138"),
		NOM139("NOM139"),
		NOM140("NOM140"),
		NOM141("NOM141"),
		NOM142("NOM142"),
		NOM143("NOM143"),
		NOM144("NOM144"),
		NOM145("NOM145"),
		NOM146("NOM146"),
		NOM147("NOM147"),
		NOM148("NOM148"),
		NOM149("NOM149"),
		NOM150("NOM150"),
		NOM151("NOM151"),
		NOM152("NOM152"),
		NOM153("NOM153"),
		NOM154("NOM154"),
		NOM155("NOM155"),
		NOM156("NOM156"),
		NOM157("NOM157"),
		NOM158("NOM158"),
		NOM159("NOM159"),
		NOM160("NOM160"),
		NOM161("NOM161"),
		NOM162("NOM162"),
		NOM163("NOM163"),
		NOM164("NOM164"),
		NOM165("NOM165"),
		NOM166("NOM166"),
		NOM167("NOM167"),
		NOM168("NOM168"),
		NOM169("NOM169"),
		NOM170("NOM170"),
		NOM171("NOM171"),
		NOM172("NOM172"),
		NOM173("NOM173"),
		NOM174("NOM174"),
		NOM175("NOM175"),
		NOM176("NOM176"),
		NOM177("NOM177"),
		NOM178("NOM178"),
		NOM179("NOM179"),
		NOM180("NOM180"),
		NOM181("NOM181"),
		NOM182("NOM182"),
		NOM183("NOM183"),
		NOM184("NOM184"),
		NOM185("NOM185"),
		NOM186("NOM186"),
		NOM187("NOM187"),
		NOM188("NOM188"),
		NOM189("NOM189"),
		NOM190("NOM190"),
		NOM191("NOM191"),
		NOM192("NOM192"),
		NOM193("NOM193"),
		NOM194("NOM194"),
		NOM195("NOM195"),
		NOM196("NOM196"),
		NOM197("NOM197"),
		NOM198("NOM198"),
		NOM199("NOM199"),
		NOM200("NOM200"),
		NOM201("NOM201"),
		NOM202("NOM202"),
		NOM203("NOM203"),
		NOM204("NOM204"),
		NOM205("NOM205"),
		NOM206("NOM206"),
		NOM207("NOM207"),
		NOM208("NOM208"),
		NOM209("NOM209"),
		NOM210("NOM210"),
		NOM211("NOM211"),
		NOM212("NOM212"),
		NOM213("NOM213"),
		NOM214("NOM214"),
		NOM215("NOM215"),
		NOM216("NOM216"),
		NOM217("NOM217"),
		NOM218("NOM218"),
		NOM219("NOM219"),
		NOM220("NOM220"),
		NOM221("NOM221"),
		NOM222("NOM222"),
		NOM223("NOM223"),
		NOM224("NOM224"),
		NOM225("NOM225"),
		
		// Region fronteriza norte
		CFDI33196CP("CFDI33196CP"),
		CFDI33196CPS("CFDI33196CPS"),
		CFDI33196RFC("CFDI33196RFC");
		
		private final String value;

		ErrorSource(String v) {
	        value = v;
	    }

	    public String value() {
	        return value;
	    }

	    public static ErrorSource fromValue(String v) {
	        return valueOf(v);
	    }
	}
}