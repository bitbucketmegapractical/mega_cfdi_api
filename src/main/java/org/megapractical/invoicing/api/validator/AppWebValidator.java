package org.megapractical.invoicing.api.validator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppWebValidator {
	private boolean error;
	private String errorCode;
	private String errorDescription;
	private String errorMessage;
	private String activateTab;
}