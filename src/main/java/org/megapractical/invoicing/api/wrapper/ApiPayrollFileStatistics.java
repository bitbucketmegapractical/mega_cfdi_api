package org.megapractical.invoicing.api.wrapper;

import java.util.List;

import org.megapractical.invoicing.api.common.ApiPayrollFileHeader;
import org.megapractical.invoicing.api.common.ApiPayrollFileSubHeader;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;

public class ApiPayrollFileStatistics {

	// Listado con todas las tramas validas
	private List<String[]> entries;
	
	//##### Total de comprobantes en el fichero
	//##### NOTA: Cada linea (no header, no subheader) con informacion representa una trama
	private Integer totalEntries;
	
	//##### Total de objetos despues de validar
	private Integer singleEntrie;
	
	//##### Header
	private ApiPayrollFileHeader payrollFileHeader;
	
	//##### Subheader
	private ApiPayrollFileSubHeader payrollFileSubHeader;
	
	//##### Estadisticas del archivo
	private ArchivoNominaEstadisticaEntity payrollFileStatistics;
	
	/* Getters and Setters */
	public List<String[]> getEntries() {
		return entries;
	}

	public void setEntries(List<String[]> entries) {
		this.entries = entries;
	}

	public Integer getSingleEntrie() {
		return singleEntrie;
	}

	public void setSingleEntrie(Integer singleEntrie) {
		this.singleEntrie = singleEntrie;
	}

	public Integer getTotalEntries() {
		return totalEntries;
	}

	public void setTotalEntries(Integer totalEntries) {
		this.totalEntries = totalEntries;
	}

	public ArchivoNominaEstadisticaEntity getPayrollFileStatistics() {
		return payrollFileStatistics;
	}

	public void setPayrollFileStatistics(ArchivoNominaEstadisticaEntity payrollFileStatistics) {
		this.payrollFileStatistics = payrollFileStatistics;
	}

	public ApiPayrollFileHeader getPayrollFileHeader() {
		return payrollFileHeader;
	}

	public void setPayrollFileHeader(ApiPayrollFileHeader payrollFileHeader) {
		this.payrollFileHeader = payrollFileHeader;
	}

	public ApiPayrollFileSubHeader getPayrollFileSubHeader() {
		return payrollFileSubHeader;
	}

	public void setPayrollFileSubHeader(ApiPayrollFileSubHeader payrollFileSubHeader) {
		this.payrollFileSubHeader = payrollFileSubHeader;
	}	
}