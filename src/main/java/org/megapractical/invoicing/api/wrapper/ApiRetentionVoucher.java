package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiRetentionVoucher {
	// Comprobante
	private Retenciones retencion;
	// Informacion para generacion de Pdf
	private ApiRetention apiRetention;
}