package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.datatype.CancellationType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiCancellationRequest extends ApiCancellation {
	/**
	 * (Requerido) - Tipo
	 */
	CancellationType type;

	/**
	 * (Requerido) - Rfc del receptor
	 */
	private String receiverRfc;

	/**
	 * (Requerido) - UUID del comprobante
	 */
	private String uuid;

	/**
	 * (Requerido) - Total del comprobante
	 */
	private String voucherTotal;

	/**
	 * (Requerido) - Clave para expresar el motivo de la cancelación
	 */
	private String reason;

	/**
	 * (Opcional) - UUID del comprobante que sustituye
	 */
	private String substitutionUuid;

	/**
	 * (Requerido) - Indica si se utiliza el servicio fake para la verificación del
	 * status del cfdi
	 */
	private boolean useCheckStatusFake;
}