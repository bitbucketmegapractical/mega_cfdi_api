package org.megapractical.invoicing.api.wrapper;

import java.io.File;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiFileDetail {
	private File file;
	private String originalFilename;
	private String fileName;
	private String fileExt;
	@Default
	private boolean hasError = false;
	
	public static ApiFileDetail withError() {
		return ApiFileDetail.builder().hasError(true).build();
	}
}