package org.megapractical.invoicing.api.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPaymentPayments {
	private String paymentNumber;
	private String paymentDate;
	private String paymentWay;
	private String currency;
	private String amount;
	private String changeType;
	private String paymentCertificate;
	private String paymentSeal;
	private String originalString;
	private String operationNumber;
	private String sourceAccountRfc;
	private String targetAccountRfc;
	private String bankName;
	private String payerAccount;
	private String receiverAccount;
	private String stringType;
}