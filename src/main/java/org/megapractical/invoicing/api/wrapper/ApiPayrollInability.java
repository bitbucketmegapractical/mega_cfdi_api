package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.TipoIncapacidadEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollInability {
	private String inabilityDays;
	private TipoIncapacidadEntity inabilityType;
	private String inabilityTypeCode;
	private String inabilityTypeValue;
	private String monetaryAmount;
}