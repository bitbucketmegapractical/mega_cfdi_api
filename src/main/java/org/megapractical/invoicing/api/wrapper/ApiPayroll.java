package org.megapractical.invoicing.api.wrapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiPayrollDeduction.PayrollDeduction;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPensionRetirement;
import org.megapractical.invoicing.api.wrapper.ApiPayrollPerception.PayrollPerception;
import org.megapractical.invoicing.dal.bean.datatype.StorageType;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoNominaEntity;
import org.megapractical.invoicing.voucher.catalogs.CTipoNomina;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayroll {

	public static final String VERSION = "1.2";

	private TipoNominaEntity payrollType;
	private CTipoNomina payrollTypeCatalog;
	private String payrollTypeCode;
	private String payrollTypeValue;
	private String payrollTypePrint;
	private LocalDate paymentDate;
	private String paymentDateString;
	private LocalDate paymentIntialDate;
	private String paymentIntialDateString;
	private LocalDate paymentEndDate;
	private String paymentEndDateString;
	private String numberDaysPaid;
	private String totalPerceptions;
	private String totalDeductions;
	private String totalOtherPayments;
	private String xmlPath;
	private String pdfPath;
	private boolean cancelled;
	private ApiPayrollEmitter payrollEmitter;
	private ApiPayrollReceiver payrollReceiver;
	private ApiPayrollPerception payrollPerception;
	private ApiPayrollDeduction payrollDeduction;
	@Getter(value = AccessLevel.NONE)
	private List<ApiPayrollOtherPayment> payrollOtherPayments;
	@Getter(value = AccessLevel.NONE)
	private List<ApiPayrollInability> payrollInabilities;
	private ApiCfdi apiCfdi;
	private ArchivoNominaEntity payrollFile;
	private StorageType storageType;
	private String xmlPdfName;

	public void addPerception(PayrollPerception perception) {
		if (this.payrollPerception != null) {
			this.payrollPerception.getPayrollPerceptions().add(perception);
		}
	}

	public void addPensionRetirement(PayrollPensionRetirement pensionRetirement) {
		if (this.payrollPerception != null) {
			this.payrollPerception.setPayrollPensionRetirement(pensionRetirement);
		}
	}

	public void addDeduction(PayrollDeduction deduction) {
		if (this.payrollDeduction != null) {
			this.payrollDeduction.getPayrollDeductions().add(deduction);
		}
	}

	/* Getters and Setters */
	public List<ApiPayrollOtherPayment> getPayrollOtherPayments() {
		if (payrollOtherPayments == null) {
			payrollOtherPayments = new ArrayList<>();
		}
		return payrollOtherPayments;
	}

	public List<ApiPayrollInability> getPayrollInabilities() {
		if (payrollInabilities == null) {
			payrollInabilities = new ArrayList<>();
		}
		return payrollInabilities;
	}

}