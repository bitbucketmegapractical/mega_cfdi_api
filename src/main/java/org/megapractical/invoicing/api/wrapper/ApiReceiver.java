package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.PaisEntity;
import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsoCfdiEntity;
import org.megapractical.invoicing.voucher.catalogs.CPais;
import org.megapractical.invoicing.voucher.catalogs.CUsoCFDI;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiReceiver {
	// @Required
    private String rfc;
    
    // @Required
    private UsoCfdiEntity usoCFDI;
    private CUsoCFDI usoCfdiCatalog;
    
    // @Nullable
    private String nombreRazonSocial;
    
    // @NUllable
    private String residente;
    
    // @Nullable
    private PaisEntity residenciaFiscal;
    private CPais countryCatalog;
    
    // @Nullable
    private String numRegIdTrib;
    
    // @Required
    private String domicilioFiscal;
    
    // @Required
    private RegimenFiscalEntity taxRegimeEntity;
    private String taxRegime;
    
    // @Additional
    // @Nullable
    private String correoElectronico;
    private String curp;
    private Long customerSelected;
}