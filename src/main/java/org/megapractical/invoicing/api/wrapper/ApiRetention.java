package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiRetention {
	@Default
	private String version = "2.0";
	
	// Retencion
	private String initialMonth;
	private String endMonth;
	private String year;
	private String folio;
	private String retentionKey;
	private String expeditionPlace;
	private String description;
	
	// Emisor
	private String emitterRfc;
	private String emitterBusinessName;
	private String emitterTaxRegime;
	
	// Receptor
	private String receiverNationality;
	private String receiverNatRfc;
	private String receiverNatBusinessName;
	private String receiverNatCurp;
	private String receiverNatPostalCode;
	private String receiverExtBusinessName;
	private String receiverExtNumber;
	private String receiverEmail;
	
	// Documentos relacionados
	private String relatedDocumentType;
	private String relatedDocumentPlainValue;
	private String relatedDocumentUuid;
	
	// Impuestos retenidos
	@Getter(AccessLevel.NONE)
	private List<ApiRetImpRetenido> apiRetImpRetenidos;
	
	// Totales
	private String totalOperationAmount;
	private String totalAmountTaxed;
	private String totalAmountExempt;
	private String totalAmountWithheld;
	private String quarterlyProfit;
	private String correspondingISR;
	
	// Complemento Dividendos
	private String cveTipDivOUtil;
	private String montISRAcredRetMexico;
	private String montISRAcredRetExtranjero;
	private String montRetExtDivExt;
	private String tipoSocDistrDiv;
	private String montISRAcredNal;
	private String montDivAcumNal;
	private String montDivAcumExt;
	private String proporcionRem;
	
	// Complemento Enajenacion de Acciones
	private String contract;
	private String profit;
	private String waste;
	
	// Complemento Intereses
	private String financeSystem;
	private String retirement;
	private String financialOperations;
	private String interestAmount;
	private String realInterest;
	private String interestWaste;
	
	// Complemento Operaciones con Derivados
	private String profitAmount;
	private String deductibleWaste;
	
	// Complemento Pagos a Extranjeros
	private boolean beneficiary;
	private String beneficiaryRfc;
	private String beneficiaryCurp;
	private String beneficiaryBusinessName;
	private String paymentConcept;
	private String conceptDescription;
	private String noBeneficiaryCountry;
	
	// Complemento Sector Financiero
	private String fideicomId;
	private String fideicomName;
	private String fideicomDescription;
	
	// Certificado
	private String certificateEmitterNumber;
	
	private String xmlPath;
	private String pdfPath;
	private StampResponse stampResponse;
	
	private ArchivoRetencionEntity retentionFile;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ApiRetImpRetenido {
		private String index;
		private String base;
		private String tax;
		private String taxValue;
		private String amount;
		private String paymentType;
	}
	
	public List<ApiRetImpRetenido> getApiRetImpRetenidos() {
		if(apiRetImpRetenidos == null) {
			apiRetImpRetenidos = new ArrayList<>();
		}
		return apiRetImpRetenidos;
	}
	
}