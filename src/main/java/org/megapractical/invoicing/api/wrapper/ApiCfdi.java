package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptCustomsInformation;
import org.megapractical.invoicing.api.wrapper.ApiConcept.ConceptPropertyAccount;
import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteSerieFolioEntity;
import org.megapractical.invoicing.dal.bean.jpa.FormaPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MetodoPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;
import org.megapractical.invoicing.voucher.catalogs.CMetodoPago;
import org.megapractical.invoicing.voucher.catalogs.CMoneda;
import org.megapractical.invoicing.voucher.catalogs.CTipoDeComprobante;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiCfdi {

	// Comprobante v4
	private ApiVoucher voucher;
	
	// Complemento
	private ApiCmp complement;
	
	// Contribuyente emisor
	private ApiEmitter emitter;
	
	// Contribuyente receptor
	private ApiReceiver receiver;
	
	// Conceptos
	@Getter(AccessLevel.NONE)
	private List<ApiConcept> concepts;
	@Getter(AccessLevel.NONE)
	private List<ConceptCustomsInformation> customsInformations;
	@Getter(AccessLevel.NONE)
	private List<ConceptPropertyAccount> propertyAccounts;
	
	// Timbrado
	@Setter(AccessLevel.NONE)
	private StampResponse stampResponse;
	
	// Moneda
	private MonedaEntity currency;
	private CMoneda currencyCatalog;
	
	// Metodo pago
	private MetodoPagoEntity paymentMethod;
	private CMetodoPago paymentMethodCatalog;
	
	// Forma pago
	private FormaPagoEntity paymentWay;
	
	// Tipo comprobante
	private TipoComprobanteEntity voucherType;
	private CTipoDeComprobante voucherTypeCatalog;
	
	// Codigo postal
	private CodigoPostalEntity postalCode;
	private String expeditionPlace;
	
	private String xmlPdfName;
	
	// Ruta XML
	private String xmlPath;
	private String xmlDbPath;
	
	// Ruta PDF
	private String pdfPath;
	private String pdfDbPath;
	
	// QR
	private String qr;
	
	@Getter(AccessLevel.NONE)
	private List<ApiConceptTaxes> transferred;
	@Getter(AccessLevel.NONE)
	private List<ApiConceptTaxes> withheld;
	
	private ContribuyenteCertificadoEntity certificate;
	private PreferenciasEntity preferences;
	private ContribuyenteSerieFolioEntity serieSheet;
	
	// Leyenda
	private String legend;
	
	// Pdf additionals
	@Getter(AccessLevel.NONE)
	private Map<String, Object> additionals;
	
	/*Getters and Setters*/
	public List<ApiConcept> getConcepts() {
		if (concepts == null) {
			concepts = new ArrayList<>();
		}
		return concepts;
	}

	public List<ApiConceptTaxes> getTransferred() {
		if (transferred == null) {
			transferred = new ArrayList<>();
		}
		return transferred;
	}

	public List<ApiConceptTaxes> getWithheld() {
		if (withheld == null) {
			withheld = new ArrayList<>();
		}
		return withheld;
	}

	public List<ConceptCustomsInformation> getCustomsInformations() {
		if (customsInformations == null) {
			customsInformations = new ArrayList<>();
		}
		return customsInformations;
	}

	public List<ConceptPropertyAccount> getPropertyAccounts() {
		if (propertyAccounts == null) {
			propertyAccounts = new ArrayList<>();
		}
		return propertyAccounts;
	}

	public void setStampResponse(StampResponse stampResponse) {
		if (stampResponse != null && stampResponse.isStamped() && this.voucher != null) {
			this.voucher.setTaxSheet(stampResponse.getUuid());
			this.voucher.setCertificateEmitterNumber(stampResponse.getCertificateEmitterNumber());
			this.voucher.setCertificateSatNumber(stampResponse.getCertificateSatNumber());
			this.voucher.setEmitterSeal(stampResponse.getEmitterSeal());
			this.voucher.setSatSeal(stampResponse.getSatSeal());
			this.voucher.setDateTimeStamp(stampResponse.getDateStamp());
			this.voucher.setOriginalString(stampResponse.getOriginalString());
			this.qr = stampResponse.getQrCode();
		}
		this.stampResponse = stampResponse;
	}

	public Map<String, Object> getAdditionals() {
		if (additionals == null) {
			additionals = new HashMap<>();
		}
		return additionals;
	}
	
}