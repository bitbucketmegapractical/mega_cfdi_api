package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;

public class ApiRetentionStamp {

	//##### Contribuyente emisor
	private ContribuyenteEntity taxpayer;
	
	//##### Usuario
	private UsuarioEntity user;
	
	//##### Ip
	private String ip;
	
	//##### Url acceso
	private String urlAcceso;
	
	//##### Emisor
	private ApiEmitter apiEmitter;
	
	//##### Configuraciones de archivo
	private ArchivoRetencionConfiguracionEntity retentionFileConfiguration;
	
	//##### Archivo de retencion registrado en DB
	private ArchivoRetencionEntity retentionFile;
	
	//##### Informacion para el timbrado
	private StampProperties stampProperties;
	
	//##### Informacion de timbres del emisor
	private TimbreEntity stampDetail;
	
	//##### Ruta absoluta del archivo
	private String absolutePath;
	
	//##### Ruta fisica del archivo
	private String uploadPath;
	
	//##### Ruta para db
	private String dbPath;

	/*Getters and Setters*/
	public ApiEmitter getApiEmitter() {
		return apiEmitter;
	}

	public void setApiEmitter(ApiEmitter apiEmitter) {
		this.apiEmitter = apiEmitter;
	}

	public ArchivoRetencionConfiguracionEntity getRetentionFileConfiguration() {
		return retentionFileConfiguration;
	}

	public void setRetentionFileConfiguration(ArchivoRetencionConfiguracionEntity retentionFileConfiguration) {
		this.retentionFileConfiguration = retentionFileConfiguration;
	}

	public ArchivoRetencionEntity getRetentionFile() {
		return retentionFile;
	}

	public void setRetentionFile(ArchivoRetencionEntity retentionFile) {
		this.retentionFile = retentionFile;
	}

	public StampProperties getStampProperties() {
		return stampProperties;
	}

	public void setStampProperties(StampProperties stampProperties) {
		this.stampProperties = stampProperties;
	}

	public TimbreEntity getStampDetail() {
		return stampDetail;
	}

	public void setStampDetail(TimbreEntity stampDetail) {
		this.stampDetail = stampDetail;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getDbPath() {
		return dbPath;
	}

	public void setDbPath(String dbPath) {
		this.dbPath = dbPath;
	}

	public ContribuyenteEntity getTaxpayer() {
		return taxpayer;
	}

	public void setTaxpayer(ContribuyenteEntity taxpayer) {
		this.taxpayer = taxpayer;
	}

	public UsuarioEntity getUser() {
		return user;
	}

	public void setUser(UsuarioEntity user) {
		this.user = user;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUrlAcceso() {
		return urlAcceso;
	}

	public void setUrlAcceso(String urlAcceso) {
		this.urlAcceso = urlAcceso;
	}
}