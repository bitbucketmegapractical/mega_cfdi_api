package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.sat.integration.v40.Comprobante;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollVoucher {
	//##### Comprobante
	private Comprobante voucher;
	//##### Informacion para generacion de Pdf
	private ApiCfdi cfdi;
	private ApiPayroll payroll;
}