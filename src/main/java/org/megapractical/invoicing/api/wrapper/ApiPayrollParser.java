package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.NominaFicheroEntity;

public class ApiPayrollParser {
	
	//##### Informacion del parser instanciado
	private NominaFicheroEntity payrollDataFile;
		
	//##### Listado de comprobantes
	private List<ApiPayrollVoucher> vouchers;
	
	//##### Listado de posibles errores
	private List<ApiPayrollError> errors;
		
	//##### Informacion general del fichero
	private ApiPayrollFileStatistics statistics;
	
	//##### Informacion complementaria para la generacion del comprobante y el timbrado
	private ApiPayrollStamp payrollStamp;

	/*Getters and Setters*/
	public NominaFicheroEntity getPayrollDataFile() {
		return payrollDataFile;
	}

	public void setPayrollDataFile(NominaFicheroEntity payrollDataFile) {
		this.payrollDataFile = payrollDataFile;
	}

	public List<ApiPayrollVoucher> getVouchers() {
		if(vouchers == null)
			vouchers = new ArrayList<>();
		return vouchers;
	}

	public void setVouchers(List<ApiPayrollVoucher> vouchers) {
		this.vouchers = vouchers;
	}

	public List<ApiPayrollError> getErrors() {
		if(errors == null)
			errors = new ArrayList<>();
		return errors;
	}

	public void setErrors(List<ApiPayrollError> errors) {
		this.errors = errors;
	}

	public ApiPayrollFileStatistics getStatistics() {
		return statistics;
	}

	public void setStatistics(ApiPayrollFileStatistics statistics) {
		this.statistics = statistics;
	}

	public ApiPayrollStamp getPayrollStamp() {
		return payrollStamp;
	}

	public void setPayrollStamp(ApiPayrollStamp payrollStamp) {
		this.payrollStamp = payrollStamp;
	}	
}