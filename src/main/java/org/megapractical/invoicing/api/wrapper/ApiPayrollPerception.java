package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoHoraEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoPercepcionEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollPerception {
	private String totalSalaries;
	private String totalSeparationCompensation;
	private String totalPensionRetirement;
	private String totalTaxed;
	private String totalExempt;
	@Getter(value = AccessLevel.NONE)
	private List<PayrollPerception> payrollPerceptions;
	private PayrollPensionRetirement payrollPensionRetirement;
	private PayrollSeparationCompensation payrollSeparationCompensation;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PayrollPerception {
		private TipoPercepcionEntity perceptionType;
		private String perceptionTypeCode;
		private String perceptionTypeValue;
		private String perceptionTypePrint;
		private String key;
		private String concept;
		private String taxableAmount;
		private String exemptAmount;
		private PayrollActionTitle payrollActionTitle;
		@Getter(value = AccessLevel.NONE)
		private List<PayrollExtraHour> payrollExtraHours;

		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class PayrollActionTitle {
			private String marketValue;
			private String priceGranted;
		}

		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class PayrollExtraHour {
			private String days;
			private TipoHoraEntity hourType;
			private String hourTypeCode;
			private String hourTypeValue;
			private String hourTypePrint;
			private String extraHour;
			private String amountPaid;
		}

		/* Getters and Setters */
		public List<PayrollExtraHour> getPayrollExtraHours() {
			if (payrollExtraHours == null)
				payrollExtraHours = new ArrayList<>();
			return payrollExtraHours;
		}
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PayrollPensionRetirement {
		private String totalExhibition;
		private String totalPartiality;
		private String dailyAmount;
		private String incomeAccumulate;
		private String incomeNoAccumulate;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PayrollSeparationCompensation {
		private String totalPaid;
		private String exercisesNumberService;
		private String lastSalary;
		private String incomeAccumulate;
		private String incomeNoAccumulate;
	}

	/* Getters and Setters */
	public List<PayrollPerception> getPayrollPerceptions() {
		if (payrollPerceptions == null)
			payrollPerceptions = new ArrayList<>();
		return payrollPerceptions;
	}

}