package org.megapractical.invoicing.api.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPaymentRelatedDocuments {
	private String paymentNumber;
	private String documentId;
	private String serie;
	private String sheet;
	private String currency;
	private String changeType;
	private String partiality;
	private String amountPartialityBefore;
	private String amountPaid;
	private String difference;
	private String taxObject;
}