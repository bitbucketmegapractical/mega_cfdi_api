package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiCancellationParser {
	@Getter(AccessLevel.NONE)
	private List<ApiCancellationRequest> cfdis;

	@Getter(AccessLevel.NONE)
	private List<ApiCancellationRequest> retentions;
	
	private String parserErrorCause;

	/* Getters */
	public List<ApiCancellationRequest> getCfdis() {
		if (cfdis == null) {
			cfdis = new ArrayList<>();
		}
		return cfdis;
	}

	public List<ApiCancellationRequest> getRetentions() {
		if (retentions == null) {
			retentions = new ArrayList<>();
		}
		return retentions;
	}	

}