package org.megapractical.invoicing.api.wrapper;

public class ApiRetentionStampResponse {

	//##### Comprobante
	//##### Si no es null, el comprobante no fue timbrado
	private ApiRetentionVoucher voucher;
	
	//##### Error al timbrar el comprobante
	private ApiRetentionError error;
	
	//##### Información de las retenciones del archivo
	private String periodInitialMonth;
	private String periodEndMonth;
	private String periodYear;
	
	/*Getters and Setters*/
	public ApiRetentionVoucher getVoucher() {
		return voucher;
	}

	public void setVoucher(ApiRetentionVoucher voucher) {
		this.voucher = voucher;
	}

	public ApiRetentionError getError() {
		return error;
	}

	public void setError(ApiRetentionError error) {
		this.error = error;
	}

	public String getPeriodInitialMonth() {
		return periodInitialMonth;
	}

	public void setPeriodInitialMonth(String periodInitialMonth) {
		this.periodInitialMonth = periodInitialMonth;
	}

	public String getPeriodEndMonth() {
		return periodEndMonth;
	}

	public void setPeriodEndMonth(String periodEndMonth) {
		this.periodEndMonth = periodEndMonth;
	}

	public String getPeriodYear() {
		return periodYear;
	}

	public void setPeriodYear(String periodYear) {
		this.periodYear = periodYear;
	}	
}