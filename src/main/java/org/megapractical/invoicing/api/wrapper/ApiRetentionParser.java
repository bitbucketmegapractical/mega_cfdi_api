package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.RetencionFicheroEntity;

public class ApiRetentionParser {
	
	//##### Informacion del parser instanciado
	private RetencionFicheroEntity retentionDataFile;
		
	//##### Listado de tramas
	private List<String[]> entries;
	
	//##### Listado de comprobantes
	private List<ApiRetentionVoucher> vouchers;
	
	//##### Listado de posibles errores
	private List<ApiRetentionError> errors;
		
	//##### Informacion complementaria para la generacion del comprobante y el timbrado
	private ApiRetentionStamp retentionStamp;

	/*Getters and Setters*/
	public RetencionFicheroEntity getRetentionDataFile() {
		return retentionDataFile;
	}

	public void setRetentionDataFile(RetencionFicheroEntity retentionDataFile) {
		this.retentionDataFile = retentionDataFile;
	}

	public List<ApiRetentionVoucher> getVouchers() {
		if(vouchers == null)
			vouchers = new ArrayList<>();
		return vouchers;
	}

	public void setVouchers(List<ApiRetentionVoucher> vouchers) {
		this.vouchers = vouchers;
	}

	public List<ApiRetentionError> getErrors() {
		if(errors == null)
			errors = new ArrayList<>();
		return errors;
	}

	public void setErrors(List<ApiRetentionError> errors) {
		this.errors = errors;
	}

	public ApiRetentionStamp getRetentionStamp() {
		return retentionStamp;
	}

	public void setRetentionStamp(ApiRetentionStamp retentionStamp) {
		this.retentionStamp = retentionStamp;
	}

	public List<String[]> getEntries() {
		return entries;
	}

	public void setEntries(List<String[]> entries) {
		this.entries = entries;
	}	
}