package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.TipoOtroPagoEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollOtherPayment {
	private TipoOtroPagoEntity otherPaymentType;
	private String otherPaymentTypeCode;
	private String otherPaymentTypeValue;
	private String otherPaymentTypePrint;
	private String key;
	private String concept;
	private String amount;
	private EmploymentSubsidy employmentSubsidy;
	private CompensationCreditBalances compensationCreditBalance;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class EmploymentSubsidy {
		private String subsidyCaused;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CompensationCreditBalances {
		private String positiveBalance;
		private String exercise;
		private String remainingPositiveBalance;
	}
}