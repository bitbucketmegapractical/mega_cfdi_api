package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.api.converter.ApiFileErrorConverter;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoTramaEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.val;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiPaymentError extends ApiFileError {
	//##### Trama asociada al error
	private ComplementoPagoTramaEntity entry;
	//##### Campo asociado al error
	private ComplementoPagoCampoEntity field;
	
	public ApiPaymentError(Integer line, Integer entryLength, Integer entryLengthOnFile, String description) {
		super(line, entryLength, entryLengthOnFile, description);
	}
	
	public ApiPaymentError(ApiReceiver receiver) {
		super(receiver);
	}
	
	public static ApiPaymentError contentError(ComplementoPagoCampoEntity field, String[] entry, int position) {
		return ApiFileErrorConverter.convert(field, entry, position);
	}
	
	public static ApiPaymentError lenghtError(ComplementoPagoTramaEntity entry, String[] item, int line) {
		val description = "Longitud de trama no válida.";
		val paymentError = new ApiPaymentError(line, entry.getLongitud(), item.length, description);
		paymentError.setEntry(entry);
		return paymentError;
	}
	
	public static ApiPaymentError stampStatusError(ApiReceiver receiver, String stampErrorMessage) {
		val paymentError = new ApiPaymentError(receiver);
		paymentError.setStampError(Boolean.TRUE);
		paymentError.setStampErrorCode("ERR0163");
		paymentError.setStampErrorMessage(stampErrorMessage);
		paymentError.setDescription("Timbres insuficientes.");
		return paymentError;
	}
	
	public static ApiPaymentError stampError(ApiReceiver receiver, String stampErrorMessage) {
		val paymentError = new ApiPaymentError(receiver);
		paymentError.setStampError(Boolean.TRUE);
		paymentError.setStampErrorCode("ERR0037");
		paymentError.setStampErrorMessage(stampErrorMessage);
		paymentError.setDescription("Error de timbrado.");
		return paymentError;
	}
	
}