package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;

public class ApiPayrollStampResponse {

	//##### Comprobante
	//##### Si no es null, el comprobante no fue timbrado
	private ApiPayrollVoucher voucher;
	
	//##### Error al timbrar el comprobante
	private ApiPayrollError error;
	
	//##### Estadisticas del archivo
	ArchivoNominaEstadisticaEntity statistics;

	/*Getters and Setters*/
	public ApiPayrollVoucher getVoucher() {
		return voucher;
	}

	public void setVoucher(ApiPayrollVoucher voucher) {
		this.voucher = voucher;
	}

	public ApiPayrollError getError() {
		return error;
	}

	public void setError(ApiPayrollError error) {
		this.error = error;
	}

	public ArchivoNominaEstadisticaEntity getStatistics() {
		return statistics;
	}

	public void setStatistics(ArchivoNominaEstadisticaEntity statistics) {
		this.statistics = statistics;
	}	
}