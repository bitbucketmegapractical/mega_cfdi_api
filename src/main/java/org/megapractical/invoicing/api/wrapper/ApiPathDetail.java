package org.megapractical.invoicing.api.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPathDetail {
	private String xmlPath;
	private String xmlDb;
	private String pdfPath;
	private String pdfDb;
}