package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaConfiguracionEntity;

public class ApiPayrollFileConfiguration {

	private ArchivoNominaConfiguracionEntity payrollFileConfiguration;
	private String action;

	public ArchivoNominaConfiguracionEntity getPayrollFileConfiguration() {
		return payrollFileConfiguration;
	}

	public void setPayrollFileConfiguration(ArchivoNominaConfiguracionEntity payrollFileConfiguration) {
		this.payrollFileConfiguration = payrollFileConfiguration;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
}
