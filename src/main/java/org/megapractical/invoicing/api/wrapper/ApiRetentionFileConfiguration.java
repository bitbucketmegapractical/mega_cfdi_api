package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoRetencionConfiguracionEntity;

public class ApiRetentionFileConfiguration {

	private ArchivoRetencionConfiguracionEntity retentionFileConfiguration;
	private String action;

	public ArchivoRetencionConfiguracionEntity getRetentionFileConfiguration() {
		return retentionFileConfiguration;
	}

	public void setRetentionFileConfiguration(ArchivoRetencionConfiguracionEntity retentionFileConfiguration) {
		this.retentionFileConfiguration = retentionFileConfiguration;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
}
