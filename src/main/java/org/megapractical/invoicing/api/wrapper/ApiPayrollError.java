package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.NominaCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaTramaEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiPayrollError extends ApiFileError {
	// Trama asociada al error
	private NominaTramaEntity entry;
	// Campo asociado al error
	private NominaCampoEntity field;
}