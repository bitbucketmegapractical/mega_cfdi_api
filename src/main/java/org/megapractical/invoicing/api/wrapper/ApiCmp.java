package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiCmp {

	private String packageClass;

	// Complemento de recepcion de pagos
	private ApiPayment apiPayment;
	@Getter(AccessLevel.NONE)
	private List<ApiPaymentPayments> apiPaymentPayments;
	@Getter(AccessLevel.NONE)
	private List<ApiPaymentRelatedDocuments> apiPaymentRelatedDocuments;
	
	// Complemento Divisas
	private ApiForeignExchange foreignExchange;

	public ApiCmp(String packageClass, ApiPayment apiPayment) {
		this.packageClass = packageClass;
		this.apiPayment = apiPayment;
	}
	
	public ApiCmp(String packageClass, ApiForeignExchange foreignExchange) {
		this.packageClass = packageClass;
		this.foreignExchange = foreignExchange;
	}
	
	public List<ApiPaymentPayments> getApiCmpPaymentPayments() {
		if (apiPaymentPayments == null) {
			apiPaymentPayments = new ArrayList<>();
		}
		return apiPaymentPayments;
	}

	public List<ApiPaymentRelatedDocuments> getApiCmpPaymentRelatedDocuments() {
		if (apiPaymentRelatedDocuments == null) {
			apiPaymentRelatedDocuments = new ArrayList<>();
		}
		return apiPaymentRelatedDocuments;
	}

}