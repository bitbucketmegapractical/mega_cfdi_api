package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.dal.bean.jpa.OrigenRecursoEntity;
import org.megapractical.invoicing.voucher.catalogs.COrigenRecurso;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollEmitter {
	private String curp;
	private String employerRegistration;
	private String rfcPatronOrigin;
	private OrigenRecursoEntity resourceOrigin;
	private String resourceOriginCode;
	private String resourceOriginValue;
	private COrigenRecurso resourceOriginCatalog;
	private String ownResourceAmount;
}