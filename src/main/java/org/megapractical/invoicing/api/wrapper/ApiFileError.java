package org.megapractical.invoicing.api.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ApiFileError {
	// Linea del error
	private Integer line;
	
	// Valor del campo en el fichero
	private String fieldValue;
	
	// Posicion del campo en el fichero
	private Integer fieldPosition;
	
	// Longitud del campo
	private Integer fieldLength;
	
	// Longitud minima del campo
	private Integer fieldMinLength;
	
	// Longitud maxima del campo
	private Integer fieldMaxLength;
	
	// Longitud del campo en el fichero
	private Integer fieldLengthOnFile;
	
	//
	private String fieldRegularExpression;
		
	// Descripcion
	private String description;
	
	// Longitud de la trma
	private Integer entryLength;
	
	// Longitud de la trama en fichero	
	private Integer entryLengthOnFile;
	
	// Define si es un error de generacion de comprobante
	private boolean isVoucherGenerateError;
	
	// Descripcion del error de generacion de comprobante
	private String voucherGenerateErrorException;
	
	// Define si es un error de timbrado
	private boolean isStampError;
	
	// Codigo del error de timbrado
	private String stampErrorCode;
	
	// Mensaje del error de timbrado
	private String stampErrorMessage;
	
	// RFC del receptor
	private String receiverRfc;
	
	// Nombre del receptor
	private String receiverName;
	
	// Curp del receptor
	private String receiverCurp;
	
	// Define si es un error inesperado
	private boolean isUnexpectedError;
	
	// Define si es un error de valdiacion del contenido del xml
	private boolean isSAXParseException;
	
	// Mensaje del error de validacion 
	private String saxParseErrorMessage;
	
	// Descripcion del error de validacion
	private String saxParseErrorLocalizedMessage;
	
	private String validationErrorCode;
	private String validationErrorMessage;
	
	public ApiFileError(Integer line, Integer entryLength, Integer entryLengthOnFile, String description) {
		this.line = line;
		this.entryLength = entryLength;
		this.entryLengthOnFile = entryLengthOnFile;
		this.description = description;
	}
	
	public ApiFileError(ApiReceiver receiver) {
		this.receiverRfc = receiver.getRfc();
		this.receiverName = receiver.getNombreRazonSocial();
		this.receiverCurp = receiver.getCurp();
	}
	
}