package org.megapractical.invoicing.api.wrapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiForeignExchange {
	private static final String CURRENT_VERSION = "1.0";
	
	@Getter(AccessLevel.NONE)
	private String version;
	
	private String operationType;
	
	public String getVersion() {
		if (version == null) {
			version = CURRENT_VERSION;
		}
		return version;
	}
}