package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteCertificadoEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.PreferenciasEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPaymentStamp {
	
	// Contribuyente emisor
	private ContribuyenteEntity taxpayer;
	
	// Usuario
	private UsuarioEntity user;
	
	// Preferencias
	private PreferenciasEntity preferences;
	
	// Certificado
	private ContribuyenteCertificadoEntity certificate;
	
	// Ip
	private String ip;
	
	// Url acceso
	private String urlAcceso;
	
	// Emisor
	private ApiEmitter apiEmitter;
	
	// Configuraciones de archivo
	private ArchivoFacturaConfiguracionEntity paymentFileConfig;
	
	// Archivo de complemento de pago registrado en DB
	private ArchivoFacturaEntity invoiceFile;
	
	// Informacion general del fichero
	private ArchivoFacturaEstadisticaEntity statistics;
	
	// Informacion para el timbrado
	private StampProperties stampProperties;
	
	// Informacion de timbres del emisor
	private TimbreEntity stampDetail;
	
	// Ruta absoluta del archivo
	private String absolutePath;
	
	// Ruta fisica del archivo
	private String uploadPath;
	
	// Ruta para db
	private String dbPath;
}