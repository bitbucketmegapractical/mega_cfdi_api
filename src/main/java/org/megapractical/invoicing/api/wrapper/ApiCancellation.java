package org.megapractical.invoicing.api.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Maikel Guerra Ferrer
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ApiCancellation {
	/**
	 * (Requerido) - RFC del emisor
	 */
	private String emitterRfc;
	/**
	 * (Requerido) - Certificado del emisor en Base64
	 */
	private String b64Cer;
	
	/**
	 * (Requerido) - Key del emisor en Base64
	 */
	private String b64Key;
	
	/**
	 * (Requerido) - Contraseña del certificado
	 */
	private String password;
	
	/**
	 * (Requerido) - Ambiente
	 */
	private String environment;
}