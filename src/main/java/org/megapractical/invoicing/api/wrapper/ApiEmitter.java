package org.megapractical.invoicing.api.wrapper;

import java.io.File;
import java.util.Date;

import org.megapractical.invoicing.dal.bean.jpa.RegimenFiscalEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiEmitter {
	// @Nullable
	private String nombreRazonSocial;
	
	// @Required
	private RegimenFiscalEntity regimenFiscal;
	private String taxRegimeCode;
	
	// @Required
	private String rfc;
	
	// @Nullable
	private String facAtrAdquirente;
	
	// @Additional
	// @Nullable
	private String correoElectronico;
	
	// @Additional
	// @Required
	private File cert;
	private byte[] certificado;
	private String rutaCertificado;
	private Date certificateCreate;
	private Date certificateExpired;
	
	// @Additional
	// @Required
	private File privateKey;
	private byte[] llavePrivada;
	private String rutaLlavePrivada;
	
	// @Additional
	// @Required
	private String passwd;
	
	private String logo;
}