package org.megapractical.invoicing.api.wrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.voucher.catalogs.CFormaPago;
import org.megapractical.invoicing.voucher.catalogs.CMoneda;
import org.megapractical.invoicing.voucher.catalogs.CTipoFactor;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayment {
	private static final String CURRENT_VERSION = "2.0";
	
	/**
	 * @Required Atributo requerido que indica la versión del complemento para
	 *           recepción de pagos
	 */
	@Getter(AccessLevel.NONE)
	private String version;

	/**
	 * @Required Elemento requerido para incorporar la información de la recepción
	 *           de pagos
	 */
	@Getter(AccessLevel.NONE)
	private List<Payment> payments;

	/**
	 * @Required Nodo requerido para especificar el monto total de los pagos y el
	 *           total de los impuestos, deben ser expresados en MXN
	 */
	private PaymentTotal paymentTotal;
	
	// Pdf additionals
	@Getter(AccessLevel.NONE)
	private Map<String, Object> additionals;
	
	public ApiPayment(PaymentTotal paymentTotal) {
		this.version = CURRENT_VERSION;
		this.paymentTotal = paymentTotal;
	}
	
	public String getVersion() {
		if (version == null) {
			version = CURRENT_VERSION;
		}
		return version;
	}
	
	public List<Payment> getPayments() {
		if (payments == null) {
			payments = new ArrayList<>();
		}
		return payments;
	}
	
	public Map<String, Object> getAdditionals() {
		if (additionals == null) {
			additionals = new HashMap<>();
		}
		return additionals;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PaymentTotal {
		/**
		 * @Conditional Atributo condicional para expresar el total de los impuestos
		 *              retenidos de IVA que se desprenden de los pagos. No se permiten
		 *              valores negativos
		 */
		private BigDecimal totalWithheldIva;
		private String totalWithheldIvaString;

		/**
		 * @Conditional Atributo condicional para expresar el total de los impuestos
		 *              retenidos de ISR que se desprenden de los pagos. No se permiten
		 *              valores negativos
		 */
		private BigDecimal totalWithheldIsr;
		private String totalWithheldIsrString;

		/**
		 * @Conditional Atributo condicional para expresar el total de los impuestos
		 *              retenidos de IEPS que se desprenden de los pagos. No se permiten
		 *              valores negativos
		 */
		private BigDecimal totalWithheldIeps;
		private String totalWithheldIepsString;

		/**
		 * @Conditional Atributo condicional para expresar el total de la base de IVA
		 *              trasladado a la tasa del 16% que se desprende de los pagos. No
		 *              se permiten valores negativos
		 */
		private BigDecimal totalTransferredBaseIva16;
		private String totalTransferredBaseIva16String;

		/**
		 * @Conditional Atributo condicional para expresar el total de los impuestos de
		 *              IVA trasladado a la tasa del 16% que se desprenden de los pagos.
		 *              No se permiten valores negativos
		 */
		private BigDecimal totalTransferredTaxIva16;
		private String totalTransferredTaxIva16String;

		/**
		 * @Conditional Atributo condicional para expresar el total de la base de IVA
		 *              trasladado a la tasa del 8% que se desprende de los pagos. No se
		 *              permiten valores negativos
		 */
		private BigDecimal totalTransferredBaseIva8;
		private String totalTransferredBaseIva8String;

		/**
		 * @Conditional Atributo condicional para expresar el total de los impuestos de
		 *              IVA trasladado a la tasa del 8% que se desprenden de los pagos.
		 *              No se permiten valores negativos
		 */
		private BigDecimal totalTransferredTaxIva8;
		private String totalTransferredTaxIva8String;

		/**
		 * @Conditional Atributo condicional para expresar el total de la base de IVA
		 *              trasladado a la tasa del 0% que se desprende de los pagos. No se
		 *              permiten valores negativos
		 */
		private BigDecimal totalTransferredBaseIva0;
		private String totalTransferredBaseIva0String;

		/**
		 * @Conditional Atributo condicional para expresar el total de los impuestos de
		 *              IVA trasladado a la tasa del 0% que se desprenden de los pagos.
		 *              No se permiten valores negativos
		 */
		private BigDecimal totalTransferredTaxIva0;
		private String totalTransferredTaxIva0String;

		/**
		 * @Conditional Atributo condicional para expresar el total de la base de IVA
		 *              trasladado exento que se desprende de los pagos. No se permiten
		 *              valores negativos
		 */
		private BigDecimal totalTransferredBaseIvaExempt;
		private String totalTransferredBaseIvaExemptString;

		/**
		 * @Required Atributo requerido para expresar el total de los pagos que se
		 *           desprenden de los nodos Pago. No se permiten valores negativos
		 */
		private BigDecimal totalAmountPayments;
		private String totalAmountPaymentsString;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Payment {
		/**
		 * @Required Atributo requerido para expresar la fecha y hora en la que el
		 *           beneficiario recibe el pago. Se expresa en la forma
		 *           aaaa-mm-ddThh:mm:ss, de acuerdo con la especificación ISO 8601.En
		 *           caso de no contar con la hora se debe registrar 12:00:00
		 */
		private String paymentDate;

		/**
		 * @Required Atributo requerido para expresar la clave de la forma en que se
		 *           realiza el pago
		 */
		private CFormaPago paymentWay;
		private String paymentWayCode;
		private String paymentWayValue;
		private String paymentWayPrint;

		/**
		 * @Required Atributo requerido para identificar la clave de la moneda utilizada
		 *           para realizar el pago conforme a la especificación ISO 4217. Cuando
		 *           se usa moneda nacional se registra MXN. El atributo
		 *           Pagos:Pago:Monto debe ser expresado en la moneda registrada en este
		 *           atributo
		 */
		private CMoneda currency;
		private MonedaEntity currencyEntity;
		private String currencyPrint;

		/**
		 * @Conditional Atributo condicional para expresar el tipo de cambio de la
		 *              moneda a la fecha en que se realizó el pago. El valor debe
		 *              reflejar el número de pesos mexicanos que equivalen a una unidad
		 *              de la divisa señalada en el atributo MonedaP. Es requerido
		 *              cuando el atributo MonedaP es diferente a MXN
		 */
		private BigDecimal changeType;
		private String changeTypeString;

		/**
		 * @Required Atributo requerido para expresar el importe del pago
		 */
		private BigDecimal amount;
		private String amountString;

		/**
		 * @Conditional Atributo condicional para expresar el número de cheque, número
		 *              de autorización, número de referencia, clave de rastreo en caso
		 *              de ser SPEI, línea de captura o algún número de referencia
		 *              análogo que identifique la operación que ampara el pago
		 *              efectuado
		 */
		private String operationNumber;

		/**
		 * @Conditional Atributo condicional para expresar la clave RFC de la entidad
		 *              emisora de la cuenta origen, es decir, la operadora, el banco,
		 *              la institución financiera, emisor de monedero electrónico, etc.,
		 *              en caso de ser extranjero colocar XEXX010101000, considerar las
		 *              reglas de obligatoriedad publicadas en la página del SAT para
		 *              éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago
		 */
		private String sourceAccountRfc;

		/**
		 * @Conditional Atributo condicional para expresar el nombre del banco
		 *              ordenante, es requerido en caso de ser extranjero. Considerar
		 *              las reglas de obligatoriedad publicadas en la página del SAT
		 *              para éste atributo de acuerdo con el catálogo
		 *              catCFDI:c_FormaPago
		 */
		private String bankName;

		/**
		 * @Conditional Atributo condicional para incorporar el número de la cuenta con
		 *              la que se realizó el pago. Considerar las reglas de
		 *              obligatoriedad publicadas en la página del SAT para éste
		 *              atributo de acuerdo con el catálogo catCFDI:c_FormaPago
		 */
		private String payerAccount;

		/**
		 * @Conditional Atributo condicional para expresar la clave RFC de la entidad
		 *              operadora de la cuenta destino, es decir, la operadora, el
		 *              banco, la institución financiera, emisor de monedero
		 *              electrónico, etc. Considerar las reglas de obligatoriedad
		 *              publicadas en la página del SAT para éste atributo de acuerdo
		 *              con el catálogo catCFDI:c_FormaPago
		 */
		private String targetAccountRfc;

		/**
		 * @Conditional Atributo condicional para incorporar el número de cuenta en
		 *              donde se recibió el pago. Considerar las reglas de
		 *              obligatoriedad publicadas en la página del SAT para éste
		 *              atributo de acuerdo con el catálogo catCFDI:c_FormaPago
		 */
		private String receiverAccount;

		/**
		 * @Conditional Atributo condicional para identificar la clave del tipo de
		 *              cadena de pago que genera la entidad receptora del pago.
		 *              Considerar las reglas de obligatoriedad publicadas en la página
		 *              del SAT para éste atributo de acuerdo con el catálogo
		 *              catCFDI:c_FormaPago
		 */
		private String stringType;
		private String stringTypeCode;

		/**
		 * @Conditional Atributo condicional que sirve para incorporar el certificado
		 *              que ampara al pago, como una cadena de texto en formato base 64.
		 *              Es requerido en caso de que el atributo TipoCadPago contenga
		 *              información
		 */
		private byte[] paymentCertificate;

		/**
		 * @Conditional Atributo condicional para expresar la cadena original del
		 *              comprobante de pago generado por la entidad emisora de la cuenta
		 *              beneficiaria. Es requerido en caso de que el atributo
		 *              TipoCadPago contenga información
		 */
		private String originalString;

		/**
		 * @Conditional Atributo condicional para integrar el sello digital que se
		 *              asocie al pago. La entidad que emite el comprobante de pago,
		 *              ingresa una cadena original y el sello digital en una sección de
		 *              dicho comprobante, este sello digital es el que se debe
		 *              registrar en este atributo. Debe ser expresado como una cadena
		 *              de texto en formato base 64. Es requerido en caso de que el
		 *              atributo TipoCadPago contenga información
		 */
		private byte[] paymentSeal;

		/**
		 * @Required Nodo requerido para expresar la lista de documentos relacionados
		 *           con los pagos. Por cada documento que se relacione se debe generar
		 *           un nodo DoctoRelacionado
		 */
		@Getter(AccessLevel.NONE)
		private List<RelatedDocument> relatedDocuments;

		/**
		 * @Conditional
		 */
		@Getter(AccessLevel.NONE)
		private List<PaymentWithheld> withhelds;
		
		/**
		 * @Required Nodo requerido para registrar la información detallada de una
		 *           retención de impuesto específico
		 */
		@Getter(AccessLevel.NONE)
		private List<BaseTax> transferreds;

		/**
		 * @Additional
		 */
		private String paymentNumber;

		/* Getters and Setters */
		public List<RelatedDocument> getRelatedDocuments() {
			if (relatedDocuments == null) {
				relatedDocuments = new ArrayList<>();
			}
			return relatedDocuments;
		}

		public List<PaymentWithheld> getWithhelds() {
			if (withhelds == null) {
				withhelds = new ArrayList<>();
			}
			return withhelds;
		}
		
		public List<BaseTax> getTransferreds() {
			if (transferreds == null) {
				transferreds = new ArrayList<>();
			}
			return transferreds;
		}

		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class RelatedDocument {
			/**
			 * @Required Atributo requerido para expresar el identificador del documento
			 *           relacionado con el pago. Este dato puede ser un Folio Fiscal de la
			 *           Factura Electrónica o bien el número de operación de un documento
			 *           digital
			 */
			private String documentId;

			/**
			 * @Conditional Atributo opcional para precisar la serie del comprobante para
			 *              control interno del contribuyente, acepta una cadena de
			 *              caracteres
			 */
			private String serie;

			/**
			 * @Conditional Atributo opcional para precisar el folio del comprobante para
			 *              control interno del contribuyente, acepta una cadena de
			 *              caracteres
			 */
			private String sheet;

			/**
			 * @Required Atributo requerido para identificar la clave de la moneda utilizada
			 *           en los importes del documento relacionado, cuando se usa moneda
			 *           nacional o el documento relacionado no especifica la moneda se
			 *           registra MXN. Los importes registrados en los atributos
			 *           “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben
			 *           corresponder a esta moneda. Conforme con la especificación ISO 4217
			 */
			private CMoneda currency;
			private MonedaEntity currencyEntity;
			private String currencyPrint;

			/**
			 * @Conditional Atributo condicional para expresar el tipo de cambio conforme
			 *              con la moneda registrada en el documento relacionado. Es
			 *              requerido cuando la moneda del documento relacionado es distinta
			 *              de la moneda de pago. Se debe registrar el número de unidades de
			 *              la moneda señalada en el documento relacionado que equivalen a
			 *              una unidad de la moneda del pago. Por ejemplo: El documento
			 *              relacionado se registra en USD. El pago se realiza por 100 EUR.
			 *              Este atributo se registra como 1.114700 USD/EUR. El importe
			 *              pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD
			 */
			private BigDecimal changeType;
			private String changeTypePrint;

			/**
			 * @Required Atributo requerido para expresar el número de parcialidad que
			 *           corresponde al pago
			 */
			private Integer partiality;

			/**
			 * @Required Atributo requerido para expresar el monto del saldo insoluto de la
			 *           parcialidad anterior. En el caso de que sea la primer parcialidad
			 *           este atributo debe contener el importe total del documento
			 *           relacionado
			 */
			private BigDecimal amountPartialityBefore;
			private String amountPartialityBeforePrint;

			/**
			 * @Required Atributo requerido para expresar el importe pagado para el
			 *           documento relacionado
			 */
			private BigDecimal amountPaid;
			private String amountPaidPrint;

			/**
			 * @Required Atributo requerido para expresar la diferencia entre el importe del
			 *           saldo anterior y el monto del pago
			 */
			private BigDecimal difference;
			private String differencePrint;

			/**
			 * @Required Atributo requerido para expresar la diferencia entre el importe del
			 *           saldo anterior y el monto del pago
			 */
			private String taxObject;
			private String taxObjectCode;

			/**
			 * @Conditional
			 */
			@Getter(AccessLevel.NONE)
			private List<BaseTax> withhelds;
			
			/**
			 * @Conditional
			 */
			@Getter(AccessLevel.NONE)
			private List<BaseTax> transferreds;

			/**
			 * @Additional
			 */
			private String paymentNumber;
			/**
			 * @Additional
			 */
			private String relatedDocumentNumber;
			
			public List<BaseTax> getWithhelds() {
				if (withhelds == null) {
					withhelds = new ArrayList<>();
				}
				return withhelds;
			}
			
			public List<BaseTax> getTransferreds() {
				if (transferreds == null) {
					transferreds = new ArrayList<>();
				}
				return transferreds;
			}
		}

		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class BaseTax {
			/**
			 * @Required Atributo requerido para señalar la base para el cálculo de la
			 *           retención conforme al monto del pago, aplicable al documento
			 *           relacionado, la determinación de la base se realiza de acuerdo con
			 *           las disposiciones fiscales vigentes. No se permiten valores
			 *           negativos
			 */
			private String base;
			private BigDecimal baseBigDecimal;

			/**
			 * @Required Atributo requerido para señalar la clave del tipo de impuesto
			 *           retenido conforme al monto del pago, aplicable al documento
			 *           relacionado
			 */
			private String tax;
			private String taxPrint;

			/**
			 * @Required Atributo requerido para señalar la clave del tipo de factor que se
			 *           aplica a la base del impuesto
			 */
			private String factor;
			private CTipoFactor factorType;

			/**
			 * @Required Atributo requerido para señalar el valor de la tasa o cuota del
			 *           impuesto que se retiene
			 */
			private String rateOrFee;
			private BigDecimal rateOrFeeBigDecimal;

			/**
			 * @Required Atributo requerido para señalar el importe del impuesto retenido
			 *           conforme al monto del pago, aplicable al documento relacionado. No
			 *           se permiten valores negativos
			 */
			private String amount;
			private BigDecimal amountBigDecimal;
			
			/**
			 * @Additional
			 */
			private String number;
		}
		
		@Data
		@Builder
		@NoArgsConstructor
		@AllArgsConstructor
		public static class PaymentWithheld {
			/**
			 * @Required Atributo requerido para señalar la clave del tipo de impuesto
			 *           retenido conforme al monto del pago
			 */
			private String tax;

			/**
			 * @Required Atributo requerido para señalar el importe del impuesto retenido
			 *           conforme al monto del pago. No se permiten valores negativos
			 */
			private String amount;
			private BigDecimal amountBigDecimal;
			
			/**
			 * @Additional
			 */
			private String number;
		}
	}

}