package org.megapractical.invoicing.api.wrapper;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.dal.bean.jpa.RetencionCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.RetencionTramaEntity;

import com.opencsv.CSVWriter;

public class ApiRetentionError {

	//##### Linea del error
	private Integer line;
	
	//##### Trama asociada al error
	private RetencionTramaEntity entry;
	
	//##### Campo asociado al error
	private RetencionCampoEntity field;
	
	//##### Valor del campo en el fichero
	private String fieldValue;
	
	//##### Posicion del campo en el fichero
	private Integer fieldPosition;
	
	//##### Longitud del campo
	private Integer fieldLength;
	
	//##### Longitud minima del campo
	private Integer fieldMinLength;
	
	//##### Longitud maxima del campo
	private Integer fieldMaxLength;
	
	//##### Longitud del campo en el fichero
	private Integer fieldLengthOnFile;
	
	//#####
	private String fieldRegularExpression;
		
	//##### Descripcion
	private String description;
	
	//##### Longitud de la trma
	private Integer entryLength;
	
	//##### Longitud de la trama en fichero	
	private Integer entryLengthOnFile;
	
	//##### Define si es un error de generacion de comprobante
	private boolean isVoucherGenerateError;
	
	//##### Descripcion del error de generacion de comprobante
	private String voucherGenerateErrorException;
	
	//##### Define si es un error de timbrado
	private boolean isStampError;
	
	//##### Codigo del error de timbrado
	private String stampErrorCode;
	
	//##### Mensaje del error de timbrado
	private String stampErrorMessage;
	
	//##### RFC del receptor
	private String receiverRfc;
	
	//##### Nombre del receptor
	private String receiverName;
	
	//##### Curp del receptor
	private String receiverCurp;
	
	//##### Define si es un error inesperado
	private boolean isUnexpectedError;
	
	//##### Define si es un error de valdiacion del contenido del xml
	private boolean isSAXParseException;
	
	//##### Mensaje del error de validacion 
	private String saxParseErrorMessage;
	
	//##### Descripcion del error de validacion
	private String saxParseErrorLocalizedMessage;
	
	private String validationErrorCode;
	private String validationErrorMessage;
	
	public static void writeRetentionError(String path, List<ApiRetentionError> errors){
		try {
			
			//##### Creando fichero de error si no existe
    		File file = new File(path);
    		if(!file.exists()){
    			file.createNewFile();
    		}else{
    			try {
				
    				file.delete();
    				
				} catch (Exception e) {
					
				}
    		}
    		
    		//##### Linea del fichero
    		Integer line = 0;
    		
    		//##### Contenido a escribir
    		String sentence = "";
    		
    		//##### Escribiendo el nuevo fichero
    		CSVWriter writer = new CSVWriter(new FileWriter(file), '|', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
    		
    		for (ApiRetentionError item : errors) {
    			
    			sentence = null;
    			
    			if(!UValidator.isNullOrEmpty(item.getDescription())){
					sentence = 	"Descripción del error: " + item.getDescription();
				}
				
    			if(!UValidator.isNullOrEmpty(item.getLine())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLínea: " + item.getLine(); 
					}else{
						sentence = "\nLínea: " + item.getLine();
					}
				}
    			
				if(!UValidator.isNullOrEmpty(item.getEntry())){
					if(!UValidator.isNullOrEmpty(item.getEntry().getNombre())){
						if(!UValidator.isNullOrEmpty(sentence)){
							sentence = sentence + "\nTrama: " + item.getEntry().getNombre(); 
						}else{
							sentence = "\nTrama: " + item.getEntry().getNombre();
						}
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getEntryLengthOnFile())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLongitud en fichero: " + item.getEntryLengthOnFile();
					}else{
						sentence = "\nLongitud en fichero: " + item.getEntryLengthOnFile();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getEntryLength())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLongitud válida: " + item.getEntryLength();
					}else{
						sentence = "\nLongitud válida: " + item.getEntryLength();
					}	   	
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldPosition())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nPosición del campo: " + item.getFieldPosition();
					}else{
						sentence = "\nPosición del campo: " + item.getFieldPosition();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getField())){
					if(!UValidator.isNullOrEmpty(item.getField().getNombre())){
						if(!UValidator.isNullOrEmpty(sentence)){
							sentence = sentence + "\nCampo: " + item.getField().getNombre(); 
						}else{
							sentence = "\nCampo: " + item.getField().getNombre();
						}
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldValue())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nValor en fichero: " + item.getFieldValue(); 
					}else{
						sentence = "\nValor en fichero: " + item.getFieldValue();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldLength())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLongitud del campo: " + item.getFieldLength(); 
					}else{
						sentence = "\nLongitud del campo: " + item.getFieldLength();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldMinLength())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLongitud mínima del campo: " + item.getFieldMinLength(); 
					}else{
						sentence = "\nLongitud mínima del campo: " + item.getFieldMinLength();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldMaxLength())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLongitud máxima del campo: " + item.getFieldMaxLength(); 
					}else{
						sentence = "\nLongitud máxima del campo: " + item.getFieldMaxLength();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldLengthOnFile())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nLongitud en fichero: " + item.getFieldLengthOnFile();
					}else{
						sentence = "\nLongitud en fichero: " + item.getFieldLengthOnFile();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getFieldRegularExpression())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nExpresión regular de validación: " + item.getFieldRegularExpression();
					}else{
						sentence = "\nExpresión regular de validación: " + item.getFieldRegularExpression();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getReceiverRfc())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nRFC receptor: " + item.getReceiverRfc();
					}else{
						sentence = "\nRFC receptor: " + item.getReceiverRfc();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getReceiverCurp())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nCURP receptor: " + item.getReceiverCurp();
					}else{
						sentence = "\nCURP receptor: " + item.getReceiverCurp();
					}
				}
							
				if(!UValidator.isNullOrEmpty(item.getReceiverName())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nNombre o razón social: " + item.getReceiverName();
					}else{
						sentence = "\nNombre o razón social: " + item.getReceiverName();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getStampErrorCode())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nCódigo del error: " + item.getStampErrorCode();
					}else{
						sentence = "\nCódigo del error: " + item.getStampErrorCode();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getValidationErrorCode())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nCódigo del error: " + item.getValidationErrorCode();
					}else{
						sentence = "\nCódigo del error: " + item.getValidationErrorCode();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getStampErrorMessage())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nMensaje del error: " + item.getStampErrorMessage();
					}else{
						sentence = "\nMensaje del error: " + item.getStampErrorMessage();
					}
				}
				
				if(!UValidator.isNullOrEmpty(item.getValidationErrorMessage())){
					if(!UValidator.isNullOrEmpty(sentence)){
						sentence = sentence + "\nMensaje del error: " + item.getValidationErrorMessage();
					}else{
						sentence = "\nMensaje del error: " + item.getValidationErrorMessage();
					}
				}
				
				sentence = sentence + "\n----------------------------------------------------------------------------------------------------------------------";
    			
    			if(!UValidator.isNullOrEmpty(sentence)){
    				String[] errorSet = new String[]{sentence + "\n"};
    				writer.writeNext(errorSet);
        			line ++;
    			}
			}
			
    		//##### Cerrando fichero
    		writer.close();
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* Getters and Setters */
	public Integer getLine() {
		return line;
	}

	public void setLine(Integer line) {
		this.line = line;
	}

	public RetencionTramaEntity getEntry() {
		return entry;
	}

	public void setEntry(RetencionTramaEntity entry) {
		this.entry = entry;
	}

	public RetencionCampoEntity getField() {
		return field;
	}

	public void setField(RetencionCampoEntity field) {
		this.field = field;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getEntryLength() {
		return entryLength;
	}

	public void setEntryLength(Integer entryLength) {
		this.entryLength = entryLength;
	}

	public Integer getEntryLengthOnFile() {
		return entryLengthOnFile;
	}

	public void setEntryLengthOnFile(Integer entryLengthOnFile) {
		this.entryLengthOnFile = entryLengthOnFile;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getStampErrorCode() {
		return stampErrorCode;
	}

	public void setStampErrorCode(String stampErrorCode) {
		this.stampErrorCode = stampErrorCode;
	}

	public boolean isStampError() {
		return isStampError;
	}

	public void setStampError(boolean isStampError) {
		this.isStampError = isStampError;
	}

	public boolean isSAXParseException() {
		return isSAXParseException;
	}

	public void setSAXParseException(boolean isSAXParseException) {
		this.isSAXParseException = isSAXParseException;
	}

	public String getSaxParseErrorMessage() {
		return saxParseErrorMessage;
	}

	public void setSaxParseErrorMessage(String saxParseErrorMessage) {
		this.saxParseErrorMessage = saxParseErrorMessage;
	}

	public String getSaxParseErrorLocalizedMessage() {
		return saxParseErrorLocalizedMessage;
	}

	public void setSaxParseErrorLocalizedMessage(String saxParseErrorLocalizedMessage) {
		this.saxParseErrorLocalizedMessage = saxParseErrorLocalizedMessage;
	}

	public boolean isVoucherGenerateError() {
		return isVoucherGenerateError;
	}

	public void setVoucherGenerateError(boolean isVoucherGenerateError) {
		this.isVoucherGenerateError = isVoucherGenerateError;
	}

	public String getVoucherGenerateErrorException() {
		return voucherGenerateErrorException;
	}

	public void setVoucherGenerateErrorException(String voucherGenerateErrorException) {
		this.voucherGenerateErrorException = voucherGenerateErrorException;
	}

	public String getStampErrorMessage() {
		return stampErrorMessage;
	}

	public void setStampErrorMessage(String stampErrorMessage) {
		this.stampErrorMessage = stampErrorMessage;
	}

	public String getReceiverRfc() {
		return receiverRfc;
	}

	public void setReceiverRfc(String receiverRfc) {
		this.receiverRfc = receiverRfc;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverCurp() {
		return receiverCurp;
	}

	public void setReceiverCurp(String receiverCurp) {
		this.receiverCurp = receiverCurp;
	}

	public boolean isUnexpectedError() {
		return isUnexpectedError;
	}

	public void setUnexpectedError(boolean isUnexpectedError) {
		this.isUnexpectedError = isUnexpectedError;
	}

	public Integer getFieldPosition() {
		return fieldPosition;
	}

	public void setFieldPosition(Integer fieldPosition) {
		this.fieldPosition = fieldPosition;
	}

	public Integer getFieldLength() {
		return fieldLength;
	}

	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}

	public Integer getFieldMinLength() {
		return fieldMinLength;
	}

	public void setFieldMinLength(Integer fieldMinLength) {
		this.fieldMinLength = fieldMinLength;
	}

	public Integer getFieldMaxLength() {
		return fieldMaxLength;
	}

	public void setFieldMaxLength(Integer fieldMaxLength) {
		this.fieldMaxLength = fieldMaxLength;
	}

	public Integer getFieldLengthOnFile() {
		return fieldLengthOnFile;
	}

	public void setFieldLengthOnFile(Integer fieldLengthOnFile) {
		this.fieldLengthOnFile = fieldLengthOnFile;
	}

	public String getFieldRegularExpression() {
		return fieldRegularExpression;
	}

	public void setFieldRegularExpression(String fieldRegularExpression) {
		this.fieldRegularExpression = fieldRegularExpression;
	}
	
	public String getValidationErrorMessage() {
		return validationErrorMessage;
	}

	public void setValidationErrorMessage(String validationErrorMessage) {
		this.validationErrorMessage = validationErrorMessage;
	}

	public String getValidationErrorCode() {
		return validationErrorCode;
	}

	public void setValidationErrorCode(String validationErrorCode) {
		this.validationErrorCode = validationErrorCode;
	}
}