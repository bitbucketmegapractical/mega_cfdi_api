package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ArchivoFacturaEstadisticaEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPaymentFileStatistics {
	// Listado con todas las tramas validas
	@Getter(AccessLevel.NONE)
	private List<String[]> entries;
	
	// Total de comprobantes en el fichero
	// NOTA: Cada linea (no header, no subheader) con informacion representa una trama
	private Integer totalEntries;
	
	// Total de objetos despues de validar
	private Integer singleEntrie;
	
	// Estadisticas del archivo
	private ArchivoFacturaEstadisticaEntity invoiceFileStatistics;
	
	/* Getters and Setters */
	public List<String[]> getEntries() {
		if (entries == null) {
			entries = new ArrayList<>();
		}
		return entries;
	}
}