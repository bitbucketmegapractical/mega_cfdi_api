package org.megapractical.invoicing.api.wrapper;

import org.megapractical.invoicing.api.stamp.payload.StampProperties;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaConfiguracionEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ArchivoNominaEstadisticaEntity;
import org.megapractical.invoicing.dal.bean.jpa.ContribuyenteEntity;
import org.megapractical.invoicing.dal.bean.jpa.TimbreEntity;
import org.megapractical.invoicing.dal.bean.jpa.UsuarioEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollStamp {

	//##### Contribuyente emisor
	private ContribuyenteEntity taxpayer;
	
	//##### Usuario
	private UsuarioEntity user;
	
	//##### Ip
	private String ip;
	
	//##### Url acceso
	private String urlAcceso;
	
	//##### Emisor
	private ApiEmitter apiEmitter;
	
	//##### Configuraciones de archivo
	private ArchivoNominaConfiguracionEntity payrollFileConfiguration;
	
	//##### Archivo de nomina registrado en DB
	private ArchivoNominaEntity payrollFile;
	
	//##### Informacion general del fichero
	private ArchivoNominaEstadisticaEntity payrollFileStatistics;
	
	//##### Informacion para el timbrado
	private StampProperties stampProperties;
	
	//##### Informacion de timbres del emisor
	private TimbreEntity stampDetail;
	
	//##### Ruta absoluta del archivo
	private String absolutePath;
	
	//##### Ruta fisica del archivo
	private String uploadPath;
	
	//##### Ruta para db
	private String dbPath;
}