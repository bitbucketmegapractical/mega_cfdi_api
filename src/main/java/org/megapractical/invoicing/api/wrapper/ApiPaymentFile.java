package org.megapractical.invoicing.api.wrapper;

import java.util.List;

import org.megapractical.invoicing.api.common.ApiVoucher;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.dal.bean.jpa.CodigoPostalEntity;
import org.megapractical.invoicing.dal.bean.jpa.MonedaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoComprobanteEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPaymentFile {
	// Comprobante v33
	private ApiVoucher voucher;

	// Contribuyente emisor
	private ApiEmitter emitter;

	// Contribuyente receptor
	private ApiReceiver receiver;

	// Concepto
	private ApiConcept concept;

	// Tipo comprobante
	private TipoComprobanteEntity voucherType;

	// Moneda
	private MonedaEntity currency;

	// Codigo postal
	private CodigoPostalEntity postalCode;

	// Timbrado
	private StampResponse stampResponse;

	private List<ApiPayment> cmpPayments;
	
	// Leyenda
	private String legend;
	
	// Package
	private String packageClass;
}