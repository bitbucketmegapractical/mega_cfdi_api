package org.megapractical.invoicing.api.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiCancellationDetail {
	
	public static final String UUID_NOT_FOUND = "El uuid no existe";
	public static final String ALREADY_CANCELLED = "Folio Fiscal Previamente Cancelado";
	
	private Integer line;
	private String uuid;
	private String detail;
	
	public static ApiCancellationDetail detail(String uuid, String detail) {
		return ApiCancellationDetail.builder().uuid(uuid).detail(detail).build();
	}
	
	public static ApiCancellationDetail uuidNotFound(String uuid) {
		return ApiCancellationDetail.builder().uuid(uuid).detail(UUID_NOT_FOUND).build();
	}
	
	public static ApiCancellationDetail uuidAlreadyCancelled(String uuid) {
		return ApiCancellationDetail.builder().uuid(uuid).detail(ALREADY_CANCELLED).build();
	}
	
}