package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.TipoDeduccionEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollDeduction {
	private String totalOtherDeductions;
	private String totalTaxesWithheld;
	@Getter(value = AccessLevel.NONE)
	private List<PayrollDeduction> payrollDeductions;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PayrollDeduction {
		private TipoDeduccionEntity deductionType;
		private String deductionTypeCode;
		private String deductionTypeValue;
		private String deductionTypePrint;
		private String key;
		private String concept;
		private String amount;
	}

	/* Getters and Setters */
	public List<PayrollDeduction> getPayrollDeductions() {
		if (payrollDeductions == null) {
			payrollDeductions = new ArrayList<>();
		}
		return payrollDeductions;
	}

}