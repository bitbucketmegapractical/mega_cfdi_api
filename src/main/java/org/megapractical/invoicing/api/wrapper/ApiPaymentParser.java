package org.megapractical.invoicing.api.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoFicheroEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPaymentParser {
	// Informacion del parser instanciado
	private ComplementoPagoFicheroEntity paymentDataFile;

	// Listado de comprobantes
	@Getter(AccessLevel.NONE)
	private List<String[]> entries;

	// Listado de posibles errores
	@Getter(AccessLevel.NONE)
	private List<ApiPaymentError> errors;

	// Informacion general del fichero
	private ApiPaymentFileStatistics statistics;
	
	//##### Informacion complementaria para la generacion del comprobante y el timbrado
	private ApiPaymentStamp paymentStamp;

	public List<String[]> getEntries() {
		if (entries == null) {
			entries = new ArrayList<>();
		}
		return entries;
	}

	public List<ApiPaymentError> getErrors() {
		if (errors == null) {
			errors = new ArrayList<>();
		}
		return errors;
	}
	
}