package org.megapractical.invoicing.api.wrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiConceptTaxes;
import org.megapractical.invoicing.dal.bean.jpa.ClaveProductoServicioEntity;
import org.megapractical.invoicing.dal.bean.jpa.UnidadMedidaEntity;
import org.megapractical.invoicing.sat.complement.concept.iedu.InstEducativas;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.CuentaPredial;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.InformacionAduanera;
import org.megapractical.invoicing.sat.integration.v40.Comprobante.Conceptos.Concepto.Parte;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiConcept {
	//##### @Nullable
	private String identificationNumber;
	
	//##### @Required
	private String description;
	
	//##### @Required
	private BigDecimal quantity;
	private String quantityString;
	
	//##### @Required
	private UnidadMedidaEntity measurementUnitEntity;
	private String measurementUnitCode;
	private String measurementUnit;
	private String measurementUnitPrint;
	
	//##### @Nullable
	private String unit;
	
	//##### @Required
	private BigDecimal unitValue;
	private String unitValueString;
	
	//##### @Required
	private BigDecimal amount;
	private String amountString;
	
	//##### @Nullable
	private BigDecimal discount;
	private String discountString;
	
	//##### @Required
	private ClaveProductoServicioEntity keyProductServiceEntity;
	private String keyProductService;
	private String keyProductServiceCode;
	
	//##### @Nullable
    @Getter(AccessLevel.NONE)
	private List<ApiConceptTaxes> taxesTransferred;
    
    //##### @Nullable
    @Getter(AccessLevel.NONE)
    private List<ApiConceptTaxes> taxesWithheld;
    
    //##### @Nullable
    @Getter(AccessLevel.NONE)
	private List<InformacionAduanera> customsInformation;
	
	//##### @Nullable
	private CuentaPredial propertyAccount;	
	
	//##### @Nullable
	private List<Parte> parteSources;
	
	//##### @Nullable
	private InstEducativas iedu;
	
	//##### @Nullable
	private String objImp;
	
	//##### @Additional
	private Long conceptSelected;
	private String conceptNumber;
	
	//##### @Additional
	// Informacion aduanera (Para impresion en PDF)
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ConceptCustomsInformation {
		private String conceptNumber;
		private String number;
	}	
	
	//##### @Additional
	// Cuenta predial (Para impresion en PDF)
	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ConceptPropertyAccount {
		private String conceptNumber;
		private String number;
	}
	
    /*Getters and Setters*/
	public List<ApiConceptTaxes> getTaxesTransferred() {
		if (taxesTransferred == null) {
			taxesTransferred = new ArrayList<>();
		}
		return taxesTransferred;
	}

	public List<ApiConceptTaxes> getTaxesWithheld() {
		if (taxesWithheld == null) {
			taxesWithheld = new ArrayList<>();
		}
		return taxesWithheld;
	}

	public List<Comprobante.Conceptos.Concepto.InformacionAduanera> getCustomsInformation() {
		if (customsInformation == null) {
			customsInformation = new ArrayList<>();
		}
		return customsInformation;
	}
	
}