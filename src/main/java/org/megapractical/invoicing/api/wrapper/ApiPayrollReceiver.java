package org.megapractical.invoicing.api.wrapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.dal.bean.jpa.BancoEntity;
import org.megapractical.invoicing.dal.bean.jpa.EntidadFederativaEntity;
import org.megapractical.invoicing.dal.bean.jpa.PeriodicidadPagoEntity;
import org.megapractical.invoicing.dal.bean.jpa.RiesgoPuestoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoContratoEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoJornadaEntity;
import org.megapractical.invoicing.dal.bean.jpa.TipoRegimenEntity;
import org.megapractical.invoicing.voucher.catalogs.CEstado;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiPayrollReceiver {
	private String curp;
	private String socialSecurityNumber;
	private LocalDate startDateEmploymentRelationship;
	private String startDateEmploymentRelationshipString;
	private String age;
	private String ageDescription;
	private TipoContratoEntity contractType;
	private String contractTypeCode;
	private String contractTypeValue;
	private String contractTypePrint;
	private String unionized;
	private TipoJornadaEntity dayType;
	private String dayTypeCode;
	private String dayTypeValue;
	private String dayTypePrint;
	private TipoRegimenEntity regimeType;
	private String regimeTypeCode;
	private String regimeTypeValue;
	private String regimeTypePrint;
	private String employeeNumber;
	private String department;
	private String jobTitle;
	private RiesgoPuestoEntity jobRisk;
	private String jobRiskCode;
	private String jobRiskValue;
	private String jobRiskPrint;
	private PeriodicidadPagoEntity paymentFrequency;
	private String paymentFrequencyCode;
	private String paymentFrequencyValue;
	private String paymentFrequencyPrint;
	private BancoEntity bank;
	private String bankCode;
	private String bankValue;
	private String bankPrint;
	private String bankAccount;
	private String shareContributionBaseSalary;
	private String integratedDailySalary;
	private EntidadFederativaEntity federalEntity;
	private String federalEntityCode;
	private String federalEntityValue;
	private String federalEntityPrint;
	private CEstado federalEntityCatalog;
	private List<PayrollOutsourcing> payrollOutsourcings;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PayrollOutsourcing {
		private String rfcBoss;
		private String percentageTime;
	}

	/* Getters and Setters */
	public List<PayrollOutsourcing> getPayrollOutsourcings() {
		if (payrollOutsourcings == null)
			payrollOutsourcings = new ArrayList<>();
		return payrollOutsourcings;
	}

}