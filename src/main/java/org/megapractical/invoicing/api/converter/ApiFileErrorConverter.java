package org.megapractical.invoicing.api.converter;

import org.megapractical.invoicing.api.wrapper.ApiFileError;
import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;
import org.megapractical.invoicing.dal.bean.common.FieldBase;
import org.megapractical.invoicing.dal.bean.jpa.ComplementoPagoCampoEntity;
import org.megapractical.invoicing.dal.bean.jpa.NominaCampoEntity;

public final class ApiFileErrorConverter {

	private ApiFileErrorConverter() {		
	}
	
	private static final String CONTENT_ERROR = "Validación de contenido incorrecta.";
	
	public static ApiPaymentError convert(ComplementoPagoCampoEntity field, String[] entry, int position) {
		ApiPaymentError fileError = ApiPaymentError
									.builder()
									.description(CONTENT_ERROR)
									.fieldPosition(position)
									.build();
		if (entry != null && field != null) {
			fileError.setField(field);
			populate(fileError, field, entry, position);
		}
		return fileError;
	}
	
	public static ApiPayrollError convert(NominaCampoEntity field, String[] entry, int position) {
		ApiPayrollError fileError = ApiPayrollError
									.builder()
									.description(CONTENT_ERROR)
									.fieldPosition(position)
									.build();
		if (entry != null && field != null) {
			fileError.setField(field);
			populate(fileError, field, entry, position);
		}
		return fileError;
	}

	private static void populate(ApiFileError fileError, FieldBase field, String[] entry, int position) {
		if (entry != null && field != null) {
			fileError.setFieldValue(entry[position]);
			fileError.setFieldLength(field.getLongitud());
			fileError.setFieldMinLength(field.getLongitudMinima());
			fileError.setFieldMaxLength(field.getLongitudMaxima());
			fileError.setFieldLengthOnFile(entry[position].length());
			fileError.setFieldRegularExpression(field.getValidacion());
		}
	}
	
}