package org.megapractical.invoicing.api.stamp.payload;

import java.io.File;

import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.util.StringUtils;
import org.megapractical.invoicing.api.util.UValue;
import org.megapractical.invoicing.sat.integration.retention.v2.RETv20;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;
import org.megapractical.invoicing.sat.integration.v40.CFDv4;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StampResponse {
	private String stampedXml;
	private String sealedXml;
	private byte[] xml;
	private byte[] xmlBeforeStamp;
	private File xmlFile;
	private File pdfFile;
	private String uuid;
	private String originalString;
	private String qrCode;
	private String dateStamp;
	private String dateExpedition;
	@Default
	private boolean stamped = Boolean.FALSE;
	private String emitterSeal;
	private String satSeal;
	private String certificateEmitterNumber;
	private String certificateSatNumber;

	private Comprobante voucher;
	private CFDv4 cfdv4;
	private Retenciones retention;
	private RETv20 retv20;
	private TimbreFiscalDigital tfd;

	private String error;
	private String errorCode;
	private String errorMessage;

	@Default
	private boolean malformedUrlException = Boolean.FALSE;
	@Default
	private boolean datatypeConfigurationException = Boolean.FALSE;
	@Default
	private boolean wsdlException = Boolean.FALSE;
	@Default
	private boolean stampException = Boolean.FALSE;
	
	public static StampResponse empty() {
		return StampResponse.builder().stamped(false).build();
	}
	
	public static StampResponse nonFiscal(Comprobante voucher, String sealedXml) {
		return StampResponse
				.builder()
				.stamped(true)
				.voucher(voucher)
				.sealedXml(sealedXml)
				.xml(UValue.stringToByte(sealedXml))
				.tfd(new TimbreFiscalDigital())
				.qrCode(ReportManagerTools.QR_BASE_URL)
				.certificateEmitterNumber(voucher.getNoCertificado())
				.dateExpedition(voucher.getFecha())
				.build();
	}
	
	public static StampResponse error(String error) {
		return StampResponse
				.builder()
				.stamped(false)
				.error(error)
				.errorCode(error)
				.errorMessage(error)
				.build();
	}
	
	public static StampResponse error(String error, String details) {
		if (!AssertUtils.hasValue(details)) {
			details = error;
		}
		return StampResponse
				.builder()
				.stamped(false)
				.error(error)
				.errorCode(error)
				.errorMessage(details)
				.build();
	}
	
	public static StampResponse error(String error, String details, String sealedXml) {
		if (!AssertUtils.hasValue(details)) {
			details = error;
		}
		return StampResponse
				.builder()
				.stamped(false)
				.sealedXml(sealedXml)
				.error(error)
				.errorCode(error)
				.errorMessage(details)
				.build();
	}
	
	public Boolean hasError() {
		return StringUtils.hasValue(error) || 
			   StringUtils.hasValue(errorCode) || 
			   StringUtils.hasValue(errorMessage) || 
			   isMalformedUrlException() || 
			   isDatatypeConfigurationException() || 
			   isWsdlException() || 
			   isStampException();
	}
	
	public String getUuidUpperCase() {
		if (this.uuid != null) {
			return this.uuid.toUpperCase();
		}
		return null;
	}
	
}