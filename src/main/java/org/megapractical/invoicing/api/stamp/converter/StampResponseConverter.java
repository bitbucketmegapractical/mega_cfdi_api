package org.megapractical.invoicing.api.stamp.converter;

import java.io.ByteArrayInputStream;

import org.megapractical.invoicing.api.report.ReportManagerTools;
import org.megapractical.invoicing.api.smarterweb.SWStampResponse;
import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.PdfPrintUtils;
import org.megapractical.invoicing.sat.integration.v40.CFDv4;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.tfd.TfdParser;
import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class StampResponseConverter {

	private StampResponseConverter() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}

	public static final StampResponse convert(SWStampResponse response) {
		try {
			if (response.cfdi != null) {
				val xml = response.cfdi.getBytes();
				val cfd = xml != null ? new CFDv4(new ByteArrayInputStream(xml)) : null;
				val voucher = cfd != null ? cfd.doGetComprobante() : null;
				val expeditionDate = voucher != null ? voucher.getFecha() : null;
				
				return StampResponse
						.builder()
						.stamped(true)
						.stampedXml(response.cfdi)
						.xml(xml)
						.uuid(response.uuid)
						.emitterSeal(response.selloCFDI)
						.satSeal(response.selloSAT)
						.certificateEmitterNumber(response.noCertificadoCFDI)
						.certificateSatNumber(response.noCertificadoSAT)
						.originalString(response.cadenaOriginalSAT)
						.qrCode(response.qrCode)
						.cfdv4(cfd)
						.voucher(voucher)
						.tfd(TfdParser.getTfd(response.cfdi))
						.dateStamp(response.fechaTimbrado)
						.dateExpedition(expeditionDate)
						.build();
			} else {
				return StampResponse.error(response.message);
			}
		} catch (Exception e) {
			System.out.println(String.format("#converter error - %s", e.getMessage()));
		}
		return null;
	}
	
	public static final StampResponse convert(Comprobante voucher, TimbreFiscalDigital tfd, String stampedXml) {
		if (tfd != null) {
			val stampResponse = StampResponse
									.builder()
									.stamped(true)
									.stampedXml(stampedXml)
									.uuid(tfd.getUUID())
									.emitterSeal(tfd.getSelloCFD())
									.satSeal(tfd.getSelloSAT())
									.certificateEmitterNumber(voucher.getNoCertificado())
									.certificateSatNumber(tfd.getNoCertificadoSAT())
									.originalString(PdfPrintUtils.generateOriginalString(tfd))
									.voucher(voucher)
									.dateStamp(tfd.getFechaTimbrado())
									.dateExpedition(voucher.getFecha())
									.build();
			
			val qrCode = ReportManagerTools.getQRString(stampResponse);
			stampResponse.setQrCode(qrCode);
			
			return stampResponse;
		}
		return null;
	}
	
}