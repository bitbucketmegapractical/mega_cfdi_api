package org.megapractical.invoicing.api.stamp.payload;

import org.megapractical.invoicing.sat.integration.v40.Comprobante;
import org.megapractical.invoicing.sat.integration.retention.v2.Retenciones;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StampProperties {
	// Id del emisor de la factura
	private String emitterId;
	
	// Id del usuario en sesion
	private String sessionId;
	
	// Id del producto
	private String sourceSystem;
	
	// Valor generado aleatorio para cada timbrado
	private String correlationId;
	
	// Comprobante
	private Comprobante voucher;
	
	// Retencion
	private Retenciones retention;
	
	// Certificado
	private byte[] certificate;
	
	// Llave privada
	private byte[] privateKey;
	
	// Clave de la llave privada
	private String passwd;
	
	// Numero del certificado
	private String certificateNumber;
	
	// Ambiente (PRO|DEV)
	private String environment;
	
	// Servicios (mega|sw)
	private String services;
}