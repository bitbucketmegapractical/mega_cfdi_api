package org.megapractical.invoicing.api.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmationCodeProperties {
	// Id del emisor de la factura
	private String emitterId;
	// Id del usuario en sesion
	private String sessionId;
	// Id del producto
	private String sourceSystem;
	// Valor generado aleatorio para cada timbrado
	private String correlationId;
	// Ambiente (PRO|DEV)
	private Integer environment;
	private String rfc;
}