package org.megapractical.invoicing.api.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmationCodeResponse {
	@Default
	private boolean isSuccess = Boolean.FALSE;
	private String confirmationCode;
	private String error;
	private String errorCode;
	private String errorMessage;
	@Default
	private Boolean malformedUrlException = Boolean.FALSE;
	@Default
    private Boolean datatypeConfigurationException = Boolean.FALSE;
	@Default
    private Boolean wsdlException = Boolean.FALSE;
}