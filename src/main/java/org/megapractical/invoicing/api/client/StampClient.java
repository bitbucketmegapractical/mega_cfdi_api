package org.megapractical.invoicing.api.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.wsdl.CFDIEncabezadoPeticion;
import org.megapractical.invoicing.api.wsdl.CFDIEncabezadoRespuesta;
import org.megapractical.invoicing.api.wsdl.CFDIv33ServiceMediationServiceagent;
import org.megapractical.invoicing.api.wsdl.ComprobanteRequest;
import org.megapractical.invoicing.api.wsdl.PortTypeCFDIv33;
import org.megapractical.invoicing.api.wsdl.WsdlError;

/**
 * @author Maikel Guerra Ferrer
 */
public class StampClient {

	private static final String OPERATION_CODE = "stamp";
    private static final String OPERATION_VERSION = UProperties.getOperationVersion();
    private static final String OPERATION_NAME = UProperties.getOperationName(OPERATION_CODE);
    private static final QName SERVICE_NAME = new QName("http://xmlns.megacfdi.com/1499759600080", "CFDIv33_service_mediation.serviceagent");
    
    private static final String WSDL_TEST = "https://servicios-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl";
    private static final String WSDL_PROD = "https://servicios-prod.megacfdi.com:30100/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl";
    
    private static final String ENDPOINT_ADDRESS_PROPERTY_QA = "https://servicios-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent/PortTypeCFDIv33Endpoint1";
    private static final String ENDPOINT_ADDRESS_PROPERTY_PROD = "https://servicios-prod.megacfdi.com:30100/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent/PortTypeCFDIv33Endpoint1";
    private static Boolean applicationPrintValues = UProperties.getApplicationPrintValues();
    
    /**
     * Send WS Stamp request.
     *
     * @param pRequest: Object comprobante to be stamping.
     * @param sourceSystem: System who are stamping.
     * @return
     * @throws org.megapractical.invoicing.api.wsdl.WsdlError 
     * @throws MalformedURLException 
     * @throws com.WsdlError.xmlns._1492125642696.Error 
     */
    public static StampResponse stampRequest(ComprobanteRequest pRequest, String emiterId, String sessionId, String sourceSystem, String correlationId, String environment) throws org.megapractical.invoicing.api.wsdl.WsdlError {
    	String endPoint = null;
    	
	    StampResponse stampResponse = new StampResponse();
    	
	    System.out.println("-----------------------------------------------------------------------------------------------------------");
    	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] STAMP OPERATION INSTANCE > STAMP REQUEST");
    	
    	try {    		
            
        	GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(new Date());
            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            
            String wsdl = "";
            
            URL url = null;
            try {	
            	if(environment.equalsIgnoreCase("prod")) {
            		url = new URL(WSDL_PROD);
                    wsdl = "wsdl/prod/StampCfdiV33.wsdl";   
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_PROD;
                } else {
                	url = new URL(WSDL_TEST);
                    wsdl = "wsdl/test/StampCfdiV33Test.wsdl";
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_QA;
                }
			} catch (MalformedURLException e) {
				e.printStackTrace();
				stampResponse.setMalformedUrlException(true);
				return stampResponse;
			}                    
            
            if (applicationPrintValues) {
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] STAMP OPERATION INSTANCE > WSDL SELECTED: " + wsdl);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] STAMP OPERATION INSTANCE > WSDL OPERATION NAME: " + OPERATION_NAME);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] STAMP OPERATION INSTANCE > INVOKING WSDL...");
            	System.out.println("-----------------------------------------------------------------------------------------------------------\n");
            }
                       
            //##### Objeto del servicio.
            CFDIv33ServiceMediationServiceagent serviceAgent = new CFDIv33ServiceMediationServiceagent(url, SERVICE_NAME);
            //PortTypeCFDIv33 port = serviceAgent.getPortTypeCFDIv33Endpoint1();
            PortTypeCFDIv33 port = serviceAgent.getPortTypeCFDIv33Endpoint1(); 
            
            //##### Especificando el endpoint
            BindingProvider bindingProvider = (BindingProvider) port;
            String timeOut = Integer.toString(1000 * 60 * 20);            
            bindingProvider.getRequestContext().put("javax.xml.ws.client.receiveTimeout", timeOut);
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);

            CFDIEncabezadoPeticion _encabezadoPeticion = new CFDIEncabezadoPeticion();
            _encabezadoPeticion.setFechaTransaccion(date);
            _encabezadoPeticion.setHoraTransaccion(date);
            _encabezadoPeticion.setNombreOperacion(OPERATION_NAME);
            _encabezadoPeticion.setVersionOperacion(OPERATION_VERSION);
            _encabezadoPeticion.setIDCliente(emiterId); // Id del emisor
            _encabezadoPeticion.setSistemaOrigen(sourceSystem); // Nombre producto o servicio
            _encabezadoPeticion.setCorrelationID(correlationId); // Codigo para la transaccion
            _encabezadoPeticion.setSesion(sessionId); // Id del usuario           

            Holder<CFDIEncabezadoRespuesta> _encabezadoSOA = new Holder<>();
            Holder<String> _respuesta = new Holder<>();

            try {
            	port.comprobanteCFDIv33(_encabezadoPeticion, pRequest, _encabezadoSOA, _respuesta);
            	
            	Integer resultado_operacion = (int)_encabezadoSOA.value.getResultado().getResultadoOperacion();
            	String error = _encabezadoSOA.value.getResultado().getSistemaRespuesta();
            	String codigoRespuesta = _encabezadoSOA.value.getResultado().getCodigoRespuesta();
            	String mensajeRespuesta = _encabezadoSOA.value.getResultado().getMensajeRespuesta();
            	
            	if(resultado_operacion == 1) {
            		stampResponse.setStampedXml(_respuesta.value);
            	} else if(resultado_operacion == 2) {
            		stampResponse.setStamped(Boolean.FALSE);
            		stampResponse.setError(error);
            		stampResponse.setErrorCode(codigoRespuesta);
            		stampResponse.setErrorMessage(mensajeRespuesta);
            		System.out.println();
                    System.out.println("-----------------------------------------------------------------------------------------------------------");
                    System.out.println(UPrint.consoleDatetime() + "[ERROR] MEGA-CFDI API > CFDI STAMP OPERATION INSTANCE > WSDL RESPONSE > " + stampResponse.getErrorMessage());
            	}
            	
		        return stampResponse;
		        
			} catch (WsdlError e) {
				stampResponse.setWsdlException(true);
				e.printStackTrace();
			}
        } catch (DatatypeConfigurationException | Error ex) {
            Logger.getLogger(StampClient.class.getName()).log(Level.SEVERE, null, ex);
            stampResponse.setDatatypeConfigurationException(true);
        }
    	
    	return null;
    }

}
