package org.megapractical.invoicing.api.client;

import org.megapractical.invoicing.api.wsdl.retention.CFDIEncabezadoPeticion;
import org.megapractical.invoicing.api.wsdl.retention.CFDIEncabezadoRespuesta;
import org.megapractical.invoicing.api.wsdl.retention.schema.RetencionesRequest;
import org.megapractical.invoicing.api.wsdl.retention.schema.RetencionesResponse;
import org.megapractical.invoicing.api.wsdl.retention.xmlns.IntfCFDIRetencionesInXmlServiceagent;
import org.megapractical.invoicing.api.wsdl.retention.xmlns.PortTypeCFDIRyP;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.megapractical.invoicing.api.stamp.payload.StampResponse;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;

/**
 * @author Maikel
 */
public class RetentionStampClient {

	private static final String OPERATION_CODE = "ret_stamp";
	private static final String OPERATION_VERSION = UProperties.getOperationVersion();
    private static final String OPERATION_NAME = UProperties.getOperationName(OPERATION_CODE);
    private static final QName SERVICE_NAME = new QName("http://xmlns.example.com/1471628556973", "intfCFDI_Retenciones_in_xml.serviceagent");

    private static final String WSDL_TEST = "https://servicios-qa.megacfdi.com:20212/CFDI_Retenciones/StarterProcesses/WebServices/intfCFDI_Retenciones_in_xml.serviceagent?wsdl";
    private static final String WSDL_PROD = "https://servicios-prod.megacfdi.com:10211/CFDI_Retenciones/StarterProcesses/WebServices/intfCFDI_Retenciones_in_xml.serviceagent?wsdl";
    private static final String ENDPOINT_ADDRESS_PROPERTY_QA = "https://servicios-qa.megacfdi.com:20212/CFDI_Retenciones/StarterProcesses/WebServices/intfCFDI_Retenciones_in_xml.serviceagent/PortTypeCFDI_RyPEndpoint1";
    private static final String ENDPOINT_ADDRESS_PROPERTY_PROD = "https://servicios-prod.megacfdi.com:10211/CFDI_Retenciones/StarterProcesses/WebServices/intfCFDI_Retenciones_in_xml.serviceagent/PortTypeCFDI_RyPEndpoint1";
    
    /**
     * Send WS Stamp request.
     *
     * @param pRequest: Object comprobante to be stamping.
     * @param sourceSystem: System who are stamping.
     * @return
     */
    public static StampResponse stampRequest(RetencionesRequest retRequest, String emiterId, String sessionId, String sourceSystem, String correlationId, String environment) {
    	StampResponse stampResponse = new StampResponse();
    	String endPoint = null;

    	System.out.println("-----------------------------------------------------------------------------------------------------------");
    	System.out.println(UPrint.consoleDatetime() + "[INFO] MEGA-CFDI API > STAMP OPERATION INSTANCE > STAMP REQUEST");
    	
    	try {
    		
    		GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(new Date());
            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            
            String wsdl = "";
            
            URL url = null;
            try {
            	
            	if(environment.equalsIgnoreCase("prod")) {//##### PROD
	            	url = new URL(WSDL_PROD);
	                wsdl = "wsdl/prod/StampRetencion.wsdl"; 
	                endPoint = ENDPOINT_ADDRESS_PROPERTY_PROD;
	            } else {//##### DEV/QA
	            	url = new URL(WSDL_TEST);
	                wsdl = "wsdl/test/StampRetencionTest.wsdl";
	                endPoint = ENDPOINT_ADDRESS_PROPERTY_QA;
	            }
            	
            } catch (MalformedURLException e1) {
				e1.printStackTrace();
				stampResponse.setMalformedUrlException(true);
				return stampResponse;
            }
            
            System.out.println("-----------------------------------------------------------------------------------------------------------");
        	System.out.println(UPrint.consoleDatetime() + "[INFO] MEGA-CFDI API > STAMP OPERATION INSTANCE > WSDL SELECTED: " + wsdl);
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
        	System.out.println(UPrint.consoleDatetime() + "[INFO] MEGA-CFDI API > STAMP OPERATION INSTANCE > WSDL OPERATION NAME: " + OPERATION_NAME);
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
        	System.out.println(UPrint.consoleDatetime() + "[INFO] MEGA-CFDI API > STAMP OPERATION INSTANCE > INVOKING WSDL...");
        	System.out.println("-----------------------------------------------------------------------------------------------------------\n");
            
            //##### Objeto del servicio.
            IntfCFDIRetencionesInXmlServiceagent ss = new IntfCFDIRetencionesInXmlServiceagent(url, SERVICE_NAME);
            PortTypeCFDIRyP port = ss.getPortTypeCFDIRyPEndpoint1();

            //##### Especificando el endpoint
	        BindingProvider bindingProvider = (BindingProvider) port;
	        String timeOut = Integer.toString(1000 * 60 * 20);            
	        bindingProvider.getRequestContext().put("javax.xml.ws.client.receiveTimeout", timeOut);
	        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            
            CFDIEncabezadoPeticion _encabezadoPeticion = new CFDIEncabezadoPeticion();
            _encabezadoPeticion.setFechaTransaccion(date);
            _encabezadoPeticion.setHoraTransaccion(date);
            _encabezadoPeticion.setNombreOperacion(OPERATION_NAME);
            _encabezadoPeticion.setVersionOperacion(OPERATION_VERSION);
            _encabezadoPeticion.setIDCliente(emiterId); //##### Id del emisor
            _encabezadoPeticion.setSistemaOrigen(sourceSystem); //##### Nombre producto o servicio
            _encabezadoPeticion.setCorrelationID(correlationId); //##### Codigo para la transaccion
            _encabezadoPeticion.setSesion(sessionId); //##### Id del usuario
            
            Holder<CFDIEncabezadoRespuesta> _encabezadoSOA = new Holder<>();
            Holder<RetencionesResponse> _respuesta = new Holder<>();

            try {
            	
				port.comprobanteCFDIRyP(_encabezadoPeticion, retRequest, _encabezadoSOA, _respuesta);
				
				Integer resultado_operación = (int)_encabezadoSOA.value.getResultado().getResultadoOperacion();
            	String error = _encabezadoSOA.value.getResultado().getSistemaRespuesta();
            	String codigoRespuesta = _encabezadoSOA.value.getResultado().getCodigoRespuesta();
            	String mensajeRespuesta = _encabezadoSOA.value.getResultado().getMensajeRespuesta();
				
            	if(resultado_operación == 1) {
            		stampResponse.setXml(_respuesta.value.getRetencionesString().getBytes());
            	} else if(resultado_operación == 2) {
            		stampResponse.setStamped(Boolean.FALSE);
            		stampResponse.setError(error);
            		stampResponse.setErrorCode(codigoRespuesta);
            		stampResponse.setErrorMessage(mensajeRespuesta);
            		System.out.println();
                    System.out.println("-----------------------------------------------------------------------------------------------------------");
                    System.out.println(UPrint.consoleDatetime() + "[ERROR] MEGA-CFDI API > CFDI STAMP OPERATION INSTANCE > WSDL RESPONSE > " + stampResponse.getErrorMessage());
            	}
				
		        return stampResponse;
		        
			} catch (org.megapractical.invoicing.api.wsdl.retention.xmlns.Error e) {				
				e.printStackTrace();
				return null;
			}
            
        } catch (DatatypeConfigurationException | Error ex) {
            Logger.getLogger(StampClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}