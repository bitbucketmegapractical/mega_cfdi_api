package org.megapractical.invoicing.api.client;

import java.util.ArrayList;
import java.util.List;

public class CancelationProperties {

	//##### Id del emisor de la factura
	private String emitterId;
	
	//##### Id del usuario en sesion
	private String sessionId;
	
	//##### Id del producto
	private String sourceSystem;
	
	//##### Valor generado aleatorio para cada timbrado
	private String correlationId;
	
	//##### Ambiente (PRO|DEV)
	private String environment;
	
	//##### Servicios (mega|sw)
	private String services;
	
	//##### Rfc del emisor
	private String emitterRfc;
	
	//##### Certificado del emisor
	private byte[] certificate;
	
	//##### Llave privada del emisor
	private byte[] privateKey;
	
	//##### Clave de la llave privada
	private String passwd;
	
	private CfdiCancelation cfdiCancelation;
	private List<CfdiCancelation> cfdiCancelations;
	
	public static class CfdiCancelation{
		private String uuid;
		private String emitterRfc;
		private String receiverRfc;
		private String voucherTotal;

		/*Getters and Setters*/
		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public String getReceiverRfc() {
			return receiverRfc;
		}

		public void setReceiverRfc(String receiverRfc) {
			this.receiverRfc = receiverRfc;
		}

		public String getVoucherTotal() {
			return voucherTotal;
		}

		public void setVoucherTotal(String voucherTotal) {
			this.voucherTotal = voucherTotal;
		}

		public String getEmitterRfc() {
			return emitterRfc;
		}

		public void setEmitterRfc(String emitterRfc) {
			this.emitterRfc = emitterRfc;
		}
	}
	
	/*Getters and Setters*/
	public CancelationProperties(){
		super();
	}

	public String getEmitterId() {
		return emitterId;
	}

	public void setEmitterId(String emitterId) {
		this.emitterId = emitterId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public byte[] getCertificate() {
		return certificate;
	}

	public void setCertificate(byte[] certificate) {
		this.certificate = certificate;
	}

	public byte[] getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(byte[] privateKey) {
		this.privateKey = privateKey;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getEmitterRfc() {
		return emitterRfc;
	}

	public void setEmitterRfc(String emitterRfc) {
		this.emitterRfc = emitterRfc;
	}

	public CfdiCancelation getCfdiCancelation() {
		return cfdiCancelation;
	}

	public void setCfdiCancelation(CfdiCancelation cfdiCancelation) {
		this.cfdiCancelation = cfdiCancelation;
	}

	public List<CfdiCancelation> getCfdiCancelations() {
		if(cfdiCancelations == null) {
			cfdiCancelations = new ArrayList<>();
		}
		return cfdiCancelations;
	}

	public void setCfdiCancelations(List<CfdiCancelation> cfdiCancelations) {
		this.cfdiCancelations = cfdiCancelations;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}
}