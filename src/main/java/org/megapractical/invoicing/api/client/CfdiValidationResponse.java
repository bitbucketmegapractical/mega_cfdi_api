package org.megapractical.invoicing.api.client;

public class CfdiValidationResponse {

	private boolean valid = Boolean.FALSE;
	private byte[] response;
	
	private String error;
    private String errorCode;    
    private String errorMessage;
    
    private Boolean malformedUrlException = Boolean.FALSE;
    private Boolean datatypeConfigurationException = Boolean.FALSE;
    private Boolean wsdlException = Boolean.FALSE;
    
    public CfdiValidationResponse(){    	
    }

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Boolean getMalformedUrlException() {
		return malformedUrlException;
	}

	public void setMalformedUrlException(Boolean malformedUrlException) {
		this.malformedUrlException = malformedUrlException;
	}

	public Boolean getDatatypeConfigurationException() {
		return datatypeConfigurationException;
	}

	public void setDatatypeConfigurationException(Boolean datatypeConfigurationException) {
		this.datatypeConfigurationException = datatypeConfigurationException;
	}

	public Boolean getWsdlException() {
		return wsdlException;
	}

	public void setWsdlException(Boolean wsdlException) {
		this.wsdlException = wsdlException;
	}

	public byte[] getResponse() {
		return response;
	}

	public void setResponse(byte[] response) {
		this.response = response;
	}	
}