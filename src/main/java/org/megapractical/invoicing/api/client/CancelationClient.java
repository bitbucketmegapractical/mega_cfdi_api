package org.megapractical.invoicing.api.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.megapractical.invoicing.api.client.CancelationProperties.CfdiCancelation;
import org.megapractical.invoicing.api.smarterweb.SWRetCancellationResponse;
import org.megapractical.invoicing.api.smarterweb.SWCfdiStatusRequest;
//import org.megapractical.invoicing.api.smarterweb.SWRetCancellationResponse.CancellationResult;
//import org.megapractical.invoicing.api.smarterweb.SWRetCancellationResponse.CancellationResult.StatusByUuid;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.util.UValidator;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelaCFDPeticion;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelaCFDRequest;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelaCFDResponse;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelaCFDResultado;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelaCFDRq;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelaCFDRs;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelacionService;
import org.megapractical.invoicing.api.wsdl.cancelation.CancelacionServiceServiceagent;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaCFDPeticion;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaSatusCFDResultado;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaStatusCFDRequest;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaStatusCFDResponse;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaStatusCFDResultado;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaStatusCFDRq;
import org.megapractical.invoicing.api.wsdl.cancelation.ConsultaStatusCFDRs;
import org.megapractical.invoicing.api.wsdl.cancelation.HeaderRq;
import org.megapractical.invoicing.api.wsdl.cancelation.HeaderRs;
import org.megapractical.invoicing.api.wsdl.cancelation.sat.SATService;
import org.megapractical.invoicing.api.wsdl.cancelation.sat.SATServiceServiceagent;

public class CancelationClient {

	/*private static final String OPERATION_CODE = "cancelaCFD";
    private static final String OPERATION_NAME = UProperties.getOperationName(OPERATION_CODE);
    private static final String OPERATION_VERSION = UProperties.getOperationVersion();
    
    private static final QName SERVICE_NAME = new QName("http://www.mcfdi.com/wsdl/cfdi/cancelacion/1.0", "CancelacionService.serviceagent");
    
    private static final String WSDL_QA = "https://servicios-qa.megacfdi.com:30210/Service/StarterProcesses/WebServices/CancelacionService.serviceagent?wsdl";
    private static final String ENDPOINT_ADDRESS_PROPERTY_QA = "https://servicios-qa.megacfdi.com:30210/CancelacionService";
    
    private static final String WSDL_PROD = "https://servicios-prod.megacfdi.com:30110/Service/StarterProcesses/WebServices/CancelacionService.serviceagent?wsdl";
    private static final String ENDPOINT_ADDRESS_PROPERTY_PROD = "https://servicios-prod.megacfdi.com:30110/CancelacionService";
    
    private static final QName SAT_SERVICE_NAME = new QName("http://www.mcfdi.com/wsdl/cfdi/sat/1.0", "SATService.serviceagent");
    
    private static final String WSDL_SAT_QA = "https://servicios-qa.megacfdi.com:30216/Service/StarterProcesses/WebServices/SATService.serviceagent?wsdl";
    private static final String SAT_ENDPOINT_ADDRESS_PROPERTY_QA = "https://servicios-qa.megacfdi.com:30216/SATService";
    
    private static final String WSDL_SAT_PROD = "https://servicios-prod.megacfdi.com:30116/Service/StarterProcesses/WebServices/SATService.serviceagent?wsdl";
    private static final String SAT_ENDPOINT_ADDRESS_PROPERTY_PROD = "https://servicios-prod.megacfdi.com:30116/SATService";
    
    private static Boolean applicationPrintValues = UProperties.getApplicationPrintValues();
    
    private static Map<String, String[]> map = null;
    
    public static SWRetCancellationResponse cancelationRequest(CancelationProperties properties){
    	SWRetCancellationResponse response = new SWRetCancellationResponse();
    	String wsdl = "";
    	String endPoint = null;
    	
    	try {
			
    		String environment = properties.getEnvironment();
    		String emiterId = properties.getEmitterId();
    		String sourceSystem = properties.getSourceSystem();
    		String correlationId = properties.getCorrelationId();
    		String sessionId = properties.getSessionId();
    		String emitterRfc = properties.getEmitterRfc();
    		
    		CfdiCancelation cfdiCancelation = properties.getCfdiCancelation();
    		List<CfdiCancelation> cfdiCancelations = properties.getCfdiCancelations();
    		
    		byte[] certificate = properties.getCertificate();
    		byte[] privateKey = properties.getPrivateKey();
    		String passwd = properties.getPasswd();
    		
    		System.out.println("-----------------------------------------------------------------------------------------------------------");
    		System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > CANCELATION REQUEST");
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
            
            URL url = null;
            String wsdlPrint = null;
            try {
            	if(environment.equalsIgnoreCase("prod")) {
            		url = new URL(WSDL_PROD);
                    wsdl = WSDL_PROD;
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_PROD;
                    wsdlPrint = "WSDL_PROD:30110";
                } else {
                	url = new URL(WSDL_QA);
                    wsdl = WSDL_QA;
                    wsdlPrint = "WSDL_QA:30210";
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_QA;
                }
			} catch (MalformedURLException e) {
				e.printStackTrace();
				response.setMalformedUrlException(true);
				return response;
			}
            
            if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > WSDL > " + wsdlPrint);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > OPERATION NAME > " + OPERATION_NAME);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > INVOKING WSDL...");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            }
            
            CancelacionServiceServiceagent serviceAgent = new CancelacionServiceServiceagent(url, SERVICE_NAME);
            CancelacionService port = serviceAgent.getCancelacionService();
            
            //##### Especificando el endpoint
            BindingProvider bindingProvider = (BindingProvider) port;
            String timeOut = Integer.toString(1000 * 60 * 20);            
            bindingProvider.getRequestContext().put("javax.xml.ws.client.receiveTimeout", timeOut);
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            
            HeaderRq headerRq = new HeaderRq();
            headerRq.setIDCorrelacion(correlationId); //##### Codigo para la transaccion
            headerRq.setNombreOperacion(OPERATION_NAME); //##### Nombre de la operacion
            headerRq.setReferencia(emiterId); //##### Id del emisor
            headerRq.setSesion(sessionId); //##### Id del usuario
            headerRq.setSistemaOrigen(sourceSystem); //##### Nombre producto o servicio
            headerRq.setVersionOperacion(OPERATION_VERSION); //##### Version de la operacion
            
            List<CancelaCFDPeticion> cancelaUuids = new ArrayList<>();
            if(!cfdiCancelations.isEmpty()) {
            	map = new HashMap<>();
            	for (CfdiCancelation item : cfdiCancelations) {
            		CancelaCFDPeticion cancelaCFDPeticion = new CancelaCFDPeticion();
                	cancelaCFDPeticion.setUUID(item.getUuid());
                	cancelaCFDPeticion.setRFCReceptor(item.getReceiverRfc());
                	cancelaCFDPeticion.setTotalComprobante(item.getVoucherTotal());
                	cancelaUuids.add(cancelaCFDPeticion);
                	
                	String[] values = {item.getReceiverRfc(), item.getVoucherTotal()};
                	map.put(item.getUuid(), values);
				}
            }else if(!UValidator.isNullOrEmpty(cfdiCancelation)) {
            	CancelaCFDPeticion cancelaCFDPeticion = new CancelaCFDPeticion();
            	cancelaCFDPeticion.setUUID(cfdiCancelation.getUuid());
            	cancelaCFDPeticion.setRFCReceptor(cfdiCancelation.getReceiverRfc());
            	cancelaCFDPeticion.setTotalComprobante(cfdiCancelation.getVoucherTotal());
                cancelaUuids.add(cancelaCFDPeticion);
            }
            
            CancelaCFDRq cancelaCFDRq = new CancelaCFDRq();
            cancelaCFDRq.setCertificadoDatos(certificate);
            cancelaCFDRq.setLlaveDatos(privateKey);
            cancelaCFDRq.setClaveLlave(passwd);
            cancelaCFDRq.setRFCEmisor(emitterRfc);
            cancelaCFDRq.getCancelaCFDPeticion().addAll(cancelaUuids);
            
            CancelaCFDRequest parameters = new CancelaCFDRequest();
            parameters.setHeaderRq(headerRq);
            parameters.setCancelaCFDRq(cancelaCFDRq);
            
            try {
				
            	CancelaCFDResponse cancelaCFD = port.cancelaCFD(parameters);
            	CancelaCFDRs cancelaCFDRs = cancelaCFD.getCancelaCFDRs();
            	
            	HeaderRs headerRs = cancelaCFD.getHeaderRs(); 
            	Integer transactionResult = headerRs.getResultadoTransaccion();
            	String transactionResultType = headerRs.getTipoResultadoTransaccion();
            	
            	String date = cancelaCFDRs.getFecha();
            	String emitterRfcResponse = cancelaCFDRs.getRFCEmisor();
            	String acuseXml = cancelaCFDRs.getAcuseDatos();
            	
            	if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > EVALUATING WSDL RESPONSE...");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
            	
            	CancellationResult cancelationResult = new CancellationResult();
            	List<StatusByUuid> statusByUuids = new ArrayList<>();
            	
            	List<CancelaCFDResultado> cancelaCFDResultados = cancelaCFDRs.getCancelaCFDResultado();
            	for (CancelaCFDResultado item : cancelaCFDResultados) {
            		ConsultaSatusCFDResultado result = item.getConsultaSatusCFDResultado();
            		String statusCode = result.getCodigoStatusCFD().replace("\"", "");
            		String cancelableText = result.getIndicadorCancelableCFD().replace("\"", "");
            		String cancelableStatus = result.getStatusCancelacion().replace("\"", "");
            		String status = result.getStatusCFD();
            		String acuseDatos = result.getAcuseDatos();
            		String statusCFD = item.getStatusCFD();
            		
            		StatusByUuid statusByUuid = new StatusByUuid();
            		if(cancelableText.equalsIgnoreCase("Cancelable sin aceptación") && !status.equalsIgnoreCase("Cancelado")) {
            			SWCfdiStatusRequest sWCfdiStatusRequest = new SWCfdiStatusRequest();
            			sWCfdiStatusRequest.setEnvironment(environment);
                		sWCfdiStatusRequest.setEmitterRfc(emitterRfc);
                		
                		if(!UValidator.isNullOrEmpty(cfdiCancelation)) {
                			sWCfdiStatusRequest.setReceiverRfc(cfdiCancelation.getReceiverRfc());
                			sWCfdiStatusRequest.setUuid(cfdiCancelation.getUuid());
                			sWCfdiStatusRequest.setVoucherTotal(cfdiCancelation.getVoucherTotal());
                		}else {
                			String[] values = map.get(item.getUUID());
                			
                			sWCfdiStatusRequest.setUuid(item.getUUID());
                			sWCfdiStatusRequest.setReceiverRfc(values[0]);
                			sWCfdiStatusRequest.setVoucherTotal(values[1]);
                		}
            			
            			SWRetCancellationResponse sWRetCancellationResponse = satStatusCheckRequest(sWCfdiStatusRequest);
            			transactionResult = sWRetCancellationResponse.getCancellationResult().getTransactionResult();
            			transactionResultType = sWRetCancellationResponse.getCancellationResult().getTransactionResultType();
            			statusByUuid = sWRetCancellationResponse.getCancellationResult().getStatusByUuid();
            		}else {
                		statusByUuid.setResponseStatusCode(statusCode);
                		statusByUuid.setResponseCancelableText(cancelableText);
                		statusByUuid.setResponseCancelableStatus(cancelableStatus);
                		statusByUuid.setResponseAcuseXml(acuseDatos);
                		statusByUuid.setResponseStatus(status);
            		}
            		statusByUuid.setUuid(item.getUUID());
            		statusByUuid.setStatus(statusCFD);	
            		
            		statusByUuids.add(statusByUuid);
				}
            	
            	cancelationResult.setEmitterRfc(emitterRfcResponse);
            	cancelationResult.setDate(date);
            	cancelationResult.setAcuseXml(acuseXml);
            	cancelationResult.setTransactionResult(transactionResult);
            	cancelationResult.setTransactionResultType(transactionResultType);
            	cancelationResult.getStatusByUuids().addAll(statusByUuids);
            	
            	response.setCancellationResult(cancelationResult);
            	response.setSuccess(Boolean.TRUE);
            	
            	if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > WSDL RESPONSE EVALUATED");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
            	
			} catch (Exception e) {
				e.printStackTrace();
				
				String uuids = null;
				List<String> uuidList = new ArrayList<>();
				if(!cancelaUuids.isEmpty()) {
					for (CancelaCFDPeticion item : cancelaUuids) {
						String uuid = item.getUUID(); 
						if(UValidator.isNullOrEmpty(uuids)) {
							uuids = uuid;
						}else {
							uuids += "," + uuid;
						}
						uuidList.add(uuid);
					}
				}
				
				response.setSuccess(Boolean.FALSE);
				response.setUuidError(uuids);
				response.getUuidsError().addAll(uuidList);
				response.setWsdlException(Boolean.TRUE);
				response.setWsdlUrl(wsdl);
				response.setWsdlOperation(OPERATION_NAME);
				response.setWsdlEndpoint(endPoint);
				response.setWsdlErrorTrace(e.getMessage());
				
				if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > WSDL RESPONSE NULL");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
			}
    		
		} catch (Exception e) {
			e.printStackTrace();
			
			response.setSuccess(Boolean.FALSE);
			response.setUuidError(properties.getCfdiCancelation().getUuid());
			response.setWsdlException(Boolean.TRUE);
			response.setWsdlUrl(wsdl);
			response.setWsdlOperation(OPERATION_NAME);
			response.setWsdlEndpoint(endPoint);
			response.setWsdlErrorTrace(e.getMessage());
			
			if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > WSDL RESPONSE ERROR");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            }
		}
    	
    	return response;
    }
    
    private static final String OPERATION_STATUS_CODE = "status";
    private static final String OPERATION_STATUS_NAME = UProperties.getOperationName(OPERATION_STATUS_CODE);
    
    public static SWRetCancellationResponse statusCheckRequest(SWCfdiStatusRequest properties){
    	SWRetCancellationResponse response = new SWRetCancellationResponse();
    	String wsdl = "";
    	String endPoint = "";
    	
    	try {
			
    		String environment = properties.getEnvironment();
    		String emitterRfc = properties.getEmitterRfc();
    		String uuid = properties.getUuid();
    		
    		System.out.println("-----------------------------------------------------------------------------------------------------------");
    		System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > CHECK STATUS REQUEST");
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
        	
            URL url = null;
            String wsdlPrint = null;
            try {
            	if(environment.equalsIgnoreCase("prod")) {
            		url = new URL(WSDL_PROD);
                    wsdl = WSDL_PROD;
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_PROD;
                    wsdlPrint = "WSDL_PROD:30110";
                } else {
                	url = new URL(WSDL_QA);
                    wsdl = WSDL_QA;
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_QA;
                    wsdlPrint = "WSDL_QA:30210";
                }
			} catch (MalformedURLException e) {
				e.printStackTrace();
				response.setMalformedUrlException(true);
				return response;
			}
            
            if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > WSDL > " + wsdlPrint);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > OPERATION NAME > " + OPERATION_STATUS_NAME);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > INVOKING WSDL...");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            }
            
            CancelacionServiceServiceagent serviceAgent = new CancelacionServiceServiceagent(url, SERVICE_NAME);
            CancelacionService port = serviceAgent.getCancelacionService();
            
            //##### Especificando el endpoint
            BindingProvider bindingProvider = (BindingProvider) port;
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            
            HeaderRq headerRq = new HeaderRq();
            headerRq.setNombreOperacion(OPERATION_STATUS_NAME); //##### Nombre de la operacion
            headerRq.setVersionOperacion(OPERATION_VERSION); //##### Version de la operacion
            
            ConsultaStatusCFDRq consultaStatusCFDRq = new ConsultaStatusCFDRq();
            consultaStatusCFDRq.setRFCEmisor(emitterRfc);
            
            ConsultaCFDPeticion consultaCFDPeticion = new ConsultaCFDPeticion();
        	consultaCFDPeticion.setUUID(uuid);
        	
            List<ConsultaCFDPeticion> consultaCFDPeticions = new ArrayList<>();
        	consultaCFDPeticions.add(consultaCFDPeticion);
            consultaStatusCFDRq.getConsultaCFDPeticion().addAll(consultaCFDPeticions);
            
            ConsultaStatusCFDRequest parameters = new ConsultaStatusCFDRequest();
            parameters.setHeaderRq(headerRq);
            parameters.setConsultaStatusCFDRq(consultaStatusCFDRq);
            
            try {
            	
            	ConsultaStatusCFDResponse consultaStatusCFDResponse = port.consultaStatusCFD(parameters);
            	ConsultaStatusCFDRs consultaStatusCFDRs = consultaStatusCFDResponse.getConsultaStatusCFDRs();
                List<ConsultaStatusCFDResultado> consultaStatusCFDResultados = consultaStatusCFDRs.getConsultaStatusCFDResultado();
                
                HeaderRs headerRs = consultaStatusCFDResponse.getHeaderRs();
            	Integer transactionResult = headerRs.getResultadoTransaccion();
            	String transactionResultType = headerRs.getTipoResultadoTransaccion();
                
                if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > EVALUATING WSDL RESPONSE...");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
                
                CancellationResult cancelationResult = new CancellationResult();
                cancelationResult.setTransactionResult(transactionResult);
            	cancelationResult.setTransactionResultType(transactionResultType);
            	
                List<StatusByUuid> statusByUuids = new ArrayList<>();
                for (ConsultaStatusCFDResultado item : consultaStatusCFDResultados) {
                	StatusByUuid statusByUuid = new StatusByUuid();
            		statusByUuid.setUuid(item.getUUID());
            		statusByUuid.setStatus(item.getStatusCFD());
            		
            		statusByUuid.setResponseAcuseXml(item.getAcuseCancelacionDatos());
            		statusByUuid.setResponseStatusCode(item.getCodigoStatusCFD().replace("\"", ""));
            		statusByUuid.setResponseCancelableText(item.getIndicadorCancelableCFD().replace("\"", ""));
            		statusByUuid.setResponseCancelableStatus(item.getStatusCancelacion().replace("\"", ""));
            		statusByUuid.setResponseStatus(item.getStatusCFD());
            		
            		statusByUuids.add(statusByUuid);
    			}
                cancelationResult.getStatusByUuids().addAll(statusByUuids);
            	response.setCancellationResult(cancelationResult);
            	
            	response.setSuccess(Boolean.TRUE);
            	
            	if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > WSDL RESPONSE EVALUATED");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
            	
			} catch (Exception e) {
				response.setSuccess(Boolean.FALSE);
				response.setUuidError(uuid);
				response.setErrorCode("CFDI205");
				
				if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > WSDL RESPONSE NULL");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
			}
    		
		} catch (Exception e) {			
			e.printStackTrace();
			
			response.setSuccess(Boolean.FALSE);
			response.setUuidsError(properties.getUuids());
			response.setWsdlException(Boolean.TRUE);
			response.setWsdlUrl(wsdl);
			response.setWsdlOperation(OPERATION_STATUS_NAME);
			response.setWsdlEndpoint(endPoint);
			response.setWsdlErrorTrace(e.getMessage());
			
			if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI CHECK STATUS INSTANCE > WSDL RESPONSE ERROR");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            }
		}
    	
    	return response;
    }
    
    public static SWRetCancellationResponse satStatusCheckRequest(SWCfdiStatusRequest properties){
    	SWRetCancellationResponse response = new SWRetCancellationResponse();
    	String wsdl = "";
    	String endPoint = "";
    	
    	try {
			
    		String environment = properties.getEnvironment();
    		String emitterRfc = properties.getEmitterRfc();
    		String receiverRfc = properties.getReceiverRfc();
    		String voucherTotal = properties.getVoucherTotal();
    		String uuid = properties.getUuid();
    		
    		System.out.println("-----------------------------------------------------------------------------------------------------------");
    		System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > CHECK STATUS REQUEST");
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
        	
            URL url = null;
            String wsdlPrint = null;
            try {
            	if(environment.equalsIgnoreCase("prod")) {
            		url = new URL(WSDL_SAT_PROD);
                    wsdl = WSDL_SAT_PROD;
                    endPoint = SAT_ENDPOINT_ADDRESS_PROPERTY_PROD;
                    wsdlPrint = "WSDL_PROD:30116";
                } else {
                	url = new URL(WSDL_SAT_QA);
                    wsdl = WSDL_SAT_QA;
                    endPoint = SAT_ENDPOINT_ADDRESS_PROPERTY_QA;
                    wsdlPrint = "WSDL_QA:30216";
                }
			} catch (MalformedURLException e) {
				e.printStackTrace();
				response.setMalformedUrlException(true);
				return response;
			}
            
            if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > WSDL > " + wsdlPrint);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > OPERATION NAME > " + OPERATION_STATUS_NAME);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > INVOKING WSDL...");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            }
            
            SATServiceServiceagent serviceAgent = new SATServiceServiceagent(url, SAT_SERVICE_NAME);
            SATService port = serviceAgent.getSATService();
            
            //##### Especificando el endpoint
            BindingProvider bindingProvider = (BindingProvider) port;
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            
            org.megapractical.invoicing.api.wsdl.cancelation.sat.HeaderRq headerRq = new org.megapractical.invoicing.api.wsdl.cancelation.sat.HeaderRq();
            headerRq.setNombreOperacion(OPERATION_STATUS_NAME); //##### Nombre de la operacion
            headerRq.setVersionOperacion(OPERATION_VERSION); //##### Version de la operacion
            
            org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaStatusCFDRq consultaStatusCFDRq = new org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaStatusCFDRq();
            consultaStatusCFDRq.setRFCEmisor(emitterRfc);
            consultaStatusCFDRq.setRFCReceptor(receiverRfc);
            consultaStatusCFDRq.setUUID(uuid);
            consultaStatusCFDRq.setTotalComprobante(voucherTotal);
            
            org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaStatusCFDRequest parameters = new org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaStatusCFDRequest();
            parameters.setHeaderRq(headerRq);
            parameters.setConsultaStatusCFDRq(consultaStatusCFDRq);
            
            try {
            	
            	org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaStatusCFDResponse consultaStatusCFDResponse = port.consultaStatusCFD(parameters);
            	org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaStatusCFDRs consultaStatusCFDRs = consultaStatusCFDResponse.getConsultaStatusCFDRs();
                org.megapractical.invoicing.api.wsdl.cancelation.sat.ConsultaSatusCFDResultado consultaStatusCFDResultado = consultaStatusCFDRs.getConsultaSatusCFDResultado();
                
                org.megapractical.invoicing.api.wsdl.cancelation.sat.HeaderRs headerRs = consultaStatusCFDResponse.getHeaderRs();
            	Integer transactionResult = headerRs.getResultadoTransaccion();
            	String transactionResultType = headerRs.getTipoResultadoTransaccion();
                
                if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > EVALUATING WSDL RESPONSE...");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
                
                CancellationResult cancelationResult = new CancellationResult();
                cancelationResult.setTransactionResult(transactionResult);
            	cancelationResult.setTransactionResultType(transactionResultType);
            	
            	StatusByUuid statusByUuid = new StatusByUuid();
        		statusByUuid.setResponseAcuseXml(consultaStatusCFDResultado.getAcuseDatos());
        		statusByUuid.setResponseStatusCode(consultaStatusCFDResultado.getCodigoStatusCFD().replace("\"", ""));
        		statusByUuid.setResponseCancelableText(consultaStatusCFDResultado.getIndicadorCancelableCFD().replace("\"", ""));
        		statusByUuid.setResponseCancelableStatus(consultaStatusCFDResultado.getStatusCancelacion().replace("\"", ""));
        		statusByUuid.setResponseStatus(consultaStatusCFDResultado.getStatusCFD());
            	
        		cancelationResult.setStatusByUuid(statusByUuid);
        		response.setCancellationResult(cancelationResult);
            	
            	response.setSuccess(Boolean.TRUE);
            	
            	if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > WSDL RESPONSE EVALUATED");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
            	
			} catch (Exception e) {
				response.setSuccess(Boolean.FALSE);
				response.setUuidError(uuid);
				response.setErrorCode("CFDI205");
				
				if (applicationPrintValues) {
                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > WSDL RESPONSE NULL");
                	System.out.println("-----------------------------------------------------------------------------------------------------------");
                }
			}
    		
		} catch (Exception e) {			
			e.printStackTrace();
			
			response.setSuccess(Boolean.FALSE);
			response.setUuidsError(properties.getUuids());
			response.setWsdlException(Boolean.TRUE);
			response.setWsdlUrl(wsdl);
			response.setWsdlOperation(OPERATION_STATUS_NAME);
			response.setWsdlEndpoint(endPoint);
			response.setWsdlErrorTrace(e.getMessage());
			
			if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] SAT CFDI CHECK STATUS INSTANCE > WSDL RESPONSE ERROR");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            }
		}
    	
    	return response;
    }*/
}