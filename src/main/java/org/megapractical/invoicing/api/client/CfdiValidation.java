package org.megapractical.invoicing.api.client;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.megapractical.invoicing.api.service.CfdiXmlService;
import org.megapractical.invoicing.api.util.UCertificateX509;
import org.megapractical.invoicing.api.util.UNetwork;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.validator.Validator;
import org.megapractical.invoicing.api.wsdl.ComprobanteRequest;
import org.megapractical.invoicing.sat.integration.v40.Comprobante;

/**
 * @author Maikel Guerra Ferrer
 * @location México D.F
 * @date 20180120
 */
public class CfdiValidation {

	private static Boolean applicationPrintValues = UProperties.getApplicationPrintValues();
	
	public static CfdiValidationResponse cfdiValidation(CfdiValidationProperties properties){
		
		System.out.println("-----------------------------------------------------------------------------------------------------------");
    	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI VALIDATION INSTANCE");
    	
    	CfdiValidationResponse cfdiValidationResponse = new CfdiValidationResponse();
    	
    	// Xml in byte array format to stmap.
        byte[] xml = null;

        // Xml stamping in byte array format.
        byte[] response = null;
        
        // Comprobante
        Comprobante voucher = properties.getVoucher();
        
        // Certificado
        byte[] certificate = properties.getCertificate();
        
        // Llave privada
        byte[] privateKey = properties.getPrivateKey();
        
        // Clave de la llave privada
        String passwd = properties.getPasswd();
        
        // Id Emisor
        String emitterId = properties.getEmitterId();
        
        // Id Usuario
        String sessionId = properties.getSessionId();
        
        // Nombre producto o servicio
        String sourceSystem = properties.getSourceSystem();
        
        // Id para la transaccion
        String correlationId = properties.getCorrelationId();
        
        // Ambiente (PROD|DEV|QA)
        Integer environment = properties.getEnvironment();
        
        try {
			
        	if(UNetwork.isAvailable()){
        		try {
                    
        			//##### Getting XML.
                    xml = CfdiXmlService.generateXml(voucher);
                    
                    if (applicationPrintValues) {
                    	System.out.println("-----------------------------------------------------------------------------------------------------------");
                    	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI VALIDATION INSTANCE > XML TO VALIDATE");
                    	System.out.println();
                        System.out.println(new String(xml, "UTF-8"));
                    }
                    
                    try {
                    	
                        ComprobanteRequest comprobanteRequest = new ComprobanteRequest();
                        comprobanteRequest.setCertByte(UCertificateX509.certificateBase64(certificate));
                        comprobanteRequest.setKeyByte(UCertificateX509.fileBase64(privateKey));
                        comprobanteRequest.setKeyPassword(passwd);
                        comprobanteRequest.setComprobanteCFDIin(new String(xml, "UTF-8"));
                        
                        // Validation request.
                        new CfdiValidationClient();
                        cfdiValidationResponse = CfdiValidationClient.cfdiValidationRequest(comprobanteRequest, emitterId, sessionId, sourceSystem, correlationId, environment);
                        response = cfdiValidationResponse.getResponse();
                        
                        if(response != null){
                            String responseString = new String(response, "UTF-8");
                            if (!responseString.contains("<?xml")) {
                                cfdiValidationResponse.setValid(Boolean.FALSE);
                                
                                try {
									
                                	cfdiValidationResponse.setError(Validator.ErrorSource.fromValue(Validator.getErrorCode(responseString)).value());
                                    cfdiValidationResponse.setErrorCode(Validator.getErrorCode(responseString));
                                    cfdiValidationResponse.setErrorMessage(Validator.getErrorMessage(responseString));
                                	
								} catch (Exception e) {

									cfdiValidationResponse.setError("Error no definido");
	                                cfdiValidationResponse.setErrorCode(Validator.getErrorCode(responseString));
	                                cfdiValidationResponse.setErrorMessage(Validator.getErrorMessage(responseString));
									
								}
                                
                                System.out.println();
                                System.out.println("-----------------------------------------------------------------------------------------------------------");
                                System.out.println(UPrint.consoleDatetime() + "[ERROR] MEGA-CFDI API > CFDI VALIDATION OPERATION INSTANCE > WSDL RESPONSE > " + responseString);
                            }else{
                            	if (applicationPrintValues) {
                            		System.out.println();
                            		System.out.println("-----------------------------------------------------------------------------------------------------------");
                                	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CFDI VALIDATION OPERATION INSTANCE > WSDL RESPONSE");
                                	System.out.println();
                                    System.out.println(new String(response, "UTF-8"));
                                }
                            	cfdiValidationResponse.setValid(Boolean.TRUE);
                            }
                        }else{
                        	return cfdiValidationResponse;
                        }
                        
                    } catch (Exception ex) {
                    	Logger.getLogger(CfdiValidation.class.getName()).log(Level.SEVERE, null, ex);
                    	cfdiValidationResponse.setValid(Boolean.FALSE);
                        cfdiValidationResponse.setError("ERR0169");
                        cfdiValidationResponse.setErrorCode("ERR0169");
                        cfdiValidationResponse.setErrorMessage("ERR0169");
                        return cfdiValidationResponse;
                    }
                    
                } catch (Exception ex) {
                    Logger.getLogger(CfdiValidation.class.getName()).log(Level.SEVERE, null, ex);
                    cfdiValidationResponse.setValid(Boolean.FALSE);
                    cfdiValidationResponse.setError("ERR0039");
                    cfdiValidationResponse.setErrorCode("ERR0039");
                    cfdiValidationResponse.setErrorMessage("ERR0039");
                    return cfdiValidationResponse;
                }

        	}else{
            	cfdiValidationResponse.setValid(Boolean.FALSE);
                cfdiValidationResponse.setError("ERR0169");
                cfdiValidationResponse.setErrorCode("ERR0169");
                cfdiValidationResponse.setErrorMessage("ERR0169");
                return cfdiValidationResponse;
        	}
        	
		} catch (Exception e) {
			Logger.getLogger(CfdiValidation.class.getName()).log(Level.SEVERE, null, e);
        	cfdiValidationResponse.setValid(Boolean.FALSE);
            cfdiValidationResponse.setError("ERR0169");
            cfdiValidationResponse.setErrorCode("ERR0169");
            cfdiValidationResponse.setErrorMessage("ERR0169");
            return cfdiValidationResponse;
		}
        return cfdiValidationResponse;
	}
}
