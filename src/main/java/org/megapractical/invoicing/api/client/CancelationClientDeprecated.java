package org.megapractical.invoicing.api.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import org.megapractical.invoicing.api.smarterweb.SWRetCancellationResponse;
import org.megapractical.invoicing.api.util.UBase64;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.wsdl.CFDIEncabezadoPeticion;
import org.megapractical.invoicing.api.wsdl.CFDIEncabezadoRespuesta;
import org.megapractical.invoicing.api.wsdl.CFDIv33ServiceMediationServiceagent;
import org.megapractical.invoicing.api.wsdl.CancelacionRequest;
import org.megapractical.invoicing.api.wsdl.CancelacionResponse;
import org.megapractical.invoicing.api.wsdl.PortTypeCFDIv33;
import org.megapractical.invoicing.api.wsdl.WsdlError;

public class CancelationClientDeprecated {

	private static final String OPERATION_CODE = "cancel";
    private static final String OPERATION_VERSION = UProperties.getOperationVersion();
    private static final String OPERATION_NAME = UProperties.getOperationName(OPERATION_CODE);
    private static final QName SERVICE_NAME = new QName("http://xmlns.megacfdi.com/1499759600080", "CFDIv33_service_mediation.serviceagent");    
    private static final String WSDL_TEST = "http://servicios-qa.megacfdi.com:30400/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl";
    private static final String WSDL_PROD = "http://ws-mediation.megacfdi.com:30100/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl";
    
    private static Boolean applicationPrintValues = UProperties.getApplicationPrintValues();
    
    public static SWRetCancellationResponse cancelationRequest(CancelationProperties properties){
    	try {
			
    		SWRetCancellationResponse response = new SWRetCancellationResponse();
    		
    		String environment = properties.getEnvironment();
    		String emiterId = properties.getEmitterId();
    		String sourceSystem = properties.getSourceSystem();
    		String correlationId = properties.getCorrelationId();
    		String sessionId = properties.getSessionId();
    		String emitterRfc = properties.getEmitterRfc();
    		String uuid = properties.getCfdiCancelation().getUuid();
    		String certificate = UBase64.base64Encode(properties.getCertificate());
    		String privateKey = UBase64.base64Encode(properties.getPrivateKey());
    		String passwd = properties.getPasswd();
    		
    		System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > CANCELATION REQUEST");
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
        	
        	GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(new Date());
            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            
            String wsdl = "";
            
            URL url = null;
            try {
            	if(environment.equalsIgnoreCase("prod")) {
            		url = new URL(WSDL_PROD);
                    wsdl = "wsdl/prod/StampCfdiV33.wsdl";
                } else {
                	url = new URL(WSDL_TEST);
                    wsdl = "wsdl/test/StampCfdiV33Test.wsdl";
                }
			} catch (MalformedURLException e) {
				e.printStackTrace();
				response.setMalformedUrlException(true);
				return response;
			}
            
            if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > WSDL > " + wsdl);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > OPERATION NAME > " + OPERATION_NAME);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CANCELATION OPERATION INSTANCE > INVOKING WSDL...");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println();
            }
            
            // Objeto del servicio.
            CFDIv33ServiceMediationServiceagent serviceAgent = new CFDIv33ServiceMediationServiceagent(url, SERVICE_NAME);
            PortTypeCFDIv33 port = serviceAgent.getPortTypeCFDIv33Endpoint1();
            
            CFDIEncabezadoPeticion _encabezadoPeticion = new CFDIEncabezadoPeticion();
            _encabezadoPeticion.setFechaTransaccion(date);
            _encabezadoPeticion.setHoraTransaccion(date);
            _encabezadoPeticion.setNombreOperacion(OPERATION_NAME);
            _encabezadoPeticion.setVersionOperacion(OPERATION_VERSION);
            _encabezadoPeticion.setIDCliente(emiterId); // Id del emisor
            _encabezadoPeticion.setSistemaOrigen(sourceSystem); // Nombre producto o servicio
            _encabezadoPeticion.setCorrelationID(correlationId); // Codigo para la transaccion
            _encabezadoPeticion.setSesion(sessionId); // Id del usuario

            Holder<CFDIEncabezadoRespuesta> _encabezadoSOA = new Holder<>();
            Holder<CancelacionResponse> _respuesta = new Holder<>();
            
            CancelacionRequest _request = new CancelacionRequest();
            _request.setRfc(emitterRfc);
            _request.setUUID(uuid);
            _request.setCertByte(certificate);
            _request.setKeyByte(privateKey);
            _request.setKeyPassword(passwd);
            
            try {
				
        		port.cancelacionCFDIv33(_encabezadoPeticion, _request, _encabezadoSOA, _respuesta);
        		//response.setResponse(_respuesta.value.getResponseCodSATCancel());
        		//response.setResponseMessage(_respuesta.value.getResponseMessageSATCancel());
		        return response;
		        
			} catch (WsdlError e) {
				response.setWsdlException(true);
				e.printStackTrace();
			}
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
}
