package org.megapractical.invoicing.api.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;
import org.megapractical.invoicing.api.wsdl.CFDIEncabezadoPeticion;
import org.megapractical.invoicing.api.wsdl.CFDIEncabezadoRespuesta;
import org.megapractical.invoicing.api.wsdl.CFDIv33ServiceMediationServiceagent;
import org.megapractical.invoicing.api.wsdl.ClaveConfirmationRequest;
import org.megapractical.invoicing.api.wsdl.ClaveConfirmationResponse;
import org.megapractical.invoicing.api.wsdl.PortTypeCFDIv33;
import org.megapractical.invoicing.api.wsdl.WsdlError;

/**
 * @author Maikel Guerra Ferrer
 */
public class ConfirmationCodeClient {

	private static final String OPERATION_CODE = "code";
    private static final String OPERATION_VERSION = UProperties.getOperationVersion();
    private static final String OPERATION_NAME = UProperties.getOperationName(OPERATION_CODE);
    
    private static final QName SERVICE_NAME = new QName("http://xmlns.megacfdi.com/1499759600080", "CFDIv33_service_mediation.serviceagent");    
    private static final String WSDL_TEST = "https://servicios-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl";
    private static final String WSDL_PROD = "https://servicios-prod.megacfdi.com:30100/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl";
    
    private static final String ENDPOINT_ADDRESS_PROPERTY_QA = "https://servicios-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent/PortTypeCFDIv33Endpoint1";
    private static final String ENDPOINT_ADDRESS_PROPERTY_PROD = "https://servicios-prod.megacfdi.com:30100/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent/PortTypeCFDIv33Endpoint1";
    
    private static Boolean applicationPrintValues = UProperties.getApplicationPrintValues();
    
    public static ConfirmationCodeResponse confirmationCodeRequest(ConfirmationCodeProperties properties){
    	String endPoint = null;
    	try {
			
    		ConfirmationCodeResponse response = new ConfirmationCodeResponse();
    		
    		Integer environment = properties.getEnvironment();
    		String emiterId = properties.getEmitterId();
    		String sourceSystem = properties.getSourceSystem();
    		String correlationId = properties.getCorrelationId();
    		String sessionId = properties.getSessionId();
    		String emitterRfc = properties.getRfc();
    		
    		System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE OPERATION INSTANCE > CONFIRMATION CODE REQUEST");
        	System.out.println("-----------------------------------------------------------------------------------------------------------");
        	
        	GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(new Date());
            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            
            String wsdl = "";
            
            URL url = null;
            try {
            	if(environment == 1) {
            		url = new URL(WSDL_PROD);
                    wsdl = "wsdl/prod/StampCfdiV33.wsdl";   
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_PROD;
                } else {
                	url = new URL(WSDL_TEST);
                    wsdl = "wsdl/test/StampCfdiV33Test.wsdl";
                    endPoint = ENDPOINT_ADDRESS_PROPERTY_QA;
                }
			} catch (MalformedURLException e) {
				e.printStackTrace();
				response.setMalformedUrlException(true);
				return response;
			}
            
            if (applicationPrintValues) {
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE OPERATION INSTANCE > WSDL > " + wsdl);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE OPERATION INSTANCE > OPERATION NAME > " + OPERATION_NAME);
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE OPERATION INSTANCE > INVOKING WSDL...");
            	System.out.println("-----------------------------------------------------------------------------------------------------------");
            	System.out.println();
            }
            
            // Objeto del servicio.
            CFDIv33ServiceMediationServiceagent serviceAgent = new CFDIv33ServiceMediationServiceagent(url, SERVICE_NAME);
            PortTypeCFDIv33 port = serviceAgent.getPortTypeCFDIv33Endpoint1();
            
          //##### Especificando el endpoint
            BindingProvider bindingProvider = (BindingProvider) port;
            String timeOut = Integer.toString(1000 * 60 * 20);            
            bindingProvider.getRequestContext().put("javax.xml.ws.client.receiveTimeout", timeOut);
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            
            CFDIEncabezadoPeticion _encabezadoPeticion = new CFDIEncabezadoPeticion();
            _encabezadoPeticion.setFechaTransaccion(date);
            _encabezadoPeticion.setHoraTransaccion(date);
            _encabezadoPeticion.setNombreOperacion(OPERATION_NAME);
            _encabezadoPeticion.setVersionOperacion(OPERATION_VERSION);
            _encabezadoPeticion.setIDCliente(emiterId); // Id del emisor
            _encabezadoPeticion.setSistemaOrigen(sourceSystem); // Nombre producto o servicio
            _encabezadoPeticion.setCorrelationID(correlationId); // Codigo para la transaccion
            _encabezadoPeticion.setSesion(sessionId); // Id del usuario

            Holder<CFDIEncabezadoRespuesta> _encabezadoSOA = new Holder<>();
            Holder<ClaveConfirmationResponse> _respuesta = new Holder<>();
            
            ClaveConfirmationRequest _request = new ClaveConfirmationRequest();
            _request.setRfc(emitterRfc);
            
        	try {
				
        		port.claveConfCFDIv33(_encabezadoPeticion, _request, _encabezadoSOA, _respuesta);
        		response.setConfirmationCode(_respuesta.value.getClaveConfirmation());
		        return response;
		        
			} catch (WsdlError e) {
				response.setWsdlException(true);
				e.printStackTrace();
			}
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
}
