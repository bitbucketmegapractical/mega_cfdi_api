package org.megapractical.invoicing.api.client;

import org.megapractical.invoicing.api.util.UNetwork;
import org.megapractical.invoicing.api.util.UPrint;
import org.megapractical.invoicing.api.util.UProperties;

public class ConfirmationCode {

	private static Boolean applicationPrintValues = UProperties.getApplicationPrintValues();
	
	public static ConfirmationCodeResponse confirmationCode(ConfirmationCodeProperties properties){
		try {
			
			ConfirmationCodeResponse response = new ConfirmationCodeResponse();
			try {
				
				if(UNetwork.isAvailable()){
					try {
						
						new ConfirmationCodeClient();
						response = ConfirmationCodeClient.confirmationCodeRequest(properties);
						
						if(response.getConfirmationCode() != null){
							response.setSuccess(Boolean.TRUE);
						}
						
						if (applicationPrintValues) {
		                	System.out.println();
		                	System.out.println("-----------------------------------------------------------------------------------------------------------");
		                    System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE > RESPONSE VALUES");
		                    System.out.println("-----------------------------------------------------------------------------------------------------------");
		                    System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE > SUCCESS: " + response.isSuccess());
		                    System.out.println("-----------------------------------------------------------------------------------------------------------");
		                    System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONFIRMATION CODE > CODE:    " + response.getConfirmationCode());
		                    System.out.println("-----------------------------------------------------------------------------------------------------------");
		                    System.out.println();
		                }
						
						return response;
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
		        	response.setSuccess(Boolean.FALSE);
		            response.setError("ERR0048");
		            response.setErrorCode("ERR0048");
		            response.setErrorMessage("ERR0048");
		            return response;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
