package org.megapractical.invoicing.api.client;

import org.megapractical.invoicing.sat.integration.v40.Comprobante;

public class CfdiValidationProperties {

	// Id del emisor de la factura
	private String emitterId;
	
	// Id del usuario en sesion
	private String sessionId;
	
	// Id del producto
	private String sourceSystem;
	
	// Valor generado aleatorio para cada timbrado
	private String correlationId;
	
	// Comprobante
	private Comprobante voucher;
	
	// Certificado
	private byte[] certificate;
	
	// Llave privada
	private byte[] privateKey;
	
	// Clave de la llave privada
	private String passwd;
	
	// Ambiente (PRO|DEV)
	private Integer environment;
	
	public CfdiValidationProperties(){
		super();
	}

	public String getEmitterId() {
		return emitterId;
	}

	public void setEmitterId(String emitterId) {
		this.emitterId = emitterId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public Comprobante getVoucher() {
		return voucher;
	}

	public void setVoucher(Comprobante voucher) {
		this.voucher = voucher;
	}

	public byte[] getCertificate() {
		return certificate;
	}

	public void setCertificate(byte[] certificate) {
		this.certificate = certificate;
	}

	public byte[] getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(byte[] privateKey) {
		this.privateKey = privateKey;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public Integer getEnvironment() {
		return environment;
	}

	public void setEnvironment(Integer environment) {
		this.environment = environment;
	}
}