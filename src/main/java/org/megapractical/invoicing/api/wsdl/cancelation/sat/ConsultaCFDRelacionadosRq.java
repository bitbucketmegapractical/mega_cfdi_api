
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}CertificadoDatos"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}LlaveDatos"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}ClaveLlave"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}UUID"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCPACEnviaSolicitud"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCReceptor" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCEmisor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "certificadoDatos",
    "llaveDatos",
    "claveLlave",
    "uuid",
    "rfcpacEnviaSolicitud",
    "rfcReceptor",
    "rfcEmisor"
})
@XmlRootElement(name = "ConsultaCFDRelacionadosRq")
public class ConsultaCFDRelacionadosRq {

    @XmlElement(name = "CertificadoDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected byte[] certificadoDatos;
    @XmlElement(name = "LlaveDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected byte[] llaveDatos;
    @XmlElement(name = "ClaveLlave", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String claveLlave;
    @XmlElement(name = "UUID", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String uuid;
    @XmlElement(name = "RFCPACEnviaSolicitud", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcpacEnviaSolicitud;
    @XmlElement(name = "RFCReceptor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String rfcReceptor;
    @XmlElement(name = "RFCEmisor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String rfcEmisor;

    /**
     * Obtiene el valor de la propiedad certificadoDatos.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getCertificadoDatos() {
        return certificadoDatos;
    }

    /**
     * Define el valor de la propiedad certificadoDatos.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setCertificadoDatos(byte[] value) {
        this.certificadoDatos = value;
    }

    /**
     * Obtiene el valor de la propiedad llaveDatos.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getLlaveDatos() {
        return llaveDatos;
    }

    /**
     * Define el valor de la propiedad llaveDatos.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setLlaveDatos(byte[] value) {
        this.llaveDatos = value;
    }

    /**
     * Obtiene el valor de la propiedad claveLlave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveLlave() {
        return claveLlave;
    }

    /**
     * Define el valor de la propiedad claveLlave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveLlave(String value) {
        this.claveLlave = value;
    }

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUID(String value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcpacEnviaSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCPACEnviaSolicitud() {
        return rfcpacEnviaSolicitud;
    }

    /**
     * Define el valor de la propiedad rfcpacEnviaSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCPACEnviaSolicitud(String value) {
        this.rfcpacEnviaSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcReceptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCReceptor() {
        return rfcReceptor;
    }

    /**
     * Define el valor de la propiedad rfcReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCReceptor(String value) {
        this.rfcReceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCEmisor() {
        return rfcEmisor;
    }

    /**
     * Define el valor de la propiedad rfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCEmisor(String value) {
        this.rfcEmisor = value;
    }

}
