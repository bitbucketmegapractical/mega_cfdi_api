
package org.megapractical.invoicing.api.wsdl.retention.schema;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}keyPassword"/>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}certByte"/>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}keyByte"/>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}retencionesCFDIin"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "keyPassword",
    "certByte",
    "keyByte",
    "retencionesCFDIin"
})
@XmlRootElement(name = "RetencionesRequest")
public class RetencionesRequest
    implements Serializable
{

    @XmlElement(required = true)
    protected String keyPassword;
    @XmlElement(required = true)
    protected String certByte;
    @XmlElement(required = true)
    protected String keyByte;
    @XmlElement(required = true)
    protected String retencionesCFDIin;
    @XmlElement(required = true)
    protected byte[] certByteArr;
    @XmlElement(required = true)
    protected byte[] keyByteArr;

    /**
     * Gets the value of the keyPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyPassword() {
        return keyPassword;
    }

    /**
     * Sets the value of the keyPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyPassword(String value) {
        this.keyPassword = value;
    }

    /**
     * Gets the value of the certByte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertByte() {
        return certByte;
    }

    /**
     * Sets the value of the certByte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertByte(String value) {
        this.certByte = value;
    }

    /**
     * Gets the value of the keyByte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyByte() {
        return keyByte;
    }

    /**
     * Sets the value of the keyByte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyByte(String value) {
        this.keyByte = value;
    }

    /**
     * Gets the value of the retencionesCFDIin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetencionesCFDIin() {
        return retencionesCFDIin;
    }

    /**
     * Sets the value of the retencionesCFDIin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetencionesCFDIin(String value) {
        this.retencionesCFDIin = value;
    }
    
    /**
     * Gets the value of the Cer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public byte[] getCerByteArr() {
        return certByteArr;
    }

     /**
     * Sets the value of the Cer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCerByteArr(byte[] value) {
        this.certByteArr = value;
    }
     /**
     * Gets the value of the Key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public byte[] getKeyByteArr() {
        return keyByteArr;
    }

     /**
     * Sets the value of the Key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyByteArr(byte[] value) {
        this.keyByteArr = value;
    }
}
