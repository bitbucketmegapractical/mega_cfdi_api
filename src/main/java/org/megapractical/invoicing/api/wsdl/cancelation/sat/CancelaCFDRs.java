
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Codigo"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Fecha"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCEmisor"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaCFDResultado" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}AcuseDatos"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codigo",
    "fecha",
    "rfcEmisor",
    "cancelaCFDResultado",
    "acuseDatos"
})
@XmlRootElement(name = "CancelaCFDRs")
public class CancelaCFDRs {

    @XmlElement(name = "Codigo", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String codigo;
    @XmlElement(name = "Fecha", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String fecha;
    @XmlElement(name = "RFCEmisor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcEmisor;
    @XmlElement(name = "CancelaCFDResultado")
    protected List<CancelaCFDResultado> cancelaCFDResultado;
    @XmlElement(name = "AcuseDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String acuseDatos;

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCEmisor() {
        return rfcEmisor;
    }

    /**
     * Define el valor de la propiedad rfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCEmisor(String value) {
        this.rfcEmisor = value;
    }

    /**
     * Gets the value of the cancelaCFDResultado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cancelaCFDResultado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCancelaCFDResultado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CancelaCFDResultado }
     * 
     * 
     */
    public List<CancelaCFDResultado> getCancelaCFDResultado() {
        if (cancelaCFDResultado == null) {
            cancelaCFDResultado = new ArrayList<CancelaCFDResultado>();
        }
        return this.cancelaCFDResultado;
    }

    /**
     * Obtiene el valor de la propiedad acuseDatos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcuseDatos() {
        return acuseDatos;
    }

    /**
     * Define el valor de la propiedad acuseDatos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcuseDatos(String value) {
        this.acuseDatos = value;
    }

}
