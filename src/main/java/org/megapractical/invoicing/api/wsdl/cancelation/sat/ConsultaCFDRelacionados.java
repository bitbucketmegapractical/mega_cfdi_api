
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaCFDRelacionadosRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaCFDRelacionadosResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaCFDRelacionadosRequest",
    "consultaCFDRelacionadosResponse"
})
@XmlRootElement(name = "ConsultaCFDRelacionados")
public class ConsultaCFDRelacionados {

    @XmlElement(name = "ConsultaCFDRelacionadosRequest")
    protected ConsultaCFDRelacionadosRequest consultaCFDRelacionadosRequest;
    @XmlElement(name = "ConsultaCFDRelacionadosResponse")
    protected ConsultaCFDRelacionadosResponse consultaCFDRelacionadosResponse;

    /**
     * Obtiene el valor de la propiedad consultaCFDRelacionadosRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCFDRelacionadosRequest }
     *     
     */
    public ConsultaCFDRelacionadosRequest getConsultaCFDRelacionadosRequest() {
        return consultaCFDRelacionadosRequest;
    }

    /**
     * Define el valor de la propiedad consultaCFDRelacionadosRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCFDRelacionadosRequest }
     *     
     */
    public void setConsultaCFDRelacionadosRequest(ConsultaCFDRelacionadosRequest value) {
        this.consultaCFDRelacionadosRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaCFDRelacionadosResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCFDRelacionadosResponse }
     *     
     */
    public ConsultaCFDRelacionadosResponse getConsultaCFDRelacionadosResponse() {
        return consultaCFDRelacionadosResponse;
    }

    /**
     * Define el valor de la propiedad consultaCFDRelacionadosResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCFDRelacionadosResponse }
     *     
     */
    public void setConsultaCFDRelacionadosResponse(ConsultaCFDRelacionadosResponse value) {
        this.consultaCFDRelacionadosResponse = value;
    }

}
