
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}responseCodSAT_cancel"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}responseMessageSAT_cancel"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseCodSATCancel",
    "responseMessageSATCancel"
})
@XmlRootElement(name = "CancelacionResponse")
public class CancelacionResponse {

    @XmlElement(name = "responseCodSAT_cancel", required = true)
    protected String responseCodSATCancel;
    @XmlElement(name = "responseMessageSAT_cancel", required = true)
    protected String responseMessageSATCancel;

    /**
     * Obtiene el valor de la propiedad responseCodSATCancel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCodSATCancel() {
        return responseCodSATCancel;
    }

    /**
     * Define el valor de la propiedad responseCodSATCancel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCodSATCancel(String value) {
        this.responseCodSATCancel = value;
    }

    /**
     * Obtiene el valor de la propiedad responseMessageSATCancel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseMessageSATCancel() {
        return responseMessageSATCancel;
    }

    /**
     * Define el valor de la propiedad responseMessageSATCancel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseMessageSATCancel(String value) {
        this.responseMessageSATCancel = value;
    }

}
