
package org.megapractical.invoicing.api.wsdl.retention.schema;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.megapractical.invoicing.api.wsdl.retention.CFDIEncabezadoSOA;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.megapractical.com/esb/comun/encabezado/1.0}CFDIEncabezadoSOA"/>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}Operations"/>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}RetencionesResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cfdiEncabezadoSOA",
    "operations",
    "retencionesResponse"
})
@XmlRootElement(name = "CFDI")
public class CFDI
    implements Serializable
{

    @XmlElement(name = "CFDIEncabezadoSOA", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0", required = true)
    protected CFDIEncabezadoSOA cfdiEncabezadoSOA;
    @XmlElement(name = "Operations", required = true)
    protected Operations operations;
    @XmlElement(name = "RetencionesResponse")
    protected RetencionesResponse retencionesResponse;

    /**
     * Gets the value of the cfdiEncabezadoSOA property.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoSOA }
     *     
     */
    public CFDIEncabezadoSOA getCFDIEncabezadoSOA() {
        return cfdiEncabezadoSOA;
    }

    /**
     * Sets the value of the cfdiEncabezadoSOA property.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoSOA }
     *     
     */
    public void setCFDIEncabezadoSOA(CFDIEncabezadoSOA value) {
        this.cfdiEncabezadoSOA = value;
    }

    /**
     * Gets the value of the operations property.
     * 
     * @return
     *     possible object is
     *     {@link Operations }
     *     
     */
    public Operations getOperations() {
        return operations;
    }

    /**
     * Sets the value of the operations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Operations }
     *     
     */
    public void setOperations(Operations value) {
        this.operations = value;
    }

    /**
     * Gets the value of the retencionesResponse property.
     * 
     * @return
     *     possible object is
     *     {@link RetencionesResponse }
     *     
     */
    public RetencionesResponse getRetencionesResponse() {
        return retencionesResponse;
    }

    /**
     * Sets the value of the retencionesResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetencionesResponse }
     *     
     */
    public void setRetencionesResponse(RetencionesResponse value) {
        this.retencionesResponse = value;
    }

}
