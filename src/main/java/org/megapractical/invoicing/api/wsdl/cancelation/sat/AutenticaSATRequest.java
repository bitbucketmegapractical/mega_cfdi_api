
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/common/1.0}HeaderRq"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaSATRq"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRq",
    "autenticaSATRq"
})
@XmlRootElement(name = "AutenticaSATRequest")
public class AutenticaSATRequest {

    @XmlElement(name = "HeaderRq", namespace = "http://www.mcfdi.com/services/common/1.0", required = true)
    protected HeaderRq headerRq;
    @XmlElement(name = "AutenticaSATRq", required = true)
    protected AutenticaSATRq autenticaSATRq;

    /**
     * Obtiene el valor de la propiedad headerRq.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRq }
     *     
     */
    public HeaderRq getHeaderRq() {
        return headerRq;
    }

    /**
     * Define el valor de la propiedad headerRq.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRq }
     *     
     */
    public void setHeaderRq(HeaderRq value) {
        this.headerRq = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaSATRq.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaSATRq }
     *     
     */
    public AutenticaSATRq getAutenticaSATRq() {
        return autenticaSATRq;
    }

    /**
     * Define el valor de la propiedad autenticaSATRq.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaSATRq }
     *     
     */
    public void setAutenticaSATRq(AutenticaSATRq value) {
        this.autenticaSATRq = value;
    }

}
