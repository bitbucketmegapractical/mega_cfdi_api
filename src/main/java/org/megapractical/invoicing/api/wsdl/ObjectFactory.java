
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.megapractical.invoicing.api.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _KeyPassword_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "keyPassword");
    private final static QName _CertByte_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "certByte");
    private final static QName _KeyByte_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "keyByte");
    private final static QName _Rfc_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "rfc");
    private final static QName _UUID_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "UUID");
    private final static QName _ResponseCodSATCancel_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "responseCodSAT_cancel");
    private final static QName _ResponseMessageSATCancel_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "responseMessageSAT_cancel");
    private final static QName _ClaveConfirmation_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "clave_Confirmation");
    private final static QName _ComprobanteCFDIin_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "comprobanteCFDIin");
    private final static QName _Undefined_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "Undefined");
    private final static QName _ComprobanteString_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "comprobanteString");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.megapractical.invoicing.api.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CFDIEncabezadoRespuesta }
     * 
     */
    public CFDIEncabezadoRespuesta createCFDIEncabezadoRespuesta() {
        return new CFDIEncabezadoRespuesta();
    }

    /**
     * Create an instance of {@link CFDIEncabezadoPeticion }
     * 
     */
    public CFDIEncabezadoPeticion createCFDIEncabezadoPeticion() {
        return new CFDIEncabezadoPeticion();
    }

    /**
     * Create an instance of {@link CFDIEncabezadoRespuesta.Resultado }
     * 
     */
    public CFDIEncabezadoRespuesta.Resultado createCFDIEncabezadoRespuestaResultado() {
        return new CFDIEncabezadoRespuesta.Resultado();
    }

    /**
     * Create an instance of {@link CFDIEncabezadoSOA }
     * 
     */
    public CFDIEncabezadoSOA createCFDIEncabezadoSOA() {
        return new CFDIEncabezadoSOA();
    }

    /**
     * Create an instance of {@link CFDIv33 }
     * 
     */
    public CFDIv33 createCFDIv33() {
        return new CFDIv33();
    }

    /**
     * Create an instance of {@link Operations }
     * 
     */
    public Operations createOperations() {
        return new Operations();
    }

    /**
     * Create an instance of {@link CancelacionRequest }
     * 
     */
    public CancelacionRequest createCancelacionRequest() {
        return new CancelacionRequest();
    }

    /**
     * Create an instance of {@link CancelacionResponse }
     * 
     */
    public CancelacionResponse createCancelacionResponse() {
        return new CancelacionResponse();
    }

    /**
     * Create an instance of {@link ClaveConfirmationRequest }
     * 
     */
    public ClaveConfirmationRequest createClaveConfirmationRequest() {
        return new ClaveConfirmationRequest();
    }

    /**
     * Create an instance of {@link ClaveConfirmationResponse }
     * 
     */
    public ClaveConfirmationResponse createClaveConfirmationResponse() {
        return new ClaveConfirmationResponse();
    }

    /**
     * Create an instance of {@link ComprobanteRequest }
     * 
     */
    public ComprobanteRequest createComprobanteRequest() {
        return new ComprobanteRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "keyPassword")
    public JAXBElement<String> createKeyPassword(String value) {
        return new JAXBElement<String>(_KeyPassword_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "certByte")
    public JAXBElement<String> createCertByte(String value) {
        return new JAXBElement<String>(_CertByte_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "keyByte")
    public JAXBElement<String> createKeyByte(String value) {
        return new JAXBElement<String>(_KeyByte_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "rfc")
    public JAXBElement<String> createRfc(String value) {
        return new JAXBElement<String>(_Rfc_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "UUID")
    public JAXBElement<String> createUUID(String value) {
        return new JAXBElement<String>(_UUID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "responseCodSAT_cancel")
    public JAXBElement<String> createResponseCodSATCancel(String value) {
        return new JAXBElement<String>(_ResponseCodSATCancel_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "responseMessageSAT_cancel")
    public JAXBElement<String> createResponseMessageSATCancel(String value) {
        return new JAXBElement<String>(_ResponseMessageSATCancel_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "clave_Confirmation")
    public JAXBElement<String> createClaveConfirmation(String value) {
        return new JAXBElement<String>(_ClaveConfirmation_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "comprobanteCFDIin")
    public JAXBElement<String> createComprobanteCFDIin(String value) {
        return new JAXBElement<String>(_ComprobanteCFDIin_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "Undefined")
    public JAXBElement<Object> createUndefined(Object value) {
        return new JAXBElement<Object>(_Undefined_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "comprobanteString")
    public JAXBElement<String> createComprobanteString(String value) {
        return new JAXBElement<String>(_ComprobanteString_QNAME, String.class, null, value);
    }

}
