
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}ConsultaStatusCFDRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}ConsultaStatusCFDResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaStatusCFDRequest",
    "consultaStatusCFDResponse"
})
@XmlRootElement(name = "ConsultaStatusCFD")
public class ConsultaStatusCFD {

    @XmlElement(name = "ConsultaStatusCFDRequest")
    protected ConsultaStatusCFDRequest consultaStatusCFDRequest;
    @XmlElement(name = "ConsultaStatusCFDResponse")
    protected ConsultaStatusCFDResponse consultaStatusCFDResponse;

    /**
     * Obtiene el valor de la propiedad consultaStatusCFDRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaStatusCFDRequest }
     *     
     */
    public ConsultaStatusCFDRequest getConsultaStatusCFDRequest() {
        return consultaStatusCFDRequest;
    }

    /**
     * Define el valor de la propiedad consultaStatusCFDRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaStatusCFDRequest }
     *     
     */
    public void setConsultaStatusCFDRequest(ConsultaStatusCFDRequest value) {
        this.consultaStatusCFDRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaStatusCFDResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaStatusCFDResponse }
     *     
     */
    public ConsultaStatusCFDResponse getConsultaStatusCFDResponse() {
        return consultaStatusCFDResponse;
    }

    /**
     * Define el valor de la propiedad consultaStatusCFDResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaStatusCFDResponse }
     *     
     */
    public void setConsultaStatusCFDResponse(ConsultaStatusCFDResponse value) {
        this.consultaStatusCFDResponse = value;
    }

}
