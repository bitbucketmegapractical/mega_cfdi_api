
package org.megapractical.invoicing.api.wsdl.retention;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.megapractical.com/esb/comun/encabezado/1.0}CFDIEncabezadoPeticion"/>
 *         &lt;element ref="{http://www.megapractical.com/esb/comun/encabezado/1.0}CFDIEncabezadoRespuesta"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cfdiEncabezadoPeticion",
    "cfdiEncabezadoRespuesta"
})
@XmlRootElement(name = "CFDIEncabezadoSOA")
public class CFDIEncabezadoSOA
    implements Serializable
{

    @XmlElement(name = "CFDIEncabezadoPeticion")
    protected CFDIEncabezadoPeticion cfdiEncabezadoPeticion;
    @XmlElement(name = "CFDIEncabezadoRespuesta")
    protected CFDIEncabezadoRespuesta cfdiEncabezadoRespuesta;

    /**
     * Gets the value of the cfdiEncabezadoPeticion property.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoPeticion }
     *     
     */
    public CFDIEncabezadoPeticion getCFDIEncabezadoPeticion() {
        return cfdiEncabezadoPeticion;
    }

    /**
     * Sets the value of the cfdiEncabezadoPeticion property.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoPeticion }
     *     
     */
    public void setCFDIEncabezadoPeticion(CFDIEncabezadoPeticion value) {
        this.cfdiEncabezadoPeticion = value;
    }

    /**
     * Gets the value of the cfdiEncabezadoRespuesta property.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoRespuesta }
     *     
     */
    public CFDIEncabezadoRespuesta getCFDIEncabezadoRespuesta() {
        return cfdiEncabezadoRespuesta;
    }

    /**
     * Sets the value of the cfdiEncabezadoRespuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoRespuesta }
     *     
     */
    public void setCFDIEncabezadoRespuesta(CFDIEncabezadoRespuesta value) {
        this.cfdiEncabezadoRespuesta = value;
    }

}
