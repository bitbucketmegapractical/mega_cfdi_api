
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}UUID"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Resultado"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CFDRelacionadosPadres" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CFDRelacionadosHijos" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uuid",
    "resultado",
    "cfdRelacionadosPadres",
    "cfdRelacionadosHijos"
})
@XmlRootElement(name = "ConsultaCFDRelacionadosRs")
public class ConsultaCFDRelacionadosRs {

    @XmlElement(name = "UUID", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String uuid;
    @XmlElement(name = "Resultado", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String resultado;
    @XmlElement(name = "CFDRelacionadosPadres")
    protected CFDRelacionadosPadres cfdRelacionadosPadres;
    @XmlElement(name = "CFDRelacionadosHijos")
    protected CFDRelacionadosHijos cfdRelacionadosHijos;

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUID(String value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad resultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * Define el valor de la propiedad resultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultado(String value) {
        this.resultado = value;
    }

    /**
     * Obtiene el valor de la propiedad cfdRelacionadosPadres.
     * 
     * @return
     *     possible object is
     *     {@link CFDRelacionadosPadres }
     *     
     */
    public CFDRelacionadosPadres getCFDRelacionadosPadres() {
        return cfdRelacionadosPadres;
    }

    /**
     * Define el valor de la propiedad cfdRelacionadosPadres.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDRelacionadosPadres }
     *     
     */
    public void setCFDRelacionadosPadres(CFDRelacionadosPadres value) {
        this.cfdRelacionadosPadres = value;
    }

    /**
     * Obtiene el valor de la propiedad cfdRelacionadosHijos.
     * 
     * @return
     *     possible object is
     *     {@link CFDRelacionadosHijos }
     *     
     */
    public CFDRelacionadosHijos getCFDRelacionadosHijos() {
        return cfdRelacionadosHijos;
    }

    /**
     * Define el valor de la propiedad cfdRelacionadosHijos.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDRelacionadosHijos }
     *     
     */
    public void setCFDRelacionadosHijos(CFDRelacionadosHijos value) {
        this.cfdRelacionadosHijos = value;
    }

}
