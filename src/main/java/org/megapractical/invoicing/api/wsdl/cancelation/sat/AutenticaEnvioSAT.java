
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaEnvioSATRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaEnvioSATResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autenticaEnvioSATRequest",
    "autenticaEnvioSATResponse"
})
@XmlRootElement(name = "AutenticaEnvioSAT")
public class AutenticaEnvioSAT {

    @XmlElement(name = "AutenticaEnvioSATRequest")
    protected AutenticaEnvioSATRequest autenticaEnvioSATRequest;
    @XmlElement(name = "AutenticaEnvioSATResponse")
    protected AutenticaEnvioSATResponse autenticaEnvioSATResponse;

    /**
     * Obtiene el valor de la propiedad autenticaEnvioSATRequest.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaEnvioSATRequest }
     *     
     */
    public AutenticaEnvioSATRequest getAutenticaEnvioSATRequest() {
        return autenticaEnvioSATRequest;
    }

    /**
     * Define el valor de la propiedad autenticaEnvioSATRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaEnvioSATRequest }
     *     
     */
    public void setAutenticaEnvioSATRequest(AutenticaEnvioSATRequest value) {
        this.autenticaEnvioSATRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaEnvioSATResponse.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaEnvioSATResponse }
     *     
     */
    public AutenticaEnvioSATResponse getAutenticaEnvioSATResponse() {
        return autenticaEnvioSATResponse;
    }

    /**
     * Define el valor de la propiedad autenticaEnvioSATResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaEnvioSATResponse }
     *     
     */
    public void setAutenticaEnvioSATResponse(AutenticaEnvioSATResponse value) {
        this.autenticaEnvioSATResponse = value;
    }

}
