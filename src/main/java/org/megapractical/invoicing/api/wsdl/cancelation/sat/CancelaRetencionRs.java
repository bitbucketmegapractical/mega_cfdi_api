
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Fecha"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCEmisor"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionResultado"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}AcuseDatos"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fecha",
    "rfcEmisor",
    "cancelaRetencionResultado",
    "acuseDatos"
})
@XmlRootElement(name = "CancelaRetencionRs")
public class CancelaRetencionRs {

    @XmlElement(name = "Fecha", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String fecha;
    @XmlElement(name = "RFCEmisor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcEmisor;
    @XmlElement(name = "CancelaRetencionResultado", required = true)
    protected CancelaRetencionResultado cancelaRetencionResultado;
    @XmlElement(name = "AcuseDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String acuseDatos;

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCEmisor() {
        return rfcEmisor;
    }

    /**
     * Define el valor de la propiedad rfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCEmisor(String value) {
        this.rfcEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaRetencionResultado.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionResultado }
     *     
     */
    public CancelaRetencionResultado getCancelaRetencionResultado() {
        return cancelaRetencionResultado;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionResultado }
     *     
     */
    public void setCancelaRetencionResultado(CancelaRetencionResultado value) {
        this.cancelaRetencionResultado = value;
    }

    /**
     * Obtiene el valor de la propiedad acuseDatos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcuseDatos() {
        return acuseDatos;
    }

    /**
     * Define el valor de la propiedad acuseDatos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcuseDatos(String value) {
        this.acuseDatos = value;
    }

}
