
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaCancelacionRetencionMasivaRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaCancelacionRetencionMasivaResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaCancelacionRetencionMasivaRequest",
    "consultaCancelacionRetencionMasivaResponse"
})
@XmlRootElement(name = "ConsultaCancelacionRetencionMasiva")
public class ConsultaCancelacionRetencionMasiva {

    @XmlElement(name = "ConsultaCancelacionRetencionMasivaRequest")
    protected ConsultaCancelacionRetencionMasivaRequest consultaCancelacionRetencionMasivaRequest;
    @XmlElement(name = "ConsultaCancelacionRetencionMasivaResponse")
    protected ConsultaCancelacionRetencionMasivaResponse consultaCancelacionRetencionMasivaResponse;

    /**
     * Obtiene el valor de la propiedad consultaCancelacionRetencionMasivaRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCancelacionRetencionMasivaRequest }
     *     
     */
    public ConsultaCancelacionRetencionMasivaRequest getConsultaCancelacionRetencionMasivaRequest() {
        return consultaCancelacionRetencionMasivaRequest;
    }

    /**
     * Define el valor de la propiedad consultaCancelacionRetencionMasivaRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCancelacionRetencionMasivaRequest }
     *     
     */
    public void setConsultaCancelacionRetencionMasivaRequest(ConsultaCancelacionRetencionMasivaRequest value) {
        this.consultaCancelacionRetencionMasivaRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaCancelacionRetencionMasivaResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCancelacionRetencionMasivaResponse }
     *     
     */
    public ConsultaCancelacionRetencionMasivaResponse getConsultaCancelacionRetencionMasivaResponse() {
        return consultaCancelacionRetencionMasivaResponse;
    }

    /**
     * Define el valor de la propiedad consultaCancelacionRetencionMasivaResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCancelacionRetencionMasivaResponse }
     *     
     */
    public void setConsultaCancelacionRetencionMasivaResponse(ConsultaCancelacionRetencionMasivaResponse value) {
        this.consultaCancelacionRetencionMasivaResponse = value;
    }

}
