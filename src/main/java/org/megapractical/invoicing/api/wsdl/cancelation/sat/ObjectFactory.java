
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.megapractical.invoicing.api.wsdl.cancelation.sat package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CodigoRespuesta_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "CodigoRespuesta");
    private final static QName _ResultadoOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "ResultadoOperacion");
    private final static QName _TipoResultadoOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "TipoResultadoOperacion");
    private final static QName _SistemaDestino_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "SistemaDestino");
    private final static QName _MensajeRespuesta_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "MensajeRespuesta");
    private final static QName _TrazaRespuesta_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "TrazaRespuesta");
    private final static QName _DireccionIP_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "DireccionIP");
    private final static QName _FechaTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "FechaTransaccion");
    private final static QName _HoraTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "HoraTransaccion");
    private final static QName _IDCorrelacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "IDCorrelacion");
    private final static QName _IDTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "IDTransaccion");
    private final static QName _NombreOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "NombreOperacion");
    private final static QName _NombreServicio_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "NombreServicio");
    private final static QName _Referencia_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "Referencia");
    private final static QName _ResultadoTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "ResultadoTransaccion");
    private final static QName _Sesion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "Sesion");
    private final static QName _SistemaOrigen_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "SistemaOrigen");
    private final static QName _TipoResultadoTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "TipoResultadoTransaccion");
    private final static QName _VersionOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "VersionOperacion");
    private final static QName _CertificadoDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "CertificadoDatos");
    private final static QName _LlaveDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "LlaveDatos");
    private final static QName _ClaveLlave_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "ClaveLlave");
    private final static QName _RFCReceptor_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RFCReceptor");
    private final static QName _RFCPACEnviaSolicitud_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RFCPACEnviaSolicitud");
    private final static QName _Fecha_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Fecha");
    private final static QName _UUID_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "UUID");
    private final static QName _RespuestaSolicitud_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RespuestaSolicitud");
    private final static QName _Codigo_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Codigo");
    private final static QName _StatusCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "StatusCFD");
    private final static QName _AcuseDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "AcuseDatos");
    private final static QName _Token_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Token");
    private final static QName _RFCEmisor_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RFCEmisor");
    private final static QName _Resultado_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Resultado");
    private final static QName _TotalComprobante_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "TotalComprobante");
    private final static QName _CodigoStatusCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "CodigoStatusCFD");
    private final static QName _IndicadorCancelableCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "IndicadorCancelableCFD");
    private final static QName _StatusCancelacion_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "StatusCancelacion");
    private final static QName _XML_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "XML");
    private final static QName _AcuseCancelacionDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "AcuseCancelacionDatos");
    private final static QName _Comprobante_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Comprobante");
    private final static QName _FechaCancelacion_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "FechaCancelacion");
    private final static QName _FechaEnvio_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "FechaEnvio");
    private final static QName _FechaTimbrado_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "FechaTimbrado");
    private final static QName _Hash_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Hash");
    private final static QName _Status_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Status");
    private final static QName _TipoCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "TipoCFD");
    private final static QName _Version_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Version");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.megapractical.invoicing.api.wsdl.cancelation.sat
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DetalleRespuesta }
     * 
     */
    public DetalleRespuesta createDetalleRespuesta() {
        return new DetalleRespuesta();
    }

    /**
     * Create an instance of {@link ListaDetalleRespuesta }
     * 
     */
    public ListaDetalleRespuesta createListaDetalleRespuesta() {
        return new ListaDetalleRespuesta();
    }

    /**
     * Create an instance of {@link ActualizaPeticionesCancelacionCFD }
     * 
     */
    public ActualizaPeticionesCancelacionCFD createActualizaPeticionesCancelacionCFD() {
        return new ActualizaPeticionesCancelacionCFD();
    }

    /**
     * Create an instance of {@link ActualizaPeticionesCancelacionCFDRequest }
     * 
     */
    public ActualizaPeticionesCancelacionCFDRequest createActualizaPeticionesCancelacionCFDRequest() {
        return new ActualizaPeticionesCancelacionCFDRequest();
    }

    /**
     * Create an instance of {@link HeaderRq }
     * 
     */
    public HeaderRq createHeaderRq() {
        return new HeaderRq();
    }

    /**
     * Create an instance of {@link ActualizaPeticionesCancelacionCFDRq }
     * 
     */
    public ActualizaPeticionesCancelacionCFDRq createActualizaPeticionesCancelacionCFDRq() {
        return new ActualizaPeticionesCancelacionCFDRq();
    }

    /**
     * Create an instance of {@link PeticionesCancelacionCFDPeticion }
     * 
     */
    public PeticionesCancelacionCFDPeticion createPeticionesCancelacionCFDPeticion() {
        return new PeticionesCancelacionCFDPeticion();
    }

    /**
     * Create an instance of {@link ActualizaPeticionesCancelacionCFDResponse }
     * 
     */
    public ActualizaPeticionesCancelacionCFDResponse createActualizaPeticionesCancelacionCFDResponse() {
        return new ActualizaPeticionesCancelacionCFDResponse();
    }

    /**
     * Create an instance of {@link HeaderRs }
     * 
     */
    public HeaderRs createHeaderRs() {
        return new HeaderRs();
    }

    /**
     * Create an instance of {@link ActualizaPeticionesCancelacionCFDRs }
     * 
     */
    public ActualizaPeticionesCancelacionCFDRs createActualizaPeticionesCancelacionCFDRs() {
        return new ActualizaPeticionesCancelacionCFDRs();
    }

    /**
     * Create an instance of {@link PeticionesCancelacionCFDResultado }
     * 
     */
    public PeticionesCancelacionCFDResultado createPeticionesCancelacionCFDResultado() {
        return new PeticionesCancelacionCFDResultado();
    }

    /**
     * Create an instance of {@link AutenticaCancelacionRetencionesSAT }
     * 
     */
    public AutenticaCancelacionRetencionesSAT createAutenticaCancelacionRetencionesSAT() {
        return new AutenticaCancelacionRetencionesSAT();
    }

    /**
     * Create an instance of {@link AutenticaCancelacionRetencionesSATRequest }
     * 
     */
    public AutenticaCancelacionRetencionesSATRequest createAutenticaCancelacionRetencionesSATRequest() {
        return new AutenticaCancelacionRetencionesSATRequest();
    }

    /**
     * Create an instance of {@link AutenticaCancelacionRetencionesSATRq }
     * 
     */
    public AutenticaCancelacionRetencionesSATRq createAutenticaCancelacionRetencionesSATRq() {
        return new AutenticaCancelacionRetencionesSATRq();
    }

    /**
     * Create an instance of {@link AutenticaCancelacionRetencionesSATResponse }
     * 
     */
    public AutenticaCancelacionRetencionesSATResponse createAutenticaCancelacionRetencionesSATResponse() {
        return new AutenticaCancelacionRetencionesSATResponse();
    }

    /**
     * Create an instance of {@link AutenticaCancelacionRetencionesSATRs }
     * 
     */
    public AutenticaCancelacionRetencionesSATRs createAutenticaCancelacionRetencionesSATRs() {
        return new AutenticaCancelacionRetencionesSATRs();
    }

    /**
     * Create an instance of {@link AutenticaEnvioSAT }
     * 
     */
    public AutenticaEnvioSAT createAutenticaEnvioSAT() {
        return new AutenticaEnvioSAT();
    }

    /**
     * Create an instance of {@link AutenticaEnvioSATRequest }
     * 
     */
    public AutenticaEnvioSATRequest createAutenticaEnvioSATRequest() {
        return new AutenticaEnvioSATRequest();
    }

    /**
     * Create an instance of {@link AutenticaEnvioSATRq }
     * 
     */
    public AutenticaEnvioSATRq createAutenticaEnvioSATRq() {
        return new AutenticaEnvioSATRq();
    }

    /**
     * Create an instance of {@link AutenticaEnvioSATResponse }
     * 
     */
    public AutenticaEnvioSATResponse createAutenticaEnvioSATResponse() {
        return new AutenticaEnvioSATResponse();
    }

    /**
     * Create an instance of {@link AutenticaEnvioSATRs }
     * 
     */
    public AutenticaEnvioSATRs createAutenticaEnvioSATRs() {
        return new AutenticaEnvioSATRs();
    }

    /**
     * Create an instance of {@link AutenticaRetencionesSAT }
     * 
     */
    public AutenticaRetencionesSAT createAutenticaRetencionesSAT() {
        return new AutenticaRetencionesSAT();
    }

    /**
     * Create an instance of {@link AutenticaRetencionesSATRequest }
     * 
     */
    public AutenticaRetencionesSATRequest createAutenticaRetencionesSATRequest() {
        return new AutenticaRetencionesSATRequest();
    }

    /**
     * Create an instance of {@link AutenticaRetencionesSATRq }
     * 
     */
    public AutenticaRetencionesSATRq createAutenticaRetencionesSATRq() {
        return new AutenticaRetencionesSATRq();
    }

    /**
     * Create an instance of {@link AutenticaRetencionesSATResponse }
     * 
     */
    public AutenticaRetencionesSATResponse createAutenticaRetencionesSATResponse() {
        return new AutenticaRetencionesSATResponse();
    }

    /**
     * Create an instance of {@link AutenticaRetencionesSATRs }
     * 
     */
    public AutenticaRetencionesSATRs createAutenticaRetencionesSATRs() {
        return new AutenticaRetencionesSATRs();
    }

    /**
     * Create an instance of {@link AutenticaSAT }
     * 
     */
    public AutenticaSAT createAutenticaSAT() {
        return new AutenticaSAT();
    }

    /**
     * Create an instance of {@link AutenticaSATRequest }
     * 
     */
    public AutenticaSATRequest createAutenticaSATRequest() {
        return new AutenticaSATRequest();
    }

    /**
     * Create an instance of {@link AutenticaSATRq }
     * 
     */
    public AutenticaSATRq createAutenticaSATRq() {
        return new AutenticaSATRq();
    }

    /**
     * Create an instance of {@link AutenticaSATResponse }
     * 
     */
    public AutenticaSATResponse createAutenticaSATResponse() {
        return new AutenticaSATResponse();
    }

    /**
     * Create an instance of {@link AutenticaSATRs }
     * 
     */
    public AutenticaSATRs createAutenticaSATRs() {
        return new AutenticaSATRs();
    }

    /**
     * Create an instance of {@link CancelaCFD }
     * 
     */
    public CancelaCFD createCancelaCFD() {
        return new CancelaCFD();
    }

    /**
     * Create an instance of {@link CancelaCFDRequest }
     * 
     */
    public CancelaCFDRequest createCancelaCFDRequest() {
        return new CancelaCFDRequest();
    }

    /**
     * Create an instance of {@link CancelaCFDRq }
     * 
     */
    public CancelaCFDRq createCancelaCFDRq() {
        return new CancelaCFDRq();
    }

    /**
     * Create an instance of {@link CancelaCFDPeticion }
     * 
     */
    public CancelaCFDPeticion createCancelaCFDPeticion() {
        return new CancelaCFDPeticion();
    }

    /**
     * Create an instance of {@link CancelaCFDResponse }
     * 
     */
    public CancelaCFDResponse createCancelaCFDResponse() {
        return new CancelaCFDResponse();
    }

    /**
     * Create an instance of {@link CancelaCFDRs }
     * 
     */
    public CancelaCFDRs createCancelaCFDRs() {
        return new CancelaCFDRs();
    }

    /**
     * Create an instance of {@link CancelaCFDResultado }
     * 
     */
    public CancelaCFDResultado createCancelaCFDResultado() {
        return new CancelaCFDResultado();
    }

    /**
     * Create an instance of {@link CancelaRetencion }
     * 
     */
    public CancelaRetencion createCancelaRetencion() {
        return new CancelaRetencion();
    }

    /**
     * Create an instance of {@link CancelaRetencionRequest }
     * 
     */
    public CancelaRetencionRequest createCancelaRetencionRequest() {
        return new CancelaRetencionRequest();
    }

    /**
     * Create an instance of {@link CancelaRetencionRq }
     * 
     */
    public CancelaRetencionRq createCancelaRetencionRq() {
        return new CancelaRetencionRq();
    }

    /**
     * Create an instance of {@link CancelaRetencionPeticion }
     * 
     */
    public CancelaRetencionPeticion createCancelaRetencionPeticion() {
        return new CancelaRetencionPeticion();
    }

    /**
     * Create an instance of {@link CancelaRetencionResponse }
     * 
     */
    public CancelaRetencionResponse createCancelaRetencionResponse() {
        return new CancelaRetencionResponse();
    }

    /**
     * Create an instance of {@link CancelaRetencionRs }
     * 
     */
    public CancelaRetencionRs createCancelaRetencionRs() {
        return new CancelaRetencionRs();
    }

    /**
     * Create an instance of {@link CancelaRetencionResultado }
     * 
     */
    public CancelaRetencionResultado createCancelaRetencionResultado() {
        return new CancelaRetencionResultado();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasiva }
     * 
     */
    public CancelaRetencionMasiva createCancelaRetencionMasiva() {
        return new CancelaRetencionMasiva();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasivaRequest }
     * 
     */
    public CancelaRetencionMasivaRequest createCancelaRetencionMasivaRequest() {
        return new CancelaRetencionMasivaRequest();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasivaRq }
     * 
     */
    public CancelaRetencionMasivaRq createCancelaRetencionMasivaRq() {
        return new CancelaRetencionMasivaRq();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasivaPeticion }
     * 
     */
    public CancelaRetencionMasivaPeticion createCancelaRetencionMasivaPeticion() {
        return new CancelaRetencionMasivaPeticion();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasivaResponse }
     * 
     */
    public CancelaRetencionMasivaResponse createCancelaRetencionMasivaResponse() {
        return new CancelaRetencionMasivaResponse();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasivaRs }
     * 
     */
    public CancelaRetencionMasivaRs createCancelaRetencionMasivaRs() {
        return new CancelaRetencionMasivaRs();
    }

    /**
     * Create an instance of {@link CancelaRetencionMasivaResultado }
     * 
     */
    public CancelaRetencionMasivaResultado createCancelaRetencionMasivaResultado() {
        return new CancelaRetencionMasivaResultado();
    }

    /**
     * Create an instance of {@link ConsultaCFDRelacionados }
     * 
     */
    public ConsultaCFDRelacionados createConsultaCFDRelacionados() {
        return new ConsultaCFDRelacionados();
    }

    /**
     * Create an instance of {@link ConsultaCFDRelacionadosRequest }
     * 
     */
    public ConsultaCFDRelacionadosRequest createConsultaCFDRelacionadosRequest() {
        return new ConsultaCFDRelacionadosRequest();
    }

    /**
     * Create an instance of {@link ConsultaCFDRelacionadosRq }
     * 
     */
    public ConsultaCFDRelacionadosRq createConsultaCFDRelacionadosRq() {
        return new ConsultaCFDRelacionadosRq();
    }

    /**
     * Create an instance of {@link ConsultaCFDRelacionadosResponse }
     * 
     */
    public ConsultaCFDRelacionadosResponse createConsultaCFDRelacionadosResponse() {
        return new ConsultaCFDRelacionadosResponse();
    }

    /**
     * Create an instance of {@link ConsultaCFDRelacionadosRs }
     * 
     */
    public ConsultaCFDRelacionadosRs createConsultaCFDRelacionadosRs() {
        return new ConsultaCFDRelacionadosRs();
    }

    /**
     * Create an instance of {@link CFDRelacionadosPadres }
     * 
     */
    public CFDRelacionadosPadres createCFDRelacionadosPadres() {
        return new CFDRelacionadosPadres();
    }

    /**
     * Create an instance of {@link CFDRelacionado }
     * 
     */
    public CFDRelacionado createCFDRelacionado() {
        return new CFDRelacionado();
    }

    /**
     * Create an instance of {@link CFDRelacionadosHijos }
     * 
     */
    public CFDRelacionadosHijos createCFDRelacionadosHijos() {
        return new CFDRelacionadosHijos();
    }

    /**
     * Create an instance of {@link ConsultaCancelacionRetencionMasiva }
     * 
     */
    public ConsultaCancelacionRetencionMasiva createConsultaCancelacionRetencionMasiva() {
        return new ConsultaCancelacionRetencionMasiva();
    }

    /**
     * Create an instance of {@link ConsultaCancelacionRetencionMasivaRequest }
     * 
     */
    public ConsultaCancelacionRetencionMasivaRequest createConsultaCancelacionRetencionMasivaRequest() {
        return new ConsultaCancelacionRetencionMasivaRequest();
    }

    /**
     * Create an instance of {@link ConsultaCancelacionRetencionMasivaRq }
     * 
     */
    public ConsultaCancelacionRetencionMasivaRq createConsultaCancelacionRetencionMasivaRq() {
        return new ConsultaCancelacionRetencionMasivaRq();
    }

    /**
     * Create an instance of {@link ConsultaCancelacionRetencionMasivaResponse }
     * 
     */
    public ConsultaCancelacionRetencionMasivaResponse createConsultaCancelacionRetencionMasivaResponse() {
        return new ConsultaCancelacionRetencionMasivaResponse();
    }

    /**
     * Create an instance of {@link ConsultaCancelacionRetencionMasivaRs }
     * 
     */
    public ConsultaCancelacionRetencionMasivaRs createConsultaCancelacionRetencionMasivaRs() {
        return new ConsultaCancelacionRetencionMasivaRs();
    }

    /**
     * Create an instance of {@link ConsultaCancelacionRetencionMasivaResultado }
     * 
     */
    public ConsultaCancelacionRetencionMasivaResultado createConsultaCancelacionRetencionMasivaResultado() {
        return new ConsultaCancelacionRetencionMasivaResultado();
    }

    /**
     * Create an instance of {@link ConsultaPeticionesCancelacionCFD }
     * 
     */
    public ConsultaPeticionesCancelacionCFD createConsultaPeticionesCancelacionCFD() {
        return new ConsultaPeticionesCancelacionCFD();
    }

    /**
     * Create an instance of {@link ConsultaPeticionesCancelacionCFDRequest }
     * 
     */
    public ConsultaPeticionesCancelacionCFDRequest createConsultaPeticionesCancelacionCFDRequest() {
        return new ConsultaPeticionesCancelacionCFDRequest();
    }

    /**
     * Create an instance of {@link ConsultaPeticionesCancelacionCFDRq }
     * 
     */
    public ConsultaPeticionesCancelacionCFDRq createConsultaPeticionesCancelacionCFDRq() {
        return new ConsultaPeticionesCancelacionCFDRq();
    }

    /**
     * Create an instance of {@link ConsultaPeticionesCancelacionCFDResponse }
     * 
     */
    public ConsultaPeticionesCancelacionCFDResponse createConsultaPeticionesCancelacionCFDResponse() {
        return new ConsultaPeticionesCancelacionCFDResponse();
    }

    /**
     * Create an instance of {@link ConsultaPeticionesCancelacionCFDRs }
     * 
     */
    public ConsultaPeticionesCancelacionCFDRs createConsultaPeticionesCancelacionCFDRs() {
        return new ConsultaPeticionesCancelacionCFDRs();
    }

    /**
     * Create an instance of {@link PeticionesCancelacionCFD }
     * 
     */
    public PeticionesCancelacionCFD createPeticionesCancelacionCFD() {
        return new PeticionesCancelacionCFD();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFD }
     * 
     */
    public ConsultaStatusCFD createConsultaStatusCFD() {
        return new ConsultaStatusCFD();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDRequest }
     * 
     */
    public ConsultaStatusCFDRequest createConsultaStatusCFDRequest() {
        return new ConsultaStatusCFDRequest();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDRq }
     * 
     */
    public ConsultaStatusCFDRq createConsultaStatusCFDRq() {
        return new ConsultaStatusCFDRq();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDResponse }
     * 
     */
    public ConsultaStatusCFDResponse createConsultaStatusCFDResponse() {
        return new ConsultaStatusCFDResponse();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDRs }
     * 
     */
    public ConsultaStatusCFDRs createConsultaStatusCFDRs() {
        return new ConsultaStatusCFDRs();
    }

    /**
     * Create an instance of {@link ConsultaSatusCFDResultado }
     * 
     */
    public ConsultaSatusCFDResultado createConsultaSatusCFDResultado() {
        return new ConsultaSatusCFDResultado();
    }

    /**
     * Create an instance of {@link EnviaRetencion }
     * 
     */
    public EnviaRetencion createEnviaRetencion() {
        return new EnviaRetencion();
    }

    /**
     * Create an instance of {@link EnviaRetencionRequest }
     * 
     */
    public EnviaRetencionRequest createEnviaRetencionRequest() {
        return new EnviaRetencionRequest();
    }

    /**
     * Create an instance of {@link EnviaRetencionRq }
     * 
     */
    public EnviaRetencionRq createEnviaRetencionRq() {
        return new EnviaRetencionRq();
    }

    /**
     * Create an instance of {@link EnviaRetencionResponse }
     * 
     */
    public EnviaRetencionResponse createEnviaRetencionResponse() {
        return new EnviaRetencionResponse();
    }

    /**
     * Create an instance of {@link EnviaRetencionRs }
     * 
     */
    public EnviaRetencionRs createEnviaRetencionRs() {
        return new EnviaRetencionRs();
    }

    /**
     * Create an instance of {@link SAT }
     * 
     */
    public SAT createSAT() {
        return new SAT();
    }

    /**
     * Create an instance of {@link Undefined }
     * 
     */
    public Undefined createUndefined() {
        return new Undefined();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "CodigoRespuesta")
    public JAXBElement<String> createCodigoRespuesta(String value) {
        return new JAXBElement<String>(_CodigoRespuesta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "ResultadoOperacion")
    public JAXBElement<Integer> createResultadoOperacion(Integer value) {
        return new JAXBElement<Integer>(_ResultadoOperacion_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "TipoResultadoOperacion")
    public JAXBElement<String> createTipoResultadoOperacion(String value) {
        return new JAXBElement<String>(_TipoResultadoOperacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "SistemaDestino")
    public JAXBElement<String> createSistemaDestino(String value) {
        return new JAXBElement<String>(_SistemaDestino_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "MensajeRespuesta")
    public JAXBElement<String> createMensajeRespuesta(String value) {
        return new JAXBElement<String>(_MensajeRespuesta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "TrazaRespuesta")
    public JAXBElement<String> createTrazaRespuesta(String value) {
        return new JAXBElement<String>(_TrazaRespuesta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "DireccionIP")
    public JAXBElement<String> createDireccionIP(String value) {
        return new JAXBElement<String>(_DireccionIP_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "FechaTransaccion")
    public JAXBElement<XMLGregorianCalendar> createFechaTransaccion(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FechaTransaccion_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "HoraTransaccion")
    public JAXBElement<XMLGregorianCalendar> createHoraTransaccion(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_HoraTransaccion_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "IDCorrelacion")
    public JAXBElement<String> createIDCorrelacion(String value) {
        return new JAXBElement<String>(_IDCorrelacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "IDTransaccion")
    public JAXBElement<String> createIDTransaccion(String value) {
        return new JAXBElement<String>(_IDTransaccion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "NombreOperacion")
    public JAXBElement<String> createNombreOperacion(String value) {
        return new JAXBElement<String>(_NombreOperacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "NombreServicio")
    public JAXBElement<String> createNombreServicio(String value) {
        return new JAXBElement<String>(_NombreServicio_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "Referencia")
    public JAXBElement<String> createReferencia(String value) {
        return new JAXBElement<String>(_Referencia_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "ResultadoTransaccion")
    public JAXBElement<Integer> createResultadoTransaccion(Integer value) {
        return new JAXBElement<Integer>(_ResultadoTransaccion_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "Sesion")
    public JAXBElement<String> createSesion(String value) {
        return new JAXBElement<String>(_Sesion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "SistemaOrigen")
    public JAXBElement<String> createSistemaOrigen(String value) {
        return new JAXBElement<String>(_SistemaOrigen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "TipoResultadoTransaccion")
    public JAXBElement<String> createTipoResultadoTransaccion(String value) {
        return new JAXBElement<String>(_TipoResultadoTransaccion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "VersionOperacion")
    public JAXBElement<String> createVersionOperacion(String value) {
        return new JAXBElement<String>(_VersionOperacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "CertificadoDatos")
    public JAXBElement<byte[]> createCertificadoDatos(byte[] value) {
        return new JAXBElement<byte[]>(_CertificadoDatos_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "LlaveDatos")
    public JAXBElement<byte[]> createLlaveDatos(byte[] value) {
        return new JAXBElement<byte[]>(_LlaveDatos_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "ClaveLlave")
    public JAXBElement<String> createClaveLlave(String value) {
        return new JAXBElement<String>(_ClaveLlave_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RFCReceptor")
    public JAXBElement<String> createRFCReceptor(String value) {
        return new JAXBElement<String>(_RFCReceptor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RFCPACEnviaSolicitud")
    public JAXBElement<String> createRFCPACEnviaSolicitud(String value) {
        return new JAXBElement<String>(_RFCPACEnviaSolicitud_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Fecha")
    public JAXBElement<String> createFecha(String value) {
        return new JAXBElement<String>(_Fecha_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "UUID")
    public JAXBElement<String> createUUID(String value) {
        return new JAXBElement<String>(_UUID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RespuestaSolicitud")
    public JAXBElement<String> createRespuestaSolicitud(String value) {
        return new JAXBElement<String>(_RespuestaSolicitud_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Codigo")
    public JAXBElement<String> createCodigo(String value) {
        return new JAXBElement<String>(_Codigo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "StatusCFD")
    public JAXBElement<String> createStatusCFD(String value) {
        return new JAXBElement<String>(_StatusCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "AcuseDatos")
    public JAXBElement<String> createAcuseDatos(String value) {
        return new JAXBElement<String>(_AcuseDatos_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Token")
    public JAXBElement<String> createToken(String value) {
        return new JAXBElement<String>(_Token_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RFCEmisor")
    public JAXBElement<String> createRFCEmisor(String value) {
        return new JAXBElement<String>(_RFCEmisor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Resultado")
    public JAXBElement<String> createResultado(String value) {
        return new JAXBElement<String>(_Resultado_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "TotalComprobante")
    public JAXBElement<String> createTotalComprobante(String value) {
        return new JAXBElement<String>(_TotalComprobante_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "CodigoStatusCFD")
    public JAXBElement<String> createCodigoStatusCFD(String value) {
        return new JAXBElement<String>(_CodigoStatusCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "IndicadorCancelableCFD")
    public JAXBElement<String> createIndicadorCancelableCFD(String value) {
        return new JAXBElement<String>(_IndicadorCancelableCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "StatusCancelacion")
    public JAXBElement<String> createStatusCancelacion(String value) {
        return new JAXBElement<String>(_StatusCancelacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "XML")
    public JAXBElement<String> createXML(String value) {
        return new JAXBElement<String>(_XML_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "AcuseCancelacionDatos")
    public JAXBElement<String> createAcuseCancelacionDatos(String value) {
        return new JAXBElement<String>(_AcuseCancelacionDatos_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Comprobante")
    public JAXBElement<String> createComprobante(String value) {
        return new JAXBElement<String>(_Comprobante_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "FechaCancelacion")
    public JAXBElement<String> createFechaCancelacion(String value) {
        return new JAXBElement<String>(_FechaCancelacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "FechaEnvio")
    public JAXBElement<String> createFechaEnvio(String value) {
        return new JAXBElement<String>(_FechaEnvio_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "FechaTimbrado")
    public JAXBElement<String> createFechaTimbrado(String value) {
        return new JAXBElement<String>(_FechaTimbrado_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Hash")
    public JAXBElement<String> createHash(String value) {
        return new JAXBElement<String>(_Hash_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Status")
    public JAXBElement<String> createStatus(String value) {
        return new JAXBElement<String>(_Status_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "TipoCFD")
    public JAXBElement<String> createTipoCFD(String value) {
        return new JAXBElement<String>(_TipoCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Version")
    public JAXBElement<String> createVersion(String value) {
        return new JAXBElement<String>(_Version_QNAME, String.class, null, value);
    }

}
