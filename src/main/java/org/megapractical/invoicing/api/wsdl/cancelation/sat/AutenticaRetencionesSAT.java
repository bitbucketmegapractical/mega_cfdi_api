
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaRetencionesSATRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaRetencionesSATResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autenticaRetencionesSATRequest",
    "autenticaRetencionesSATResponse"
})
@XmlRootElement(name = "AutenticaRetencionesSAT")
public class AutenticaRetencionesSAT {

    @XmlElement(name = "AutenticaRetencionesSATRequest")
    protected AutenticaRetencionesSATRequest autenticaRetencionesSATRequest;
    @XmlElement(name = "AutenticaRetencionesSATResponse")
    protected AutenticaRetencionesSATResponse autenticaRetencionesSATResponse;

    /**
     * Obtiene el valor de la propiedad autenticaRetencionesSATRequest.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaRetencionesSATRequest }
     *     
     */
    public AutenticaRetencionesSATRequest getAutenticaRetencionesSATRequest() {
        return autenticaRetencionesSATRequest;
    }

    /**
     * Define el valor de la propiedad autenticaRetencionesSATRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaRetencionesSATRequest }
     *     
     */
    public void setAutenticaRetencionesSATRequest(AutenticaRetencionesSATRequest value) {
        this.autenticaRetencionesSATRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaRetencionesSATResponse.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaRetencionesSATResponse }
     *     
     */
    public AutenticaRetencionesSATResponse getAutenticaRetencionesSATResponse() {
        return autenticaRetencionesSATResponse;
    }

    /**
     * Define el valor de la propiedad autenticaRetencionesSATResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaRetencionesSATResponse }
     *     
     */
    public void setAutenticaRetencionesSATResponse(AutenticaRetencionesSATResponse value) {
        this.autenticaRetencionesSATResponse = value;
    }

}
