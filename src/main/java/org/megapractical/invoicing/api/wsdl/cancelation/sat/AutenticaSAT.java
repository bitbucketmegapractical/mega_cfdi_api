
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaSATRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaSATResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autenticaSATRequest",
    "autenticaSATResponse"
})
@XmlRootElement(name = "AutenticaSAT")
public class AutenticaSAT {

    @XmlElement(name = "AutenticaSATRequest")
    protected AutenticaSATRequest autenticaSATRequest;
    @XmlElement(name = "AutenticaSATResponse")
    protected AutenticaSATResponse autenticaSATResponse;

    /**
     * Obtiene el valor de la propiedad autenticaSATRequest.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaSATRequest }
     *     
     */
    public AutenticaSATRequest getAutenticaSATRequest() {
        return autenticaSATRequest;
    }

    /**
     * Define el valor de la propiedad autenticaSATRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaSATRequest }
     *     
     */
    public void setAutenticaSATRequest(AutenticaSATRequest value) {
        this.autenticaSATRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaSATResponse.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaSATResponse }
     *     
     */
    public AutenticaSATResponse getAutenticaSATResponse() {
        return autenticaSATResponse;
    }

    /**
     * Define el valor de la propiedad autenticaSATResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaSATResponse }
     *     
     */
    public void setAutenticaSATResponse(AutenticaSATResponse value) {
        this.autenticaSATResponse = value;
    }

}
