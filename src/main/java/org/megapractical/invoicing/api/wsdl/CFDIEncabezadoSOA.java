
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.megapractical.com/esb/comun/encabezado/1.0}CFDIEncabezadoPeticion"/&gt;
 *         &lt;element ref="{http://www.megapractical.com/esb/comun/encabezado/1.0}CFDIEncabezadoRespuesta"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cfdiEncabezadoPeticion",
    "cfdiEncabezadoRespuesta"
})
@XmlRootElement(name = "CFDIEncabezadoSOA", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
public class CFDIEncabezadoSOA {

    @XmlElement(name = "CFDIEncabezadoPeticion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected CFDIEncabezadoPeticion cfdiEncabezadoPeticion;
    @XmlElement(name = "CFDIEncabezadoRespuesta", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected CFDIEncabezadoRespuesta cfdiEncabezadoRespuesta;

    /**
     * Obtiene el valor de la propiedad cfdiEncabezadoPeticion.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoPeticion }
     *     
     */
    public CFDIEncabezadoPeticion getCFDIEncabezadoPeticion() {
        return cfdiEncabezadoPeticion;
    }

    /**
     * Define el valor de la propiedad cfdiEncabezadoPeticion.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoPeticion }
     *     
     */
    public void setCFDIEncabezadoPeticion(CFDIEncabezadoPeticion value) {
        this.cfdiEncabezadoPeticion = value;
    }

    /**
     * Obtiene el valor de la propiedad cfdiEncabezadoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoRespuesta }
     *     
     */
    public CFDIEncabezadoRespuesta getCFDIEncabezadoRespuesta() {
        return cfdiEncabezadoRespuesta;
    }

    /**
     * Define el valor de la propiedad cfdiEncabezadoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoRespuesta }
     *     
     */
    public void setCFDIEncabezadoRespuesta(CFDIEncabezadoRespuesta value) {
        this.cfdiEncabezadoRespuesta = value;
    }

}
