
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;group ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}Operations"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actualizaPeticionesCancelacionCFD",
    "autenticaCancelacionRetencionesSAT",
    "autenticaEnvioSAT",
    "autenticaRetencionesSAT",
    "autenticaSAT",
    "cancelaCFD",
    "cancelaRetencion",
    "cancelaRetencionMasiva",
    "consultaCFDRelacionados",
    "consultaCancelacionRetencionMasiva",
    "consultaPeticionesCancelacionCFD",
    "consultaStatusCFD",
    "enviaRetencion",
    "undefined"
})
@XmlRootElement(name = "SAT")
public class SAT {

    @XmlElement(name = "ActualizaPeticionesCancelacionCFD")
    protected ActualizaPeticionesCancelacionCFD actualizaPeticionesCancelacionCFD;
    @XmlElement(name = "AutenticaCancelacionRetencionesSAT")
    protected AutenticaCancelacionRetencionesSAT autenticaCancelacionRetencionesSAT;
    @XmlElement(name = "AutenticaEnvioSAT")
    protected AutenticaEnvioSAT autenticaEnvioSAT;
    @XmlElement(name = "AutenticaRetencionesSAT")
    protected AutenticaRetencionesSAT autenticaRetencionesSAT;
    @XmlElement(name = "AutenticaSAT")
    protected AutenticaSAT autenticaSAT;
    @XmlElement(name = "CancelaCFD")
    protected CancelaCFD cancelaCFD;
    @XmlElement(name = "CancelaRetencion")
    protected CancelaRetencion cancelaRetencion;
    @XmlElement(name = "CancelaRetencionMasiva")
    protected CancelaRetencionMasiva cancelaRetencionMasiva;
    @XmlElement(name = "ConsultaCFDRelacionados")
    protected ConsultaCFDRelacionados consultaCFDRelacionados;
    @XmlElement(name = "ConsultaCancelacionRetencionMasiva")
    protected ConsultaCancelacionRetencionMasiva consultaCancelacionRetencionMasiva;
    @XmlElement(name = "ConsultaPeticionesCancelacionCFD")
    protected ConsultaPeticionesCancelacionCFD consultaPeticionesCancelacionCFD;
    @XmlElement(name = "ConsultaStatusCFD")
    protected ConsultaStatusCFD consultaStatusCFD;
    @XmlElement(name = "EnviaRetencion")
    protected EnviaRetencion enviaRetencion;
    @XmlElement(name = "Undefined")
    protected Undefined undefined;

    /**
     * Obtiene el valor de la propiedad actualizaPeticionesCancelacionCFD.
     * 
     * @return
     *     possible object is
     *     {@link ActualizaPeticionesCancelacionCFD }
     *     
     */
    public ActualizaPeticionesCancelacionCFD getActualizaPeticionesCancelacionCFD() {
        return actualizaPeticionesCancelacionCFD;
    }

    /**
     * Define el valor de la propiedad actualizaPeticionesCancelacionCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link ActualizaPeticionesCancelacionCFD }
     *     
     */
    public void setActualizaPeticionesCancelacionCFD(ActualizaPeticionesCancelacionCFD value) {
        this.actualizaPeticionesCancelacionCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaCancelacionRetencionesSAT.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaCancelacionRetencionesSAT }
     *     
     */
    public AutenticaCancelacionRetencionesSAT getAutenticaCancelacionRetencionesSAT() {
        return autenticaCancelacionRetencionesSAT;
    }

    /**
     * Define el valor de la propiedad autenticaCancelacionRetencionesSAT.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaCancelacionRetencionesSAT }
     *     
     */
    public void setAutenticaCancelacionRetencionesSAT(AutenticaCancelacionRetencionesSAT value) {
        this.autenticaCancelacionRetencionesSAT = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaEnvioSAT.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaEnvioSAT }
     *     
     */
    public AutenticaEnvioSAT getAutenticaEnvioSAT() {
        return autenticaEnvioSAT;
    }

    /**
     * Define el valor de la propiedad autenticaEnvioSAT.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaEnvioSAT }
     *     
     */
    public void setAutenticaEnvioSAT(AutenticaEnvioSAT value) {
        this.autenticaEnvioSAT = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaRetencionesSAT.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaRetencionesSAT }
     *     
     */
    public AutenticaRetencionesSAT getAutenticaRetencionesSAT() {
        return autenticaRetencionesSAT;
    }

    /**
     * Define el valor de la propiedad autenticaRetencionesSAT.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaRetencionesSAT }
     *     
     */
    public void setAutenticaRetencionesSAT(AutenticaRetencionesSAT value) {
        this.autenticaRetencionesSAT = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaSAT.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaSAT }
     *     
     */
    public AutenticaSAT getAutenticaSAT() {
        return autenticaSAT;
    }

    /**
     * Define el valor de la propiedad autenticaSAT.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaSAT }
     *     
     */
    public void setAutenticaSAT(AutenticaSAT value) {
        this.autenticaSAT = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaCFD.
     * 
     * @return
     *     possible object is
     *     {@link CancelaCFD }
     *     
     */
    public CancelaCFD getCancelaCFD() {
        return cancelaCFD;
    }

    /**
     * Define el valor de la propiedad cancelaCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaCFD }
     *     
     */
    public void setCancelaCFD(CancelaCFD value) {
        this.cancelaCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaRetencion.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencion }
     *     
     */
    public CancelaRetencion getCancelaRetencion() {
        return cancelaRetencion;
    }

    /**
     * Define el valor de la propiedad cancelaRetencion.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencion }
     *     
     */
    public void setCancelaRetencion(CancelaRetencion value) {
        this.cancelaRetencion = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaRetencionMasiva.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionMasiva }
     *     
     */
    public CancelaRetencionMasiva getCancelaRetencionMasiva() {
        return cancelaRetencionMasiva;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionMasiva.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionMasiva }
     *     
     */
    public void setCancelaRetencionMasiva(CancelaRetencionMasiva value) {
        this.cancelaRetencionMasiva = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaCFDRelacionados.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCFDRelacionados }
     *     
     */
    public ConsultaCFDRelacionados getConsultaCFDRelacionados() {
        return consultaCFDRelacionados;
    }

    /**
     * Define el valor de la propiedad consultaCFDRelacionados.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCFDRelacionados }
     *     
     */
    public void setConsultaCFDRelacionados(ConsultaCFDRelacionados value) {
        this.consultaCFDRelacionados = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaCancelacionRetencionMasiva.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaCancelacionRetencionMasiva }
     *     
     */
    public ConsultaCancelacionRetencionMasiva getConsultaCancelacionRetencionMasiva() {
        return consultaCancelacionRetencionMasiva;
    }

    /**
     * Define el valor de la propiedad consultaCancelacionRetencionMasiva.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaCancelacionRetencionMasiva }
     *     
     */
    public void setConsultaCancelacionRetencionMasiva(ConsultaCancelacionRetencionMasiva value) {
        this.consultaCancelacionRetencionMasiva = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaPeticionesCancelacionCFD.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPeticionesCancelacionCFD }
     *     
     */
    public ConsultaPeticionesCancelacionCFD getConsultaPeticionesCancelacionCFD() {
        return consultaPeticionesCancelacionCFD;
    }

    /**
     * Define el valor de la propiedad consultaPeticionesCancelacionCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPeticionesCancelacionCFD }
     *     
     */
    public void setConsultaPeticionesCancelacionCFD(ConsultaPeticionesCancelacionCFD value) {
        this.consultaPeticionesCancelacionCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaStatusCFD.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaStatusCFD }
     *     
     */
    public ConsultaStatusCFD getConsultaStatusCFD() {
        return consultaStatusCFD;
    }

    /**
     * Define el valor de la propiedad consultaStatusCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaStatusCFD }
     *     
     */
    public void setConsultaStatusCFD(ConsultaStatusCFD value) {
        this.consultaStatusCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad enviaRetencion.
     * 
     * @return
     *     possible object is
     *     {@link EnviaRetencion }
     *     
     */
    public EnviaRetencion getEnviaRetencion() {
        return enviaRetencion;
    }

    /**
     * Define el valor de la propiedad enviaRetencion.
     * 
     * @param value
     *     allowed object is
     *     {@link EnviaRetencion }
     *     
     */
    public void setEnviaRetencion(EnviaRetencion value) {
        this.enviaRetencion = value;
    }

    /**
     * Obtiene el valor de la propiedad undefined.
     * 
     * @return
     *     possible object is
     *     {@link Undefined }
     *     
     */
    public Undefined getUndefined() {
        return undefined;
    }

    /**
     * Define el valor de la propiedad undefined.
     * 
     * @param value
     *     allowed object is
     *     {@link Undefined }
     *     
     */
    public void setUndefined(Undefined value) {
        this.undefined = value;
    }

}
