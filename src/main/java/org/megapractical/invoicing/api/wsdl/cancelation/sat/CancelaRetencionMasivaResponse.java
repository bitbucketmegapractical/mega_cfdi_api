
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/common/1.0}HeaderRs"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionMasivaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRs",
    "cancelaRetencionMasivaRs"
})
@XmlRootElement(name = "CancelaRetencionMasivaResponse")
public class CancelaRetencionMasivaResponse {

    @XmlElement(name = "HeaderRs", namespace = "http://www.mcfdi.com/services/common/1.0", required = true)
    protected HeaderRs headerRs;
    @XmlElement(name = "CancelaRetencionMasivaRs")
    protected CancelaRetencionMasivaRs cancelaRetencionMasivaRs;

    /**
     * Obtiene el valor de la propiedad headerRs.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRs }
     *     
     */
    public HeaderRs getHeaderRs() {
        return headerRs;
    }

    /**
     * Define el valor de la propiedad headerRs.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRs }
     *     
     */
    public void setHeaderRs(HeaderRs value) {
        this.headerRs = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaRetencionMasivaRs.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionMasivaRs }
     *     
     */
    public CancelaRetencionMasivaRs getCancelaRetencionMasivaRs() {
        return cancelaRetencionMasivaRs;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionMasivaRs.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionMasivaRs }
     *     
     */
    public void setCancelaRetencionMasivaRs(CancelaRetencionMasivaRs value) {
        this.cancelaRetencionMasivaRs = value;
    }

}
