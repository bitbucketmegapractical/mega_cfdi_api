
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CorrelationID" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="64"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FechaTransaccion" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="HoraTransaccion" type="{http://www.w3.org/2001/XMLSchema}time"/&gt;
 *         &lt;element name="NombreOperacion"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="VersionOperacion" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="12"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DireccionIP" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="SistemaOrigen"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="64"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Sesion" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="256"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="IDCliente" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="64"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "correlationID",
    "fechaTransaccion",
    "horaTransaccion",
    "nombreOperacion",
    "versionOperacion",
    "direccionIP",
    "sistemaOrigen",
    "sesion",
    "idCliente"
})
@XmlRootElement(name = "CFDIEncabezadoPeticion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
public class CFDIEncabezadoPeticion {

    @XmlElement(name = "CorrelationID", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected String correlationID;
    @XmlElement(name = "FechaTransaccion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaTransaccion;
    @XmlElement(name = "HoraTransaccion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0", required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar horaTransaccion;
    @XmlElement(name = "NombreOperacion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0", required = true)
    protected String nombreOperacion;
    @XmlElement(name = "VersionOperacion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected String versionOperacion;
    @XmlElement(name = "DireccionIP", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected String direccionIP;
    @XmlElement(name = "SistemaOrigen", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0", required = true)
    protected String sistemaOrigen;
    @XmlElement(name = "Sesion", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected String sesion;
    @XmlElement(name = "IDCliente", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0")
    protected String idCliente;

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * Define el valor de la propiedad fechaTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaTransaccion(XMLGregorianCalendar value) {
        this.fechaTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad horaTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHoraTransaccion() {
        return horaTransaccion;
    }

    /**
     * Define el valor de la propiedad horaTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHoraTransaccion(XMLGregorianCalendar value) {
        this.horaTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOperacion() {
        return nombreOperacion;
    }

    /**
     * Define el valor de la propiedad nombreOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOperacion(String value) {
        this.nombreOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad versionOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionOperacion() {
        return versionOperacion;
    }

    /**
     * Define el valor de la propiedad versionOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionOperacion(String value) {
        this.versionOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionIP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionIP() {
        return direccionIP;
    }

    /**
     * Define el valor de la propiedad direccionIP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionIP(String value) {
        this.direccionIP = value;
    }

    /**
     * Obtiene el valor de la propiedad sistemaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    /**
     * Define el valor de la propiedad sistemaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaOrigen(String value) {
        this.sistemaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad sesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSesion() {
        return sesion;
    }

    /**
     * Define el valor de la propiedad sesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSesion(String value) {
        this.sesion = value;
    }

    /**
     * Obtiene el valor de la propiedad idCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCliente() {
        return idCliente;
    }

    /**
     * Define el valor de la propiedad idCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCliente(String value) {
        this.idCliente = value;
    }

}
