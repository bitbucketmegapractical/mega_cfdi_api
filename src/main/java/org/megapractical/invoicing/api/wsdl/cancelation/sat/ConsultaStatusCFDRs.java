
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaSatusCFDResultado" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaSatusCFDResultado"
})
@XmlRootElement(name = "ConsultaStatusCFDRs")
public class ConsultaStatusCFDRs {

    @XmlElement(name = "ConsultaSatusCFDResultado")
    protected ConsultaSatusCFDResultado consultaSatusCFDResultado;

    /**
     * Obtiene el valor de la propiedad consultaSatusCFDResultado.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaSatusCFDResultado }
     *     
     */
    public ConsultaSatusCFDResultado getConsultaSatusCFDResultado() {
        return consultaSatusCFDResultado;
    }

    /**
     * Define el valor de la propiedad consultaSatusCFDResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaSatusCFDResultado }
     *     
     */
    public void setConsultaSatusCFDResultado(ConsultaSatusCFDResultado value) {
        this.consultaSatusCFDResultado = value;
    }

}
