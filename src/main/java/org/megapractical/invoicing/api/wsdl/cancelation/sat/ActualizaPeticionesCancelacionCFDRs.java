
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCReceptor"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCPACEnviaSolicitud"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Codigo"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Fecha"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}PeticionesCancelacionCFDResultado" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}AcuseDatos"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rfcReceptor",
    "rfcpacEnviaSolicitud",
    "codigo",
    "fecha",
    "peticionesCancelacionCFDResultado",
    "acuseDatos"
})
@XmlRootElement(name = "ActualizaPeticionesCancelacionCFDRs")
public class ActualizaPeticionesCancelacionCFDRs {

    @XmlElement(name = "RFCReceptor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcReceptor;
    @XmlElement(name = "RFCPACEnviaSolicitud", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcpacEnviaSolicitud;
    @XmlElement(name = "Codigo", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String codigo;
    @XmlElement(name = "Fecha", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String fecha;
    @XmlElement(name = "PeticionesCancelacionCFDResultado")
    protected List<PeticionesCancelacionCFDResultado> peticionesCancelacionCFDResultado;
    @XmlElement(name = "AcuseDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String acuseDatos;

    /**
     * Obtiene el valor de la propiedad rfcReceptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCReceptor() {
        return rfcReceptor;
    }

    /**
     * Define el valor de la propiedad rfcReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCReceptor(String value) {
        this.rfcReceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcpacEnviaSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCPACEnviaSolicitud() {
        return rfcpacEnviaSolicitud;
    }

    /**
     * Define el valor de la propiedad rfcpacEnviaSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCPACEnviaSolicitud(String value) {
        this.rfcpacEnviaSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the peticionesCancelacionCFDResultado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the peticionesCancelacionCFDResultado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPeticionesCancelacionCFDResultado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PeticionesCancelacionCFDResultado }
     * 
     * 
     */
    public List<PeticionesCancelacionCFDResultado> getPeticionesCancelacionCFDResultado() {
        if (peticionesCancelacionCFDResultado == null) {
            peticionesCancelacionCFDResultado = new ArrayList<PeticionesCancelacionCFDResultado>();
        }
        return this.peticionesCancelacionCFDResultado;
    }

    /**
     * Obtiene el valor de la propiedad acuseDatos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcuseDatos() {
        return acuseDatos;
    }

    /**
     * Define el valor de la propiedad acuseDatos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcuseDatos(String value) {
        this.acuseDatos = value;
    }

}
