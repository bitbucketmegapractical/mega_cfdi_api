
package org.megapractical.invoicing.api.wsdl.retention.schema;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tibco.schemas.bw_cfdi_v01_cfdi_ryp_in_xml.sharedresources.schemadefinitions.serviceschemas.schema package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _KeyByte_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "keyByte");
    private final static QName _CertByte_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "certByte");
    private final static QName _Undefined_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "Undefined");
    private final static QName _KeyPassword_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "keyPassword");
    private final static QName _RetencionesString_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "retencionesString");
    private final static QName _RetencionesCFDIin_QNAME = new QName("http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", "retencionesCFDIin");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tibco.schemas.bw_cfdi_v01_cfdi_ryp_in_xml.sharedresources.schemadefinitions.serviceschemas.schema
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Operations }
     * 
     */
    public Operations createOperations() {
        return new Operations();
    }

    /**
     * Create an instance of {@link CFDI }
     * 
     */
    public CFDI createCFDI() {
        return new CFDI();
    }

    /**
     * Create an instance of {@link RetencionesResponse }
     * 
     */
    public RetencionesResponse createRetencionesResponse() {
        return new RetencionesResponse();
    }

    /**
     * Create an instance of {@link RetencionesRequest }
     * 
     */
    public RetencionesRequest createRetencionesRequest() {
        return new RetencionesRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "keyByte")
    public JAXBElement<String> createKeyByte(String value) {
        return new JAXBElement<String>(_KeyByte_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "certByte")
    public JAXBElement<String> createCertByte(String value) {
        return new JAXBElement<String>(_CertByte_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "Undefined")
    public JAXBElement<Object> createUndefined(Object value) {
        return new JAXBElement<Object>(_Undefined_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "keyPassword")
    public JAXBElement<String> createKeyPassword(String value) {
        return new JAXBElement<String>(_KeyPassword_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "retencionesString")
    public JAXBElement<String> createRetencionesString(String value) {
        return new JAXBElement<String>(_RetencionesString_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd", name = "retencionesCFDIin")
    public JAXBElement<String> createRetencionesCFDIin(String value) {
        return new JAXBElement<String>(_RetencionesCFDIin_QNAME, String.class, null, value);
    }

}
