
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaCFDRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaCFDResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelaCFDRequest",
    "cancelaCFDResponse"
})
@XmlRootElement(name = "CancelaCFD")
public class CancelaCFD {

    @XmlElement(name = "CancelaCFDRequest")
    protected CancelaCFDRequest cancelaCFDRequest;
    @XmlElement(name = "CancelaCFDResponse")
    protected CancelaCFDResponse cancelaCFDResponse;

    /**
     * Obtiene el valor de la propiedad cancelaCFDRequest.
     * 
     * @return
     *     possible object is
     *     {@link CancelaCFDRequest }
     *     
     */
    public CancelaCFDRequest getCancelaCFDRequest() {
        return cancelaCFDRequest;
    }

    /**
     * Define el valor de la propiedad cancelaCFDRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaCFDRequest }
     *     
     */
    public void setCancelaCFDRequest(CancelaCFDRequest value) {
        this.cancelaCFDRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaCFDResponse.
     * 
     * @return
     *     possible object is
     *     {@link CancelaCFDResponse }
     *     
     */
    public CancelaCFDResponse getCancelaCFDResponse() {
        return cancelaCFDResponse;
    }

    /**
     * Define el valor de la propiedad cancelaCFDResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaCFDResponse }
     *     
     */
    public void setCancelaCFDResponse(CancelaCFDResponse value) {
        this.cancelaCFDResponse = value;
    }

}
