
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}keyPassword"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}certByte"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}keyByte"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}rfc"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}UUID"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "keyPassword",
    "certByte",
    "keyByte",
    "rfc",
    "uuid"
})
@XmlRootElement(name = "CancelacionRequest")
public class CancelacionRequest {

    @XmlElement(required = true)
    protected String keyPassword;
    @XmlElement(required = true)
    protected String certByte;
    @XmlElement(required = true)
    protected String keyByte;
    @XmlElement(required = true)
    protected String rfc;
    @XmlElement(name = "UUID", required = true)
    protected String uuid;

    /**
     * Obtiene el valor de la propiedad keyPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyPassword() {
        return keyPassword;
    }

    /**
     * Define el valor de la propiedad keyPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyPassword(String value) {
        this.keyPassword = value;
    }

    /**
     * Obtiene el valor de la propiedad certByte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertByte() {
        return certByte;
    }

    /**
     * Define el valor de la propiedad certByte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertByte(String value) {
        this.certByte = value;
    }

    /**
     * Obtiene el valor de la propiedad keyByte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyByte() {
        return keyByte;
    }

    /**
     * Define el valor de la propiedad keyByte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyByte(String value) {
        this.keyByte = value;
    }

    /**
     * Obtiene el valor de la propiedad rfc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * Define el valor de la propiedad rfc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRfc(String value) {
        this.rfc = value;
    }

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUID(String value) {
        this.uuid = value;
    }

}
