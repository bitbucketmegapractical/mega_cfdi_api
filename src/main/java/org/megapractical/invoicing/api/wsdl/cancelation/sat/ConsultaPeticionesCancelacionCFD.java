
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaPeticionesCancelacionCFDRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ConsultaPeticionesCancelacionCFDResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaPeticionesCancelacionCFDRequest",
    "consultaPeticionesCancelacionCFDResponse"
})
@XmlRootElement(name = "ConsultaPeticionesCancelacionCFD")
public class ConsultaPeticionesCancelacionCFD {

    @XmlElement(name = "ConsultaPeticionesCancelacionCFDRequest")
    protected ConsultaPeticionesCancelacionCFDRequest consultaPeticionesCancelacionCFDRequest;
    @XmlElement(name = "ConsultaPeticionesCancelacionCFDResponse")
    protected ConsultaPeticionesCancelacionCFDResponse consultaPeticionesCancelacionCFDResponse;

    /**
     * Obtiene el valor de la propiedad consultaPeticionesCancelacionCFDRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPeticionesCancelacionCFDRequest }
     *     
     */
    public ConsultaPeticionesCancelacionCFDRequest getConsultaPeticionesCancelacionCFDRequest() {
        return consultaPeticionesCancelacionCFDRequest;
    }

    /**
     * Define el valor de la propiedad consultaPeticionesCancelacionCFDRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPeticionesCancelacionCFDRequest }
     *     
     */
    public void setConsultaPeticionesCancelacionCFDRequest(ConsultaPeticionesCancelacionCFDRequest value) {
        this.consultaPeticionesCancelacionCFDRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaPeticionesCancelacionCFDResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPeticionesCancelacionCFDResponse }
     *     
     */
    public ConsultaPeticionesCancelacionCFDResponse getConsultaPeticionesCancelacionCFDResponse() {
        return consultaPeticionesCancelacionCFDResponse;
    }

    /**
     * Define el valor de la propiedad consultaPeticionesCancelacionCFDResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPeticionesCancelacionCFDResponse }
     *     
     */
    public void setConsultaPeticionesCancelacionCFDResponse(ConsultaPeticionesCancelacionCFDResponse value) {
        this.consultaPeticionesCancelacionCFDResponse = value;
    }

}
