
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}CodigoStatusCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}IndicadorCancelableCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}StatusCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}StatusCancelacion" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}AcuseDatos" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codigoStatusCFD",
    "indicadorCancelableCFD",
    "statusCFD",
    "statusCancelacion",
    "acuseDatos"
})
@XmlRootElement(name = "ConsultaCancelacionRetencionMasivaResultado")
public class ConsultaCancelacionRetencionMasivaResultado {

    @XmlElement(name = "CodigoStatusCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String codigoStatusCFD;
    @XmlElement(name = "IndicadorCancelableCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String indicadorCancelableCFD;
    @XmlElement(name = "StatusCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String statusCFD;
    @XmlElement(name = "StatusCancelacion", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String statusCancelacion;
    @XmlElement(name = "AcuseDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String acuseDatos;

    /**
     * Obtiene el valor de la propiedad codigoStatusCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoStatusCFD() {
        return codigoStatusCFD;
    }

    /**
     * Define el valor de la propiedad codigoStatusCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoStatusCFD(String value) {
        this.codigoStatusCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorCancelableCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorCancelableCFD() {
        return indicadorCancelableCFD;
    }

    /**
     * Define el valor de la propiedad indicadorCancelableCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorCancelableCFD(String value) {
        this.indicadorCancelableCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad statusCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCFD() {
        return statusCFD;
    }

    /**
     * Define el valor de la propiedad statusCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCFD(String value) {
        this.statusCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad statusCancelacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCancelacion() {
        return statusCancelacion;
    }

    /**
     * Define el valor de la propiedad statusCancelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCancelacion(String value) {
        this.statusCancelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad acuseDatos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcuseDatos() {
        return acuseDatos;
    }

    /**
     * Define el valor de la propiedad acuseDatos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcuseDatos(String value) {
        this.acuseDatos = value;
    }

}
