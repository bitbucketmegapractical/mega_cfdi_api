
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}ResultadoOperacion"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}TipoResultadoOperacion"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}SistemaDestino"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}MensajeRespuesta"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}CodigoRespuesta"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}TrazaRespuesta" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultadoOperacion",
    "tipoResultadoOperacion",
    "sistemaDestino",
    "mensajeRespuesta",
    "codigoRespuesta",
    "trazaRespuesta"
})
@XmlRootElement(name = "DetalleRespuesta", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
public class DetalleRespuesta {

    @XmlElement(name = "ResultadoOperacion", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
    protected int resultadoOperacion;
    @XmlElement(name = "TipoResultadoOperacion", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String tipoResultadoOperacion;
    @XmlElement(name = "SistemaDestino", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String sistemaDestino;
    @XmlElement(name = "MensajeRespuesta", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String mensajeRespuesta;
    @XmlElement(name = "CodigoRespuesta", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String codigoRespuesta;
    @XmlElement(name = "TrazaRespuesta", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
    protected String trazaRespuesta;

    /**
     * Obtiene el valor de la propiedad resultadoOperacion.
     * 
     */
    public int getResultadoOperacion() {
        return resultadoOperacion;
    }

    /**
     * Define el valor de la propiedad resultadoOperacion.
     * 
     */
    public void setResultadoOperacion(int value) {
        this.resultadoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoResultadoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoResultadoOperacion() {
        return tipoResultadoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoResultadoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoResultadoOperacion(String value) {
        this.tipoResultadoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad sistemaDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaDestino() {
        return sistemaDestino;
    }

    /**
     * Define el valor de la propiedad sistemaDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaDestino(String value) {
        this.sistemaDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajeRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }

    /**
     * Define el valor de la propiedad mensajeRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeRespuesta(String value) {
        this.mensajeRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Define el valor de la propiedad codigoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRespuesta(String value) {
        this.codigoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad trazaRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrazaRespuesta() {
        return trazaRespuesta;
    }

    /**
     * Define el valor de la propiedad trazaRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrazaRespuesta(String value) {
        this.trazaRespuesta = value;
    }

}
