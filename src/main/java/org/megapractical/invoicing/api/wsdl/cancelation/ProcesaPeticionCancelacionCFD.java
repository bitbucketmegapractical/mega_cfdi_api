
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}ProcesaPeticionCancelacionCFDRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}ProcesaPeticionCancelacionCFDResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "procesaPeticionCancelacionCFDRequest",
    "procesaPeticionCancelacionCFDResponse"
})
@XmlRootElement(name = "ProcesaPeticionCancelacionCFD")
public class ProcesaPeticionCancelacionCFD {

    @XmlElement(name = "ProcesaPeticionCancelacionCFDRequest")
    protected ProcesaPeticionCancelacionCFDRequest procesaPeticionCancelacionCFDRequest;
    @XmlElement(name = "ProcesaPeticionCancelacionCFDResponse")
    protected ProcesaPeticionCancelacionCFDResponse procesaPeticionCancelacionCFDResponse;

    /**
     * Obtiene el valor de la propiedad procesaPeticionCancelacionCFDRequest.
     * 
     * @return
     *     possible object is
     *     {@link ProcesaPeticionCancelacionCFDRequest }
     *     
     */
    public ProcesaPeticionCancelacionCFDRequest getProcesaPeticionCancelacionCFDRequest() {
        return procesaPeticionCancelacionCFDRequest;
    }

    /**
     * Define el valor de la propiedad procesaPeticionCancelacionCFDRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcesaPeticionCancelacionCFDRequest }
     *     
     */
    public void setProcesaPeticionCancelacionCFDRequest(ProcesaPeticionCancelacionCFDRequest value) {
        this.procesaPeticionCancelacionCFDRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad procesaPeticionCancelacionCFDResponse.
     * 
     * @return
     *     possible object is
     *     {@link ProcesaPeticionCancelacionCFDResponse }
     *     
     */
    public ProcesaPeticionCancelacionCFDResponse getProcesaPeticionCancelacionCFDResponse() {
        return procesaPeticionCancelacionCFDResponse;
    }

    /**
     * Define el valor de la propiedad procesaPeticionCancelacionCFDResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcesaPeticionCancelacionCFDResponse }
     *     
     */
    public void setProcesaPeticionCancelacionCFDResponse(ProcesaPeticionCancelacionCFDResponse value) {
        this.procesaPeticionCancelacionCFDResponse = value;
    }

}
