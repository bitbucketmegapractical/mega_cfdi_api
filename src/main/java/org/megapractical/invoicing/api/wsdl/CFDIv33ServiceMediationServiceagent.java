package org.megapractical.invoicing.api.wsdl;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.3.0
 * 2019-07-17T14:02:12.969-05:00
 * Generated source version: 3.3.0
 *
 */
@WebServiceClient(name = "CFDIv33_service_mediation.serviceagent",
                  wsdlLocation = "https://servicios-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl",
                  targetNamespace = "http://xmlns.megacfdi.com/1499759600080")
public class CFDIv33ServiceMediationServiceagent extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://xmlns.megacfdi.com/1499759600080", "CFDIv33_service_mediation.serviceagent");
    public final static QName PortTypeCFDIv33Endpoint1 = new QName("http://xmlns.megacfdi.com/1499759600080", "PortTypeCFDIv33Endpoint1");
    static {
        URL url = null;
        try {
            url = new URL("http://servicios01-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(CFDIv33ServiceMediationServiceagent.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://servicios01-qa.megacfdi.com:30200/CFDI_v33/StarterProcesses/WebServices/CFDIv33_service_mediation.serviceagent?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public CFDIv33ServiceMediationServiceagent(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public CFDIv33ServiceMediationServiceagent(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CFDIv33ServiceMediationServiceagent() {
        super(WSDL_LOCATION, SERVICE);
    }

    public CFDIv33ServiceMediationServiceagent(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public CFDIv33ServiceMediationServiceagent(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public CFDIv33ServiceMediationServiceagent(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns PortTypeCFDIv33
     */
    @WebEndpoint(name = "PortTypeCFDIv33Endpoint1")
    public PortTypeCFDIv33 getPortTypeCFDIv33Endpoint1() {
        return super.getPort(PortTypeCFDIv33Endpoint1, PortTypeCFDIv33.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortTypeCFDIv33
     */
    @WebEndpoint(name = "PortTypeCFDIv33Endpoint1")
    public PortTypeCFDIv33 getPortTypeCFDIv33Endpoint1(WebServiceFeature... features) {
        return super.getPort(PortTypeCFDIv33Endpoint1, PortTypeCFDIv33.class, features);
    }

}
