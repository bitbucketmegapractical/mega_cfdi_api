
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionMasivaRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionMasivaResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelaRetencionMasivaRequest",
    "cancelaRetencionMasivaResponse"
})
@XmlRootElement(name = "CancelaRetencionMasiva")
public class CancelaRetencionMasiva {

    @XmlElement(name = "CancelaRetencionMasivaRequest")
    protected CancelaRetencionMasivaRequest cancelaRetencionMasivaRequest;
    @XmlElement(name = "CancelaRetencionMasivaResponse")
    protected CancelaRetencionMasivaResponse cancelaRetencionMasivaResponse;

    /**
     * Obtiene el valor de la propiedad cancelaRetencionMasivaRequest.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionMasivaRequest }
     *     
     */
    public CancelaRetencionMasivaRequest getCancelaRetencionMasivaRequest() {
        return cancelaRetencionMasivaRequest;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionMasivaRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionMasivaRequest }
     *     
     */
    public void setCancelaRetencionMasivaRequest(CancelaRetencionMasivaRequest value) {
        this.cancelaRetencionMasivaRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaRetencionMasivaResponse.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionMasivaResponse }
     *     
     */
    public CancelaRetencionMasivaResponse getCancelaRetencionMasivaResponse() {
        return cancelaRetencionMasivaResponse;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionMasivaResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionMasivaResponse }
     *     
     */
    public void setCancelaRetencionMasivaResponse(CancelaRetencionMasivaResponse value) {
        this.cancelaRetencionMasivaResponse = value;
    }

}
