
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.megapractical.com/esb/comun/encabezado/1.0}CFDIEncabezadoSOA"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}Operations"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}comprobanteString" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cfdiEncabezadoSOA",
    "operations",
    "comprobanteString"
})
@XmlRootElement(name = "CFDIv33")
public class CFDIv33 {

    @XmlElement(name = "CFDIEncabezadoSOA", namespace = "http://www.megapractical.com/esb/comun/encabezado/1.0", required = true)
    protected CFDIEncabezadoSOA cfdiEncabezadoSOA;
    @XmlElement(name = "Operations", required = true)
    protected Operations operations;
    protected String comprobanteString;

    /**
     * Obtiene el valor de la propiedad cfdiEncabezadoSOA.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoSOA }
     *     
     */
    public CFDIEncabezadoSOA getCFDIEncabezadoSOA() {
        return cfdiEncabezadoSOA;
    }

    /**
     * Define el valor de la propiedad cfdiEncabezadoSOA.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoSOA }
     *     
     */
    public void setCFDIEncabezadoSOA(CFDIEncabezadoSOA value) {
        this.cfdiEncabezadoSOA = value;
    }

    /**
     * Obtiene el valor de la propiedad operations.
     * 
     * @return
     *     possible object is
     *     {@link Operations }
     *     
     */
    public Operations getOperations() {
        return operations;
    }

    /**
     * Define el valor de la propiedad operations.
     * 
     * @param value
     *     allowed object is
     *     {@link Operations }
     *     
     */
    public void setOperations(Operations value) {
        this.operations = value;
    }

    /**
     * Obtiene el valor de la propiedad comprobanteString.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComprobanteString() {
        return comprobanteString;
    }

    /**
     * Define el valor de la propiedad comprobanteString.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComprobanteString(String value) {
        this.comprobanteString = value;
    }

}
