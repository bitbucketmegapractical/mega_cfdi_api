
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}ConsultaPeticionCancelacionCFDRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}ConsultaPeticionCancelacionCFDResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaPeticionCancelacionCFDRequest",
    "consultaPeticionCancelacionCFDResponse"
})
@XmlRootElement(name = "ConsultaPeticionCancelacionCFD")
public class ConsultaPeticionCancelacionCFD {

    @XmlElement(name = "ConsultaPeticionCancelacionCFDRequest")
    protected ConsultaPeticionCancelacionCFDRequest consultaPeticionCancelacionCFDRequest;
    @XmlElement(name = "ConsultaPeticionCancelacionCFDResponse")
    protected ConsultaPeticionCancelacionCFDResponse consultaPeticionCancelacionCFDResponse;

    /**
     * Obtiene el valor de la propiedad consultaPeticionCancelacionCFDRequest.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPeticionCancelacionCFDRequest }
     *     
     */
    public ConsultaPeticionCancelacionCFDRequest getConsultaPeticionCancelacionCFDRequest() {
        return consultaPeticionCancelacionCFDRequest;
    }

    /**
     * Define el valor de la propiedad consultaPeticionCancelacionCFDRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPeticionCancelacionCFDRequest }
     *     
     */
    public void setConsultaPeticionCancelacionCFDRequest(ConsultaPeticionCancelacionCFDRequest value) {
        this.consultaPeticionCancelacionCFDRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaPeticionCancelacionCFDResponse.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPeticionCancelacionCFDResponse }
     *     
     */
    public ConsultaPeticionCancelacionCFDResponse getConsultaPeticionCancelacionCFDResponse() {
        return consultaPeticionCancelacionCFDResponse;
    }

    /**
     * Define el valor de la propiedad consultaPeticionCancelacionCFDResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPeticionCancelacionCFDResponse }
     *     
     */
    public void setConsultaPeticionCancelacionCFDResponse(ConsultaPeticionCancelacionCFDResponse value) {
        this.consultaPeticionCancelacionCFDResponse = value;
    }

}
