
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}keyPassword"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}certByte"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}keyByte"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}comprobanteCFDIin"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "keyPassword",
    "certByte",
    "keyByte",
    "comprobanteCFDIin"
})
@XmlRootElement(name = "ComprobanteRequest")
public class ComprobanteRequest {

    @XmlElement(required = true)
    protected String keyPassword;
    @XmlElement(required = true)
    protected String certByte;
    @XmlElement(required = true)
    protected String keyByte;
    @XmlElement(required = true)
    protected String comprobanteCFDIin;
    @XmlElement(required = true)
    protected byte[] certByteArr;
    @XmlElement(required = true)
    protected byte[] keyByteArr;

    /**
     * Obtiene el valor de la propiedad keyPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyPassword() {
        return keyPassword;
    }

    /**
     * Define el valor de la propiedad keyPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyPassword(String value) {
        this.keyPassword = value;
    }

    /**
     * Obtiene el valor de la propiedad certByte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertByte() {
        return certByte;
    }

    /**
     * Define el valor de la propiedad certByte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertByte(String value) {
        this.certByte = value;
    }

    /**
     * Obtiene el valor de la propiedad keyByte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyByte() {
        return keyByte;
    }

    /**
     * Define el valor de la propiedad keyByte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyByte(String value) {
        this.keyByte = value;
    }

    /**
     * Obtiene el valor de la propiedad comprobanteCFDIin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    
    public String getComprobanteCFDIin() {
        return comprobanteCFDIin;
    }

    /**
     * Define el valor de la propiedad comprobanteCFDIin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    
    public void setComprobanteCFDIin(String value) {
        this.comprobanteCFDIin = value;
    }
    
    /**
     * Gets the value of the Cer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public byte[] getCerByteArr() {
        return certByteArr;
    }

     /**
     * Sets the value of the Cer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCerByteArr(byte[] value) {
        this.certByteArr = value;
    }
    
     /**
     * Gets the value of the Key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public byte[] getKeyByteArr() {
        return keyByteArr;
    }

     /**
     * Sets the value of the Key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyByteArr(byte[] value) {
        this.keyByteArr = value;
    }
}
