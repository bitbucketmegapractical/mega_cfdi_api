
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}Referencia" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}IDCorrelacion" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}NombreOperacion"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}VersionOperacion"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}SistemaOrigen"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}Sesion" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "referencia",
    "idCorrelacion",
    "nombreOperacion",
    "versionOperacion",
    "sistemaOrigen",
    "sesion"
})
@XmlRootElement(name = "HeaderRq", namespace = "http://www.mcfdi.com/services/common/1.0")
public class HeaderRq {

    @XmlElement(name = "Referencia", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
    protected String referencia;
    @XmlElement(name = "IDCorrelacion", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
    protected String idCorrelacion;
    @XmlElement(name = "NombreOperacion", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String nombreOperacion;
    @XmlElement(name = "VersionOperacion", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String versionOperacion;
    @XmlElement(name = "SistemaOrigen", namespace = "http://www.mcfdi.com/schemas/common/header/1.0", required = true)
    protected String sistemaOrigen;
    @XmlElement(name = "Sesion", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
    protected String sesion;

    /**
     * Obtiene el valor de la propiedad referencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Define el valor de la propiedad referencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

    /**
     * Obtiene el valor de la propiedad idCorrelacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCorrelacion() {
        return idCorrelacion;
    }

    /**
     * Define el valor de la propiedad idCorrelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCorrelacion(String value) {
        this.idCorrelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOperacion() {
        return nombreOperacion;
    }

    /**
     * Define el valor de la propiedad nombreOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOperacion(String value) {
        this.nombreOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad versionOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionOperacion() {
        return versionOperacion;
    }

    /**
     * Define el valor de la propiedad versionOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionOperacion(String value) {
        this.versionOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad sistemaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    /**
     * Define el valor de la propiedad sistemaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaOrigen(String value) {
        this.sistemaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad sesion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSesion() {
        return sesion;
    }

    /**
     * Define el valor de la propiedad sesion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSesion(String value) {
        this.sesion = value;
    }

}
