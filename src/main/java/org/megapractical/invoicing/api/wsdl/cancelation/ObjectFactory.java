
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.megapractical.invoicing.api.wsdl.cancelation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CodigoRespuesta_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "CodigoRespuesta");
    private final static QName _ResultadoOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "ResultadoOperacion");
    private final static QName _TipoResultadoOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "TipoResultadoOperacion");
    private final static QName _SistemaDestino_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "SistemaDestino");
    private final static QName _MensajeRespuesta_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "MensajeRespuesta");
    private final static QName _TrazaRespuesta_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "TrazaRespuesta");
    private final static QName _DireccionIP_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "DireccionIP");
    private final static QName _FechaTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "FechaTransaccion");
    private final static QName _HoraTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "HoraTransaccion");
    private final static QName _IDCorrelacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "IDCorrelacion");
    private final static QName _IDTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "IDTransaccion");
    private final static QName _NombreOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "NombreOperacion");
    private final static QName _NombreServicio_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "NombreServicio");
    private final static QName _Referencia_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "Referencia");
    private final static QName _ResultadoTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "ResultadoTransaccion");
    private final static QName _Sesion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "Sesion");
    private final static QName _SistemaOrigen_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "SistemaOrigen");
    private final static QName _TipoResultadoTransaccion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "TipoResultadoTransaccion");
    private final static QName _VersionOperacion_QNAME = new QName("http://www.mcfdi.com/schemas/common/header/1.0", "VersionOperacion");
    private final static QName _CertificadoDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "CertificadoDatos");
    private final static QName _LlaveDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "LlaveDatos");
    private final static QName _ClaveLlave_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "ClaveLlave");
    private final static QName _RFCEmisor_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RFCEmisor");
    private final static QName _UUID_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "UUID");
    private final static QName _RFCReceptor_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RFCReceptor");
    private final static QName _TotalComprobante_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "TotalComprobante");
    private final static QName _Fecha_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Fecha");
    private final static QName _StatusCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "StatusCFD");
    private final static QName _CodigoStatusCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "CodigoStatusCFD");
    private final static QName _IndicadorCancelableCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "IndicadorCancelableCFD");
    private final static QName _StatusCancelacion_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "StatusCancelacion");
    private final static QName _AcuseDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "AcuseDatos");
    private final static QName _IDInterno_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", "IDInterno");
    private final static QName _Status_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Status");
    private final static QName _TipoCFD_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "TipoCFD");
    private final static QName _Version_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Version");
    private final static QName _FechaTimbrado_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "FechaTimbrado");
    private final static QName _FechaEnvio_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "FechaEnvio");
    private final static QName _FechaCancelacion_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "FechaCancelacion");
    private final static QName _Hash_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Hash");
    private final static QName _Comprobante_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Comprobante");
    private final static QName _StatusMessage_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", "_StatusMessage");
    private final static QName _IsEnable_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", "_IsEnable");
    private final static QName _IsDeteled_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", "_IsDeteled");
    private final static QName _AcuseCancelacionDatos_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "AcuseCancelacionDatos");
    private final static QName _Codigo_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Codigo");
    private final static QName _RFCPACEnviaSolicitud_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RFCPACEnviaSolicitud");
    private final static QName _RespuestaSolicitud_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "RespuestaSolicitud");
    private final static QName _Resultado_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Resultado");
    private final static QName _Token_QNAME = new QName("http://www.mcfdi.com/schemas/cfdi/sat/1.0", "Token");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.megapractical.invoicing.api.wsdl.cancelation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DetalleRespuesta }
     * 
     */
    public DetalleRespuesta createDetalleRespuesta() {
        return new DetalleRespuesta();
    }

    /**
     * Create an instance of {@link ListaDetalleRespuesta }
     * 
     */
    public ListaDetalleRespuesta createListaDetalleRespuesta() {
        return new ListaDetalleRespuesta();
    }

    /**
     * Create an instance of {@link CancelaCFD }
     * 
     */
    public CancelaCFD createCancelaCFD() {
        return new CancelaCFD();
    }

    /**
     * Create an instance of {@link CancelaCFDRequest }
     * 
     */
    public CancelaCFDRequest createCancelaCFDRequest() {
        return new CancelaCFDRequest();
    }

    /**
     * Create an instance of {@link HeaderRq }
     * 
     */
    public HeaderRq createHeaderRq() {
        return new HeaderRq();
    }

    /**
     * Create an instance of {@link CancelaCFDRq }
     * 
     */
    public CancelaCFDRq createCancelaCFDRq() {
        return new CancelaCFDRq();
    }

    /**
     * Create an instance of {@link CancelaCFDPeticion }
     * 
     */
    public CancelaCFDPeticion createCancelaCFDPeticion() {
        return new CancelaCFDPeticion();
    }

    /**
     * Create an instance of {@link CancelaCFDResponse }
     * 
     */
    public CancelaCFDResponse createCancelaCFDResponse() {
        return new CancelaCFDResponse();
    }

    /**
     * Create an instance of {@link HeaderRs }
     * 
     */
    public HeaderRs createHeaderRs() {
        return new HeaderRs();
    }

    /**
     * Create an instance of {@link CancelaCFDRs }
     * 
     */
    public CancelaCFDRs createCancelaCFDRs() {
        return new CancelaCFDRs();
    }

    /**
     * Create an instance of {@link CancelaCFDResultado }
     * 
     */
    public CancelaCFDResultado createCancelaCFDResultado() {
        return new CancelaCFDResultado();
    }

    /**
     * Create an instance of {@link ConsultaSatusCFDResultado }
     * 
     */
    public ConsultaSatusCFDResultado createConsultaSatusCFDResultado() {
        return new ConsultaSatusCFDResultado();
    }

    /**
     * Create an instance of {@link Cancelacion }
     * 
     */
    public Cancelacion createCancelacion() {
        return new Cancelacion();
    }

    /**
     * Create an instance of {@link ConsultaPeticionCancelacionCFD }
     * 
     */
    public ConsultaPeticionCancelacionCFD createConsultaPeticionCancelacionCFD() {
        return new ConsultaPeticionCancelacionCFD();
    }

    /**
     * Create an instance of {@link ConsultaPeticionCancelacionCFDRequest }
     * 
     */
    public ConsultaPeticionCancelacionCFDRequest createConsultaPeticionCancelacionCFDRequest() {
        return new ConsultaPeticionCancelacionCFDRequest();
    }

    /**
     * Create an instance of {@link ConsultaPeticionCancelacionCFDRq }
     * 
     */
    public ConsultaPeticionCancelacionCFDRq createConsultaPeticionCancelacionCFDRq() {
        return new ConsultaPeticionCancelacionCFDRq();
    }

    /**
     * Create an instance of {@link ConsultaPeticionCancelacionCFDResponse }
     * 
     */
    public ConsultaPeticionCancelacionCFDResponse createConsultaPeticionCancelacionCFDResponse() {
        return new ConsultaPeticionCancelacionCFDResponse();
    }

    /**
     * Create an instance of {@link ConsultaPeticionCancelacionCFDRs }
     * 
     */
    public ConsultaPeticionCancelacionCFDRs createConsultaPeticionCancelacionCFDRs() {
        return new ConsultaPeticionCancelacionCFDRs();
    }

    /**
     * Create an instance of {@link ConsultaPeticionCancelacionResultado }
     * 
     */
    public ConsultaPeticionCancelacionResultado createConsultaPeticionCancelacionResultado() {
        return new ConsultaPeticionCancelacionResultado();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFD }
     * 
     */
    public ConsultaStatusCFD createConsultaStatusCFD() {
        return new ConsultaStatusCFD();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDRequest }
     * 
     */
    public ConsultaStatusCFDRequest createConsultaStatusCFDRequest() {
        return new ConsultaStatusCFDRequest();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDRq }
     * 
     */
    public ConsultaStatusCFDRq createConsultaStatusCFDRq() {
        return new ConsultaStatusCFDRq();
    }

    /**
     * Create an instance of {@link ConsultaCFDPeticion }
     * 
     */
    public ConsultaCFDPeticion createConsultaCFDPeticion() {
        return new ConsultaCFDPeticion();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDResponse }
     * 
     */
    public ConsultaStatusCFDResponse createConsultaStatusCFDResponse() {
        return new ConsultaStatusCFDResponse();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDRs }
     * 
     */
    public ConsultaStatusCFDRs createConsultaStatusCFDRs() {
        return new ConsultaStatusCFDRs();
    }

    /**
     * Create an instance of {@link ConsultaStatusCFDResultado }
     * 
     */
    public ConsultaStatusCFDResultado createConsultaStatusCFDResultado() {
        return new ConsultaStatusCFDResultado();
    }

    /**
     * Create an instance of {@link ProcesaPeticionCancelacionCFD }
     * 
     */
    public ProcesaPeticionCancelacionCFD createProcesaPeticionCancelacionCFD() {
        return new ProcesaPeticionCancelacionCFD();
    }

    /**
     * Create an instance of {@link ProcesaPeticionCancelacionCFDRequest }
     * 
     */
    public ProcesaPeticionCancelacionCFDRequest createProcesaPeticionCancelacionCFDRequest() {
        return new ProcesaPeticionCancelacionCFDRequest();
    }

    /**
     * Create an instance of {@link ProcesaPeticionCancelacionCFDRq }
     * 
     */
    public ProcesaPeticionCancelacionCFDRq createProcesaPeticionCancelacionCFDRq() {
        return new ProcesaPeticionCancelacionCFDRq();
    }

    /**
     * Create an instance of {@link ProcesaPeticionCancelacionCFDResponse }
     * 
     */
    public ProcesaPeticionCancelacionCFDResponse createProcesaPeticionCancelacionCFDResponse() {
        return new ProcesaPeticionCancelacionCFDResponse();
    }

    /**
     * Create an instance of {@link ProcesaPeticionCancelacionCFDRs }
     * 
     */
    public ProcesaPeticionCancelacionCFDRs createProcesaPeticionCancelacionCFDRs() {
        return new ProcesaPeticionCancelacionCFDRs();
    }

    /**
     * Create an instance of {@link Undefined }
     * 
     */
    public Undefined createUndefined() {
        return new Undefined();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "CodigoRespuesta")
    public JAXBElement<String> createCodigoRespuesta(String value) {
        return new JAXBElement<String>(_CodigoRespuesta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "ResultadoOperacion")
    public JAXBElement<Integer> createResultadoOperacion(Integer value) {
        return new JAXBElement<Integer>(_ResultadoOperacion_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "TipoResultadoOperacion")
    public JAXBElement<String> createTipoResultadoOperacion(String value) {
        return new JAXBElement<String>(_TipoResultadoOperacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "SistemaDestino")
    public JAXBElement<String> createSistemaDestino(String value) {
        return new JAXBElement<String>(_SistemaDestino_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "MensajeRespuesta")
    public JAXBElement<String> createMensajeRespuesta(String value) {
        return new JAXBElement<String>(_MensajeRespuesta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "TrazaRespuesta")
    public JAXBElement<String> createTrazaRespuesta(String value) {
        return new JAXBElement<String>(_TrazaRespuesta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "DireccionIP")
    public JAXBElement<String> createDireccionIP(String value) {
        return new JAXBElement<String>(_DireccionIP_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "FechaTransaccion")
    public JAXBElement<XMLGregorianCalendar> createFechaTransaccion(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FechaTransaccion_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "HoraTransaccion")
    public JAXBElement<XMLGregorianCalendar> createHoraTransaccion(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_HoraTransaccion_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "IDCorrelacion")
    public JAXBElement<String> createIDCorrelacion(String value) {
        return new JAXBElement<String>(_IDCorrelacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "IDTransaccion")
    public JAXBElement<String> createIDTransaccion(String value) {
        return new JAXBElement<String>(_IDTransaccion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "NombreOperacion")
    public JAXBElement<String> createNombreOperacion(String value) {
        return new JAXBElement<String>(_NombreOperacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "NombreServicio")
    public JAXBElement<String> createNombreServicio(String value) {
        return new JAXBElement<String>(_NombreServicio_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "Referencia")
    public JAXBElement<String> createReferencia(String value) {
        return new JAXBElement<String>(_Referencia_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "ResultadoTransaccion")
    public JAXBElement<Integer> createResultadoTransaccion(Integer value) {
        return new JAXBElement<Integer>(_ResultadoTransaccion_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "Sesion")
    public JAXBElement<String> createSesion(String value) {
        return new JAXBElement<String>(_Sesion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "SistemaOrigen")
    public JAXBElement<String> createSistemaOrigen(String value) {
        return new JAXBElement<String>(_SistemaOrigen_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "TipoResultadoTransaccion")
    public JAXBElement<String> createTipoResultadoTransaccion(String value) {
        return new JAXBElement<String>(_TipoResultadoTransaccion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/common/header/1.0", name = "VersionOperacion")
    public JAXBElement<String> createVersionOperacion(String value) {
        return new JAXBElement<String>(_VersionOperacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "CertificadoDatos")
    public JAXBElement<byte[]> createCertificadoDatos(byte[] value) {
        return new JAXBElement<byte[]>(_CertificadoDatos_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "LlaveDatos")
    public JAXBElement<byte[]> createLlaveDatos(byte[] value) {
        return new JAXBElement<byte[]>(_LlaveDatos_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "ClaveLlave")
    public JAXBElement<String> createClaveLlave(String value) {
        return new JAXBElement<String>(_ClaveLlave_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RFCEmisor")
    public JAXBElement<String> createRFCEmisor(String value) {
        return new JAXBElement<String>(_RFCEmisor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "UUID")
    public JAXBElement<String> createUUID(String value) {
        return new JAXBElement<String>(_UUID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RFCReceptor")
    public JAXBElement<String> createRFCReceptor(String value) {
        return new JAXBElement<String>(_RFCReceptor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "TotalComprobante")
    public JAXBElement<String> createTotalComprobante(String value) {
        return new JAXBElement<String>(_TotalComprobante_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Fecha")
    public JAXBElement<String> createFecha(String value) {
        return new JAXBElement<String>(_Fecha_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "StatusCFD")
    public JAXBElement<String> createStatusCFD(String value) {
        return new JAXBElement<String>(_StatusCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "CodigoStatusCFD")
    public JAXBElement<String> createCodigoStatusCFD(String value) {
        return new JAXBElement<String>(_CodigoStatusCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "IndicadorCancelableCFD")
    public JAXBElement<String> createIndicadorCancelableCFD(String value) {
        return new JAXBElement<String>(_IndicadorCancelableCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "StatusCancelacion")
    public JAXBElement<String> createStatusCancelacion(String value) {
        return new JAXBElement<String>(_StatusCancelacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "AcuseDatos")
    public JAXBElement<String> createAcuseDatos(String value) {
        return new JAXBElement<String>(_AcuseDatos_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", name = "IDInterno")
    public JAXBElement<String> createIDInterno(String value) {
        return new JAXBElement<String>(_IDInterno_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Status")
    public JAXBElement<String> createStatus(String value) {
        return new JAXBElement<String>(_Status_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "TipoCFD")
    public JAXBElement<String> createTipoCFD(String value) {
        return new JAXBElement<String>(_TipoCFD_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Version")
    public JAXBElement<String> createVersion(String value) {
        return new JAXBElement<String>(_Version_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "FechaTimbrado")
    public JAXBElement<String> createFechaTimbrado(String value) {
        return new JAXBElement<String>(_FechaTimbrado_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "FechaEnvio")
    public JAXBElement<String> createFechaEnvio(String value) {
        return new JAXBElement<String>(_FechaEnvio_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "FechaCancelacion")
    public JAXBElement<String> createFechaCancelacion(String value) {
        return new JAXBElement<String>(_FechaCancelacion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Hash")
    public JAXBElement<String> createHash(String value) {
        return new JAXBElement<String>(_Hash_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Comprobante")
    public JAXBElement<String> createComprobante(String value) {
        return new JAXBElement<String>(_Comprobante_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", name = "_StatusMessage")
    public JAXBElement<String> createStatusMessage(String value) {
        return new JAXBElement<String>(_StatusMessage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", name = "_IsEnable")
    public JAXBElement<String> createIsEnable(String value) {
        return new JAXBElement<String>(_IsEnable_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", name = "_IsDeteled")
    public JAXBElement<String> createIsDeteled(String value) {
        return new JAXBElement<String>(_IsDeteled_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "AcuseCancelacionDatos")
    public JAXBElement<String> createAcuseCancelacionDatos(String value) {
        return new JAXBElement<String>(_AcuseCancelacionDatos_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Codigo")
    public JAXBElement<String> createCodigo(String value) {
        return new JAXBElement<String>(_Codigo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RFCPACEnviaSolicitud")
    public JAXBElement<String> createRFCPACEnviaSolicitud(String value) {
        return new JAXBElement<String>(_RFCPACEnviaSolicitud_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "RespuestaSolicitud")
    public JAXBElement<String> createRespuestaSolicitud(String value) {
        return new JAXBElement<String>(_RespuestaSolicitud_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Resultado")
    public JAXBElement<String> createResultado(String value) {
        return new JAXBElement<String>(_Resultado_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", name = "Token")
    public JAXBElement<String> createToken(String value) {
        return new JAXBElement<String>(_Token_QNAME, String.class, null, value);
    }

}
