
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/common/header/1.0}DetalleRespuesta" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "detalleRespuesta"
})
@XmlRootElement(name = "ListaDetalleRespuesta", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
public class ListaDetalleRespuesta {

    @XmlElement(name = "DetalleRespuesta", namespace = "http://www.mcfdi.com/schemas/common/header/1.0")
    protected List<DetalleRespuesta> detalleRespuesta;

    /**
     * Gets the value of the detalleRespuesta property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the detalleRespuesta property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDetalleRespuesta().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DetalleRespuesta }
     * 
     * 
     */
    public List<DetalleRespuesta> getDetalleRespuesta() {
        if (detalleRespuesta == null) {
            detalleRespuesta = new ArrayList<DetalleRespuesta>();
        }
        return this.detalleRespuesta;
    }

}
