
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0}IDInterno"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}UUID"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Status" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}TipoCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Version" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}FechaTimbrado" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}FechaEnvio" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}FechaCancelacion" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCEmisor"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Hash" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Comprobante" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0}_StatusMessage" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0}_IsEnable" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0}_IsDeteled" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}CodigoStatusCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}IndicadorCancelableCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}StatusCFD" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}StatusCancelacion" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}AcuseCancelacionDatos" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idInterno",
    "uuid",
    "status",
    "tipoCFD",
    "version",
    "fechaTimbrado",
    "fechaEnvio",
    "fechaCancelacion",
    "rfcEmisor",
    "hash",
    "comprobante",
    "statusMessage",
    "isEnable",
    "isDeteled",
    "codigoStatusCFD",
    "indicadorCancelableCFD",
    "statusCFD",
    "statusCancelacion",
    "acuseCancelacionDatos"
})
@XmlRootElement(name = "ConsultaStatusCFDResultado")
public class ConsultaStatusCFDResultado {

    @XmlElement(name = "IDInterno", namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", required = true)
    protected String idInterno;
    @XmlElement(name = "UUID", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String uuid;
    @XmlElement(name = "Status", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String status;
    @XmlElement(name = "TipoCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String tipoCFD;
    @XmlElement(name = "Version", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String version;
    @XmlElement(name = "FechaTimbrado", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String fechaTimbrado;
    @XmlElement(name = "FechaEnvio", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String fechaEnvio;
    @XmlElement(name = "FechaCancelacion", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String fechaCancelacion;
    @XmlElement(name = "RFCEmisor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcEmisor;
    @XmlElement(name = "Hash", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String hash;
    @XmlElement(name = "Comprobante", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String comprobante;
    @XmlElement(name = "_StatusMessage", namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0")
    protected String statusMessage;
    @XmlElement(name = "_IsEnable", namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0")
    protected String isEnable;
    @XmlElement(name = "_IsDeteled", namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0")
    protected String isDeteled;
    @XmlElement(name = "CodigoStatusCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String codigoStatusCFD;
    @XmlElement(name = "IndicadorCancelableCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String indicadorCancelableCFD;
    @XmlElement(name = "StatusCFD", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String statusCFD;
    @XmlElement(name = "StatusCancelacion", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String statusCancelacion;
    @XmlElement(name = "AcuseCancelacionDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0")
    protected String acuseCancelacionDatos;

    /**
     * Obtiene el valor de la propiedad idInterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDInterno() {
        return idInterno;
    }

    /**
     * Define el valor de la propiedad idInterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDInterno(String value) {
        this.idInterno = value;
    }

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUID(String value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCFD() {
        return tipoCFD;
    }

    /**
     * Define el valor de la propiedad tipoCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCFD(String value) {
        this.tipoCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaTimbrado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaTimbrado() {
        return fechaTimbrado;
    }

    /**
     * Define el valor de la propiedad fechaTimbrado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaTimbrado(String value) {
        this.fechaTimbrado = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaEnvio() {
        return fechaEnvio;
    }

    /**
     * Define el valor de la propiedad fechaEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaEnvio(String value) {
        this.fechaEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCancelacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCancelacion() {
        return fechaCancelacion;
    }

    /**
     * Define el valor de la propiedad fechaCancelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCancelacion(String value) {
        this.fechaCancelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCEmisor() {
        return rfcEmisor;
    }

    /**
     * Define el valor de la propiedad rfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCEmisor(String value) {
        this.rfcEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad hash.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHash() {
        return hash;
    }

    /**
     * Define el valor de la propiedad hash.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHash(String value) {
        this.hash = value;
    }

    /**
     * Obtiene el valor de la propiedad comprobante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComprobante() {
        return comprobante;
    }

    /**
     * Define el valor de la propiedad comprobante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComprobante(String value) {
        this.comprobante = value;
    }

    /**
     * Obtiene el valor de la propiedad statusMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Define el valor de la propiedad statusMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusMessage(String value) {
        this.statusMessage = value;
    }

    /**
     * Obtiene el valor de la propiedad isEnable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsEnable() {
        return isEnable;
    }

    /**
     * Define el valor de la propiedad isEnable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsEnable(String value) {
        this.isEnable = value;
    }

    /**
     * Obtiene el valor de la propiedad isDeteled.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDeteled() {
        return isDeteled;
    }

    /**
     * Define el valor de la propiedad isDeteled.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDeteled(String value) {
        this.isDeteled = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoStatusCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoStatusCFD() {
        return codigoStatusCFD;
    }

    /**
     * Define el valor de la propiedad codigoStatusCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoStatusCFD(String value) {
        this.codigoStatusCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad indicadorCancelableCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorCancelableCFD() {
        return indicadorCancelableCFD;
    }

    /**
     * Define el valor de la propiedad indicadorCancelableCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorCancelableCFD(String value) {
        this.indicadorCancelableCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad statusCFD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCFD() {
        return statusCFD;
    }

    /**
     * Define el valor de la propiedad statusCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCFD(String value) {
        this.statusCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad statusCancelacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCancelacion() {
        return statusCancelacion;
    }

    /**
     * Define el valor de la propiedad statusCancelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCancelacion(String value) {
        this.statusCancelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad acuseCancelacionDatos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcuseCancelacionDatos() {
        return acuseCancelacionDatos;
    }

    /**
     * Define el valor de la propiedad acuseCancelacionDatos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcuseCancelacionDatos(String value) {
        this.acuseCancelacionDatos = value;
    }

}
