
package org.megapractical.invoicing.api.wsdl.retention;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorrelationID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TransactionID">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="36"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Resultado">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResultadoOperacion">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *                         &lt;enumeration value="1"/>
 *                         &lt;enumeration value="2"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="SistemaRespuesta" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="64"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="MensajeRespuesta" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="256"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="CodigoRespuesta" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="32"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FechaTransaccion" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="HoraTransaccion" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *         &lt;element name="NombreOperacion">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="40"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VersionOperacion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DireccionIP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SistemaOrigen">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Sesion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="256"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IDCliente" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "correlationID",
    "transactionID",
    "resultado",
    "fechaTransaccion",
    "horaTransaccion",
    "nombreOperacion",
    "versionOperacion",
    "direccionIP",
    "sistemaOrigen",
    "sesion",
    "idCliente"
})
@XmlRootElement(name = "CFDIEncabezadoRespuesta")
public class CFDIEncabezadoRespuesta
    implements Serializable
{

    @XmlElement(name = "CorrelationID")
    protected String correlationID;
    @XmlElement(name = "TransactionID", required = true)
    protected String transactionID;
    @XmlElement(name = "Resultado", required = true)
    protected CFDIEncabezadoRespuesta.Resultado resultado;
    @XmlElement(name = "FechaTransaccion", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaTransaccion;
    @XmlElement(name = "HoraTransaccion", required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar horaTransaccion;
    @XmlElement(name = "NombreOperacion", required = true)
    protected String nombreOperacion;
    @XmlElement(name = "VersionOperacion")
    protected String versionOperacion;
    @XmlElement(name = "DireccionIP")
    protected String direccionIP;
    @XmlElement(name = "SistemaOrigen", required = true)
    protected String sistemaOrigen;
    @XmlElement(name = "Sesion")
    protected String sesion;
    @XmlElement(name = "IDCliente")
    protected String idCliente;

    /**
     * Gets the value of the correlationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Sets the value of the correlationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the resultado property.
     * 
     * @return
     *     possible object is
     *     {@link CFDIEncabezadoRespuesta.Resultado }
     *     
     */
    public CFDIEncabezadoRespuesta.Resultado getResultado() {
        return resultado;
    }

    /**
     * Sets the value of the resultado property.
     * 
     * @param value
     *     allowed object is
     *     {@link CFDIEncabezadoRespuesta.Resultado }
     *     
     */
    public void setResultado(CFDIEncabezadoRespuesta.Resultado value) {
        this.resultado = value;
    }

    /**
     * Gets the value of the fechaTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * Sets the value of the fechaTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaTransaccion(XMLGregorianCalendar value) {
        this.fechaTransaccion = value;
    }

    /**
     * Gets the value of the horaTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHoraTransaccion() {
        return horaTransaccion;
    }

    /**
     * Sets the value of the horaTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHoraTransaccion(XMLGregorianCalendar value) {
        this.horaTransaccion = value;
    }

    /**
     * Gets the value of the nombreOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreOperacion() {
        return nombreOperacion;
    }

    /**
     * Sets the value of the nombreOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreOperacion(String value) {
        this.nombreOperacion = value;
    }

    /**
     * Gets the value of the versionOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionOperacion() {
        return versionOperacion;
    }

    /**
     * Sets the value of the versionOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionOperacion(String value) {
        this.versionOperacion = value;
    }

    /**
     * Gets the value of the direccionIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionIP() {
        return direccionIP;
    }

    /**
     * Sets the value of the direccionIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionIP(String value) {
        this.direccionIP = value;
    }

    /**
     * Gets the value of the sistemaOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    /**
     * Sets the value of the sistemaOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaOrigen(String value) {
        this.sistemaOrigen = value;
    }

    /**
     * Gets the value of the sesion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSesion() {
        return sesion;
    }

    /**
     * Sets the value of the sesion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSesion(String value) {
        this.sesion = value;
    }

    /**
     * Gets the value of the idCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCliente() {
        return idCliente;
    }

    /**
     * Sets the value of the idCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCliente(String value) {
        this.idCliente = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResultadoOperacion">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
     *               &lt;enumeration value="1"/>
     *               &lt;enumeration value="2"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="SistemaRespuesta" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="64"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="MensajeRespuesta" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="256"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="CodigoRespuesta" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="32"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultadoOperacion",
        "sistemaRespuesta",
        "mensajeRespuesta",
        "codigoRespuesta"
    })
    public static class Resultado
        implements Serializable
    {

        @XmlElement(name = "ResultadoOperacion")
        protected short resultadoOperacion;
        @XmlElement(name = "SistemaRespuesta")
        protected String sistemaRespuesta;
        @XmlElement(name = "MensajeRespuesta")
        protected String mensajeRespuesta;
        @XmlElement(name = "CodigoRespuesta")
        protected String codigoRespuesta;

        /**
         * Gets the value of the resultadoOperacion property.
         * 
         */
        public short getResultadoOperacion() {
            return resultadoOperacion;
        }

        /**
         * Sets the value of the resultadoOperacion property.
         * 
         */
        public void setResultadoOperacion(short value) {
            this.resultadoOperacion = value;
        }

        /**
         * Gets the value of the sistemaRespuesta property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSistemaRespuesta() {
            return sistemaRespuesta;
        }

        /**
         * Sets the value of the sistemaRespuesta property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSistemaRespuesta(String value) {
            this.sistemaRespuesta = value;
        }

        /**
         * Gets the value of the mensajeRespuesta property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMensajeRespuesta() {
            return mensajeRespuesta;
        }

        /**
         * Sets the value of the mensajeRespuesta property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMensajeRespuesta(String value) {
            this.mensajeRespuesta = value;
        }

        /**
         * Gets the value of the codigoRespuesta property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoRespuesta() {
            return codigoRespuesta;
        }

        /**
         * Sets the value of the codigoRespuesta property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoRespuesta(String value) {
            this.codigoRespuesta = value;
        }

    }

}
