
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/common/1.0}HeaderRq"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaEnvioSATRq"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRq",
    "autenticaEnvioSATRq"
})
@XmlRootElement(name = "AutenticaEnvioSATRequest")
public class AutenticaEnvioSATRequest {

    @XmlElement(name = "HeaderRq", namespace = "http://www.mcfdi.com/services/common/1.0", required = true)
    protected HeaderRq headerRq;
    @XmlElement(name = "AutenticaEnvioSATRq", required = true)
    protected AutenticaEnvioSATRq autenticaEnvioSATRq;

    /**
     * Obtiene el valor de la propiedad headerRq.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRq }
     *     
     */
    public HeaderRq getHeaderRq() {
        return headerRq;
    }

    /**
     * Define el valor de la propiedad headerRq.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRq }
     *     
     */
    public void setHeaderRq(HeaderRq value) {
        this.headerRq = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaEnvioSATRq.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaEnvioSATRq }
     *     
     */
    public AutenticaEnvioSATRq getAutenticaEnvioSATRq() {
        return autenticaEnvioSATRq;
    }

    /**
     * Define el valor de la propiedad autenticaEnvioSATRq.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaEnvioSATRq }
     *     
     */
    public void setAutenticaEnvioSATRq(AutenticaEnvioSATRq value) {
        this.autenticaEnvioSATRq = value;
    }

}
