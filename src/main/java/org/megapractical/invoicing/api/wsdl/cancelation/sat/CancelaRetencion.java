
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelaRetencionRequest",
    "cancelaRetencionResponse"
})
@XmlRootElement(name = "CancelaRetencion")
public class CancelaRetencion {

    @XmlElement(name = "CancelaRetencionRequest")
    protected CancelaRetencionRequest cancelaRetencionRequest;
    @XmlElement(name = "CancelaRetencionResponse")
    protected CancelaRetencionResponse cancelaRetencionResponse;

    /**
     * Obtiene el valor de la propiedad cancelaRetencionRequest.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionRequest }
     *     
     */
    public CancelaRetencionRequest getCancelaRetencionRequest() {
        return cancelaRetencionRequest;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionRequest }
     *     
     */
    public void setCancelaRetencionRequest(CancelaRetencionRequest value) {
        this.cancelaRetencionRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelaRetencionResponse.
     * 
     * @return
     *     possible object is
     *     {@link CancelaRetencionResponse }
     *     
     */
    public CancelaRetencionResponse getCancelaRetencionResponse() {
        return cancelaRetencionResponse;
    }

    /**
     * Define el valor de la propiedad cancelaRetencionResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaRetencionResponse }
     *     
     */
    public void setCancelaRetencionResponse(CancelaRetencionResponse value) {
        this.cancelaRetencionResponse = value;
    }

}
