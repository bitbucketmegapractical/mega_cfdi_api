
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0}IDInterno"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}UUID"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}TotalComprobante"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCEmisor"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCReceptor"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idInterno",
    "uuid",
    "totalComprobante",
    "rfcEmisor",
    "rfcReceptor"
})
@XmlRootElement(name = "ConsultaPeticionCancelacionResultado")
public class ConsultaPeticionCancelacionResultado {

    @XmlElement(name = "IDInterno", namespace = "http://www.mcfdi.com/schemas/cfdi/cancelacion/1.0", required = true)
    protected String idInterno;
    @XmlElement(name = "UUID", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String uuid;
    @XmlElement(name = "TotalComprobante", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String totalComprobante;
    @XmlElement(name = "RFCEmisor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcEmisor;
    @XmlElement(name = "RFCReceptor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcReceptor;

    /**
     * Obtiene el valor de la propiedad idInterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDInterno() {
        return idInterno;
    }

    /**
     * Define el valor de la propiedad idInterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDInterno(String value) {
        this.idInterno = value;
    }

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUID(String value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad totalComprobante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalComprobante() {
        return totalComprobante;
    }

    /**
     * Define el valor de la propiedad totalComprobante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalComprobante(String value) {
        this.totalComprobante = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCEmisor() {
        return rfcEmisor;
    }

    /**
     * Define el valor de la propiedad rfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCEmisor(String value) {
        this.rfcEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcReceptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCReceptor() {
        return rfcReceptor;
    }

    /**
     * Define el valor de la propiedad rfcReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCReceptor(String value) {
        this.rfcReceptor = value;
    }

}
