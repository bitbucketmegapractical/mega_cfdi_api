
package org.megapractical.invoicing.api.wsdl.cancelation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;group ref="{http://www.mcfdi.com/services/cfdi/cancelacion/1.0}Operations"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelaCFD",
    "consultaPeticionCancelacionCFD",
    "consultaStatusCFD",
    "procesaPeticionCancelacionCFD",
    "undefined"
})
@XmlRootElement(name = "Cancelacion")
public class Cancelacion {

    @XmlElement(name = "CancelaCFD")
    protected CancelaCFD cancelaCFD;
    @XmlElement(name = "ConsultaPeticionCancelacionCFD")
    protected ConsultaPeticionCancelacionCFD consultaPeticionCancelacionCFD;
    @XmlElement(name = "ConsultaStatusCFD")
    protected ConsultaStatusCFD consultaStatusCFD;
    @XmlElement(name = "ProcesaPeticionCancelacionCFD")
    protected ProcesaPeticionCancelacionCFD procesaPeticionCancelacionCFD;
    @XmlElement(name = "Undefined")
    protected Undefined undefined;

    /**
     * Obtiene el valor de la propiedad cancelaCFD.
     * 
     * @return
     *     possible object is
     *     {@link CancelaCFD }
     *     
     */
    public CancelaCFD getCancelaCFD() {
        return cancelaCFD;
    }

    /**
     * Define el valor de la propiedad cancelaCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelaCFD }
     *     
     */
    public void setCancelaCFD(CancelaCFD value) {
        this.cancelaCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaPeticionCancelacionCFD.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPeticionCancelacionCFD }
     *     
     */
    public ConsultaPeticionCancelacionCFD getConsultaPeticionCancelacionCFD() {
        return consultaPeticionCancelacionCFD;
    }

    /**
     * Define el valor de la propiedad consultaPeticionCancelacionCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPeticionCancelacionCFD }
     *     
     */
    public void setConsultaPeticionCancelacionCFD(ConsultaPeticionCancelacionCFD value) {
        this.consultaPeticionCancelacionCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad consultaStatusCFD.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaStatusCFD }
     *     
     */
    public ConsultaStatusCFD getConsultaStatusCFD() {
        return consultaStatusCFD;
    }

    /**
     * Define el valor de la propiedad consultaStatusCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaStatusCFD }
     *     
     */
    public void setConsultaStatusCFD(ConsultaStatusCFD value) {
        this.consultaStatusCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad procesaPeticionCancelacionCFD.
     * 
     * @return
     *     possible object is
     *     {@link ProcesaPeticionCancelacionCFD }
     *     
     */
    public ProcesaPeticionCancelacionCFD getProcesaPeticionCancelacionCFD() {
        return procesaPeticionCancelacionCFD;
    }

    /**
     * Define el valor de la propiedad procesaPeticionCancelacionCFD.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcesaPeticionCancelacionCFD }
     *     
     */
    public void setProcesaPeticionCancelacionCFD(ProcesaPeticionCancelacionCFD value) {
        this.procesaPeticionCancelacionCFD = value;
    }

    /**
     * Obtiene el valor de la propiedad undefined.
     * 
     * @return
     *     possible object is
     *     {@link Undefined }
     *     
     */
    public Undefined getUndefined() {
        return undefined;
    }

    /**
     * Define el valor de la propiedad undefined.
     * 
     * @param value
     *     allowed object is
     *     {@link Undefined }
     *     
     */
    public void setUndefined(Undefined value) {
        this.undefined = value;
    }

}
