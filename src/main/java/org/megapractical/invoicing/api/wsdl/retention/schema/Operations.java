
package org.megapractical.invoicing.api.wsdl.retention.schema;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}RetencionesRequest"/>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}Undefined"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retencionesRequest",
    "undefined"
})
@XmlRootElement(name = "Operations")
public class Operations
    implements Serializable
{

    @XmlElement(name = "RetencionesRequest")
    protected RetencionesRequest retencionesRequest;
    @XmlElement(name = "Undefined")
    protected Object undefined;

    /**
     * Gets the value of the retencionesRequest property.
     * 
     * @return
     *     possible object is
     *     {@link RetencionesRequest }
     *     
     */
    public RetencionesRequest getRetencionesRequest() {
        return retencionesRequest;
    }

    /**
     * Sets the value of the retencionesRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetencionesRequest }
     *     
     */
    public void setRetencionesRequest(RetencionesRequest value) {
        this.retencionesRequest = value;
    }

    /**
     * Gets the value of the undefined property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getUndefined() {
        return undefined;
    }

    /**
     * Sets the value of the undefined property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setUndefined(Object value) {
        this.undefined = value;
    }

}
