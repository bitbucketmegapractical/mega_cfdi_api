
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ActualizaPeticionesCancelacionCFDRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}ActualizaPeticionesCancelacionCFDResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actualizaPeticionesCancelacionCFDRequest",
    "actualizaPeticionesCancelacionCFDResponse"
})
@XmlRootElement(name = "ActualizaPeticionesCancelacionCFD")
public class ActualizaPeticionesCancelacionCFD {

    @XmlElement(name = "ActualizaPeticionesCancelacionCFDRequest")
    protected ActualizaPeticionesCancelacionCFDRequest actualizaPeticionesCancelacionCFDRequest;
    @XmlElement(name = "ActualizaPeticionesCancelacionCFDResponse")
    protected ActualizaPeticionesCancelacionCFDResponse actualizaPeticionesCancelacionCFDResponse;

    /**
     * Obtiene el valor de la propiedad actualizaPeticionesCancelacionCFDRequest.
     * 
     * @return
     *     possible object is
     *     {@link ActualizaPeticionesCancelacionCFDRequest }
     *     
     */
    public ActualizaPeticionesCancelacionCFDRequest getActualizaPeticionesCancelacionCFDRequest() {
        return actualizaPeticionesCancelacionCFDRequest;
    }

    /**
     * Define el valor de la propiedad actualizaPeticionesCancelacionCFDRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActualizaPeticionesCancelacionCFDRequest }
     *     
     */
    public void setActualizaPeticionesCancelacionCFDRequest(ActualizaPeticionesCancelacionCFDRequest value) {
        this.actualizaPeticionesCancelacionCFDRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad actualizaPeticionesCancelacionCFDResponse.
     * 
     * @return
     *     possible object is
     *     {@link ActualizaPeticionesCancelacionCFDResponse }
     *     
     */
    public ActualizaPeticionesCancelacionCFDResponse getActualizaPeticionesCancelacionCFDResponse() {
        return actualizaPeticionesCancelacionCFDResponse;
    }

    /**
     * Define el valor de la propiedad actualizaPeticionesCancelacionCFDResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ActualizaPeticionesCancelacionCFDResponse }
     *     
     */
    public void setActualizaPeticionesCancelacionCFDResponse(ActualizaPeticionesCancelacionCFDResponse value) {
        this.actualizaPeticionesCancelacionCFDResponse = value;
    }

}
