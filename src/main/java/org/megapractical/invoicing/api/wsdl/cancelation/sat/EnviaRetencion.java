
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}EnviaRetencionRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}EnviaRetencionResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enviaRetencionRequest",
    "enviaRetencionResponse"
})
@XmlRootElement(name = "EnviaRetencion")
public class EnviaRetencion {

    @XmlElement(name = "EnviaRetencionRequest")
    protected EnviaRetencionRequest enviaRetencionRequest;
    @XmlElement(name = "EnviaRetencionResponse")
    protected EnviaRetencionResponse enviaRetencionResponse;

    /**
     * Obtiene el valor de la propiedad enviaRetencionRequest.
     * 
     * @return
     *     possible object is
     *     {@link EnviaRetencionRequest }
     *     
     */
    public EnviaRetencionRequest getEnviaRetencionRequest() {
        return enviaRetencionRequest;
    }

    /**
     * Define el valor de la propiedad enviaRetencionRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnviaRetencionRequest }
     *     
     */
    public void setEnviaRetencionRequest(EnviaRetencionRequest value) {
        this.enviaRetencionRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad enviaRetencionResponse.
     * 
     * @return
     *     possible object is
     *     {@link EnviaRetencionResponse }
     *     
     */
    public EnviaRetencionResponse getEnviaRetencionResponse() {
        return enviaRetencionResponse;
    }

    /**
     * Define el valor de la propiedad enviaRetencionResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link EnviaRetencionResponse }
     *     
     */
    public void setEnviaRetencionResponse(EnviaRetencionResponse value) {
        this.enviaRetencionResponse = value;
    }

}
