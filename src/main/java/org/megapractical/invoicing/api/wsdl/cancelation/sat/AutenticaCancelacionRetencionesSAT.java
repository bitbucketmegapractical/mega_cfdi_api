
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaCancelacionRetencionesSATRequest"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}AutenticaCancelacionRetencionesSATResponse" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autenticaCancelacionRetencionesSATRequest",
    "autenticaCancelacionRetencionesSATResponse"
})
@XmlRootElement(name = "AutenticaCancelacionRetencionesSAT")
public class AutenticaCancelacionRetencionesSAT {

    @XmlElement(name = "AutenticaCancelacionRetencionesSATRequest")
    protected AutenticaCancelacionRetencionesSATRequest autenticaCancelacionRetencionesSATRequest;
    @XmlElement(name = "AutenticaCancelacionRetencionesSATResponse")
    protected AutenticaCancelacionRetencionesSATResponse autenticaCancelacionRetencionesSATResponse;

    /**
     * Obtiene el valor de la propiedad autenticaCancelacionRetencionesSATRequest.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaCancelacionRetencionesSATRequest }
     *     
     */
    public AutenticaCancelacionRetencionesSATRequest getAutenticaCancelacionRetencionesSATRequest() {
        return autenticaCancelacionRetencionesSATRequest;
    }

    /**
     * Define el valor de la propiedad autenticaCancelacionRetencionesSATRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaCancelacionRetencionesSATRequest }
     *     
     */
    public void setAutenticaCancelacionRetencionesSATRequest(AutenticaCancelacionRetencionesSATRequest value) {
        this.autenticaCancelacionRetencionesSATRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad autenticaCancelacionRetencionesSATResponse.
     * 
     * @return
     *     possible object is
     *     {@link AutenticaCancelacionRetencionesSATResponse }
     *     
     */
    public AutenticaCancelacionRetencionesSATResponse getAutenticaCancelacionRetencionesSATResponse() {
        return autenticaCancelacionRetencionesSATResponse;
    }

    /**
     * Define el valor de la propiedad autenticaCancelacionRetencionesSATResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link AutenticaCancelacionRetencionesSATResponse }
     *     
     */
    public void setAutenticaCancelacionRetencionesSATResponse(AutenticaCancelacionRetencionesSATResponse value) {
        this.autenticaCancelacionRetencionesSATResponse = value;
    }

}
