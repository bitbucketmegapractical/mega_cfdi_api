
package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}CertificadoDatos"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}LlaveDatos"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}ClaveLlave"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}RFCEmisor"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/schemas/cfdi/sat/1.0}Fecha"/&gt;
 *         &lt;element ref="{http://www.mcfdi.com/services/cfdi/sat/1.0}CancelaRetencionMasivaPeticion" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "certificadoDatos",
    "llaveDatos",
    "claveLlave",
    "rfcEmisor",
    "fecha",
    "cancelaRetencionMasivaPeticion"
})
@XmlRootElement(name = "CancelaRetencionMasivaRq")
public class CancelaRetencionMasivaRq {

    @XmlElement(name = "CertificadoDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected byte[] certificadoDatos;
    @XmlElement(name = "LlaveDatos", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected byte[] llaveDatos;
    @XmlElement(name = "ClaveLlave", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String claveLlave;
    @XmlElement(name = "RFCEmisor", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String rfcEmisor;
    @XmlElement(name = "Fecha", namespace = "http://www.mcfdi.com/schemas/cfdi/sat/1.0", required = true)
    protected String fecha;
    @XmlElement(name = "CancelaRetencionMasivaPeticion")
    protected List<CancelaRetencionMasivaPeticion> cancelaRetencionMasivaPeticion;

    /**
     * Obtiene el valor de la propiedad certificadoDatos.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getCertificadoDatos() {
        return certificadoDatos;
    }

    /**
     * Define el valor de la propiedad certificadoDatos.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setCertificadoDatos(byte[] value) {
        this.certificadoDatos = value;
    }

    /**
     * Obtiene el valor de la propiedad llaveDatos.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getLlaveDatos() {
        return llaveDatos;
    }

    /**
     * Define el valor de la propiedad llaveDatos.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setLlaveDatos(byte[] value) {
        this.llaveDatos = value;
    }

    /**
     * Obtiene el valor de la propiedad claveLlave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveLlave() {
        return claveLlave;
    }

    /**
     * Define el valor de la propiedad claveLlave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveLlave(String value) {
        this.claveLlave = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFCEmisor() {
        return rfcEmisor;
    }

    /**
     * Define el valor de la propiedad rfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFCEmisor(String value) {
        this.rfcEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the cancelaRetencionMasivaPeticion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cancelaRetencionMasivaPeticion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCancelaRetencionMasivaPeticion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CancelaRetencionMasivaPeticion }
     * 
     * 
     */
    public List<CancelaRetencionMasivaPeticion> getCancelaRetencionMasivaPeticion() {
        if (cancelaRetencionMasivaPeticion == null) {
            cancelaRetencionMasivaPeticion = new ArrayList<CancelaRetencionMasivaPeticion>();
        }
        return this.cancelaRetencionMasivaPeticion;
    }

}
