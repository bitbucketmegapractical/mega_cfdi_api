
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}CancelacionRequest"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}CancelacionResponse"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}ClaveConfirmationRequest"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}ClaveConfirmationResponse"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}ComprobanteRequest"/&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}Undefined"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelacionRequest",
    "cancelacionResponse",
    "claveConfirmationRequest",
    "claveConfirmationResponse",
    "comprobanteRequest",
    "undefined"
})
@XmlRootElement(name = "Operations")
public class Operations {

    @XmlElement(name = "CancelacionRequest")
    protected CancelacionRequest cancelacionRequest;
    @XmlElement(name = "CancelacionResponse")
    protected CancelacionResponse cancelacionResponse;
    @XmlElement(name = "ClaveConfirmationRequest")
    protected ClaveConfirmationRequest claveConfirmationRequest;
    @XmlElement(name = "ClaveConfirmationResponse")
    protected ClaveConfirmationResponse claveConfirmationResponse;
    @XmlElement(name = "ComprobanteRequest")
    protected ComprobanteRequest comprobanteRequest;
    @XmlElement(name = "Undefined")
    protected Object undefined;

    /**
     * Obtiene el valor de la propiedad cancelacionRequest.
     * 
     * @return
     *     possible object is
     *     {@link CancelacionRequest }
     *     
     */
    public CancelacionRequest getCancelacionRequest() {
        return cancelacionRequest;
    }

    /**
     * Define el valor de la propiedad cancelacionRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelacionRequest }
     *     
     */
    public void setCancelacionRequest(CancelacionRequest value) {
        this.cancelacionRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelacionResponse.
     * 
     * @return
     *     possible object is
     *     {@link CancelacionResponse }
     *     
     */
    public CancelacionResponse getCancelacionResponse() {
        return cancelacionResponse;
    }

    /**
     * Define el valor de la propiedad cancelacionResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelacionResponse }
     *     
     */
    public void setCancelacionResponse(CancelacionResponse value) {
        this.cancelacionResponse = value;
    }

    /**
     * Obtiene el valor de la propiedad claveConfirmationRequest.
     * 
     * @return
     *     possible object is
     *     {@link ClaveConfirmationRequest }
     *     
     */
    public ClaveConfirmationRequest getClaveConfirmationRequest() {
        return claveConfirmationRequest;
    }

    /**
     * Define el valor de la propiedad claveConfirmationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaveConfirmationRequest }
     *     
     */
    public void setClaveConfirmationRequest(ClaveConfirmationRequest value) {
        this.claveConfirmationRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad claveConfirmationResponse.
     * 
     * @return
     *     possible object is
     *     {@link ClaveConfirmationResponse }
     *     
     */
    public ClaveConfirmationResponse getClaveConfirmationResponse() {
        return claveConfirmationResponse;
    }

    /**
     * Define el valor de la propiedad claveConfirmationResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaveConfirmationResponse }
     *     
     */
    public void setClaveConfirmationResponse(ClaveConfirmationResponse value) {
        this.claveConfirmationResponse = value;
    }

    /**
     * Obtiene el valor de la propiedad comprobanteRequest.
     * 
     * @return
     *     possible object is
     *     {@link ComprobanteRequest }
     *     
     */
    public ComprobanteRequest getComprobanteRequest() {
        return comprobanteRequest;
    }

    /**
     * Define el valor de la propiedad comprobanteRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComprobanteRequest }
     *     
     */
    public void setComprobanteRequest(ComprobanteRequest value) {
        this.comprobanteRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad undefined.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getUndefined() {
        return undefined;
    }

    /**
     * Define el valor de la propiedad undefined.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setUndefined(Object value) {
        this.undefined = value;
    }

}
