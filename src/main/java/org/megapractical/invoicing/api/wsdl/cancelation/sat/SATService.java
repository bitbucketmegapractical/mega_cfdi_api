package org.megapractical.invoicing.api.wsdl.cancelation.sat;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.3.0
 * 2019-07-26T10:39:08.564-05:00
 * Generated source version: 3.3.0
 *
 */
@WebService(targetNamespace = "http://www.mcfdi.com/wsdl/cfdi/sat/1.0", name = "SATService")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface SATService {

    @WebMethod(operationName = "ConsultaCFDRelacionados", action = "/ConsultaCFDRelacionados")
    @WebResult(name = "ConsultaCFDRelacionadosResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public ConsultaCFDRelacionadosResponse consultaCFDRelacionados(
        @WebParam(partName = "parameters", name = "ConsultaCFDRelacionadosRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        ConsultaCFDRelacionadosRequest parameters
    );

    @WebMethod(operationName = "AutenticaCancelacionRetencionesSAT", action = "/AutenticaCancelacionRetencionesSAT")
    @WebResult(name = "AutenticaCancelacionRetencionesSATResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public AutenticaCancelacionRetencionesSATResponse autenticaCancelacionRetencionesSAT(
        @WebParam(partName = "parameters", name = "AutenticaCancelacionRetencionesSATRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        AutenticaCancelacionRetencionesSATRequest parameters
    );

    @WebMethod(operationName = "ActualizaPeticionesCancelacionCFD", action = "/ActualizaPeticionesCancelacionCFD")
    @WebResult(name = "ActualizaPeticionesCancelacionCFDResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public ActualizaPeticionesCancelacionCFDResponse actualizaPeticionesCancelacionCFD(
        @WebParam(partName = "parameters", name = "ActualizaPeticionesCancelacionCFDRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        ActualizaPeticionesCancelacionCFDRequest parameters
    );

    @WebMethod(operationName = "CancelaRetencion", action = "/CancelaRetencion")
    @WebResult(name = "CancelaRetencionResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public CancelaRetencionResponse cancelaRetencion(
        @WebParam(partName = "parameters", name = "CancelaRetencionRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        CancelaRetencionRequest parameters
    );

    @WebMethod(operationName = "CancelaCFD", action = "/CancelaCFD")
    @WebResult(name = "CancelaCFDResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public CancelaCFDResponse cancelaCFD(
        @WebParam(partName = "parameters", name = "CancelaCFDRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        CancelaCFDRequest parameters
    );

    @WebMethod(operationName = "AutenticaEnvioSAT", action = "/AutenticaEnvioSAT")
    @WebResult(name = "AutenticaEnvioSATResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public AutenticaEnvioSATResponse autenticaEnvioSAT(
        @WebParam(partName = "parameters", name = "AutenticaEnvioSATRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        AutenticaEnvioSATRequest parameters
    );

    @WebMethod(operationName = "AutenticaRetencionesSAT", action = "/AutenticaRetencionesSAT")
    @WebResult(name = "AutenticaRetencionesSATResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public AutenticaRetencionesSATResponse autenticaRetencionesSAT(
        @WebParam(partName = "parameters", name = "AutenticaRetencionesSATRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        AutenticaRetencionesSATRequest parameters
    );

    @WebMethod(operationName = "EnviaRetencion", action = "/EnviaRetencion")
    @WebResult(name = "EnviaRetencionResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public EnviaRetencionResponse enviaRetencion(
        @WebParam(partName = "parameters", name = "EnviaRetencionRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        EnviaRetencionRequest parameters
    );

    @WebMethod(operationName = "ConsultaStatusCFD", action = "/ConsultaStatusCFD")
    @WebResult(name = "ConsultaStatusCFDResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public ConsultaStatusCFDResponse consultaStatusCFD(
        @WebParam(partName = "parameters", name = "ConsultaStatusCFDRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        ConsultaStatusCFDRequest parameters
    );

    @WebMethod(operationName = "ConsultaCancelacionRetencionMasiva", action = "/ConsultaCancelacionRetencionMasiva")
    @WebResult(name = "ConsultaCancelacionRetencionMasivaResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public ConsultaCancelacionRetencionMasivaResponse consultaCancelacionRetencionMasiva(
        @WebParam(partName = "parameters", name = "ConsultaCancelacionRetencionMasivaRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        ConsultaCancelacionRetencionMasivaRequest parameters
    );

    @WebMethod(operationName = "CancelaRetencionMasiva", action = "/CancelaRetencionMasiva")
    @WebResult(name = "CancelaRetencionMasivaResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public CancelaRetencionMasivaResponse cancelaRetencionMasiva(
        @WebParam(partName = "parameters", name = "CancelaRetencionMasivaRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        CancelaRetencionMasivaRequest parameters
    );

    @WebMethod(operationName = "ConsultaPeticionesCancelacionCFD", action = "/ConsultaPeticionesCancelacionCFD")
    @WebResult(name = "ConsultaPeticionesCancelacionCFDResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public ConsultaPeticionesCancelacionCFDResponse consultaPeticionesCancelacionCFD(
        @WebParam(partName = "parameters", name = "ConsultaPeticionesCancelacionCFDRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        ConsultaPeticionesCancelacionCFDRequest parameters
    );

    @WebMethod(operationName = "AutenticaSAT", action = "/AutenticaSAT")
    @WebResult(name = "AutenticaSATResponse", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0", partName = "parameters")
    public AutenticaSATResponse autenticaSAT(
        @WebParam(partName = "parameters", name = "AutenticaSATRequest", targetNamespace = "http://www.mcfdi.com/services/cfdi/sat/1.0")
        AutenticaSATRequest parameters
    );
}
