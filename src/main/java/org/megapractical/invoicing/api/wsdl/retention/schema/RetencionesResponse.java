
package org.megapractical.invoicing.api.wsdl.retention.schema;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V01_CFDI_RyP_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}retencionesString"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retencionesString"
})
@XmlRootElement(name = "RetencionesResponse")
public class RetencionesResponse
    implements Serializable
{

    @XmlElement(required = true)
    protected String retencionesString;

    /**
     * Gets the value of the retencionesString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetencionesString() {
        return retencionesString;
    }

    /**
     * Sets the value of the retencionesString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetencionesString(String value) {
        this.retencionesString = value;
    }

}
