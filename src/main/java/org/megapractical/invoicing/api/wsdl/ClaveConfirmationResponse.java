
package org.megapractical.invoicing.api.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.tibco.com/schemas/BW_CFDI_V33_in_xml/SharedResources/SchemaDefinitions/SERVICESchemas/Schema.xsd}clave_Confirmation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "claveConfirmation"
})
@XmlRootElement(name = "ClaveConfirmationResponse")
public class ClaveConfirmationResponse {

    @XmlElement(name = "clave_Confirmation", required = true)
    protected String claveConfirmation;

    /**
     * Obtiene el valor de la propiedad claveConfirmation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveConfirmation() {
        return claveConfirmation;
    }

    /**
     * Define el valor de la propiedad claveConfirmation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveConfirmation(String value) {
        this.claveConfirmation = value;
    }

}
