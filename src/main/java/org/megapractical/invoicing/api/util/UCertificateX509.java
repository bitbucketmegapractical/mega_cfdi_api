package org.megapractical.invoicing.api.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.ssl.PKCS8Key;
import org.megapractical.invoicing.api.stub.KeyLoader;

import lombok.val;

public class UCertificateX509 {
	
	public static X509Certificate getX509CertificateFromFile(File file) throws IOException, CertificateException {
        return getX509CertificateFromFile(UFile.fileToByte(file));
    }

	public static X509Certificate getX509CertificateFromFile(byte[] file) throws IOException, CertificateException {
        // Check availablity and readability of the given file first.
        if (file == null) {
            throw new NullPointerException("The given file is null.");
        }

        // Since the file seems to be ok, try to make a X509 certificate from it.
        CertificateFactory certificateFactory = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
            //certificateFactory = CertificateFactory.getInstance("X.509",BouncyCastleProvider.PROVIDER_NAME);
        } catch (Exception e) {
            // Shouldn't happen, since the BouncyCastle provider was added on class loading or even before
            throw new IOException("Certificate provider not found.", e);
        }

        Certificate certificate = null;
        try {
            InputStream is = new ByteArrayInputStream(file);
            certificate = certificateFactory.generateCertificate(is);
        } catch (CertificateException e) {
            throw new IOException("The given file is not an certificate", e);
        }

        if (certificate == null) {
            String message = "The given file does not contain a X.509 certificate.";
            throw new CertificateException(message);
        }
        if (!certificate.getType().equalsIgnoreCase("x.509")) {
            String message = "The certificate contained in the given file is not a X.509 certificate.";
            throw new CertificateException(message);
        }

        X509Certificate x509Certificate = (X509Certificate) certificate;

        return x509Certificate;
    }
	
	public static X509Certificate getX509CertificateFromFile(InputStream is) throws IOException, CertificateException {
		try {
			
			byte[] certificateByte = UFile.inputStreamToByte(is);
			
			ByteArrayInputStream bais = new ByteArrayInputStream(certificateByte);
			
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
			Certificate certificate = certificateFactory.generateCertificate(bais);
			bais.close();
			
			if (certificate == null) {
	            String message = "The given file does not contain a X.509 certificate.";
	            throw new CertificateException(message);
	        }
			
			if (!certificate.getType().equalsIgnoreCase("x.509")) {
	            String message = "The certificate contained in the given file is not a X.509 certificate.";
	            throw new CertificateException(message);
	        }

	        X509Certificate x509Certificate = (X509Certificate) certificate;

	        return x509Certificate;
			
		} catch (IOException e) {
			// Shouldn't happen, since the BouncyCastle provider was added on class loading or even before
            throw new IOException("Certificate provider not found.", e);
		} catch (CertificateException e) {
			throw new CertificateException("The given file does not contain a X.509 certificate.");
		}
    }
	
	// Validacion del certificado digital
	public static String certificateValidate(File pCer, File pKey, String pPasswd){
		String result = "VALIDATE_SUCCESSFULY";
		try {
			
			byte[] certificateByte = UFile.fileToByte(pCer);
			byte[] keyByte = UFile.fileToByte(pKey);
			
			ByteArrayInputStream bais = new ByteArrayInputStream(certificateByte);
			
			CertificateFactory factory = CertificateFactory.getInstance("X.509");
			Certificate certificate = factory.generateCertificate(bais);
			bais.close();
			
			RSAPublicKey publicKey = (RSAPublicKey) certificate.getPublicKey();
			
			PKCS8Key pkcs8Key = new PKCS8Key(keyByte, pPasswd.toCharArray());
			RSAPrivateCrtKey key = (RSAPrivateCrtKey) pkcs8Key.getPrivateKey();
			
			if (key.getModulus().compareTo(publicKey.getModulus()) != 0){
				// No se corresponde el certificado y la clave privada
				result = "CERTIFICATE_AND_PRIVATE_KEY_DOES_NOT_CORRESPOND";
			}
			
		} catch (IOException e) {
			// Excepcion
			result = "IO_EXCEPTION";
        } catch (BadPaddingException e) {
        	// La contraseña no se corresponde a la Clave Privada
            result = "BAD_PADDING_EXCEPTION"; 
        }catch (GeneralSecurityException e) {
        	// Excepcion
            result = "GENERAL_SECURITY_EXCEPTION"; 
        }
		return result;
	}
	
	public static String certificateValidate(InputStream pCer, InputStream pKey, String pPasswd){
		String result = "VALIDATE_SUCCESSFULY";
		try {
			
			byte[] certificateByte = UFile.inputStreamToByte(pCer);
			byte[] keyByte = UFile.inputStreamToByte(pKey);
			
			ByteArrayInputStream bais = new ByteArrayInputStream(certificateByte);
			
			CertificateFactory factory = CertificateFactory.getInstance("X.509");
			Certificate certificate = factory.generateCertificate(bais);
			bais.close();
			
			RSAPublicKey publicKey = (RSAPublicKey) certificate.getPublicKey();
			
			PKCS8Key pkcs8Key = new PKCS8Key(keyByte, pPasswd.toCharArray());
			RSAPrivateCrtKey key = (RSAPrivateCrtKey) pkcs8Key.getPrivateKey();
			
			if (key.getModulus().compareTo(publicKey.getModulus()) != 0){
				// No se corresponde el certificado y la clave privada
				result = "CERTIFICATE_AND_PRIVATE_KEY_DOES_NOT_CORRESPOND";
			}
			
		} catch (IOException e) {
			// Excepcion
			result = "IO_EXCEPTION";
        } catch (BadPaddingException e) {
        	// La contraseña no se corresponde a la Clave Privada
            result = "BAD_PADDING_EXCEPTION"; 
        }catch (GeneralSecurityException e) {
        	// Excepcion
            result = "GENERAL_SECURITY_EXCEPTION"; 
        }
		return result;
	}
	
	public static boolean isFiel(File file) {
        try {
			return isFiel(UFile.fileToByte(file));
		} catch (IOException e) {
			return false;
		}
    }

	public static boolean isFiel(byte[] file) {
		try {
			
			// Obtenieno certificado en formato estandar X.509 (Doc: https://es.wikipedia.org/wiki/X.509)
	        val certificadox509 = getX509CertificateFromFile(file);
	        return determineFiel(certificadox509);
			
		} catch (CertificateException | IOException e) {
			return false;
		}
    }
	
	public static boolean isFiel(InputStream is) throws CertificateException, IOException {
        // Obtenieno certificado en formato estandar X.509 (Doc: https://es.wikipedia.org/wiki/X.509)
        val certificadox509 = getX509CertificateFromFile(is);
        return determineFiel(certificadox509);
    }
	
	private static boolean determineFiel(X509Certificate certificadox509) {
		try {
			
			if (certificadox509.getExtendedKeyUsage() != null) {
			    for (val key : certificadox509.getExtendedKeyUsage()) {
			        if (key.trim().equals("1.3.6.1.5.5.7.3.4") || key.trim().equals("1.3.6.1.5.5.7.3.2")) {
			        	return true;
			        }
			    }
			}
			
		} catch (CertificateParsingException e) {
			e.printStackTrace();
		}
		return false;
	}

	// Fecha de creacion
	public static Date certificateCreationDate(String path) throws FileNotFoundException, CertificateException {
		try (val inStream = new FileInputStream(path)) {
	        
			val cf = CertificateFactory.getInstance("X.509");
	        val cert = (X509Certificate) cf.generateCertificate(inStream);
	        return cert.getNotBefore();
	        
		} catch (CertificateException | IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Date certificateCreationDate(InputStream is) throws CertificateException {
		try {
			
	        val cf = CertificateFactory.getInstance("X.509");
	        val cert = (X509Certificate)cf.generateCertificate(is);
	        return cert.getNotBefore();
	        
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Date certificateCreationDate(File file) {
        try {
			return certificateCreationDate(UFile.fileToByte(file));
		} catch (CertificateException | IOException e) {
			e.printStackTrace();
		}
        return null;
    }
	
	public static Date certificateCreationDate(byte[] file) throws FileNotFoundException, CertificateException, IOException {
        if (file == null) {
            return null;
        }

        val cert = getX509CertificateFromFile(file);
        return cert.getNotBefore();
    }
	
	// Fecha de expiracion
	public static Date certificateExpirationDate(String path) {
		try (val inStream = new FileInputStream(path)) {
			
	        val cf = CertificateFactory.getInstance("X.509");
	        val cert = (X509Certificate) cf.generateCertificate(inStream);
	        return cert.getNotAfter();
	        
		} catch (CertificateException | IOException e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	public static Date certificateExpirationDate(InputStream is) throws FileNotFoundException, CertificateException {
		try {
			
	        CertificateFactory cf = CertificateFactory.getInstance("X.509");
	        X509Certificate cert = (X509Certificate)cf.generateCertificate(is);
	        
	        return cert.getNotAfter();
	        
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Date certificateExpirationDate(File file) {
        try {
			return certificateExpirationDate(UFile.fileToByte(file));
		} catch (CertificateException | IOException e) {
			e.printStackTrace();
		}
        return null;
    }
	
	public static Date certificateExpirationDate(byte[] file) throws FileNotFoundException, CertificateException, IOException {
        if (file == null) {
            return null;
        }

        val cert = getX509CertificateFromFile(file);
        return cert.getNotAfter();
    }
	
	// Numero del certificado
    public static String certificateSerialNumber(byte[] file) throws IOException, CertificateException {
    	try {
			
    		if (file == null) {
                return null;
            }

            X509Certificate certificate = getX509CertificateFromFile(file);

            BigInteger serialNumber = certificate.getSerialNumber();
            byte[] serialNumberByteArray = serialNumber.toByteArray();

            return new String(serialNumberByteArray);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
    
    // Certificado en Base64
    public static String certificateBase64(File file) {
        try {
            
        	return certificateBase64(UFile.fileToByte(file));
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Certificado en Base64
    public static String certificateBase64(byte[] file) {
        try {
            
        	X509Certificate certificate = getX509CertificateFromFile(file);
            byte[] bytes = certificate.getEncoded();
            Base64 b64 = new Base64(-1);
            return b64.encodeToString(bytes);
            
        } catch (CertificateEncodingException ex) {
            Logger.getLogger(UCertificateX509.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | CertificateException ex) {
            Logger.getLogger(UCertificateX509.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // Certificado en Base64
    public static String fileBase64(File file) {
        String digitalSign;
        byte[] bytes;

        try {
            
        	bytes = Base64.encodeBase64(UFile.fileToByte(file));
            
        	digitalSign = new String(bytes);
            digitalSign = digitalSign.replace("\r\n", "");
            digitalSign = digitalSign.replace("\n", "");
            digitalSign = digitalSign.replace("\r", "");

            return digitalSign;
            
        } catch (IOException ex) {
            Logger.getLogger(UCertificateX509.class.getName()).log(Level.SEVERE, null, ex);           
        }
        return null;
    }

    // Certificado en Base64
    public static String fileBase64(byte[] certificateBytes) {
        String digitalSign;
        byte[] bytes;

        try {
			
        	bytes = Base64.encodeBase64(certificateBytes);

            digitalSign = new String(bytes);
            digitalSign = digitalSign.replace("\r\n", "");
            digitalSign = digitalSign.replace("\n", "");
            digitalSign = digitalSign.replace("\r", "");

            return digitalSign;
        	
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
    
    public static String certificateSeal(byte[] certificate, byte[] privateKey, String passwd){
    	try {
			
    		PrivateKey key = KeyLoader.loadPKCS8PrivateKey(new ByteArrayInputStream(privateKey), passwd);
    		X509Certificate certificateX509 = getX509CertificateFromFile(certificate);
    		certificateX509.checkValidity();
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initSign(key);
            sig.update(certificate);
            byte[] signed = sig.sign();
            Base64 b64 = new Base64(-1);
            return b64.encodeToString(signed);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static boolean isMoralPerson(File file) throws IOException, CertificateException, Exception {
        return isMoralPerson(UFile.fileToByte(file));
    }
    
    public static boolean isMoralPerson(byte[] file) throws IOException, CertificateException, Exception {
        String rfc = getCertPersonRFC(file);

        switch (rfc.length()) {
            case 13:
                return false;
            case 12:
                return true;
            default:
                throw new Exception("It cant not idetify, the person type.");
        }
    }
    
    public static String getCertPersonRFC(File file) {
        try {
			return getCertPersonRFC(UFile.fileToByte(file));
		} catch (CertificateException | IOException e) {
			e.printStackTrace();
		}
        return null;
    }
    
    public static String getCertPersonRFC(byte[] file) throws IOException, CertificateException {
        String certText;
        String[] certRFC = null;

        val cert = getX509CertificateFromFile(file);

        certText = cert.getSubjectDN().toString();
        val certArray = certText.split(",");
        for (String element : certArray) {
            if (element.trim().startsWith("OID.2.5.4.45")) {
                certText = element.split("=")[1];
                certRFC = certText.split("/");
                break;
            }
        }
        
        if (certRFC != null) {
        	return certRFC[0].trim();
        }
        return null;
    }
    
    public static String getCertLegalRepresentativePersonRFC(File file) {
        try {
			return getCertLegalRepresentativePersonRFC(UFile.fileToByte(file));
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
    
    public static String getCertLegalRepresentativePersonRFC(byte[] file) throws IOException, Exception {
        String certText;
        String[] certRFC = null;

        X509Certificate cert = (X509Certificate) getX509CertificateFromFile(file);

        certText = cert.getSubjectDN().toString();
        String[] certArray = certText.split(",");
        for (String element : certArray) {
            if (element.trim().startsWith("OID.2.5.4.45")) {
                certText = element.split("=")[1];
                certRFC = certText.split("/");
                break;
            }
        }

        /* If only have one position, it mean that is not an moral person certificate.
         Other validation can be use the "isMoralPerson()" method but in this case
         we go to use this way to not depend of the "isMOralPerson()" method.*/
        if (certRFC != null && certRFC.length == 1) {
            // This is not an moral person certificate
            return "";
        }
        return certRFC[0].trim();
    }
}
