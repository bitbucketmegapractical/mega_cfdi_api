package org.megapractical.invoicing.api.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.megapractical.invoicing.api.common.ApiConsolidate;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

public final class UConsolidate {

	private UConsolidate() {
	}

	// ##### Genera consolidado (1 pdf por pagina)
	public static ApiConsolidate consolidateSingle(ApiConsolidate consolidate) {
		try {

			System.out.println(
					"-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONSOLIDATE INSTANCE > SOURCE PATH: "
					+ consolidate.getSourcePath());

			ApiConsolidate apiConsolidate = new ApiConsolidate();
			apiConsolidate.setSourcePath(consolidate.getSourcePath());

			// ##### Determinando la ruta del consolidado
			String consolidatePath = consolidate.getAbsolutePath() + File.separator + "CONSOLIDATE1X";
			apiConsolidate.setConsolidatePath(consolidatePath);

			// ##### Nombre del consolidado
			String consolidateName = consolidate.getFileName() + "_CONSOLIDATE1X.pdf";
			apiConsolidate.setConsolidateName(consolidateName);

			// ##### Determinando ruta de db
			String dbPath = consolidate.getConsolidateDbPath() + File.separator + "CONSOLIDATE1X" + File.separator
					+ consolidate.getFileName() + "_CONSOLIDATE1X.pdf";
			apiConsolidate.setConsolidateDbPath(dbPath);

			apiConsolidate.setConsolidateType("1x");

			apiConsolidate = consolidate(apiConsolidate);

			return apiConsolidate;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// ##### Genera consolidado (2 pdf por pagina)
	public static ApiConsolidate consolidateDual(ApiConsolidate consolidate) {
		try {

			System.out.println(
					"-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONSOLIDATE INSTANCE > SOURCE PATH: "
					+ consolidate.getSourcePath());

			ApiConsolidate apiConsolidate = new ApiConsolidate();
			apiConsolidate.setSourcePath(consolidate.getSourcePath());

			// ##### Determinando la ruta del consolidado
			String consolidatePath = consolidate.getAbsolutePath() + File.separator + "CONSOLIDATE2X";
			apiConsolidate.setConsolidatePath(consolidatePath);

			// ##### Nombre del consolidado
			String consolidateName = consolidate.getFileName() + "_CONSOLIDATE2X.pdf";
			apiConsolidate.setConsolidateName(consolidateName);

			// ##### Determinando ruta de db
			String dbPath = consolidate.getConsolidateDbPath() + File.separator + "CONSOLIDATE2X" + File.separator
					+ consolidate.getFileName() + "_CONSOLIDATE2X.pdf";
			apiConsolidate.setConsolidateDbPath(dbPath);

			apiConsolidate.setConsolidateType("2x");

			apiConsolidate = consolidate(apiConsolidate);

			return apiConsolidate;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static ApiConsolidate consolidate(ApiConsolidate consolidate) {
		try {

			// ##### Cargando listado de PDFs
			File directory = new File(consolidate.getSourcePath());
			File[] files = directory.listFiles();

			Integer totalFile = 0;

			List<String> consolidateFiles = new ArrayList<>();

			for (int i = 0; i < files.length; i++) {
				File folder = files[i];
				File folderDirectory = new File(folder.getAbsolutePath());
				File[] folderFile = folderDirectory.listFiles();

				for (File file : folderFile) {
					if (file.getAbsolutePath().contains(".pdf") || file.getAbsolutePath().contains(".PDF")) {
						consolidateFiles.add(file.getAbsolutePath());
						totalFile++;
					}
				}
			}

			System.out.println(
					"-----------------------------------------------------------------------------------------------------------");
			System.out.println(UPrint.consoleDatetime() + "MEGA-CFDI API > [INFO] CONSOLIDATE INSTANCE > " + totalFile
					+ " PDFs SELECTED");

			try {
				if (!consolidateFiles.isEmpty()) {

					// ##### Creando la ruta del consolidado
					File sourceDir = new File(consolidate.getConsolidatePath());
					if (!sourceDir.exists()) {
						sourceDir.mkdirs();
					}

					String absoluteConsolidatePath = sourceDir.getAbsoluteFile() + File.separator
							+ consolidate.getConsolidateName();

					// ##### Objeto Document para escribir el nuevo PDF
					Document document = new Document(PageSize.LETTER);

					try {
						PdfWriter writer = PdfWriter.getInstance(document,
								new FileOutputStream(absoluteConsolidatePath));
						writer.setFullCompression();
						document.open();

						for (String pdf : consolidateFiles) {
							File file = new File(pdf);
							PdfReader reader = new PdfReader(file.getAbsolutePath());

							// ##### Obteniendo la cantidad de paginas del pdf
							Integer pages = reader.getNumberOfPages();

							// ##### Determinando tipo de consolidado
							if (consolidate.getConsolidateType().equals("1x") || pages > 1) {
								// ##### Escribiendo primera pagina del PDF
								PdfImportedPage page = writer.getImportedPage(reader, 1);
								Image image = Image.getInstance(page);
								image.scalePercent(90f);
								document.add(image);

								// ##### Determinando si tiene mas paginas el PDF
								if (pages > 1) {
									for (int i = 2; i <= pages; i++) {
										// ##### Escribiendo siguientes paginas del PDF
										page = writer.getImportedPage(reader, i);
										image = Image.getInstance(page);
										image.scalePercent(90f);
										document.add(image);
									}
								}
							} else if (consolidate.getConsolidateType().equals("2x")) {
								// ##### Escribiendo PDF
								PdfImportedPage page = writer.getImportedPage(reader, 1);
								Image image = Image.getInstance(page);
								image.scalePercent(90f);
								image.setRotationDegrees(-90f);
								image.setBorder(Rectangle.BOX);
								image.setBorderWidth(0.6f);
								image.setBorderColor(new GrayColor(0.5f));
								document.add(image);

								// ##### Escribiendo nuevamente el PDF
								page = writer.getImportedPage(reader, 1);
								image = Image.getInstance(page);
								image.scalePercent(90f);
								image.setRotationDegrees(-90f);
								image.setBorder(Rectangle.BOX);
								image.setBorderWidth(0.6f);
								// image.setBorderWidthTop(2f);
								image.setBorderColor(new GrayColor(0.5f));
								document.add(image);
							}
						}

						document.close();

						System.out.println(
								"-----------------------------------------------------------------------------------------------------------");
						System.out.println(UPrint.consoleDatetime()
								+ "MEGA-CFDI API > [INFO] CONSOLIDATE INSTANCE > CONSOLIDATE GENERATED SUCCESSFULY");

						consolidate.setConsolidateTotalFile(totalFile);
						return consolidate;

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
