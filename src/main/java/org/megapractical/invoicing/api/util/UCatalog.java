package org.megapractical.invoicing.api.util;

import java.util.regex.Pattern;

import org.megapractical.invoicing.voucher.catalogs.CEstado;
import org.megapractical.invoicing.voucher.catalogs.CFormaPago;
import org.megapractical.invoicing.voucher.catalogs.CMetodoPago;
import org.megapractical.invoicing.voucher.catalogs.CMoneda;
import org.megapractical.invoicing.voucher.catalogs.COrigenRecurso;
import org.megapractical.invoicing.voucher.catalogs.CPais;
import org.megapractical.invoicing.voucher.catalogs.CTipoDeComprobante;
import org.megapractical.invoicing.voucher.catalogs.CTipoFactor;
import org.megapractical.invoicing.voucher.catalogs.CTipoNomina;
import org.megapractical.invoicing.voucher.catalogs.CUsoCFDI;

import lombok.val;
import lombok.var;

public final class UCatalog {

	private UCatalog() {		
	}
	
	public static String plainValue(String input) {
		if (!UValidator.isNullOrEmpty(input)) {
			val firstIndex = input.indexOf('(');
			val lastIndex = input.lastIndexOf(')');
			if (firstIndex != -1 && lastIndex != -1) {
				return input.substring(0, firstIndex) +
					   input.substring(firstIndex+1, lastIndex).trim() +
					   input.substring(lastIndex+1);
			}
			return input;
		}
		return null;
	}
	
	public static String[] getCatalog(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			value = UValue.stringValue(value);
			var catalog = value.split(Pattern.quote(" ("), 2);
			try {
				if (catalog[1].endsWith(")")) {
					catalog[1] = catalog[1].substring(0, catalog[1].length() - 1).trim();
				}
			} catch (Exception e) {
				catalog = new String[] { catalog[0], null };
			}
			return catalog;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getCode(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			value = UValue.stringValue(value);
			val splited = value.split(Pattern.quote(" ("), 2);
			return UValue.stringValue(splited[0]).trim();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getValue(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			value = UValue.stringValue(value);
			val splited = value.split(Pattern.quote(" ("), 2);
			try {
				if (splited[1].endsWith(")")) {
					value = splited[1].substring(0, splited[1].length() - 1).trim();
				}
			} catch (Exception e) {
				value = splited[0];
			}
			return value;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CUsoCFDI usoCFDI(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CUsoCFDI.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CPais pais(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CPais.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CTipoDeComprobante tipoDeComprobante(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CTipoDeComprobante.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CMoneda moneda(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CMoneda.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CMetodoPago metodoPago(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CMetodoPago.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CFormaPago formaPago(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CFormaPago.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CTipoFactor factorType(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CTipoFactor.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static COrigenRecurso sourceResource(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return COrigenRecurso.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CTipoNomina payrollType(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CTipoNomina.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static CEstado estate(String value) {
		if (UValidator.isNullOrEmpty(value)) {
			return null;
		}
		
		try {
			return CEstado.fromValue(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}