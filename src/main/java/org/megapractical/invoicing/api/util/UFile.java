package org.megapractical.invoicing.api.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.megapractical.common.validator.AssertUtils;
import org.megapractical.invoicing.api.wrapper.ApiFileDetail;
import org.mozilla.universalchardet.UniversalDetector;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Preconditions;

import lombok.val;

public final class UFile {
	
	private UFile() {		
	}

	public static void isEmpty(File file) throws FileNotFoundException {
        if (file == null) {
            throw new FileNotFoundException("The given file is null.");
        }
    }
	
	public static byte[] fileToByte(File file) throws IOException {
		byte[] buffer = new byte[(int) file.length()];
		try (val is = new FileInputStream(file)) {
			if ( is.read(buffer) == -1 ) {
				throw new IOException("EOF reached while trying to read the whole file");
			}
		}
		return buffer;
    }
	
	public static File byteToFile(String fileName, byte[] bytea) throws IOException {
		try {
			
			String tmpDir = UProperties.getTmpDir();

			File fileFolder = new File(tmpDir); 
			if (!fileFolder.exists()) {
				fileFolder.mkdirs();
			}
			
			String fullPath = tmpDir + File.separator + fileName;
			
			FileUtils.writeByteArrayToFile(new File(fullPath), bytea);
			
			return new File(fullPath);
			
		} catch (Exception e) {
			throw new IOException("EOF reached while trying to read the bytea file");
		}
	}
	
	public static ByteArrayInputStream byteArrayInputStream(String filePath) {
        try (val byteArray = new ByteArrayInputStream(fileToByte(new File(filePath)))) {
            return byteArray;
        } catch (IOException ex) {
            return null;
        }
    }
	
	public static byte[] inputStreamToByte(InputStream is){
		byte[] bytes;
		try {
			
			bytes = IOUtils.toByteArray(is);
			return bytes;
			
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return null;
	}
	
	public static File inputStreamToFile(InputStream is, String prefix, String suffix){
		try {
			
			final File tempFile = File.createTempFile(prefix, suffix);
	        tempFile.deleteOnExit();
	        try (FileOutputStream out = new FileOutputStream(tempFile)) {
	            IOUtils.copy(is, out);
	        }
	        return tempFile;
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static InputStream fileToInputStream(File file){
		try {
			
			return new FileInputStream(file);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static File multipartFileToFile(MultipartFile multipartFile) {
		try {
			
			val tmpDir = UProperties.getTmpDir() + File.separator + multipartFile.getOriginalFilename();
			System.out.println(String.format("Temporal dir: %s", tmpDir));
			
			val file = new File(tmpDir);
			
			if (file.exists()) {
				file.delete();
			}
			
		    val isFileCreated = file.createNewFile();
		    if (isFileCreated) {
		    	try (val fos = new FileOutputStream(file)) {
		    		fos.write(multipartFile.getBytes());
		    		return file;
		    	}
		    }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return null;
	}
	
	public static ApiFileDetail fileDetails(MultipartFile multipartFile) {
		try {
			val file = multipartFileToFile(multipartFile);
			val originalFilename = multipartFile.getOriginalFilename();
			val fileName = getName(originalFilename);
			val fileExt = getExtension(originalFilename);
			
			return ApiFileDetail
					.builder()
					.file(file)
					.originalFilename(originalFilename)
					.fileName(fileName)
					.fileExt(fileExt)
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiFileDetail.withError();
	}
	
	public static ApiFileDetail fileDetails(File file) {
		try {
			val originalFilename = file.getName();
			val fileName = getName(originalFilename);
			val fileExt = getExtension(originalFilename);
			
			return ApiFileDetail
					.builder()
					.file(file)
					.originalFilename(originalFilename)
					.fileName(fileName)
					.fileExt(fileExt)
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApiFileDetail.withError();
	}
	
	public static InputStream checkStreamIsNotEmpty(InputStream inputStream) throws IOException {
	    Preconditions.checkArgument(inputStream != null, "The InputStream is mandatory");
	    PushbackInputStream pushbackInputStream = new PushbackInputStream(inputStream);
	    int b;
	    b = pushbackInputStream.read();
	    if ( b == -1 ) {
	    	//No byte can be read from inputStream
	    	return null;
	    }
	    pushbackInputStream.unread(b);
	    return pushbackInputStream;
	}
	
	public static String getFileName(File file) throws FileNotFoundException {
        // Validation exceptions.
        isEmpty(file);

        return getName(file.getName());
    }
	
	public static String getName(String name) {
        // Getting file name.
        return name.substring(0, name.lastIndexOf("."));
    }
	
	public static String getFileExtension(File file) throws FileNotFoundException {
        // Validation exceptions.
        isEmpty(file);

        return getFileExtension(file.getName());
    }
	
	public static String getFileExtension(String name) {
        // Validation exceptions.
        try {
            int index = name.lastIndexOf(".");
            if (index != -1) {
                return name.substring(index + 1, name.length());
            }
            return "";
        } catch (Exception e) {
            return null;
        }
    }
	
	public static String getFileFullName(String name) {
        // Getting file name and extension.
        return name.substring(name.lastIndexOf(File.separator) + 1, name.length());
    }
	
	public static String getFileName(final Part part) {
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}
	
	public static String getExtension(String fileName){
		String[] extension = fileName.split("\\.");
		if (extension.length != 0) {
            return extension[extension.length - 1];
        }
		return null;
	}
	
	public static void writeFile(byte[] file2Write, String path2Write) {
        try {

            // Backup Physically file path. 
            val des = new File(path2Write);

            // Validating if path exist
            if (des.getParentFile().exists()) {
            	// Creating buffer object to write a file.
                try (val bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(des))) {
                	bufferedOutputStream.write(file2Write, 0, file2Write.length);
                }
            }

        } catch (IOException ex) {
        	System.out.println("Error when try to write a file;");
        	ex.printStackTrace();
        }
    }
	
	public static String fileEncoding(File file){
    	String encoding = null;
    	try (val fis = new FileInputStream(file.getAbsolutePath())) {
    		final UniversalDetector detector = new UniversalDetector(null);
    		
    		handleData(fis, detector);  
    		
    		encoding = getEncoding(detector);  
            detector.reset();
    	} catch (IOException e) {  
            encoding = "";  
        }  
    	return encoding;
    }
    
    private static String getEncoding(UniversalDetector detector) {
    	return detector.getDetectedCharset();
        /*if(detector.isDone()) {  
            return detector.getDetectedCharset();  
        }  
        return "";*/
    } 
    
    private static void handleData(FileInputStream fis, UniversalDetector detector) throws IOException {  
        int nread;  
        final byte[] buf = new byte[4096];  
        while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {  
            detector.handleData(buf, 0, nread);  
        }
        detector.dataEnd();  
    }
    
    public static Boolean utf8Encoding(File file){
    	try {
    		
    		Path p = Paths.get(file.getAbsolutePath());
            ByteBuffer byteBuffer = ByteBuffer.wrap(Files.readAllBytes(p));
            CharBuffer charBuffer = Charset.forName("windows-1252").decode(byteBuffer);
            byteBuffer = StandardCharsets.UTF_8.encode(charBuffer);
            Files.write(p, byteBuffer.array());
            return true;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return false;
    }
    
    public static Boolean fileCopyContent(File sourceFile, File targetFile){
		try (val in = new FileInputStream(sourceFile);
			 val out = new FileOutputStream(targetFile);) {

			int c;
			while( (c = in.read() ) != -1){
				out.write(c);
			}
			
			return true;
			
		} catch(IOException e) {
			System.err.println("Hubo un error de entrada/salida!!!");
		}
    	return false;
    }
    
    public static Boolean fileUtf8(File file){
    	try {
    		
    		// Bakup path
    		String fileBakupPath = file.getAbsolutePath();
    		
    		// Original file
    		File sourceFile = new File(file.getAbsolutePath());
    		
    		// Renaming original file
    		File sourceFileRenamed = new File(file.getParent() + File.separator + getFileName(file) + "_ORG." + getFileExtension(file));
    		sourceFile.renameTo(sourceFileRenamed);    	    	
    		
    		// Create target file
    		File targetFile = new File(fileBakupPath);
    		if(!file.exists()){
    			file.createNewFile();
    		}
    		
    		// Encoding renamed file
    		utf8Encoding(sourceFileRenamed);
    		
    		// Writin original file content to target file
    		fileCopyContent(sourceFileRenamed, targetFile);
    		
    		// Deleting renamed file
    		sourceFileRenamed.delete();
    		
            return true;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return false;
    }
    
    public static File createFileIfNotExist(String path) {
    	return createFileIfNotExist(new File(path));
	}
    
    public static File createFileIfNotExist(File file) {
    	try {
			if (file != null && !file.exists()) {
				val created = file.createNewFile();
				if (!created) {
	                System.out.println("Error creating file: " + file.getAbsolutePath());
	                return null;
	            }
				System.out.println("File created successfully.");
			}
			return file;
		} catch (IOException e) {
			return null;
		}
    }
    
    public static void createParentDirectory(String path) {
    	createParentDirectory(new File(path));
    }
    
    public static void createParentDirectory(File file) {
    	if (file != null) {
    		val directory = file.getParentFile();
            // Create the directory structure if it doesn't exist
    		if (!directory.exists()) {
                val created = directory.mkdirs();
                if (created) {
                	System.out.println("Directory created successfully.");
                } else {
                	System.out.println("Error creating directory: " + directory.getAbsolutePath());
                }
            }
    	}
    }
    
    public static boolean createDirectory(String path) {
    	val directory = Paths.get(path);
    	return createDirectory(directory);
    }
    
    public static boolean createDirectory(File file) {
        val directory = Paths.get(file.getAbsolutePath());
        return createDirectory(directory);
    }
    
    public static boolean createDirectory(Path path) {
    	if (Files.exists(path)) {
            return true;
        }
        
        try {
        	// Create the directory structure if it doesn't exist
            Files.createDirectories(path);
            System.out.println("Directory created successfully.");
            return true;
        } catch (IOException e) {
        	
        	System.out.println("Error creating directory");
            e.printStackTrace();
        }
        return false;
    }
    
    public static void copy(String sourcePath, String destinationPath) {
    	val source = Paths.get(sourcePath);
        val destination = Paths.get(destinationPath);
        copy(source, destination);
    }
    
    public static void copy(File source, File destination) {
    	if (source != null && destination != null) {
    		copy(source.toPath(), destination.toPath());
    	}
    }
    
    public static void copy(Path sourcePath, Path destinationPath) {
    	try {
            Files.copy(sourcePath, destinationPath);
            System.out.println("File copied successfully.");
        } catch (IOException e) {
            System.out.println("Error copying file: " + e.getMessage());
        }
    }
    
	public static long totalFilesInDirectory(String path) throws IOException {
		try {
			if (AssertUtils.hasValue(path)) {
				val directory = Paths.get(path);
				if (Files.exists(directory)) {
					try (val stream = Files.list(directory)) {
						return stream.filter(Files::isRegularFile).count();
					}
				}
			}
		} catch (InvalidPathException | IOException e) {
			System.out.println(String.format("Directory %s doesn't exist", path));
		}
		return -1;
	}
	
	public static boolean deleteDirectory(String path) {
		try {
			val directory = Paths.get(path);
	        if (!Files.exists(directory)) {
	        	System.out.println("Directory doesn't exist");
	            return false;
	        }
	        
	        try (val files = Files.walk(directory)) {
				files.map(Path::toFile).sorted((f1, f2) -> f1.compareTo(f2)).forEach(File::delete);
	        }
	        val deleted = Files.deleteIfExists(directory);
	        System.out.println(deleted ? "Directory deleted successfully" : "Directory hasn't been deleted");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
    }
	
	public static boolean deleteFile(String path) {
        val file = new File(path);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }
}