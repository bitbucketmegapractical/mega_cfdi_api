package org.megapractical.invoicing.api.util;

import java.time.LocalDateTime;

import lombok.val;

public final class UPrint {
	
	private UPrint() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	public static void log(String env, String log) {
		val print = String.format("%s %s %s", consoleDatetime(), env, log);
		System.out.println(print);
	}
	
	public static void log(String log) {
		System.out.println(log);
	}
	
	public static void logWithLine(String env, String log) {
		val print = String.format("%s %s %s", consoleDatetime(), env, log);
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		System.out.println(print);
	}
	
	public static void logWithLine(String log) {
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		System.out.println(log);
	}
	
	public static String consoleDatetime() {
		return LocalDateTime.now().toString();
	}
	
}