package org.megapractical.invoicing.api.util;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class StringUtils {
	
	private StringUtils() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	public static boolean isEmpty(String str) {
		if (str == null || str.isEmpty()) {
			return true;
		}
		return str.trim().isEmpty();
	}
	
	public static boolean hasValue(String str) {
		return !isEmpty(str);
	}
	
}