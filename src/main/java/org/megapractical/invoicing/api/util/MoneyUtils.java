package org.megapractical.invoicing.api.util;

import static org.megapractical.invoicing.api.util.UValue.bigDecimalStrict;
import static org.megapractical.invoicing.api.util.UValue.bigDecimalString;
import static org.megapractical.invoicing.api.util.UValue.stringValue;

import java.math.BigDecimal;

import org.megapractical.common.validator.AssertUtils;

import lombok.val;

public final class MoneyUtils {

	private MoneyUtils() {
	}

	public static BigDecimal formatStrict(String value) {
		return bigDecimalStrict(stringValue(value));
	}
	
	public static String fullFormatStrict(String value) {
		return bigDecimalString(bigDecimalStrict(stringValue(value)));
	}

	public static String format(BigDecimal value) {
		return format(bigDecimalString(value));
	}

	public static String format(String value) {
		return bigDecimalString(stringValue(value));
	}
	
	public static String formatSafe(String value) {
		if (!AssertUtils.hasValue(value)) {
			return "";
		}
		return bigDecimalString(stringValue(value));
	}
	
	public static String formatWithDecimal(String value) {
		if (!AssertUtils.hasValue(value)) {
			return "0.00";
		}
		
		val decimals = UTools.decimals(value);
		return bigDecimalString(stringValue(value), decimals);
	}
	
	public static String formatWithDecimalSafe(String value) {
		if (!AssertUtils.hasValue(value)) {
			return "";
		}
		
		val decimals = UTools.decimals(value);
		return bigDecimalString(stringValue(value), decimals);
	}
	
	public static String formatWith4Decimal(String value) {
		if (!AssertUtils.hasValue(value)) {
			return "";
		}
		return bigDecimalString(stringValue(value), 4);
	}
	
}