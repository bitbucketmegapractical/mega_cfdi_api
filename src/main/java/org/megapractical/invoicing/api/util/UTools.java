package org.megapractical.invoicing.api.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.var;

public final class UTools {
	
	private UTools() {
	}

	public static boolean isMoralPerson(String rfc){
		if (!UValidator.isNullOrEmpty(rfc)) {
			switch (rfc.length()) {
				case 13: return false;
				case 12: return true;
				default: return false;
			}
		}
		return false;
	}
	
	public static Integer decimals(String value){
		try {
			
			if(UValidator.isNullOrEmpty(value))
				return 0;
			
			int pos = value.indexOf(".");
			if(pos <= 0)
				return 0;
			
			return value.substring(pos + 1).length();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer decimals(BigDecimal value){
		try {
			
			if(UValidator.isNullOrEmpty(value))
				return 0;
			
			if(value.scale() == 0)
				return 0;
			
			BigDecimal onlyDecimalPart = value.subtract(value.setScale(0, RoundingMode.FLOOR)).movePointRight(value.scale());
			Integer decimalPartInteger = onlyDecimalPart.intValue();
			
			if(decimalPartInteger == 0)
				return 2;
			
			return decimalPartInteger.toString().length();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer decimalValue(String value){
		try {
			
			if(UValidator.isNullOrEmpty(value))
				return null;

			int pos = value.indexOf(".");
			return pos >= 0 ? value.substring(pos + 1).length() : 0;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer decimalValue(BigDecimal value){
		try {
			
			if(UValidator.isNullOrEmpty(value))
				return null;
			
			BigDecimal onlyDecimalPart = value.subtract(value.setScale(0, RoundingMode.FLOOR)).movePointRight(value.scale());
			Integer decimalPartInteger = onlyDecimalPart.intValue();
			return decimalPartInteger.toString().length();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String jobAge(String value) {
		String result = null;

		try {

			if (UValidator.isNullOrEmpty(value))
				return null;

			// ##### Si no inicia con P retornar el mismo valor
			if (!value.startsWith("P"))
				return value;

			value = value.toUpperCase();
			// ##### Formato semanas: P589W
			if (value.contains("W") || value.contains("w")) {
				value = value.replace("P", "").replace("W", "");
				var valueInt = Integer.parseInt(value);

				// ##### 1 año = 52 semanas
				if (valueInt <= 52) {
					valueInt = (valueInt == 52) ? (valueInt / 4) - 1 : valueInt / 4;
					if (valueInt <= 11) {
						result = (valueInt == 1) ? valueInt + " mes" : valueInt + " meses";
					} else if (valueInt == 12) {
						result = "1 año";
					}
				} else {
					valueInt = valueInt / 52;
					result = valueInt + (valueInt == 1 ? " año" : " años");
				}
			} else if (value.contains("D")) {
				if (value.contains("M")) {
					if (value.contains("Y")) {
						String years = value.substring(value.indexOf("P") + 1, value.indexOf("Y"));
						String months = value.substring(value.indexOf("Y") + 1, value.indexOf("M"));
						String days = value.substring(value.indexOf("M") + 1, value.indexOf("D"));

						int yearsInteger = Integer.parseInt(years);
						int monthsInteger = Integer.parseInt(months);
						int daysInteger = Integer.parseInt(days);

						result = years + (yearsInteger == 1 ? " año" : " años ") + months
								+ (monthsInteger == 1 ? " mes" : " meses ") + days
								+ (daysInteger == 1 ? " día" : " días");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
