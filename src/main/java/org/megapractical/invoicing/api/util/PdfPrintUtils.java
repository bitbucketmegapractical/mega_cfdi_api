package org.megapractical.invoicing.api.util;

import org.megapractical.invoicing.sat.tfd.TimbreFiscalDigital;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class PdfPrintUtils {
	
	private PdfPrintUtils() {
	}
	
	public static final String generateOriginalString(TimbreFiscalDigital tfd) {
		if (tfd != null) {
			val format = "||%s|%s|%s|%s|%s|%s||";
			val s1 = tfd.getVersion();
			val s2 = tfd.getUUID();
			val s3 = tfd.getFechaTimbrado();
			val s4 = tfd.getRfcProvCertif();
			val s5 = tfd.getSelloCFD();
			val s6 = tfd.getNoCertificadoSAT();
			return String.format(format, s1, s2, s3, s4, s5, s6);
		}
		return null;
	}
	
}