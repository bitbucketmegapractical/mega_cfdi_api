package org.megapractical.invoicing.api.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lombok.val;

/**
 * @author Maikel Guerra Ferrer
 *
 */
public final class UXml {
	
	private UXml() {
	}
	
	public static String getAttribute(String xml, String node, String attribute) throws IOException, SAXException, ParserConfigurationException {
		String attributeValue = null;
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new InputSource(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));

			NodeList nodelist = doc.getElementsByTagName("*");

			Node noder;
			Element element;
			NamedNodeMap nnm = null;

			String attrname;
			String elemname;
			
			int i, len;
			len = nodelist.getLength();

			boolean finish = false;
			for (int j = 0; j < len; j++) {
				element = (Element) nodelist.item(j);
				elemname = element.getTagName();
				if (elemname.equals(node)) {
					nnm = element.getAttributes();
					if (nnm != null) {
						for (i = 0; i < nnm.getLength(); i++) {
							noder = nnm.item(i);
							attrname = noder.getNodeName();
							if (attrname.equals(attribute)) {
								attributeValue = noder.getNodeValue();
								finish = true;
								break;
							}
						}

						if (finish) {
							break;
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return attributeValue;
	}
	 
	public List<String> getNodes(String xml, String parentNode) {
		val nodes = new ArrayList<String>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document doc = db.parse(new InputSource(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));

			NodeList nodelist = doc.getElementsByTagName("*");

			Element element;
			int len = nodelist.getLength();

			for (int j = 0; j < len; j++) {
				element = (Element) nodelist.item(j);
				if (element.getParentNode().getNodeName().equals(parentNode)) {
					Node nx = element;
					TransformerFactory transFactory = TransformerFactory.newInstance();
					Transformer transformer = transFactory.newTransformer();
					StringWriter buffer = new StringWriter();
					transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
					transformer.transform(new DOMSource(nx), new StreamResult(buffer));
					nodes.add(buffer.toString());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nodes;
	}
   
	public static String getAttributeNodeValue(String xml, String node) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));

        NodeList nodelist = doc.getElementsByTagName("*");

        Element element;
        String elemname;
        int len = nodelist.getLength();
        String attrVal = "";
        
        for (int j = 0; j < len; j++) {
            element = (Element) nodelist.item(j);
            elemname = element.getTagName();
            if (elemname.equals(node)) {
                attrVal = element.getTextContent();
                break;
            }
        }
        return attrVal;
    }
}
