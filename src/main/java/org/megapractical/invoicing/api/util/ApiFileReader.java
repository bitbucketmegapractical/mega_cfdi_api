package org.megapractical.invoicing.api.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.ICSVParser;
import com.opencsv.exceptions.CsvException;

import lombok.val;

public final class ApiFileReader {
	
	private ApiFileReader() {		
	}
	
	public static List<String[]> readFile(String path) {
		//##### Cargando el fichero
		File file = new File(path);
		return readFile(file);
	}
	
	public static List<String[]> readFile(File file) {
		try {
			//##### Leyendo fichero
			CSVParser parser = new CSVParserBuilder()
									.withSeparator('|')
									.withQuoteChar(ICSVParser.DEFAULT_QUOTE_CHARACTER)
									.withIgnoreQuotations(false)
								    .build();
			
			CSVReader reader = new CSVReaderBuilder(new FileReader(file))
									.withCSVParser(parser)
									.build();
	    	
	    	//##### Contenido del fichero
	    	val entries = reader.readAll();
	    	
	    	//##### Cerrando el fichero
	    	reader.close();
	    	
	    	return entries;
		} catch (IOException | CsvException e) {
			return new ArrayList<>();
		}
	}
	
}