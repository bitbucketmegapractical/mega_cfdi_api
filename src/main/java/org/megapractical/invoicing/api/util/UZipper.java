package org.megapractical.invoicing.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import lombok.val;

public final class UZipper {

	private static final int BUFFER_SIZE = 1024;
	private static List<String> filesListInDir = new ArrayList<>();
	
	private UZipper() {		
	}
	
	/**
	 * This method zips the directory
	 * 
	 * @param dir
	 * @param zipDirName
	 */
	public static void zipping(File dir, String zipDirName) {
        try {
        	// Cleaning list
        	filesListInDir = new ArrayList<>();
        	
        	// Zip files one by one
        	populateFilesList(dir);
            
            // Create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            try (val zos = new ZipOutputStream(fos)) {
            	for (String filePath : filesListInDir) {
            		// For ZipEntry we need to keep only relative file path, so we used substring on absolute path
            		val ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
            		zos.putNextEntry(ze);
            		
            		// Read the file and write to ZipOutputStream
            		try (val fis = new FileInputStream(filePath);) {
            			byte[] buffer = new byte[BUFFER_SIZE];
            			
            			int len;
            			while ((len = fis.read(buffer)) > 0) {
            				zos.write(buffer, 0, len);
            			}
            			
            			zos.closeEntry();
            		}
            	}
            }
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
    /**
     * This method populates all the files in a directory to a List
     * 
     * @param dir
     * @throws IOException
     */
    private static void populateFilesList(File dir) throws IOException {
        val files = dir.listFiles();
        for (val file : files) {
            if (file.isFile()) {
                filesListInDir.add(file.getAbsolutePath());
            } else {
                populateFilesList(file);
            }
        }
    }
}