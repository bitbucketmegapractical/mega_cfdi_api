package org.megapractical.invoicing.api.util;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import lombok.val;

/*
 * 	@author Maikel Guerra Ferrer
 * 	@location México DF
 * 	@company Megapractical S.A de C.V	
 */

public final class UDateTime {

	private static Date today;
	private static Calendar calendarToday = Calendar.getInstance();
	private static LocalDate localDateToday;
	private static Integer month;
	
	private UDateTime() {		
	}
	
	public static void init(){
		month = calendarToday.get(Calendar.MONTH) + 1;
		localDateToday = LocalDate.of(calendarToday.get(Calendar.YEAR), month, calendarToday.get(Calendar.DAY_OF_MONTH));
	}
	
	public static String format(LocalDateTime ldt) {
		if (ldt != null) {
			val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
			return ldt.format(formatter);
		}
		return null;
	}
	
	public static String format(LocalDate ld) {
		if (ld != null) {
			val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			return ld.format(formatter);
		}
		return null;
	}
	
	public static String format(LocalTime lt) {
		if (lt != null) {
			val formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
			return lt.format(formatter);
		}
		return null;
	}
	
	public static String formatShort(LocalTime lt) {
		if (lt != null) {
			val formatter = DateTimeFormatter.ofPattern("hh:mm a");
			return lt.format(formatter);
		}
		return null;
	}
	
	public static String clean(LocalDateTime ldt) {
		if (ldt != null) {
			val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
			return clean(ldt.format(formatter));
		}
		return null;
	}
	
	public static String clean(String dateStr) {
		if (dateStr != null) {
			return dateStr.replace("T", "").replace("-", "").replace(":", "");
		}
		return null;
	}
	
	public static Calendar calendarFromDate(Date date){
		try {
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			
			return calendar;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer yearOfDate(Date date){
		try {
			
			Calendar calendar = calendarFromDate(date);
			Integer month = calendar.get(Calendar.YEAR);
			return month;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer monthOfDate(Date date){
		try {
			
			Calendar calendar = calendarFromDate(date);
			Integer month = calendar.get(Calendar.MONTH) + 1;
			return month;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer dayOfDate(Date date){
		try {
			
			Calendar calendar = calendarFromDate(date);
			Integer month = calendar.get(Calendar.DAY_OF_MONTH);
			return month;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String monthOfDate(Date date, Boolean monthName, Boolean abbreviation){
		try {
			
			Calendar calendar = calendarFromDate(date);
			Integer month = calendar.get(Calendar.MONTH) + 1;
			
			String value = month.toString();
			
			if(!monthName && !abbreviation){
				return value;
			}else if(monthName || abbreviation){
				switch (value) {
					case "1":
						value = monthName ? "ENERO" : (abbreviation ? "ENE" : value);
						break;
					case "2":
						value = monthName ? "FEBRERO" : (abbreviation ? "FEB" : value);
						break;
					case "3":
						value = monthName ? "MARZO" : (abbreviation ? "MAR" : value);
						break;
					case "4":
						value = monthName ? "ABRIL" : (abbreviation ? "ABR" : value);
						break;
					case "5":
						value = monthName ? "MAYO" : (abbreviation ? "MAY" : value);
						break;
					case "6":
						value = monthName ? "JUNIO" : (abbreviation ? "JUN" : value);
						break;
					case "7":
						value = monthName ? "JULIO" : (abbreviation ? "JUL" : value);
						break;
					case "8":
						value = monthName ? "AGOSTO" : (abbreviation ? "AGO" : value);
						break;
					case "9":
						value = monthName ? "SEPTIEMBRE" : (abbreviation ? "SEP" : value);
						break;
					case "10":
						value = monthName ? "OCTUBRE" : (abbreviation ? "OCT" : value);
						break;
					case "11":
						value = monthName ? "NOVIEMBRE" : (abbreviation ? "NOV" : value);
						break;
					case "12":
						value = monthName ? "DICIEMBRE" : (abbreviation ? "DIC" : value);
						break;
					default:
						break;
				}
				return value;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String monthOfDate(Integer month, Boolean abbreviation){
		try {
			
			String value = null;
			
			switch (month) {
				case 1:
					value = !abbreviation ? "ENERO" : "ENE";
					break;
				case 2:
					value = !abbreviation ? "FEBRERO" : "FEB";
					break;
				case 3:
					value = !abbreviation ? "MARZO" : "MAR";
					break;
				case 4:
					value = !abbreviation ? "ABRIL" : "ABR";
					break;
				case 5:
					value = !abbreviation ? "MAYO" : "MAY";
					break;
				case 6:
					value = !abbreviation ? "JUNIO" : "JUN";
					break;
				case 7:
					value = !abbreviation ? "JULIO" : "JUL";
					break;
				case 8:
					value = !abbreviation ? "AGOSTO" : "AGO";
					break;
				case 9:
					value = !abbreviation ? "SEPTIEMBRE" : "SEP";
					break;
				case 10:
					value = !abbreviation ? "OCTUBRE" : "OCT";
					break;
				case 11:
					value = !abbreviation ? "NOVIEMBRE" : "NOV";
					break;
				case 12:
					value = !abbreviation ? "DICIEMBRE" : "DIC";
					break;
				default:
					break;
			}
			
			return value;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static LocalDate localDateFromDate(Date date) {
		try {
			
			Calendar calendar = calendarFromDate(date);
			Integer month = calendar.get(Calendar.MONTH) + 1;
			LocalDate localDate = LocalDate.of(calendar.get(Calendar.YEAR), month, calendar.get(Calendar.DAY_OF_MONTH));
			
			return localDate;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static LocalTime localTimeFromDate(Date date) {
		try {
			
			Calendar calendar = calendarFromDate(date);
			LocalTime localTime = LocalTime.of(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
			
			return localTime;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static LocalDateTime localDateTimeFromDate(Date date) {
		try {
			
			LocalDate localDate = localDateFromDate(date);
			LocalTime localTime = localTimeFromDate(date);
			LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
			
			return localDateTime;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static LocalDate localDateFromString(String stringDate, String pattern){
		try {
			
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
			LocalDate localDate = LocalDate.parse(stringDate, dateTimeFormatter);
			
			return localDate;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static LocalDateTime localDateTime(LocalDate localDate, LocalTime localTime) {
		try {
			
			LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
			
			return localDateTime;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date dateFormLocalDate(LocalDate localDate){
		try {
			
			Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
			Date date = Date.from(instant);
			
			return date;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date dateFromLocalDateTime(LocalDateTime localDateTime) {
		try {
			
			Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
			Date date = Date.from(instant);
			
			return date;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean isBeforeLocalDate(Date dateOne, Date dateTwo) {
		try {
			
			LocalDate dateOneLocalDate = localDateFromDate(dateOne);
			LocalDate dateTwoLocaldate = localDateFromDate(dateTwo);
			
			return dateOneLocalDate.isBefore(dateTwoLocaldate);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isAfterLocalDate(Date dateOne, Date dateTwo) {
		try {
			
			LocalDate dateOneLocalDate = localDateFromDate(dateOne);
			LocalDate dateTwoLocaldate = localDateFromDate(dateTwo);
			
			return dateOneLocalDate.isAfter(dateTwoLocaldate);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isEqualLocalDate(Date dateOne, Date dateTwo) {
		try {
			
			LocalDate dateOneLocalDate = localDateFromDate(dateOne);
			LocalDate dateTwoLocaldate = localDateFromDate(dateTwo);
			
			return dateOneLocalDate.isEqual(dateTwoLocaldate);
						
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isBeforeLocalTime(Date dateOne, Date dateTwo) {
		try {
			
			LocalTime timeOne = localTimeFromDate(dateOne);
			LocalTime timeTwo = localTimeFromDate(dateTwo);
			
			return timeOne.isBefore(timeTwo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isAfterLocalTime(Date dateOne, Date dateTwo) {
		try {
			
			LocalTime timeOne = localTimeFromDate(dateOne);
			LocalTime timeTwo = localTimeFromDate(dateTwo);
			
			return timeOne.isAfter(timeTwo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isEqualLocalTime(Date dateOne, Date dateTwo) {
		try {
			
			LocalTime timeOne = localTimeFromDate(dateOne);
			LocalTime timeTwo = localTimeFromDate(dateTwo);
			
			return timeOne.equals(timeTwo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String formattedStringDateFromLocalDate(LocalDate localDate){
		try {
			
			return localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Duration durationBetweenDates(Date dateOne, Date dateTwo){
		try {
			
			Duration duration = Duration.between(localDateTimeFromDate(dateOne), localDateTimeFromDate(dateTwo));
			return duration;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer durationBetweenDatesInDates(Date dateOne, Date dateTwo){
		try {
			
			Duration duration = durationBetweenDates(dateOne, dateTwo);
			Long durationInDays = duration.toDays();
			Integer days = durationInDays.intValue(); 
			
			return days;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer durationBetweenDatesInHours(Date dateOne, Date dateTwo){
		try {
			
			Duration duration = durationBetweenDates(dateOne, dateTwo);
			Long durationInHours = duration.toHours();
			Integer hours = durationInHours.intValue(); 
			
			return hours;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer durationBetweenDatesInMinutes(Date dateOne, Date dateTwo){
		try {
			
			Duration duration = durationBetweenDates(dateOne, dateTwo);
			Long durationInMinutes = duration.toMinutes();
			Integer minutes = durationInMinutes.intValue(); 
			
			return minutes;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer durationBetweenDatesInMilliseconds(Date dateOne, Date dateTwo){
		try {
			
			Duration duration = durationBetweenDates(dateOne, dateTwo);
			Long durationInMilliseconds = duration.toMillis();
			Integer milliseconds = durationInMilliseconds.intValue(); 
			
			return milliseconds;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer elapsedTime(Date date, Date time, String type){
		try {
			
			LocalDateTime ldt = localDateTime(localDateFromDate(date), localTimeFromDate(time));
			Duration duration = Duration.between(ldt, localDateTimeFromDate(getToday()));
			
			Long durationValue;			
			
			switch (type) {
				case "days":
					durationValue = duration.toDays();
					break;
				case "hours":
					durationValue = duration.toHours();
					break;
				default:
					durationValue = duration.toMinutes();
					break;
			}
			Integer result = durationValue.intValue();
			
			return result;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Date dateUTC(){
		try {
			
	    	ZonedDateTime zdt = ZonedDateTime.now(ZoneOffset.UTC);
	    	LocalDateTime ldt = zdt.toLocalDateTime();
			Date now = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
			
			return now;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Boolean isLeapYear(Integer year){
		try {
			
			GregorianCalendar calendar = new GregorianCalendar();
			return calendar.isLeapYear(year);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/* Getters and Setters */
	public static LocalDate getLocalDateToday() {
		init();
		return localDateToday;
	}

	public static void setLocalDateToday(LocalDate localDateToday) {
		UDateTime.localDateToday = localDateToday;
	}

	public static Date getToday() {
		today = new Date();
		return today;
	}		
}
