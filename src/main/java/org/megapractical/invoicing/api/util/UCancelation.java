package org.megapractical.invoicing.api.util;

import java.io.IOException;
import java.io.OutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class UCancelation {

	public static void estampaPDF(String fileNameOrigen, OutputStream outputStream) {
		try {			
			
		     PdfReader pdfReader = new PdfReader(fileNameOrigen);
		     PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);
		 
		     float alto_pagina = 0;
		     float ancho_pagina = 0;
		     float angulo_marca_agua = 0;
		 
		     // Por cada una de las páginas
		     for(int i = 1; i <= pdfReader.getNumberOfPages(); i ++){
		 
		          PdfContentByte content = pdfStamper.getOverContent(i);
		          PdfContentByte content_fondo = pdfStamper.getUnderContent(i);
		 
		          // Comprobamos si la página está en vertical o en horizontal
		          if((pdfReader.getPageRotation(i) == 0) || (pdfReader.getPageRotation(i) == 180)){
		               alto_pagina = pdfReader.getPageSize(i).getHeight();
		               ancho_pagina = pdfReader.getPageSize(i).getWidth();
		               angulo_marca_agua = 60;
		          }else{ // Si no es vertical, será apaisada
		               alto_pagina = pdfReader.getPageSize(i).getWidth();
		               ancho_pagina = pdfReader.getPageSize(i).getHeight();
		               angulo_marca_agua = 30;
		          }
		 
		         // Costadillo
		         String texto_costadillo = "CFDI CANCELADO";
		 
		         // Marca de agua
		         String marca_agua = "CFDI CANCELADO";
		 
		 
		         Phrase frase = new Phrase(texto_costadillo, FontFactory.getFont(BaseFont.HELVETICA, 8));
		         ColumnText.showTextAligned(content, Element.ALIGN_CENTER, frase, 30, alto_pagina/2, 90);
		 
		         // Marca de agua en color gris, rotada y con relleno blanco
		         Phrase frase_marca_agua= new Phrase(marca_agua, FontFactory.getFont(BaseFont.HELVETICA, 85, Font.BOLD));
		         content_fondo.setTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
		         content_fondo.setColorStroke(new BaseColor(0xD7, 0xD7, 0xD7));
		         content_fondo.setColorFill(BaseColor.RED);
		 
		         ColumnText.showTextAligned(content_fondo, Element.ALIGN_CENTER, frase_marca_agua,ancho_pagina/2 , alto_pagina/2, angulo_marca_agua);
		 
		      }
		 
		      pdfStamper.close();
		 
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
