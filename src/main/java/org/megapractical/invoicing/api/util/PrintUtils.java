package org.megapractical.invoicing.api.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class PrintUtils {
	
	private PrintUtils() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	private static final String LINE = "-----------------------------------------------------------------------------------------------------------";
	
	public static final void print() {
		System.out.println();
	}
	
	public static final void print(String str) {
		System.out.println(str);
	}
	
	public static final void line() {
		System.out.println(LINE);
	}
	
	public static final void info(String str) {
		System.out.println(LINE);
  		System.out.println(String.format("%s %s", consoleTime(), str));
	}
	
	public static final void infoLines(String str) {
		System.out.println(LINE);
		System.out.println(String.format("%s %s", consoleTime(), str));
		System.out.println(LINE);
	}
	
	public static final void lastInfo(String str) {
		System.out.println(String.format("%s %s", consoleTime(), str));
		System.out.println(LINE);
	}
	
	private static String consoleTime() {
		return LocalDateTime.now().format(formatter);
	}
	
}