package org.megapractical.invoicing.api.util;

import java.io.IOException;
import java.util.Properties;

import lombok.val;
import lombok.var;

/**
 *
 * @author Maikel Guerra Ferrer
 */
public final class UProperties {

	private UProperties() {
		throw new IllegalStateException("This is a utility class and cannot be instantiated");
	}
	
	public static String getTmpDir() {
		return getApiPropertyValue("application.tmp.dir");
    }
	
	public static String getApplicationPropertieValue(String key) {
		return getApiPropertyValue(key);
	}
	
    public static boolean getApplicationPrintValues() {
    	val printValues = getApiPropertyValue("application.print.values");
    	if (printValues != null) {
    		return printValues.equals("true");
    	}
    	return false;
    }

    public static String getOperationVersion() {
    	return getApiPropertyValue("cfdi.operation.version");
    }
    
    public static String getOperationName(String value) {
    	String propertyCode = null;
    	switch (value) {
    	case "stamp":
    		propertyCode = "cfdi.stamp.operation.name";
    		break;
    	case "cancel":
    		propertyCode = "cfdi.cancel.operation.name";
    		break;
    	case "cancelaCFD":
    		propertyCode = "cfdi.cancelacfd.operation.name";
    		break;	
    	case "code":
    		propertyCode = "cfdi.confirmation.code.operation.name";
    		break;
    	case "validate":
    		propertyCode = "cfdi.validation.operation.name";
    		break;
    	case "status":
    		propertyCode = "cfdi.status.operation.name";
    		break;
    	case "ret_stamp":
    		propertyCode = "ret.operation.name";
    		break;	
    	default:
    		break;
    	}
    	
    	if (propertyCode != null) {
    		return getApiPropertyValue(propertyCode);
    	}
        return null;
    }

    public static final String getApiPropertyValue(String key) {
		if (key != null) {
			val apiEnvironment = getApiEnvironment();
			var loadPropertiesFrom = "config/api-dev.config.properties";
			if (apiEnvironment.equals("qa")) {
				loadPropertiesFrom = "config/api-qa.config.properties";
			} else if (apiEnvironment.equals("prod")) {
				loadPropertiesFrom = "config/api-prod.config.properties";
			}
			
			var propertyValue = getPropertyByEnvironment(key, loadPropertiesFrom);
			if (propertyValue == null) {
				propertyValue = getPropertyByDefaultEnvironment(key);
			}
			return propertyValue;
		}
		return key;
	}
    
    private static final String getApiEnvironment() {
    	return getPropertyByDefaultEnvironment("cfdi.environment");
    }
    
    private static final String getPropertyByDefaultEnvironment(String key) {
    	val env = "config/api.config.properties";
    	return getPropertyByEnvironment(key, env);
    }
    
    private static final String getPropertyByEnvironment(String key, String env) {
		try {
			
			if (key != null && env != null) {
				val properties = new Properties();
				properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(env));
				return properties.getProperty(key);
			}
			
		} catch (IOException e) {
			System.out.println(String.format("#getPropertyValue error - %s", e.getMessage()));
		}
		return null;
	}
    
}