package org.megapractical.invoicing.api.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.List;

import org.megapractical.invoicing.api.wrapper.ApiFileError;
import org.megapractical.invoicing.api.wrapper.ApiPaymentError;
import org.megapractical.invoicing.api.wrapper.ApiPayrollError;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

import lombok.val;

public final class ApiFileErrorWritter {
	
	private ApiFileErrorWritter() {		
	}
	
	public static void writeError(String path, List<? extends ApiFileError> errors) {
		try {

			// Creando fichero de error si no existe
			File file = errorFile(path);
			if (file != null) {
				// Escribiendo el nuevo fichero
				try (val writer = new CSVWriter(new FileWriter(file), '|', ICSVWriter.NO_QUOTE_CHARACTER,
						ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END)) {
					for (val item : errors) {
						// Contenido a escribir
						val sentence = new StringBuilder();
						
						entryInfo(item, sentence);
						fieldInfo(item, sentence);
						receiverInfo(item, sentence);
						errorInfo(item, sentence);

						sentence.append(
								"%n----------------------------------------------------------------------------------------------------------------------");

						if (!UValidator.isNullOrEmpty(sentence)) {
							String[] errorSet = new String[] { sentence.toString() + "%n" };
							writer.writeNext(errorSet);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static File errorFile(String path) {
		try {
			val file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			} else {
				Files.delete(Paths.get(path));
			}
			return file;
		} catch (InvalidPathException | IOException e) {
			return null;
		}
	}
	
	private static void entryInfo(ApiFileError item, final StringBuilder sentence) {
		if (!UValidator.isNullOrEmpty(item.getDescription())) {
			sentence.append(String.format("Descripción del error: %s", item.getDescription()));
		}

		if (!UValidator.isNullOrEmpty(item.getLine())) {
			sentence.append(String.format("%nLínea: %s", item.getLine()));
		}

		val entryName = entryName(item);
		if (!UValidator.isNullOrEmpty(entryName)) {
			sentence.append(String.format("%nTrama: %s", entryName));
		}

		if (!UValidator.isNullOrEmpty(item.getEntryLengthOnFile())) {
			sentence.append(String.format("%nLongitud en fichero: %s", item.getEntryLengthOnFile()));
		}

		if (!UValidator.isNullOrEmpty(item.getEntryLength())) {
			sentence.append(String.format("%nLongitud válida: %s", item.getEntryLength()));
		}
	}
	
	private static void fieldInfo(ApiFileError item, final StringBuilder sentence) {
		if (!UValidator.isNullOrEmpty(item.getFieldPosition())) {
			sentence.append(String.format("%nPosición del campo: %s", item.getFieldPosition()));
		}

		val fieldName = fieldName(item);
		if (!UValidator.isNullOrEmpty(fieldName)) {
			sentence.append(String.format("%nCampo: %s", fieldName));
		}

		if (!UValidator.isNullOrEmpty(item.getFieldValue())) {
			sentence.append(String.format("%nValor en fichero: %s", item.getFieldValue()));
		}

		if (!UValidator.isNullOrEmpty(item.getFieldLength())) {
			sentence.append(String.format("%nLongitud del campo: %s", item.getFieldLength()));
		}

		if (!UValidator.isNullOrEmpty(item.getFieldMinLength())) {
			sentence.append(String.format("%nLongitud mínima del campo: %s", item.getFieldMinLength()));
		}

		if (!UValidator.isNullOrEmpty(item.getFieldMaxLength())) {
			sentence.append(String.format("%nLongitud máxima del campo: %s", item.getFieldMaxLength()));
		}

		if (!UValidator.isNullOrEmpty(item.getFieldLengthOnFile())) {
			sentence.append(String.format("%nLongitud en fichero: %s", item.getFieldLengthOnFile()));
		}

		if (!UValidator.isNullOrEmpty(item.getFieldRegularExpression())) {
			sentence.append(String.format("%nExpresión regular de validación: %s", item.getFieldRegularExpression()));
		}
	}

	private static void receiverInfo(ApiFileError item, final StringBuilder sentence) {
		if (!UValidator.isNullOrEmpty(item.getReceiverRfc())) {
			sentence.append(String.format("%nRFC receptor: %s", item.getReceiverRfc()));
		}

		if (!UValidator.isNullOrEmpty(item.getReceiverCurp())) {
			sentence.append(String.format("%nCURP receptor: %s", item.getReceiverCurp()));
		}

		if (!UValidator.isNullOrEmpty(item.getReceiverName())) {
			sentence.append(String.format("%nNombre o razón social: %s", item.getReceiverName()));
		}
	}

	private static void errorInfo(ApiFileError item, final StringBuilder sentence) {
		if (!UValidator.isNullOrEmpty(item.getStampErrorCode())) {
			sentence.append(String.format("%nCódigo del error: %s", item.getStampErrorCode()));
		}

		if (!UValidator.isNullOrEmpty(item.getValidationErrorCode())) {
			sentence.append(String.format("%nCódigo del error: %s", item.getValidationErrorCode()));
		}

		if (!UValidator.isNullOrEmpty(item.getStampErrorMessage())) {
			sentence.append(String.format("%nMensaje del error: %s", item.getStampErrorMessage()));
		}

		if (!UValidator.isNullOrEmpty(item.getValidationErrorMessage())) {
			sentence.append(String.format("%nMensaje del error: %s", item.getValidationErrorMessage()));
		}
	}
	
	private static String entryName(ApiFileError item) {
		if (item instanceof ApiPayrollError) {
			val entry = ((ApiPayrollError) item).getEntry();
			if (!UValidator.isNullOrEmpty(entry) && !UValidator.isNullOrEmpty(entry.getNombre())) {
				return entry.getNombre();
			}
		} else if (item instanceof ApiPaymentError) {
			val entry = ((ApiPaymentError) item).getEntry();
			if (!UValidator.isNullOrEmpty(entry) && !UValidator.isNullOrEmpty(entry.getNombre())) {
				return entry.getNombre();
			}
		}
		return null;
	}
	
	private static String fieldName(ApiFileError item) {
		if (item instanceof ApiPayrollError) {
			val field = ((ApiPayrollError) item).getField();
			if (!UValidator.isNullOrEmpty(field) && !UValidator.isNullOrEmpty(field.getNombre())) {
				return field.getNombre();
			}
		} else if (item instanceof ApiPaymentError) {
			val field = ((ApiPaymentError) item).getField();
			if (!UValidator.isNullOrEmpty(field) && !UValidator.isNullOrEmpty(field.getNombre())) {
				return field.getNombre();
			}
		}
		return null;
	}
	
}