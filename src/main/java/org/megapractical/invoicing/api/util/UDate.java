package org.megapractical.invoicing.api.util;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.dbcp2.BasicDataSource;

import lombok.val;

public final class UDate {

	private UDate() {		
	}
	
	// XMLGregorianCalendar
	public static XMLGregorianCalendar xmlGregorianCalendar() {
        try {
        	
        	XMLGregorianCalendar xmlCalendar;
        	GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());
        	xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        	xmlCalendar.setFractionalSecond(BigDecimal.ZERO);
            return xmlCalendar;
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
	
    // XMLGregorianCalendar
    public static XMLGregorianCalendar xmlGregorianCalendar(String date) {
        try {
            
        	if(date == null)
            	return null;
        	
        	XMLGregorianCalendar xmlCalendar;
            GregorianCalendar calendar = new GregorianCalendar();
            
            String[] datesplit = date.split("-");
            Integer year = Integer.parseInt(datesplit[0]);
            Integer month = Integer.parseInt(datesplit[1]) - 1;
            Integer day = Integer.parseInt(datesplit[2]);
            calendar.set(year, month, day);
            
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            xmlCalendar.setFractionalSecond(BigDecimal.ZERO);
            return xmlCalendar;
            
        } catch (DatatypeConfigurationException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    // XMLGregorianCalendar formatted with 'yyyy-MM-dd' 
    public static XMLGregorianCalendar xmlGregorianCalendarFormat(String strDate){
    	try {
    		
    		try {
    			String[] splitStrDate = strDate.split("/");
    			if(splitStrDate[2] != null && splitStrDate[1] != null && splitStrDate[0] != null){
					strDate = splitStrDate[2] + "-" + splitStrDate[1] + "-" + splitStrDate[0];
				}
			} catch (Exception e) {}
    		
    		String formatter = "yyyy-MM-dd";
    		
    		Date date = null;
    		SimpleDateFormat sdf = null;
    		DateFormat df = null;
    		
    		boolean isValid = false;    		
    		try {
    			sdf = new SimpleDateFormat(formatter);
    			date = sdf.parse(strDate);
    			df = new SimpleDateFormat(formatter);
    			isValid = true;
			} catch (Exception e) {
				isValid = false;
			}
    		
    		if(!isValid)
    			return null;
    		
			XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(df.format(date));
			return xmlCalendar;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    // XMLGregorianCalendar formatted with specified formatter
    public static XMLGregorianCalendar xmlGregorianCalendarFormat(String strDate, String formatter){
    	try {
    		
			SimpleDateFormat sdf = new SimpleDateFormat(formatter);
			Date date = sdf.parse(strDate);
			DateFormat df = new SimpleDateFormat(formatter);
			XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(df.format(date));
			return xmlCalendar;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    // String from XMLGregorianCalendar formatted with specified formatter
    public static String xmlGregorianCalendarString(String strDate, String formatter){
    	try {
			
    		SimpleDateFormat sdf = new SimpleDateFormat(formatter);
			Date date = sdf.parse(strDate);
			DateFormat df = new SimpleDateFormat(formatter);
			XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(df.format(date));
			return xmlCalendar.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    } 
    
    // Date from XMLGregorianCalendar
    public static Date dateFromXMLGregorianCalendar(XMLGregorianCalendar date) {
    	try {
			
    		GregorianCalendar gc = date.toGregorianCalendar();
            return gc.getTime();
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
    
    public static String formattedSingleDate(Date date){
    	try {
			
    		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    		return sdf.format(date);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String formattedTime(Date date){
    	try {
			
    		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    		return sdf.format(date);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String formattedShortTime(Date date){
    	try {
			
    		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
    		return sdf.format(date);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static String formattedDateTime(){
    	try {
			
    		Date now = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm a");
    		return sdf.format(now);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    // String formatted to yyyy-MM-dd'T'HH:mm:ss
    public static String formattedDate(){
    	try {
			
    		Date now = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    		return sdf.format(now);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    // String from Date formatted to yyyy-MM-dd'T'HH:mm:ss
    public static String formattedDate(Date date) {
    	try {
    		if (date != null) {
    			val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    			return sdf.format(date);
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
    
    public static String formattedDate(Date date, String format){
    	try {
    		if (date != null && format != null) {
    			val sdf = new SimpleDateFormat(format);
    			return sdf.format(date);
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
    
    // String form Calendar formatted to yyyy-MM-dd'T'HH:mm:ss
    public static String formattedDate(Calendar calendar){
    	try {
    		
    		Date date = calendar.getTime();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    		return sdf.format(date);
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    // Date from String with format yyyy-MM-dd'T'HH:mm:ss
    public static Date formattedDate(String dateStr){
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			return sdf.parse(dateStr);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
    }
    
    // Calendar from String with format yyyy-MM-dd'T'HH:mm:ss
    public static Calendar calendarFromStringFormatted(String dateStr){
    	try {
			
    		Calendar calendar = Calendar.getInstance();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
    		calendar.setTime(sdf.parse(dateStr));
    		return calendar;
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    public static Date formattedDate(String dateStr, String format){
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.parse(dateStr);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
    }
    
	public static Date date(String dateStr) {
		try {
			if (dateStr != null) {
				val formatter = new SimpleDateFormat("yyyy-MM-dd");
				return formatter.parse(dateStr);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
    
	public static String date() {
		try {

			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			return sdf.format(date);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
    
	public static String getDateByZipCode(String zipCode) {
		val url = UProperties.getApiPropertyValue("cfdi.datasource.url");
		val user = UProperties.getApiPropertyValue("cfdi.datasource.username");
		val passwd = UProperties.getApiPropertyValue("cfdi.datasource.password");

		try (val dataSource = new BasicDataSource()) {
			dataSource.setDriverClassName("org.postgresql.Driver");
			dataSource.setUrl(url);
			dataSource.setUsername(user);
			dataSource.setPassword(passwd);
			dataSource.setValidationQuery("SELECT 1");

			
			val query = "SELECT public.get_cfd_date('" + zipCode + "')";

			try (val connection = dataSource.getConnection();
					val stmt = connection.createStatement();
					val resultSet = stmt.executeQuery(query);) {
				while (resultSet.next()) {
					return resultSet.getString(1);
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return formattedDate(new Date());
	}
    
}