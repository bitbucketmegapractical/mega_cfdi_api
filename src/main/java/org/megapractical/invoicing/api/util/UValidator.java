package org.megapractical.invoicing.api.util;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.megapractical.invoicing.api.validation.BigDecimalValidationRules;
import org.megapractical.invoicing.api.validation.BigDecimalValidator;

import lombok.val;

public final class UValidator {
	
	private UValidator() {		
	}
	
	public static boolean isNullOrEmpty(Object value){
		if(value == null || value.equals("-")){
			return true;
		}
		value = value.toString();
		return value instanceof String && ((String) value).isEmpty();
	}

	public static boolean isDouble(Object value){
		try {
			
			if(value == null)
				return false;
			
			value = value.toString().trim();
			Double.parseDouble((String) value);
			return true;
			
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isInteger(Object value) {
        Pattern patron = Pattern.compile("^[0-9]*$");
        Matcher match = patron.matcher(value.toString());
        return match.find();
    }
	
	public static boolean isBigDecimal(Object value){
		try {
		
			BigDecimalValidator validator = new BigDecimalValidator();
			BigDecimalValidationRules.Builder builder = new BigDecimalValidationRules.Builder();
			BigDecimalValidationRules rules = new BigDecimalValidationRules(builder);
			
			value = value.toString().trim();
			BigDecimal bigDecimal = UValue.bigDecimalStrict((String) value);
			return validator.validate(bigDecimal, rules).isValid();
			
		} catch (Exception e) {
			return false;
		}		
	}
	
	public static boolean isValidDecimalPart(BigDecimal value, Integer decimal) {
		try {

			if (isNullOrEmpty(value)) {
				return false;
			}

			val decimalResult = UTools.decimalValue(value.toString());
			return decimalResult == decimal;

		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isValidTime(String time){
		try {
			
			String[] timeSplitted = time.split(Pattern.quote(":"));
			
			Integer hour = UValue.integerValue(timeSplitted[0]);
			if(hour == null || (hour < 0 || hour > 23))
				return false;
			
			Integer minute = UValue.integerValue(timeSplitted[1]);
			if(minute == null || (minute < 0 || minute > 59))
				return false;
			
			Integer second = UValue.integerValue(timeSplitted[2]);
			if(second == null || (second < 0 || second > 59))
				return false;
			
			return true;
			
		} catch (Exception e) {
			return false;
		}		
	}
	
	/*public static boolean isValidDecimalPart(BigDecimal value, Integer decimal){
		try {
			
			if(isNullOrEmpty(value))
				return false;
			
			BigDecimal onlyDecimalPart = value.subtract(value.setScale(0, RoundingMode.FLOOR)).movePointRight(value.scale());
			Integer decimalPartInteger = onlyDecimalPart.intValue();
			String decimalPartString = decimalPartInteger.toString();
			Integer decimalResult = decimalPartString.length(); 
			return  decimalResult == decimal ? true : false;
			
		} catch (Exception e) {
			return false;
		}
	}*/
}
