package org.megapractical.invoicing.api.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class UCalculation {

	private static final RoundingMode roundingMode = RoundingMode.HALF_EVEN;
	
	public static BigDecimal percentage(BigDecimal amount, BigDecimal percentage){
		try {
			
			// IVA = TASA IVA / 100 * IMPORTE
			BigDecimal iva = amount.multiply(percentage.divide(new BigDecimal(100), 2, roundingMode));
			return iva;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal ivaRetenido(BigDecimal iva){
		try {
			
			// IVA RETENIDO = 2/3 IVA
			BigDecimal ivaRetenido = new BigDecimal(2).divide(new BigDecimal(3), 2, roundingMode).multiply(iva);
			return ivaRetenido;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal ieps(BigDecimal importe, BigDecimal tasaIeps){
		try {
			
			// IEPS = TASA IEPS / 100 * IMPORTE
			BigDecimal ieps = importe.multiply(tasaIeps.divide(new BigDecimal(100), 2, roundingMode));
			return ieps;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal isr(BigDecimal importe, BigDecimal tasaIsr){
		try {
			
			// ISR = TASA ISR / 100 * IMPORTE
			BigDecimal isr = importe.multiply(tasaIsr.divide(new BigDecimal(100), 2, roundingMode));
			return isr;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal amount(BigDecimal quantity, BigDecimal unitValue){
		try {
			
			// IMPORTE = (CANTIDAD * VALOR UNITARIO)
			BigDecimal amount = quantity.multiply(unitValue);
			return amount;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal amountWithDiscount(BigDecimal quantity, BigDecimal unitValue, BigDecimal discount){
		try {
			
			// IMPORTE = (CANTIDAD * VALOR UNITARIO) * (1 - DESCUENTO / 100)
			BigDecimal amount = discount != null ? quantity.multiply(unitValue).multiply(new BigDecimal(1).subtract(discount.divide(new BigDecimal(100), 2, roundingMode))) : quantity.multiply(unitValue);
			return amount;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal discount(BigDecimal cantidad, BigDecimal valorUnitario, BigDecimal descuento){
		try {
			
			// DESCUENTO = (CANTIDAD * VALOR UNITARIO) / 100 * DESCUENTO			
			BigDecimal discount = cantidad.multiply(valorUnitario).multiply(descuento).divide(new BigDecimal(100));
			return discount;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BigDecimal total(BigDecimal subTotal, BigDecimal discount, BigDecimal transferred, BigDecimal withheld, Integer decimal){
		try {
			String valueNull = null;
			
			if(transferred == null)
				transferred = UValue.bigDecimal(valueNull, decimal);
			
			if(withheld == null)
				withheld = UValue.bigDecimal(valueNull, decimal);
			
			// TOTAL = SUBTOTAL + IMPUESTOS TRASLADADOS – IMPUESTOS RETENIDOS - DESCUENTO
			BigDecimal total = !UValidator.isNullOrEmpty(discount) ? subTotal.add(transferred).subtract(withheld).subtract(discount) : subTotal.add(transferred).subtract(withheld);
			return total;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
