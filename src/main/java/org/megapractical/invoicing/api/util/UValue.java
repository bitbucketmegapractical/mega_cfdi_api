package org.megapractical.invoicing.api.util;

/*
 * 	@author Maikel Guerra Ferrer
 */

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.UUID;

import lombok.val;
import lombok.var;

public final class UValue {

	private UValue() {
	}

	public static String uuid() {
		return UUID.randomUUID().toString();
	}

	public static String stringValue(String str) {
		if (!UValidator.isNullOrEmpty(str)) {
			return str.trim();
		}
		return null;
	}
	
	public static String stringValue(Object str) {
		if (!UValidator.isNullOrEmpty(str)) {
			return str.toString().trim();
		}
		return null;
	}
	
	public static String stringSafeValue(String str) {
		if (!UValidator.isNullOrEmpty(str)) {
			return str.trim();
		}
		return "";
	}

	public static String stringValueSingleReplace(String str) {
		str = singleReplace(str);
		if (str != null) {
			return str.trim();
		}
		return null;
	}

	public static String stringValueUppercase(String str) {
		if (!UValidator.isNullOrEmpty(str)) {
			return str.toUpperCase().trim();
		}
		return null;
	}

	public static Double doubleValue(String doubleStr) {
		try {

			val df = new DecimalFormat("#,###.00");

			if (!UValidator.isNullOrEmpty(doubleStr)) {
				doubleStr = doubleStr.trim();
			}

			if (UValidator.isNullOrEmpty(doubleStr)) {
				val value = df.format(0);
				return Double.parseDouble(value);
			}

			val doubleValue = new Double(doubleStr.replace(",", ""));
			val value = df.format(doubleValue);
			return Double.parseDouble(value.replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Double doubleValueStrict(String doubleStr) {
		if (UValidator.isNullOrEmpty(doubleStr)) {
			return null;
		}
		return doubleValue(doubleStr);
	}

	public static Double doubleValue(BigDecimal bigdecimal) {
		if (bigdecimal != null) {
			return bigdecimal.doubleValue();
		}
		return null;
	}

	public static String doubleString(String doubleStr) {
		try {

			val df = new DecimalFormat("#,###.00");

			if (!UValidator.isNullOrEmpty(doubleStr)) {
				doubleStr = doubleStr.trim();
			}

			if (UValidator.isNullOrEmpty(doubleStr) || doubleStr.equals("0") || doubleStr.equals("0.0")
					|| doubleStr.equals(".0") || doubleStr.equals(".00") || doubleStr.equals("0.00")) {
				return "0.00";
			}

			val doubleValue = new Double(doubleStr.replace(",", ""));
			return df.format(doubleValue);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String doubleString(Double doubleValue) {
		try {

			DecimalFormat df = new DecimalFormat("#,###.00");

			if (!UValidator.isNullOrEmpty(doubleValue)) {
				String value = df.format(doubleValue);

				if (value.equals("0") || value.equals("0.0") || value.equals(".0") || value.equals(".00")
						|| value.equals("0.00")) {
					return "0.00";
				} else {
					if (value.startsWith(".")) {
						value = "0" + value;
					}
				}
				return value;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String doubleStringStrict(Double doubleValue) {
		try {

			if (UValidator.isNullOrEmpty(doubleValue))
				return null;

			DecimalFormat df = new DecimalFormat("#,###.00");

			if (doubleValue != null && !UValidator.isNullOrEmpty(doubleValue)) {
				return df.format(doubleValue);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String decimalStringForce(String value) {
		try {

			if (!UValidator.isNullOrEmpty(value)) {
				value = value.trim();
				if (value.startsWith(".")) {
					value = "0" + value;
				}
			}

			if (UValidator.isNullOrEmpty(value) || value.equals("0") || value.equals("0.0") || value.equals(".0")
					|| value.equals(".00") || value.equals("0.00")) {
				return "0.00";
			}
			return value.replace(",", "");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String bigDecimalStringForce(String value) {
		if (value == null || value.equals("") || value.equals("0") || value.equals("0.0") || value.equals(".0")
				|| value.equals(".00") || value.equals("0.00")) {
			return "0.00";
		} else if (value.startsWith(".")) {
			value = "0" + value;
		}
		return value.trim();
	}

	public static String bigDecimalString(BigDecimal bigDecimalValue) {
		String str = null;
		try {

			if (!UValidator.isNullOrEmpty(bigDecimalValue)) {
				str = String.valueOf(bigDecimalValue);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
	
	public static String bigDecimalString(BigDecimal bigDecimalValue, Integer decimal) {
		val bdStr = bigDecimalString(bigDecimalValue);
		return bigDecimalString(bdStr, decimal);
	}
	
	public static String bigDecimalStringForce(BigDecimal bigDecimalValue, Integer decimal) {
		var bdStr = bigDecimalString(bigDecimalValue);
		bdStr = bigDecimalStringForce(bdStr);
		return bigDecimalString(bdStr, decimal);
	}

	public static String bigDecimalString(String bigDecimalStr) {
		try {

			val df = new DecimalFormat("#,###.00");
			
			BigDecimal bigDecimal = null;
			if (UValidator.isNullOrEmpty(bigDecimalStr)) {
				bigDecimal = bigDecimal(bigDecimalStr);
			} else {
				bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));
			}

			return bigDecimalStringForce(df.format(bigDecimal));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String bigDecimalString(String bigDecimalStr, Integer decimal) {
		try {

			val df = getDecimalFormat(decimal);

			var value = df.format(BigDecimal.ZERO);
			if (value.startsWith(".")) {
				value = "0" + value;
			}

			if (bigDecimalStr == null || bigDecimalStr.equals("")) {
				return value;
			}

			val bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));

			value = df.format(bigDecimal);
			if (value.startsWith(".")) {
				value = "0" + value;
			}

			return value;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigDecimal bigDecimal(String bigDecimalStr) {
		try {

			val df = new DecimalFormat("#,###.00");

			if (UValidator.isNullOrEmpty(bigDecimalStr)) {
				String value = df.format(BigDecimal.ZERO);
				return new BigDecimal(value);
			}

			if (!UValidator.isNullOrEmpty(bigDecimalStr)) {
				bigDecimalStr = bigDecimalStr.trim();
			}

			BigDecimal bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));
			String value = df.format(bigDecimal);
			return new BigDecimal(value.replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return BigDecimal.ZERO;
	}

	public static BigDecimal moneyFormat(String bigDecimalStr) {
		if (!UValidator.isNullOrEmpty(bigDecimalStr)) {
			val decimal = UTools.decimals(bigDecimalStr);
			return bigDecimal(bigDecimalStr, decimal);
		}
		return BigDecimal.ZERO;
	}

	public static BigDecimal bigDecimal(String bigDecimalStr, Integer decimal) {
		try {

			val df = getDecimalFormat(decimal);

			if (UValidator.isNullOrEmpty(bigDecimalStr)) {
				bigDecimalStr = "0";
			} else if (!UValidator.isNullOrEmpty(bigDecimalStr)) {
				bigDecimalStr = bigDecimalStr.trim();
			}

			val bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));
			val value = df.format(bigDecimal);
			return new BigDecimal(value.replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigDecimal bigDecimal(Integer decimal) {
		try {

			val df = getDecimalFormat(decimal);
			val bigDecimalStr = "0";
			val bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));
			val value = df.format(bigDecimal);
			return new BigDecimal(value.replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static DecimalFormat getDecimalFormat(Integer decimal) {
		var df = new DecimalFormat("#,###");
		if (decimal == 1) {
			df = new DecimalFormat("#,###.0");
		} else if (decimal == 2) {
			df = new DecimalFormat("#,###.00");
		} else if (decimal == 3) {
			df = new DecimalFormat("#,###.000");
		} else if (decimal == 4) {
			df = new DecimalFormat("#,###.0000");
		} else if (decimal == 5) {
			df = new DecimalFormat("#,###.00000");
		} else if (decimal == 6) {
			df = new DecimalFormat("#,###.000000");
		}
		return df;
	}

	public static BigDecimal bigDecimalStrict(String bigDecimalStr) {
		try {

			if (UValidator.isNullOrEmpty(bigDecimalStr)) {
				return null;
			}

			return new BigDecimal(bigDecimalStr.replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigDecimal bigDecimalStrict(String bigDecimalStr, Integer decimal) {
		try {

			if (UValidator.isNullOrEmpty(bigDecimalStr)) {
				return null;
			}

			DecimalFormat df = null;
			if (decimal == 0) {
				df = new DecimalFormat("#,###");
			} else if (decimal == 1) {
				df = new DecimalFormat("#,###.0");
			} else if (decimal == 2) {
				df = new DecimalFormat("#,###.00");
			} else if (decimal == 3) {
				df = new DecimalFormat("#,###.000");
			} else if (decimal == 4) {
				df = new DecimalFormat("#,###.0000");
			} else if (decimal == 5) {
				df = new DecimalFormat("#,###.00000");
			} else if (decimal == 6) {
				df = new DecimalFormat("#,###.000000");
			}

			if (df != null) {
				val bigDecimal = new BigDecimal(bigDecimalStr.trim().replace(",", ""));
				val value = df.format(bigDecimal);
				return new BigDecimal(value.replace(",", ""));
			}
			return new BigDecimal(bigDecimalStr.trim().replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigDecimal bigDecimalWithSixDecimals(String bigDecimalStr) {
		try {

			val df = new DecimalFormat("#,###.000000");

			if (!UValidator.isNullOrEmpty(bigDecimalStr)) {
				bigDecimalStr = bigDecimalStr.trim();
			}

			if (UValidator.isNullOrEmpty(bigDecimalStr)) {
				val value = df.format(BigDecimal.ZERO);
				return bigDecimalWithSixDecimals(value);
			}

			val bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));
			val value = df.format(bigDecimal);
			return new BigDecimal(value.replace(",", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigDecimal bigDecimalWithSixDecimalsStrict(String bigDecimalStr) {
		try {

			if (bigDecimalStr != null) {
				bigDecimalStr = bigDecimalStr.trim();

				val df = new DecimalFormat("#,###.000000");
				val bigDecimal = new BigDecimal(bigDecimalStr.replace(",", ""));
				val value = df.format(bigDecimal);
				return new BigDecimal(value.replace(",", ""));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigDecimal getBigDecimal(BigDecimal value) {
		return value != null ? value : null;
	}

	public static Long longValue(String longStr) {
		try {

			if (UValidator.isNullOrEmpty(longStr)) {
				return null;
			}
			return Long.valueOf(longStr);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String longString(Long longValue) {
		try {

			if (UValidator.isNullOrEmpty(longValue)) {
				return null;
			}

			return Long.toString(longValue);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Integer integerValue(String integerStr) {
		try {

			if (UValidator.isNullOrEmpty(integerStr))
				return null;

			return Integer.valueOf(integerStr);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String integerString(Integer value) {
		try {

			if (UValidator.isNullOrEmpty(value))
				return null;

			return value.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigInteger bigInteger(Integer value) {
		try {

			if (UValidator.isNullOrEmpty(value))
				return null;

			return BigInteger.valueOf(value.intValue());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static BigInteger bigInteger(String value) {
		try {

			if (UValidator.isNullOrEmpty(value))
				return null;

			return new BigInteger(value);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Short shortValue(String value) {
		try {

			if (UValidator.isNullOrEmpty(value)) {
				return null;
			}
			return Short.parseShort(value);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] stringToByte(String value) {
		if (!UValidator.isNullOrEmpty(value)) {
			return value.getBytes(StandardCharsets.UTF_8);
		}
		return null;
	}

	public static String byteToString(byte[] value) {
		return new String(value, StandardCharsets.UTF_8);
	}

	public static String singleReplace(String str) {
		if (str != null) {
			return str.replace("&", "&amp;")
					  .replace("\"", "&quot;")
					  .replace("<", "&lt;")
					  .replace(">", "&gt;")
					  .replace("'", "&apos;");
		}
		return null;
	}

	public static String revertSingleReplace(String str) {
		if (str != null) {
			return str.replace("&amp;", "&")
					  .replace("&quot;", "\"")
					  .replace("&lt;", "<")
					  .replace("&gt;", ">")
					  .replace("&apos;", "'");
		}
		return null;
	}
	
	public static String unescape(String str) {
		if (str != null) {
			return str.replace("&amp;amp;", "&amp;");
		}
		return null;
	}
	
	public static String fixQuotes(String field) {
	    int numQuotes = 0;
	    for (int i = 0; i < field.length(); i++) {
	        if (field.charAt(i) == '\"') {
	            numQuotes++;
	        }
	    }
	    
	    if (numQuotes % 2 != 0) {
	        return "\"" + field;
	    }
	    return field;
	}

}