package org.megapractical.invoicing.api.util;

import java.nio.charset.StandardCharsets;

import org.springframework.security.crypto.codec.Base64;

public final class UBase64 {

	private UBase64() {
	}

	public static String base64Encode(String token) {
		try {

			if (token != null) {
				@SuppressWarnings("deprecation")
				byte[] encodedBytes = Base64.encode(token.getBytes());
				return new String(encodedBytes, StandardCharsets.UTF_8);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public static String base64Decode(String token) {
		try {

			if (token != null) {
				byte[] decodedBytes = Base64.decode(token.getBytes());
				return new String(decodedBytes, StandardCharsets.UTF_8);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public static String base64Encode(byte[] value) {
		String digitalSign = null;
		try {

			byte[] bytes = Base64.encode(value);

			digitalSign = new String(bytes);
			digitalSign = digitalSign.replace("\r\n", "");
			digitalSign = digitalSign.replace("\n", "");
			digitalSign = digitalSign.replace("\r", "");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return digitalSign;
	}
	
}