/*
 */
package org.megapractical.invoicing.api.util;

/**
 * Clase útil para facilitar la conversión de numeros a letras
 *
 * @author Alain
 * @since Nov 28, 2012
 */
public class UNumberToLetter {

    static String[][] monedas = {
        {"AED", "Dirham de los Emiratos Árabes Unidos"},
        {"AFN", "Afgani afgano"},
        {"ALL", "Lek albanés"},
        {"AMD", "Dram armenio"},
        {"ANG", "Florín antillano neerlandés"},
        {"AOA", "Kwanza angoleño"},
        {"ARS", "Peso argentino"},
        {"AUD", "Dólar australiano"},
        {"AWG", "Florín arubeño"},
        {"AZN", "Manat azerbaiyano"},
        {"BAM", "Marco convertible de Bosnia-Herzegovina"},
        {"BBD", "Dólar de Barbados"},
        {"BDT", "Taka de Bangladés"},
        {"BGN", "Lev búlgaro"},
        {"BHD", "Dinar bahreiní"},
        {"BIF", "Franco burundés"},
        {"BMD", "Dólar de Bermuda"},
        {"BND", "Dólar de Brunéi"},
        {"BOB", "Boliviano"},
        {"BOV", "Mvdol boliviano (código de fondos)"},
        {"BRL", "Real brasileño"},
        {"BSD", "Dólar bahameño"},
        {"BTN", "Ngultrum de Bután"},
        {"BWP", "Pula de Botsuana"},
        {"BYR", "Rublo bielorruso"},
        {"BZD", "Dólar de Belice"},
        {"CAD", "Dólar canadiense"},
        {"CDF", "Franco congoleño, o congolés"},
        {"CHF", "Franco suizo"},
        {"CLF", "Unidades de fomento chilenas (código de fondos)"},
        {"CLP", "Peso chileno"},
        {"CNY", "Yuan chino"},
        {"COP", "Peso colombiano"},
        {"COU", "Unidad de valor real colombiana (añadida al COP)"},
        {"CRC", "Colón costarricense"},
        {"CSD", "Dinar serbio (Reemplazado por RSD el 25 de octubre de 2006)"},
        {"CUP", "Peso cubano"},
        {"CUC", "Peso cubano convertible"},
        {"CVE", "Escudo caboverdiano"},
        {"CZK", "Koruna checa"},
        {"DJF", "Franco yibutiano"},
        {"DKK", "Corona danesa"},
        {"DOP", "Peso dominicano"},
        {"DZD", "Dinar argelino"},
        {"EGP", "Libra egipcia"},
        {"ERN", "Nakfa eritreo"},
        {"ETB", "Birr etíope"},
        {"EUR", "Euro"},
        {"FJD", "Dólar fiyiano"},
        {"FKP", "Libra malvinense"},
        {"GBP", "Libra esterlina (libra de Gran Bretaña)"},
        {"GEL", "Lari georgiano"},
        {"GHS", "Cedi ghanés"},
        {"GIP", "Libra de Gibraltar"},
        {"GMD", "Dalasi gambiano"},
        {"GNF", "Franco guineano"},
        {"GTQ", "Quetzal guatemalteco"},
        {"GYD", "Dólar guyanés"},
        {"HKD", "Dólar de Hong Kong"},
        {"HNL", "Lempira hondureño"},
        {"HRK", "Kuna croata"},
        {"HTG", "Gourde haitiano"},
        {"HUF", "Forint húngaro"},
        {"IDR", "Rupiah indonesia"},
        {"ILS", "Nuevo shéquel israelí"},
        {"INR", "Rupia india"},
        {"IQD", "Dinar iraquí"},
        {"IRR", "Rial iraní"},
        {"ISK", "Króna islandesa"},
        {"JMD", "Dólar jamaicano"},
        {"JOD", "Dinar jordano"},
        {"JPY", "Yen japonés"},
        {"KES", "Chelín keniata"},
        {"KGS", "Som kirguís (de Kirguistán)"},
        {"KHR", "Riel camboyano"},
        {"KMF", "Franco comoriano (de Comoras)"},
        {"KPW", "Won norcoreano"},
        {"KRW", "Won surcoreano"},
        {"KWD", "Dinar kuwaití"},
        {"KYD", "Dólar caimano (de Islas Caimán)"},
        {"KZT", "Tenge kazajo"},
        {"LAK", "Kip lao"},
        {"LBP", "Libra libanesa"},
        {"LKR", "Rupia de Sri Lanka"},
        {"LRD", "Dólar liberiano"},
        {"LSL", "Loti lesotense"},
        {"LTL", "Litas lituano"},
        {"LVL", "Lat letón"},
        {"LYD", "Dinar libio"},
        {"MAD", "Dirham marroquí"},
        {"MDL", "Leu moldavo"},
        {"MGA", "Ariary malgache"},
        {"MKD", "Denar macedonio"},
        {"MMK", "Kyat birmano"},
        {"MNT", "Tughrik mongol"},
        {"MOP", "Pataca de Macao"},
        {"MRO", "Ouguiya mauritana"},
        {"MUR", "Rupia mauricia"},
        {"MVR", "Rufiyaa maldiva"},
        {"MWK", "Kwacha malauí"},
        {"MXN", "Pesos"},
        {"MXV", "Unidad de Inversión (UDI) mexicana (código de fondos)"},
        {"MYR", "Ringgit malayo"},
        {"MZN", "Metical mozambiqueño"},
        {"NAD", "Dólar namibio"},
        {"NGN", "Naira nigeriana"},
        {"NIO", "Córdoba nicaragüense"},
        {"NOK", "Corona noruega"},
        {"NPR", "Rupia nepalesa"},
        {"NZD", "Dólar neozelandés"},
        {"OMR", "Rial omaní"},
        {"PAB", "Balboa panameña"},
        {"PEN", "Nuevo sol peruano"},
        {"PGK", "Kina de Papúa Nueva Guinea"},
        {"PHP", "Peso filipino"},
        {"PKR", "Rupia pakistaní"},
        {"PLN", "zloty polaco"},
        {"PYG", "Guaraní paraguayo"},
        {"QAR", "Rial qatarí"},
        {"RON", "Leu rumano"},
        {"RUB", "Rublo ruso"},
        {"RWF", "Franco ruandés"},
        {"SAR", "Riyal saudí"},
        {"SBD", "Dólar de las Islas Salomón"},
        {"SCR", "Rupia de Seychelles"},
        {"SDG", "Dinar sudanés"},
        {"SEK", "Corona sueca"},
        {"SGD", "Dólar de Singapur"},
        {"SHP", "Libra de Santa Helena"},
        {"SLL", "Leone de Sierra Leona"},
        {"SOS", "Chelín somalí"},
        {"SRD", "Dólar surinamés"},
        {"STD", "Dobra de Santo Tomé y Príncipe"},
        {"SYP", "Libra siria"},
        {"SZL", "Lilangeni suazi"},
        {"THB", "Baht tailandés"},
        {"TJS", "Somoni tayik (de Tayikistán)"},
        {"TMT", "Manat turcomano"},
        {"TND", "Dinar tunecino"},
        {"TOP", "Pa'anga tongano"},
        {"TRY", "Lira turca"},
        {"TTD", "Dólar de Trinidad y Tobago"},
        {"TWD", "Dólar taiwanés"},
        {"TZS", "Chelín tanzano"},
        {"UAH", "Grivna ucraniana"},
        {"UGX", "Chelín ugandés"},
        {"USD", "Dólar estadounidense"},
        {"USN", "Dólar estadounidense (Siguiente día) (código de fondos)"},
        {"USS", "Dólar estadounidense (Mismo día) (código de fondos)"},
        {"UYU", "Peso uruguayo"},
        {"UZS", "Som uzbeko"},
        {"VEF", "Bolívar fuerte venezolano"},
        {"VND", "Dong vietnamita"},
        {"VUV", "Vatu vanuatense"},
        {"WST", "Tala samoana"},
        {"XAF", "Franco CFA de África Central"},
        {"XAG", "Onza de plata"},
        {"XAU", "Onza de oro"},
        {"XBA", "European Composite Unit (EURCO) (unidad del mercado de bonos)"},
        {"XBB", "European Monetary Unit (E.M.U.-6) (unidad del mercado de bonos)"},
        {"XBC", "European Unit of Account 9 (E.U.A.-9) (unidad del mercado de bonos)"},
        {"XBD", "European Unit of Account 17 (E.U.A.-17) (unidad del mercado de bonos)"},
        {"XCD", "Dólar del Caribe Oriental"},
        {"XDR", "Derechos Especiales de Giro (FMI)"},
        {"XFO", "Franco de oro (Special settlement currency)"},
        {"XFU", "Franco UIC (Special settlement currency)"},
        {"XOF", "Franco CFA de África Occidental"},
        {"XPD", "Onza de paladio"},
        {"XPF", "Franco CFP"},
        {"XPT", "Onza de platino"},
        {"XTS", "Reservado para pruebas"},
        {"XXX", ""}, //{"XXX", "Sin divisa"},
        {"YER", "Rial yemení (de Yemen)"},
        {"ZAR", "Rand sudafricano"},
        {"ZMW", "Kwacha zambiano"},
        {"ZWL", "Dólar zimbabuense"}};

    private static String getCurrencyDescriptionByClave(String clave) {
        for (String[] moneda : monedas) {
            if (clave.equals(moneda[0])) {
                return moneda[1];
            }
        }
        return "";
    }

    /**
     * Convierte un número real a su correspondiente representación en letras en
     * Español.
     *
     * @param number
     */
    //hice el cambio del tipo de number para poder pasar un numero correcto.
    public static String numberToLetter(String number, int decimalSpaces, String currency) {
        if (currency == null) {
            currency = "";
        }
        String currencyCode = currency;
        String currencyDescription = getCurrencyDescriptionByClave(currency.trim());

        String cadena = number + "";
        String cadenaNew = "";

        if (cadena.contains("E")) {
            String aux = cadena.replace('E', '_');
            String[] partsAux = aux.split("_");
            String num = partsAux[0].replace('.', '_').replaceAll("_", "");
            int spaces = Integer.parseInt(partsAux[1]);
            if (spaces > 0) {
                cadenaNew = num.substring(0, spaces + 1) + "_" + num.substring(spaces + 1, num.length());
            } else {
                cadenaNew = "0_";
                for (int i = 0; i < (spaces * (-1)) - 1; i++) {
                    cadenaNew += "0";
                }
                cadenaNew += num;
            }
        } else {
            cadenaNew = cadena.replace('.', '_');
        }

        String[] parts = cadenaNew.split("_");
        
        Integer fullPart = null;
        Long fullPartLong = null;
        
        //##### Corrigiendo java.lang.NumberFormatException
        //##### cuando el valor sea mayor que <Integer.MAX_VALUE = 2147483647>
        try {
        	fullPart = Integer.parseInt(parts[0]);
		} catch (Exception e) {
			fullPartLong = Long.parseLong(parts[0]);
		}

        String decimalPart = parts.length > 1 ? parts[1] : "0";

        if (currencyCode.equals("MXN")) {
            currencyCode = "M.N.";
        }

        String result = "";
        if(fullPart != null) {
        	if (fullPart == 0) {
                result = ("cero " + currencyDescription + " " + decimalPartToString(decimalPart, decimalSpaces) + " ").trim() + " " + currencyCode + "";
            } else {
                if (fullPart == 1 && currencyCode.equals("MXN")) {
                    currencyDescription = "Peso";
                }
                result = (fullPartToString(fullPart) + " " + currencyDescription + " " + decimalPartToString(decimalPart, decimalSpaces) + " ").trim() + " " + currencyCode + "";
            }
        } else if(fullPartLong != null) {
        	if (fullPartLong == 0) {
                result = ("cero " + currencyDescription + " " + decimalPartToString(decimalPart, decimalSpaces) + " ").trim() + " " + currencyCode + "";
            } else {
                if (fullPartLong == 1 && currencyCode.equals("MXN")) {
                    currencyDescription = "Peso";
                }
                result = (fullPartToString(fullPartLong) + " " + currencyDescription + " " + decimalPartToString(decimalPart, decimalSpaces) + " ").trim() + " " + currencyCode + "";
            }
        }
        return result.substring(0, 1).toUpperCase() + result.substring(1, result.length());
    }

    private static String digitToLetter(int digit) {
        switch (digit) {
            case 0:
                return " cero";
            case 1:
                return " un";
            case 2:
                return " dos";
            case 3:
                return " tres";
            case 4:
                return " cuatro";
            case 5:
                return " cinco";
            case 6:
                return " seis";
            case 7:
                return " siete";
            case 8:
                return " ocho";
            case 9:
                return " nueve";
        }
        return "";
    }

    private static String thousandToString(int value) {
        String cadena = "";
        int millares = value / 1000;
        int centenas = value % 1000;

        if (millares == 0) {
            return hundredsToString(centenas);
        }

        if (millares == 1) {
            cadena += "mil";
        } else if (millares <= 9) {
            cadena += digitToLetter(millares) + " mil ";
        } else if (millares >= 10 && millares <= 99) {
            cadena += tensToString(millares) + " mil";
        } else if (millares > 99) {
            cadena += hundredsToString(millares) + " mil";
        }

        return cadena + hundredsToString(centenas);
    }

    private static String hundredsToString(int value) {
        String cadena = "";
        if (value == 100) {
            cadena = "cien";
        } else {
            int centenas = value / 100;
            int decenas = value % 100;
            if (centenas == 1) {
                cadena += " ciento" + tensToString(decenas);
            } else if (centenas == 5) {
                cadena += " quinientos" + tensToString(decenas);
            } else if (centenas == 7) {
                cadena += " setecientos" + tensToString(decenas);
            } else if (centenas == 9) {
                cadena += " novecientos" + tensToString(decenas);
            } else if (centenas > 1) {
                cadena += digitToLetter(centenas) + "cientos" + tensToString(decenas);
            } else {
                return tensToString(decenas);
            }
        }

        return cadena;
    }

    private static String tensToString(int value) {
        int decenas = value / 10;
        int unidades = value % 10;
        boolean finalize = unidades == 0;
        String cadena = "";
        if (decenas > 1) {
            switch (decenas) {
                case 2:
                    cadena += "veint" + (finalize ? "e" : "i");
                    break;
                case 3:
                    cadena += "treinta" + (finalize ? "" : " y ");
                    break;
                case 4:
                    cadena += "cuarenta" + (finalize ? "" : " y ");
                    break;
                case 5:
                    cadena += "cincuenta" + (finalize ? "" : " y ");
                    break;
                case 6:
                    cadena += "sesenta" + (finalize ? "" : " y ");
                    break;
                case 7:
                    cadena += "setenta" + (finalize ? "" : " y ");
                    break;
                case 8:
                    cadena += "ochenta" + (finalize ? "" : " y ");
                    break;
                case 9:
                    cadena += "noventa" + (finalize ? "" : " y ");
                    break;
            }

            cadena += !finalize ? digitToLetter(unidades).trim() : "";
        } else if (decenas == 1) {
            switch (value) {
                case 10:
                    cadena += "diez";
                    break;
                case 11:
                    cadena += "once";
                    break;
                case 12:
                    cadena += "doce";
                    break;
                case 13:
                    cadena += "trece";
                    break;
                case 14:
                    cadena += "catorce";
                    break;
                case 15:
                    cadena += "quince";
                    break;
                case 16:
                    cadena += "dieciséis";
                    break;
                case 17:
                    cadena += "diecisiete";
                    break;
                case 18:
                    cadena += "dieciocho";
                    break;
                case 19:
                    cadena += "diecinueve";
                    break;
            }
        } else if (value == 0) {
            return "";
        } else {
            return digitToLetter(unidades);
        }

        return " " + cadena + " ";
    }

    private static String decimalPartToString(String decimalPart, int decimalSpaces) {
        String cadena = "";

        if (decimalPart.equals("0")) {
            for (int i = 0; i < decimalSpaces; i++) {
                cadena += "0";
            }
        } else if (decimalPart.length() == decimalSpaces) {
            cadena = decimalPart;
        } else {
            int i = 0;
            while (decimalPart.charAt(i) == '0') {
                i++;
            }
            cadena = decimalPart.substring(i, decimalPart.length()) + "";

            int left = decimalSpaces - (i + cadena.length());

            for (int j = 0; j < left; j++) {
                cadena += "0";
            }
        }

        cadena += "/1";

        for (int i = 0; i < decimalSpaces; i++) {
            cadena += "0";
        }

        return cadena;
    }

    private static String fullPartToString(int fullPart) {
        String cadena = "";
        int millones = 0;
        int millares = 0;

        if (fullPart >= 1000000) {
            millones = Integer.parseInt((fullPart / 1000000 + "").replace('.', '_').split("_")[0]);
            millares = Integer.parseInt((fullPart % 1000000 + "").replace('.', '_').split("_")[0]);
        } else {
            millares = (int) fullPart;
        }

        if (millones > 999) {
            cadena += thousandToString(millones) + " millones ";
        } else if (millones > 1) {
            cadena += hundredsToString(millones) + " millones ";
        } else if (millones == 1) {
            cadena = "un millón";
        }

        return cadena.trim() + thousandToString(millares);
    }
    
    private static String fullPartToString(long fullPart) {
        String cadena = "";
        int millones = 0;
        int millares = 0;

        if (fullPart >= 1000000) {
            millones = Integer.parseInt((fullPart / 1000000 + "").replace('.', '_').split("_")[0]);
            millares = Integer.parseInt((fullPart % 1000000 + "").replace('.', '_').split("_")[0]);
        } else {
            millares = (int) fullPart;
        }

        if (millones > 999) {
            cadena += thousandToString(millones) + " millones ";
        } else if (millones > 1) {
            cadena += hundredsToString(millones) + " millones ";
        } else if (millones == 1) {
            cadena = "un millón";
        }

        return cadena.trim() + thousandToString(millares);
    }

}
