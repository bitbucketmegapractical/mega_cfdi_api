package org.megapractical.invoicing.api.util;

import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.SystemUtils;

public class UNetwork {

    /**
     * 
     * Search all network interface that haver the machine.
     *
     * If you have a virtual network this functionality can be return true and
     * thats not means than exist Internet connection.
     *
     * @return true = exist connection, false = not exist connection
     */
    public static boolean isAvailable() {
        try {
        	
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface interf = interfaces.nextElement();
                if (interf.isUp() && !interf.isLoopback()) {
                    //return true;
                	return ping();
                }
            }
            
        } catch (SocketException e) {
            Logger.getLogger(UNetwork.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public static boolean ping() {
    	
        String ip = "www.google.com";
        try {
           if(SystemUtils.IS_OS_LINUX){
        	   boolean reachable = (java.lang.Runtime.getRuntime().exec("ping -c 3 " + ip).waitFor() == 0);
               if (reachable) {
                   return true;
               }
           } else {
        	   boolean reachable = (java.lang.Runtime.getRuntime().exec("ping " + ip).waitFor() == 0);
               if (reachable) {
                   return true;
               }
           }
        } catch (IOException | InterruptedException e) {
            Logger.getLogger(UNetwork.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
}
