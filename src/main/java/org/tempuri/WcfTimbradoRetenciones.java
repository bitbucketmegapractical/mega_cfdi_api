/**
 * WcfTimbradoRetenciones.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface WcfTimbradoRetenciones extends javax.xml.rpc.Service {
    public java.lang.String getwcfTimbradoRetencionesEndpointAddress();

    public org.tempuri.IwcfTimbradoRetenciones getwcfTimbradoRetencionesEndpoint() throws javax.xml.rpc.ServiceException;

    public org.tempuri.IwcfTimbradoRetenciones getwcfTimbradoRetencionesEndpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getwcfTimbradoRetencionesEndpoint1Address();

    public org.tempuri.IwcfTimbradoRetenciones getwcfTimbradoRetencionesEndpoint1() throws javax.xml.rpc.ServiceException;

    public org.tempuri.IwcfTimbradoRetenciones getwcfTimbradoRetencionesEndpoint1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
