# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* API de componentes para la gestión de la facturación electrónica. Inicia con el Comprobante Fiscal Digital v3.3
* 1.0.0
* https://www.megapractical.com

### How do I get set up? ###

* HTML 5
* Bootstrap 3
* JQuery
* Spring Framework
* Spring Security
* Spring JPA
* Thymeleaf
* PostgreSQL
* Java 8
* Apache CXF

### Contribution guidelines ###

* Nothing to show

### Who do I talk to? ###

* Maikel Guerra Ferrer
* maikel.guerra@megapractical.com
* Megapractical S.A de C.V